---
aliases:
- /server/jira/platform/4227151.html
- /server/jira/platform/4227151.md
category: devguide
confluence_id: 4227151
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227151
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227151
date: '2017-12-08'
legacy_title: How do I get a handle to the 'components' of a JIRA project?
platform: server
product: jira
subcategory: other
title: How do I get a handle to the 'components' of a JIRA project?
---
# How do I get a handle to the 'components' of a JIRA project?

``` javascript
    /**
     * get all the components for this project
     */
    public Collection getAllComponents() {
Long currentProjectId = (Long) ActionContext.getSession().get(SessionKeys.SELECTED_PROJECT);
GenericValue currentProject = (GenericValue) projectManager.getProject(currentProjectId);
        Collection components = new ArrayList(projectManager.getComponents(currentProject));
        return components;
    }
```
