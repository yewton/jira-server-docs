---
aliases:
- /server/jira/platform/project-tab-panel-plugin-module-4227187.html
- /server/jira/platform/project-tab-panel-plugin-module-4227187.md
category: reference
confluence_id: 4227187
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227187
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227187
date: '2017-12-08'
legacy_title: Project Tab Panel Plugin Module
platform: server
product: jira
subcategory: modules
title: Project tab panel
---
# Project tab panel

The Project Tab Panel plugin module allows you to add new tabs to the 'Browse Projects' page.

All these tab panels ('Road Map', 'Change Log', 'Popular Issues' and 'Open Issues') are implemented as plugins. New project tab panels can be implemented to display project-specific info.

Here is an example panel module descriptor:

``` xml
<!--
The class defined should implement
com.atlassian.jira.plugin.projectpanel.ProjectTabPanel
and it may be useful to use the functionality provided by
com.atlassian.jira.plugin.projectpanel.impl.GenericProjectTabPanel.
-->
<project-tabpanel key="roadmap-panel" name="Road Map Panel"
    class="com.atlassian.jira.plugin.projectpanel.impl.VersionsProjectTabPanel">
    <description key="projectpanels.roadmap.description">
    A roadmap of the upcoming versions in this project.
    </description>
    <label key="common.concepts.roadmap" />
    <!-- this is a number defining the order of all panels.
    The system panels are 10, 20, 30 and 40. -->
    <order>20</order>
    <!-- this template produces the HTML for the panel -->
    <resource type="velocity" name="view"
    location="templates/plugins/jira/projectpanels/roadmap-panel.vm" />
    <!-- this properties files contains i18n keys for the panel -->
    <resource type="i18n" name="i18n"
    location="com.atlassian.jira.plugins.projectpanels.roadmap" />
    <!-- this parameter prevents the heading from appearing at the top of the tab panel area -->
    <param name="noTitle">true</param>
</project-tabpanel>
```

![](/server/jira/platform/images/image2013-11-11-20:27:23.png)
