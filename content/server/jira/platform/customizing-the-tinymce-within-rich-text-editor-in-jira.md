---
aliases:
- /server/jira/platform/tutorial-customizing-the-tinymce-within-rich-text-editor-in-jira-44054122.html
- /server/jira/platform/tutorial-customizing-the-tinymce-within-rich-text-editor-in-jira-44054122.md
category: devguide
confluence_id: 44054122
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44054122
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44054122
date: '2017-12-08'
guides: tutorials
legacy_title: Tutorial - Customizing the TinyMCE within Rich Text Editor in JIRA
platform: server
product: jira
subcategory: learning
title: Customizing the TinyMCE within Rich Text Editor in JIRA
---
# Customizing the TinyMCE within Rich Text Editor in JIRA

|                       |           |
|-----------------------|-----------|
| Level of experience   | ADVANCED  |
| Time estimate         | 0:15      |
| Atlassian application | JIRA 7.3+ |

## Feature overview

If you've read these two tutorials: [Tutorial - Writing plugin for Rich Text Editor in JIRA](/server/jira/platform/extending-the-rich-text-editor-in-jira) and [Tutorial - Customizing Rich Text Editor in JIRA](/server/jira/platform/customizing-rich-text-editor-in-jira), you'll know how to add new functionality to the Rich Text Editor. After reading this tutorial, you'll know how to customize settings in underlying <a href="https://www.tinymce.com/" class="external-link">TinyMCE</a> editor. Let's assume you want to allow typing markdown-like headings. The end result:

<img src="/server/jira/platform/images/mnstr378-tut4.jpg" height="250" />

<img src="/server/jira/platform/images/mnstr-378-markdown-demo.gif" height="250" />

Let's dive into the details.

## Creating a file with the customization logic

**Listing of markdown-headings.js**

``` javascript
require(["jira/editor/customizer"], function (EditorCustomizer) {
    EditorCustomizer.customizeSettings(function (tinymceSettings, tinymce, SchemaBuilder) {
        if (tinymceSettings.plugins.indexOf('textpattern') === -1) {
            tinymceSettings.plugins.push('textpattern');
        }

        tinymceSettings.textpattern_patterns = tinymceSettings.textpattern_patterns || [];

        Array.prototype.push.apply(tinymceSettings.textpattern_patterns, [
            {start: '#', format: 'h1'},
            {start: '##', format: 'h2'},
            {start: '###', format: 'h3'}
        ]);
    });
});
```

What's going on in the code?

-   Firstly, there is `require` of `jira/editor/customizer` which allows us to modify TinyMCE settings by using `customizeSettings` method.
-   Given callback `function (tinymceSettings, tinymce, SchemaBuilder) { ... }` will be called before TinyMCE editor instance is initialised.  
    There are three parameters that we can use:
    -    `tinymceSettings` object which is used for initialising TinyMCE editor instance: `tinymce.init(tinymceSettings);`  
        (for more details take a look at the <a href="https://www.tinymce.com/docs/" class="external-link">TinyMCE documentation</a>) 

    -   `tinymce` TinyMCE main object which we can use for example to add new TinyMCE plugin  
        (some examples are provided in <a href="https://www.tinymce.com/docs/api/tinymce/tinymce.addonmanager/" class="external-link">this part of TinyMCE documentation</a>)
    -   `SchemaBuilder` Rich Text Editor controls TinyMCE schema-related settings such as `schema`, `valid_elements`, `valid_children` or `custom_elements` because only the subset of HTML is supported by Wiki Markup format which is used as a storage format in JIRA. That parameter has been covered in depth on the following page: [Tutorial - Customizing Rich Text Editor in JIRA](/server/jira/platform/customizing-rich-text-editor-in-jira).

-   Inside the callback body, we're adding *textpattern* TinyMCE plugin, if it doesn't already exists, and we're configuring that plugin, according to <a href="https://www.tinymce.com/docs/plugins/textpattern/" class="external-link">it specification</a>.

## Loading the file into editor context

**Listing of atlassian-plugin.xml**

``` javascript
<web-resource key="customizations" name="JIRA Editor Reference Plugin Customizations">
    <context>jira.rich.editor</context>

    <resource type="download" name="js/markdown-headings.js" location="js/markdown-headings.js"/>
</web-resource>
```

And that's it! <img src="https://extranet.atlassian.com/s/en_GB/7101/39f493eb262ae06a946282731783a56d9b647f63/_/images/icons/emoticons/smile.png" alt="(smile)" class="confluence-external-resource emoticon-smile" />

## Links

-   Reference plugin source code: <a href="https://bitbucket.org/atlassian/jira-editor-ref-plugin/overview" class="uri external-link">bitbucket.org/atlassian/jira-editor-ref-plugin/overview</a>
-   TinyMCE documentation: <a href="https://www.tinymce.com/docs/" class="uri external-link">www.tinymce.com/docs/</a>
