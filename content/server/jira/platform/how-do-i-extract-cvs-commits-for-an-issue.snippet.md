---
aliases:
- /server/jira/platform/how-do-i-extract-cvs-commits-for-an-issue-4227174.html
- /server/jira/platform/how-do-i-extract-cvs-commits-for-an-issue-4227174.md
category: devguide
confluence_id: 4227174
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227174
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227174
date: '2017-12-08'
legacy_title: How do I extract CVS commits for an issue
platform: server
product: jira
subcategory: other
title: How do I extract CVS commits for an issue
---
# How do I extract CVS commits for an issue

This code snippet was extracted from the DefaultActionManager class in JIRA's source code:

``` javascript

/**
* Retrieves all of the commits for this issue from ALL of the repositories associated with the issue's project
*
* @param issue
* @param remoteUser
*/

    public List getCommits(GenericValue issue, User remoteUser) throws GenericEntityException, RepositoryException
    {
        List commits = new ArrayList();

        if (issue == null)
            throw new IllegalArgumentException("Issue cannot be null.");
        if (!"Issue".equals(issue.getEntityName()))
            throw new IllegalArgumentException("Entity must be of type Issue");

        if (!hasPermission(issue, remoteUser))
        {
            // If the user does not have the reuqited permission, do not return any information.
            return Collections.EMPTY_LIST;
        }

        Collection repositories = getRepositories(issue);
        for (Iterator iterator = repositories.iterator(); iterator.hasNext();)
        {
            Repository repository = (Repository) iterator.next();
            try
            {
                List coms = repository.getCommitsForIssue(issue.getString("key"));
                for (int i = 0; i < coms.size(); i++)
                {
                    commits.add(new Commit((VCSCommit) coms.get(i), remoteUser, repository.getName(), repository.getRepositoryBrowser()));
                }
            }
            catch (OutOfMemoryError e)
            {
                // Add an issue action that represents OutOFMemeory error
                commits.add(new OutOfMemoryCommitIssueAction(remoteUser, new Timestamp(System.currentTimeMillis()), repository.getName()));
            }
        }

        // Sort by date
        Collections.sort(commits);
        return commits;
    }
```
