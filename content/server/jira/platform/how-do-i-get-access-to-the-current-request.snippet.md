---
aliases:
- /server/jira/platform/4227147.html
- /server/jira/platform/4227147.md
category: devguide
confluence_id: 4227147
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227147
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227147
date: '2017-12-08'
legacy_title: How do I get access to the current request?
platform: server
product: jira
subcategory: other
title: How do I get access to the current request?
---
# How do I get access to the current request?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get access to the current request?

Within your plugin code, you can perform the following static call to get access to the `HttpServletRequest` object. This then in turn gives you access to the session etc.

``` javascript
HttpServletRequest request = ExecutingHttpRequest.get();
```

{{% note %}}

The request object may be null in some contexts, so always do a null-test before using the returned `request`.

{{% /note %}}

Be aware that your dependency in **pom.xml** may need amending, like so:

``` javascript
<dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>2.4</version>
      <type>jar</type>
      <scope>provided</scope>
</dependency>
```
