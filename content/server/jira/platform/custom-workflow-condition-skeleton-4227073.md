---
title: Custom Workflow Condition Skeleton 4227073
aliases:
    - /server/jira/platform/custom-workflow-condition-skeleton-4227073.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227073
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227073
confluence_id: 4227073
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Custom Workflow Condition Skeleton

This is a 'general' workflow condition skeleton which can be used to call the example condition subroutines which link to this page.

Some conditions will use different parameters than what's used in this example.

**blockingLinksClosed subroutine**

``` javascript
package com.newisys.jira.workflow.condition;

import org.apache.log4j.Category;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.GenericEntityException;

import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.module.propertyset.PropertySet;

import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkType;

import java.util.Map;
import java.util.List;

import java.util.Iterator;
import java.util.Collection;

/**
 * Passes if all subtask issues have current status equal to passed status id
 *
 *  required argument 'closedstate'
 */
public class BlockingLinksClosedCondition implements Condition
{
    private static final Category log = Category.getInstance(BlockingLinksClosedCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        try
        {
            WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");
            GenericValue issue = null;
            try
            {
                issue = ManagerFactory.getIssueManager().getIssueByWorkflow(new Long(entry.getId()));
            }
            catch (GenericEntityException e)
            {
                log.error("Exception: " + e, e);
                return false;
            }

            // Get the id of the workflow
            String closedState = (String) args.get("closedstate");
            if (closedState == null) throw new IllegalArgumentException("Must specify a 'closedstate' arg specifying ids of valid states");

RETURN RESULT OF CALL TO WORKFLOW CONDITION SUBROUTINE

        }
        catch(Exception e)
        {
            log.error("Exception: " + e, e);
            return false;
        }

        return true;
    }
}
```
