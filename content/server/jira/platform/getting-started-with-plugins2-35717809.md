---
title: Getting Started with Plugins2 35717809
aliases:
    - /server/jira/platform/getting-started-with-plugins2-35717809.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35717809
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35717809
confluence_id: 35717809
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Getting started with Plugins2

### Developing add-ons for JIRA Server

##### Get hands-on with the Atlassian Plugin SDK

Never built a Plugins2 add-on before? Try our Atlassian Plugin SDK tutorial. This will help you set up the Atlassian Plugin SDK and lead you through the creation of a JIRA plugin module. You can also bring your plugin into an Eclipse IDE project, if you choose.

1.  Get the SDK:

    <a href="#install-the-latest-version" class="sdk-installer">››› Install the latest version ‹‹‹</a>
    <a href="https://developer.atlassian.com/display/DOCS/Downloads" class="sdk-download-link">Downloads for other systems...</a>

2.  Start the tutorial**: [Atlassian Plugin SDK tutorial](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) ***(SDK documentation)*

##### Try a few JIRA tutorials

A great way to get acquainted with the JIRA platform is to try a few of our hands-on tutorials. The JIRA platform section has quick reference pages for the major components of JIRA (e.g. projects, issues, etc), On each page, you'll find links to step-by-step tutorials. The beginner tutorials, like [this one](/server/jira/platform/adding-menu-items-to-jira), are a good place to start.

Learn more**: [JIRA Platform](/server/jira/platform/integrating-with-jira-server)**

##### Learn about the JIRA architecture

If you're ready to learn more about the nuts and bolts of JIRA development, check out the [JIRA architecture](/server/jira/platform/jira-architecture-32344051.html) section. You'll find articles on topics like JIRA's dependencies, webhooks, web panels, plugin modules, and more.

Learn more**: [JIRA architecture](/server/jira/platform/jira-architecture-32344051.html)**
