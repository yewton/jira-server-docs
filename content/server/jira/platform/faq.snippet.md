---
aliases:
- /server/jira/platform/faq-4227157.html
- /server/jira/platform/faq-4227157.md
category: devguide
confluence_id: 4227157
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227157
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227157
date: '2017-12-08'
legacy_title: FAQ
platform: server
product: jira
subcategory: other
title: FAQ
---
# FAQ

This is a constantly updated FAQ listing questions and answers asked by people developing JIRA plugins and working with the JIRA code base in general.

{{% note %}}

If you have a question, please ask it on our <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">Developer Forums</a> and certain threads will be merged back into the FAQ.

{{% /note %}}

## Questions

**Content by label**

There is no content with the specified labels
