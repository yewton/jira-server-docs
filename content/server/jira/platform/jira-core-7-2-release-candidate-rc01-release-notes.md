---
aliases:
- /server/jira/platform/40502542.html
- /server/jira/platform/40502542.md
category: devguide
confluence_id: 40502542
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40502542
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40502542
date: '2017-12-08'
legacy_title: JIRA Core 7.2 Release Candidate (RC01) release notes
platform: server
product: jira
subcategory: updates
title: JIRA Core 7.2 Release Candidate (RC01) release notes
---
# JIRA Core 7.2 Release Candidate (RC01) release notes

**02 July 2016**

Atlassian is proud to present **JIRA Core 7.2 RC01**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Core 7.2** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.2, read the developer change management guide: [Preparing for JIRA 7.2](/server/jira/platform/preparing-for-jira-7-2). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

[<img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" />](#downloads)

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}
{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.2 EAP`

{{% /note %}}

## Project navigation

We've improved the project experience by adding the project sidebar to more features that you use in the context of your project. For users, when you access a report you're now kept in the context of your sidebar, making navigating back to your project quick and easy.

For project administrators and JIRA administrators, we've changed the name of Project administration to **Project settings**, and we've also changed the layout and positioning:

-   Accessing **Project settings** displays your Project settings menu while still displaying the project sidebar.
-   Your project **Details** can now be accessed and edited in the context of your Project settings menu, and they now include Project category.
-   **Re-index project** has been moved into the Project settings menu.

These changes will help JIRA administrators and Project administrators work more efficiently and effectively.

<img src="/server/jira/platform/images/projectsettings-sidebar7.2.png" class="image-center" width="800" height="415" />

## Components and Versions

We've streamlined how you manage components and versions in JIRA Core, and once you've created your first component or version, JIRA administrators and project administrators can manage components and versions directly from the project sidebar.

<img src="/server/jira/platform/images/components7.2.png" class="image-center" width="910" height="250" />

## Onboarding changes and demo projects

We've revamped the onboarding for JIRA administrators, when you first log in and select your language and avatar, you're now presented with the options of creating a project, creating a project with demo data, or importing data from an external system. These options are also available to JIRA administrators after they've logged in for the first time:

-   Create a new project by selecting **Projects &gt; Create project**, and selecting a project template.
-   Create a project with demo data by selecting **Projects &gt; Create project**, and select the **Create sample project** link. Then follow the prompts.
-   Import data from an external system by selecting ![](/server/jira/platform/images/bluecog-icon.png)**&gt; System**, and select **External System Import** from the left hand menu.

Your users will also see a more streamlined onboarding, after selecting their language and avatar they can jump right in by looking for existing projects and issues. JIRA administrators can also create projects with sample data for users to look access, the contents of the sample data has been created to give people an overview of JIRA Core and it's features. Give it a try!

## CSV export

We've added the ability to export a list of issues and their associated details in a CSV file. When you search for issues and view the results in a list view, selecting **Export** now allows you to select a CSV export of either all fields or the currently displayed fields. This export format gives you more options with manipulating the data, and you can even create your own CSV file to import back into JIRA.

## Sidebar in reports

We've added the project sidebar to reports, so that you can always stay in the context of your project when viewing JIRA reports. Previously, you would lose your sidebar and have to navigate back to your project, now you're kept in the context of your project.

## Rich text editing

User's are used to a what you see is what you get experience (WYSIWYG), and expect to see their formatting applied directly to their content. JIRA uses wiki markdown, and previously you had to preview your descriptions, comments and multi-line text area fields to get an understanding of how your wiki markdown syntax will be saved and presented. Now, JIRA administrators can enable the rich text editor, and allow their users to select whether they prefer to stay in wiki markdown, or prefer to experience a WYSIWYG editor. 

To enable the rich text editor, select ![](/server/jira/platform/images/bluecog-icon.png)**&gt; System**, and select **Rich text editor** from the left hand menu. Then enable rich text editing.

## Language support

We now include Portuguese (PT), Korean and Russian as supported languages in JIRA.

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review *all* applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Core 7.1</a>
-   <a href="https://confluence.atlassian.com/migration/jira-7/server_jira_upgrade" class="external-link">Upgrading to JIRA Core 7.0</a>
-   <a href="https://confluence.atlassian.com/display/JIRA064/Important+Version-Specific+Upgrade+Notes" class="external-link">Upgrading from JIRA 6.3 or earlier</a>

## Downloads

-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2.0-RC-20160629075822-x32.exe" class="external-link">JIRA Core 7.2.0-RC01 (Windows 32 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2.0-RC-20160629075822-x64.exe" class="external-link">JIRA Core 7.2.0-RC01 (Windows 64 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2.0-RC-20160629075822-x32.bin" class="external-link">JIRA Core 7.2.0-RC01 (Linux 32 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2.0-RC-20160629075822-x64.bin" class="external-link">JIRA Core 7.2.0-RC01 (Linux 64 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2.0-RC-20160629075822.zip" class="external-link">JIRA Core 7.2.0-RC01 (ZIP Archive)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.2-EAP20160530074712.tar.gz" class="external-link">JIRA Core 7.2.0-m01 (TAR.GZ Archive)</a>
