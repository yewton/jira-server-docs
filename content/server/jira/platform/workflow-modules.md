---
aliases:
- /server/jira/platform/workflow-plugin-modules-4227191.html
- /server/jira/platform/workflow-plugin-modules-4227191.md
category: reference
confluence_id: 4227191
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227191
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227191
date: '2017-12-08'
legacy_title: Workflow Plugin Modules
platform: server
product: jira
subcategory: modules
title: Workflow modules
---
# Workflow modules

## Purpose of this Module Type

The workflow plugin modules allow you to add new capabilities to JIRA's workflow engine.

JIRA uses <a href="http://www.opensymphony.com/osworkflow" class="external-link">OSWorkflow</a> as its workflow engine. The web-based workflow editor has a number of plugin modules which allow you to build workflows more easily.

The modules are:

-   [Condition Configuration](#condition-configuration) - check whether or not a given workflow transition can be executed by a given user
-   [Function Configuration](#function-configuration) - perform actions after a workflow transition has been executed
-   [Validator Configuration](#validator-configuration) - check that the data given to a workflow transition is valid

## Condition Configuration

The root element for the workflow condition plugin module is `workflow-condition`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the workflow condition, which must implement <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/plugin/workflow/WorkflowPluginConditionFactory.html" class="external-link">com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory</a>. This class is used to provide context for the rendered Velocity templates that supply the condition's views.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. I.e. the identifier of the workflow condition.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Defaut: N/A</strong></p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the workflow condition.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut:</strong> The plugin key.</p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this workflow condition module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>condition-class</p></td>
<td><p>Determines whether the user is allowed to perform this workflow transition. Must implement the OSWorkflow class <a href="http://www.opensymphony.com/osworkflow/api/com/opensymphony/workflow/Condition.html" class="external-link">com.opensymphony.workflow.Condition</a>, but JIRA extensions are strongly recommended to extend <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/workflow/condition/AbstractJiraCondition.html" class="external-link">com.atlassian.jira.workflow.condition.AbstractJiraCondition</a>; this implementation provides efficient access to the Issue object.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Used to render the views for the condition. The template contexts are populated by the <code>workflow-condition</code>'s <code>class</code>.</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

#### Example

The following condition prohibits all users other than the issue assignee from performing transitions on any given issue.
The following code should be placed in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/).

``` xml
<workflow-condition key="onlyassignee-condition" name="Only Assignee Condition"
    i18n-name-key="admin.workflow.condition.onlyassignee.display.name"
    class="com.atlassian.jira.plugin.workflow.WorkflowAllowOnlyAssigneeConditionFactoryImpl">

    <description key="admin.workflow.condition.onlyassignee">Condition to allow only the assignee to execute a transition.</description>

    <condition-class>com.atlassian.jira.workflow.condition.AllowOnlyAssignee</condition-class>

    <resource type="velocity" name="view"
        location="templates/jira/workflow/com/atlassian/jira/plugin/onlyassignee-condition-view.vm"/>
</workflow-condition>
```

## Function Configuration

Workflow functions always execute **after** the workflow transition is executed; they might be more properly named \_post-\_workflow functions.

The root element for the workflow function plugin module is `workflow-function`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the workflow function. Functions that don't require input should use <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/plugin/workflow/WorkflowNoInputPluginFactory.html" class="external-link">com.atlassian.jira.plugin.workflow.WorkflowNoInputPluginFactory</a>; those that do must implement <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/plugin/workflow/WorkflowPluginFunctionFactory.html" class="external-link">com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory</a>.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. I.e. the identifier of the workflow function.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Defaut: N/A</strong></p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the workflow function.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut:</strong> The plugin key.</p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this workflow function module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>function-class</p></td>
<td><p>Class that implements the function's logic. Must extend <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/workflow/function/issue/AbstractJiraFunctionProvider.html" class="external-link">com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider</a>.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Used to render the views for the function.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>orderable</p></td>
<td><p>(true/false) Specifies if this function can be re-ordered within the list of functions associated with a transition. The postion within the list determines when the function actually executes.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td><p>unique</p></td>
<td><p>(true/false) Specifies if this function is unique; i.e., if it is possible to add multiple instances of this post function on a single transition.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>deletable</p></td>
<td><p>(true/false) Specifies if this function can be removed from a transition.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td><p>addable</p></td>
<td><p>Valid values are the <code>ACTION_TYPE</code> constants of the <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/workflow/JiraWorkflow.html" class="external-link">com.atlassian.jira.workflow.JiraWorkflow</a>; multiple values can be specified through comma-delineation.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="#notes">JIRA:weight</a></p></td>
<td><p>An integer value indicating where this function should be called if it is default (see below).</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="#notes">JIRA:default</a></p></td>
<td><p>(true/false) Specifies if this function should be applied to all workflows.</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

#### Example

``` javascript
<workflow-function key="update-issue-field-function" name="Update Issue Field"
    class="com.atlassian.jira.plugin.workflow.UpdateIssueFieldFunctionPluginFactory">
    <description>Updates a simple issue field to a given value.</description>

    <function-class>
        com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction
    </function-class>

    <orderable>true</orderable>
    <unique>false</unique>
    <deletable>true</deletable>

    <resource type="velocity" name="view"
        location="templates/jira/.../update-issue-field-function-view.vm"/>
    <resource type="velocity" name="input-parameters"
        location="templates/jira/.../update-issue-field-function-input-params.vm"/>
</workflow-function>
```

## Validator Configuration

The root element for the workflow validator plugin module is `workflow-validator`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the workflow validator, which must implement <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/plugin/workflow/WorkflowPluginValidatorFactory.html" class="external-link">com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory</a>. This class is used to provide context for the rendered Velocity templates that supply the validator's views.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. I.e. the identifier of the workflow validator.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Defaut: N/A</strong></p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut: -</strong></p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the workflow validator.</p>
<p><strong>Required: -</strong></p>
<p><strong>Defaut:</strong> The plugin key.</p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this workflow validator module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>validator-class</p></td>
<td><p>Class that performs the validation logic. Must implement <code>com.opensymphony.workflow.Validator</code>.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Used to render the views for the validator.</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

#### Example

``` xml
<workflow-validator key="permission-validator" name="Permission Validator"
    class="com.atlassian.jira.plugin.workflow.WorkflowPermissionValidatorPluginFactory">
    <description>Validates that the user has a permission.</description>

    <validator-class>
        com.atlassian.jira.workflow.validator.PermissionValidator
    </validator-class>

    <resource type="velocity" name="view"
        location="templates/jira/.../permission-validator-view.vm"/>
    <resource type="velocity" name="input-parameters"
        location="templates/jira/.../permission-validator-input-params.vm"/>
</workflow-validator>
```

## Notes

-   The `weight` and `default` parameters for workflow functions **should not be used** by plugin authors and **should be considered reserved for JIRA's use**.

{{% note %}}

For more details, see [Tutorial - How to create custom workflow elements for JIRA 3.x](/server/jira/platform/creating-custom-workflow-elements).

{{% /note %}}
