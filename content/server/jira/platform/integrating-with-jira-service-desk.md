---
aliases:
- /server/jira/platform/jira-service-desk-33727325.html
- /server/jira/platform/jira-service-desk-33727325.md
category: devguide
confluence_id: 33727325
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33727325
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33727325
date: '2017-12-08'
legacy_title: JIRA Service Desk
platform: server
product: jira
subcategory: intro
title: Integrating with Jira Service Desk
---
# Integrating with Jira Service Desk

Welcome to Jira Service Desk Server development! This overview will cover everything you need to know to integrate with Jira Service Desk Server. This includes the P2 framework for building plugins, as well as Jira Service Desk features and services that you can use when integrating with Jira Service Desk.

{{% tip %}}

Hello world

If you already know the theory and want to jump straight into development, read our [Getting started guide](/server/jira/platform/getting-started) to build your first Jira Server plugin.

{{% /tip %}}

## What is Jira Service Desk?

Jira Service Desk is primarily used as a service solution, from asset management to DevOps. A diverse range of IT teams use Jira Service Desk, including support desk teams, operations teams, and more. With over 15,000 of these teams using Jira Service Desk, there's plenty of potential to extend it. Jump in and get started!

{{< youtube 5u62pSDVB_I >}}

If you haven't used Jira Service Desk before, check out the <a href="https://www.atlassian.com/software/jira/service-desk" class="external-link">product overview</a> for more information.

## Building plugins for Jira Service Desk Server

If you want to build a plugin for Jira Service Desk Server, you should use the Atlassian development platform, which includes the Plugins2 framework. Add-ons are used to extend the functionality of Jira Service Desk, which could be an an integration with another existing service, new features for Jira Service Desk, or even a new product that runs within Jira Service Desk.

#### The basic development flow for building a Jira Service Desk  using the Atlassian SDK is:

1.  Create your plugin project as you would any other Jira plugin, for instance, by using the [atlas-create-jira-plugin](https://developer.atlassian.com/display/DOCS/atlas-create-jira-plugin) SDK command. This command creates the plugin project files and skeleton code.

2.  When ready to build and test your plugin, run the [atlas-package](https://developer.atlassian.com/display/DOCS/atlas-package) command from the project root directory. This command creates a packaged JAR file that you can install into Jira (in the `target` sub-directory)

3.  Configure AMPS to run Jira with Jira Service Desk installed. If you haven't done this before, see [Configure AMPS to run Jira Core with additional applications installed](https://developer.atlassian.com/display/DOCS/Configure+AMPS+to+run+JIRA+Core+with+additional+applications+installed).
4.  Start up Jira. For early development and testing, you can use the development instance that the SDK gives you when you run the following command *(set the version to 7.0.0 or later)*: `atlas-run-standalone --product jira --version 7.0.0.` 
5.  Log in to Jira as an administrator, using the default username and password combination of **admin/admin**.  
6.  Install the plugin you built in step \#2 with the `atlas-package` command. There are two ways that you can do this:
    1.  Go to the Administration Console and open the "Manage Add-ons" page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.
    2.  Using the SDK command line tools: using you terminal, step into your project home directory and simply run [atlas-install-plugin](https://developer.atlassian.com/display/DOCS/atlas-install-plugin) (after having packaged your plugin JAR using the `atlas-package` SDK command). This command will simply upload your plugin to UPM (and can considerably shorten your development flow).

7.  You should now be able to see your plugin in the Jira "Manage Add-ons" page as an installed plugin. Every time you change your plugin code or resources, reload your plugin repeating step \#6 above.

If you haven't built a plugin before, check out the [Getting started guide](/server/jira/platform/getting-started). This guide will help you learn how to set up a development environment and build a Jira Server plugin.

### Developing with Plugins2

If you are building a plugin with Plugins2, you'll also need to understand a number of key development processes:

-   **Implementing security:** Implementing security is a crucial part of integrating with Jira. To learn more about authentication and authorization for Jira Server, see the [Security overview](/server/jira/platform/security-overview).
-   **Listing your plugin on the Atlassian Marketplace and implementing licensing:** Listing your plugin on the Atlassian Marketplaces and licensing it are necessary, if you want to make your plugin available to the public. See the [Marketplace documentation](https://developer.atlassian.com/display/HOME/Marketplace) for more instructions.
-   **Designing your plugin:** Since Plugins2 plugins can insert content directly into the Atlassian host application, it is critical that plugins are visually compatible with the Atlassian application's design. Our designers and developers have created a number of resources to help you with this:
    -   <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> -- Our design guidelines define core interactions with the Atlassian applications.
    -   <a href="https://docs.atlassian.com/aui/" class="external-link">Atlassian User Interface (AUI)</a> -- AUI is a library of reusable front-end UI components.

## Building blocks for integrating with Jira Service Desk Server

The three building blocks of integrating with Jira Service Desk are the APIs, webhooks, and modules.

### Jira Service Desk Server APIs

The Jira Service Desk Server APIs lets your integration communicate with Jira Service Desk Server. For example, using the REST API, you can retrieve a queue's requests to display in your plugin or create requests from phone calls. For most other integrations, you should use the REST API. The Java API should only be used if you are building a Plugins2 plugin.

-   <a href="https://docs.atlassian.com/jira-servicedesk/REST/server/" class="external-link">Jira Service Desk Server REST API</a> *(latest production version)*
-   <a href="https://docs.atlassian.com/jira-servicedesk/server/" class="external-link">Jira Service Desk Server Java API</a> *(latest production version)*

*Note, Jira Service Desk is built on the Jira platform, so you can also use the <a href="https://docs.atlassian.com/jira/REST/server/" class="external-link">Jira Server Platform REST API</a> and <a href="https://docs.atlassian.com/jira/server/" class="external-link">Jira Server Platform Java API</a> to interact with Jira Service Desk Server.*

### Webhooks and automation rules

Add-ons and applications can react to conditions/events in Jira Service Desk via automation rules. You can implement an "automation action" that performs actions in a remote system as part of an automation rule. An automation rule can also be configured to fire a webhooks that notifies your plugin or application. For more information, see [Jira Service Desk webhooks](/server/jira/platform/jira-service-desk-webhooks).

### Jira Service Desk modules

A module is simply a UI element, like a tab or a menu. Jira Service Desk UI modules allow plugins to interact with the Jira Service Desk UI. For example, your plugin can use a Jira Service Desk UI module to add a panel to the top of customer portals. For more information, see [About Jira modules](/server/jira/platform/about-jira-modules).

## Jira Service Desk Server and the Jira platform

Jira Service Desk is an application built on the Jira platform. The Jira platform provides a set of base functionality that is shared across all Jira applications, like issues, workflows, search, email, and more. A Jira application is an extension of the Jira platform that provides specific functionality. For example, Jira Service Desk adds customer request portals, support queues, SLAs, a knowledge base, and automation.

This means that when you develop for Jira Service Desk, you are actually integrating with the Jira Service Desk application as well as the Jira platform. The Jira Service Desk application and Jira platform each have their own REST APIs, webhook events, and web fragments.

Read the [Jira Server platform documentation](/server/jira/platform/integrating-with-jira-server) for more information.

## Looking for inspiration? <span id="inspiration"></span>

If you are looking for ideas on building the next Jira Service Desk Server integration, the following use cases and examples may help.

Here are some common Jira Service Desk use cases:

-   **Support helpdesk**: An easy way to provide support to anyone in the organization with IT requests such as hardware (laptop) requests, or software requests.
-   **ITSM/ITIL**: More advanced IT teams want to use a service solution to support ITSM and ITIL processes, including incident, problem, and change management.
-   **Asset management**: IT teams want to discover, control, monitor, and track key IT assets such as hardware and servers.
-   **DevOps**: Developer, Operations, and IT teams can use Jira Service Desk to collaborate together and solve problems faster.
-   **Business teams**: Finance and HR teams may want to use Jira Service Desk to collect requests from anyone in the organization.

Here are a few examples of what you can build on top of Jira Service Desk:

-   **Customer portal customization:** Jira Service Desk provides an intuitive customer portal that makes it easy for non-technical end users to interact with service teams like IT and support. By extending this, you can build a completely tailored interface for the customer portal that matches your company's branding.
-   **Collect requests outside of Jira Service Desk:** Build functionality to create requests on behalf of customers in any number of ways. For example, integrate it into the support section of your website, or have a get help menu on your mobile plugin, or hook up alerts from a system monitoring tool to create incidents in Jira Service Desk.
-   **SLA integration**: Jira Service Desk introduces the notion of service level agreements (SLAs) by letting teams accurately measure and set goals based on time metrics, e.g. time to assign, time to respond, time to resolution. With the Jira Service Desk REST API, you can now get detailed SLA information and create your own reports.
-   **Telephony integration:** Create requests based on incoming voice calls by integrating your telephony system with Jira Service Desk, via the REST API.
-   **Supplement request information:** Add information about assets, the customer, or other relevant information to requests to make it easier for agents to solve problems and close requests.

## More information

-   <a href="https://answers.atlassian.com/questions/topics/31850522/jira-servicedesk-development" class="external-link">jira-servicedesk-development tag</a> on the Atlassian Answers forum - Join the discussion on Jira Service Desk development.
