---
aliases:
- /server/jira/platform/creating-custom-renderers-for-remote-issue-links-6848571.html
- /server/jira/platform/creating-custom-renderers-for-remote-issue-links-6848571.md
category: devguide
confluence_id: 6848571
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848571
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848571
date: '2017-12-08'
legacy_title: Creating Custom Renderers for Remote Issue Links
platform: server
product: jira
subcategory: other
title: Creating custom renderers for remote issue links
---
# Creating custom renderers for remote issue links

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 5.0 and later.</p></td>
</tr>
</tbody>
</table>

When a Remote Issue Link is rendered, if there is no custom renderer for the application type of the link, a default renderer is used. You can use the [Issue Link Renderer plugin module](/server/jira/platform/issue-link-renderer) to add new *custom* renderers for issue links to Jira.

