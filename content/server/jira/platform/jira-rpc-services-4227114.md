---
title: JIRA Rpc Services 4227114
aliases:
    - /server/jira/platform/jira-rpc-services-4227114.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227114
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227114
confluence_id: 4227114
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA RPC Services

{{% note %}}

 

 

 

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

 

 

***
***


*JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](/server/jira/platform/jira-soap-to-rest-migration-guide)).*

{{% /note %}}

## Overview of the RPC Services

JIRA ships with an RPC plugin that enables limited remote access to JIRA through <a href="http://www.w3.org/TR/soap/" class="external-link">SOAP</a> and <a href="http://www.xmlrpc.com/" class="external-link">XML-RPC</a> interfaces.

You can find the latest news about the RPC plugin in the <a href="https://studio.plugins.atlassian.com/wiki/display/JRPC/JIRA+RPC+Plugin" class="external-link">Atlassian Plugins project</a>. The <a href="https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin" class="external-link">full source</a> of the plugin is available and you are free to modify and extend the source. See the [RPC Endpoint Plugin Module](/server/jira/platform/rpc-endpoint-plugin-module-4227189.html) for more information.

### SOAP Service

The JIRA SOAP service is backed by <a href="http://ws.apache.org/axis/" class="external-link">Apache Axis</a>, an open-sourced Java web services framework.

{{% note %}}

The JIRA SOAP API is not compatible with Axis2 or Xfire. See <a href="http://jira.atlassian.com/browse/JRA-12152" class="external-link">JRA-12152</a> for more information.

Also note that there is an unrelated patch for Axis1, which you can download from <a href="http://jira.atlassian.com/browse/JRA-20351" class="external-link">JRA-20351</a>.

{{% /note %}}

If you're familiar with SOAP, ensure that [RPC is enabled](/server/jira/platform/enabling-the-rpc-plugin-4227091.html) and point your stub generator to the ` /rpc/soap/jirasoapservice-v2?wsdl` path of your JIRA installation (for example, <a href="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2?wsdl" class="uri external-link">http://jira.atlassian.com/rpc/soap/jirasoapservice-v2?wsdl</a>) and you're away. If you are less familiar with SOAP, you should first take a look at the [SOAP tutorial](/server/jira/platform/creating-a-jira-soap-client-4227095.html).

When developing SOAP clients, keep in mind that the SOAP service respects the permissions and <a href="https://confluence.atlassian.com/display/JIRA/Defining+a+Screen" class="external-link">screen configurations</a> that are set up in JIRA. For example, if you have configured JIRA so that the screen for a given issue type does not include a 'Summary' field, then you will not be permitted to set a value for the 'Summary' field through the SOAP request.

For the latest information on the remote methods available, see the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/index.html?com/atlassian/jira/rpc/soap/JiraSoapService.html" class="external-link">latest Javadoc for JiraSoapService</a>.

***Hint:*** Take a look at the <a href="https://plugins.atlassian.com/plugin/details/6398" class="external-link">Command Line Interface</a> for an existing remote scripting tool.

### XML-RPC Service

XML-RPC can also be used as an alternative where SOAP is unavailable. You can find some background information on XML-RPC by reading the [overview](/server/jira/platform/jira-xml-rpc-overview-4227112.html). Start building your own client by following the instructions in the [tutorial](/server/jira/platform/creating-an-xml-rpc-client-4227133.html). The XML-RPC interface is backed by the SOAP interface, and will usually have a subset of methods from the SOAP interface.

For the latest information on the remote methods available, see the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/index.html?com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">latest Javadoc for XmlRpcService</a>. Method calls should be prefixed with ` jira1.` and be made to the URL ` /rpc/xmlrpc` of your installation. The Javadoc will often refer to "hashtables with fields from RemoteObject".

The hashtable will contain keys that map to the fields available <a href="http://jakarta.apache.org/commons/beanutils/apidocs/org/apache/commons/beanutils/BeanUtils.html#describe(java.lang.Object)" class="external-link">through reflection</a> of the particular RemoteObject. For example, the object ` RemoteVersion` has the methods getReleaseDate(), getSequence(), isArchived() and isReleased(). This will be converted into a Hashtable with keys releaseDate, sequence, archived and released.

More information:

-   [JIRA XML-RPC Overview](/server/jira/platform/jira-xml-rpc-overview-4227112.html)
-   [Creating an XML-RPC Client](/server/jira/platform/creating-an-xml-rpc-client-4227133.html)
-   <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/index.html?com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">Latest Javadoc for XmlRpcService</a>

### JSON-RPC Service

We added JSON-RPC capabilities to JIRA to help you move from the SOAP API to a mechanism that uses more JSON and JavaScript, on the way towards the REST APIs.

The code uses the Java server code that makes up the existing SOAP interface in JIRA. There is a one-to-one mapping between the methods and parameters in the JIRA SOAP API and the equivalent JSON-RPC methods and parameters. The main difference lies in the JSON-RPC wire format.

See the [JIRA JSON-RPC Overview](/server/jira/platform/jira-json-rpc-overview-6291518.html).
