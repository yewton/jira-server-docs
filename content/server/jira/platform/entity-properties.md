---
aliases:
- /server/jira/platform/jira-entity-properties-overview-27558108.html
- /server/jira/platform/jira-entity-properties-overview-27558108.md
category: devguide
confluence_id: 27558108
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27558108
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27558108
date: '2018-06-13'
legacy_title: JIRA Entity Properties Overview
platform: server
product: jira
subcategory: blocks
title: "Entity properties"
---
# Entity properties

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 6.2 and later.</p></td>
</tr>
</tbody>
</table>

This page provides an overview of Jira entity properties.

For the Atlassian Cloud documentation on Jira Entity Properties, go to:
[Storing data without a database](/cloud/jira/platform/storing-data-without-a-database/).

## What are entity properties?

Entity properties allow plugins to add key/value stores to Jira entities, such as issues or projects. These values
can be indexed by Jira and queried via REST API or JQL. 

## How do I store data against an entity using REST?

The [curl](http://curl.haxx.se/) examples below show you how you can store data against an issue and retrieve
the data from an issue using REST. 

*   For information on how to manipulate properties of other Jira entities (for example, projects), see the 
[Jira REST API documentation](/server/jira/platform/rest-apis/). 
*   P2 plugins can store/retrieve data using 
[IssuePropertyService](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/bc/issue/properties/IssuePropertyService.html)
that you can inject IssuePropertyService using [Atlassian Spring Scanner](https://bitbucket.org/atlassian/atlassian-spring-scanner/src/1.2.x/).

{{% note %}}
To modify, add, or remove the properties, the user who executes the request must have permission to edit the entity.
For example, to add new property to issue ENPR-4, you need permission to edit the issue. To retrieve a property,
the user must have read permissions for the entity.
{{% /note %}}

#### Example 1: Storing data

The following example will store the JSON object `{"content":"Test if works on Jira Cloud", "completed" : 1}` 
against issue ENPR-4 with the key `tasks`.

``` bash
curl -X PUT \
  -H "Content-type: application/json" \
  -d '{"content":"Test if works on Jira Cloud", "completed" : 1}' \
  https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks
```

Note the following:

*   The `key` has a maximum length of 255 characters. 
*   The `value` must be a valid JSON Object and have a maximum size of 32 KB.

#### Example 2: Retrieving data

The following example shows how to get all of the properties stored against an issue:

``` bash
curl -X GET https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/
```

The response from server will contain keys and URLs of all properties of the issue ENPR-4. 

``` javascript
{
   "keys" : [
      {
         "key" : "tasks",
         "self" : "https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks"
      }
   ]
}
```

#### Example 3: Removing a property

The following example shows how to remove a property from an issue:

``` bash
curl -X DELETE https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks
```

## How do I make the properties of an entity searchable?

Jira Server plugins can provide a module descriptor that will make the issue properties searchable
using JQL. For example, to index the data from the first example, Jira Server plugins can provide the
following module descriptor:

``` xml
<index-document-configuration 
  entity-key="IssueProperty" 
  key="jira-issue-tasklist-indexing">
  <key property-key="tasks">
    <extract path="content" type="text" />
  </key>
</index-document-configuration>
```

The descriptors shown above will make Jira index the object `content` of the issue property `tasks` as a text.

The available index data types are:

*   `number` — indexed as a number and allows for range ordering and searching on this field.
*   `text` — tokenized before indexing and allows for searching for particular words.
*   `string` — indexed as-is and allows for searching for the exact phrase only.
*   `date` — indexed as a date and allows for date range searching and ordering. The expected date format is
\[YYYY\]-\[MM\]-\[DD\]. The expected date time format is \[YYYY\]-\[MM\]-\[DD\]T\[hh\]:\[mm\]:\[ss\] with optional
offset from UTC: +/-\[hh\]:\[mm\] or `Z` for no offset. For reference, see [ISO_8601 standard](http://www.w3.org/TR/NOTE-datetime).

The indexed data is available for a JQL search. The result of the JQL query —
 `issue.property\[tasks\].completed = 1 AND issue.property\[tasks\].content ~ "works"` — is shown in the screenshot below. 

<img src="/server/jira/platform/images/image2014-3-22-23:0:16.png" class="image-center" width="500" height="238" />

For detailed explanations on how to use the module descriptors, see the [Index document configuration](/server/jira/platform/index-document-configuration).
