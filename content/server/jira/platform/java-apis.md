---
aliases:
- /server/jira/platform/about-the-jira-server-java-apis-52430879.html
- /server/jira/platform/about-the-jira-server-java-apis-52430879.md
category: reference
confluence_id: 52430879
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430879
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430879
date: '2018-06-08'
legacy_title: About the JIRA Server Java APIs
platform: server
product: jira
subcategory: api
title: "Java APIs"
---
# Java APIs

The Jira platform provides Java APIs that you can use to interact with Jira programmatically. These APIs are
common to all Jira applications. In addition, Jira Software and Jira Service Desk provide APIs for application-specific
functionality. For example, the Jira Software API has methods for sprints and so on. 

The Jira Java APIs are typically used when building Plugins2 apps (for Jira Server). If you haven't used the
Jira Java APIs before, make sure you read the [Java API Policy for Jira](#java-api-policy-for-jira) below.

## Java API reference

Jira Platform Java APIs:

*   [Latest production version](https://docs.atlassian.com/software/jira/docs/api/latest/).
*   [All versions (including EAPs)](https://docs.atlassian.com/software/jira/docs/api/).

Jira Software Java API:

*   [All versions](https://docs.atlassian.com/jira-software/).

{{% note %}}
Currently there is no Javadoc for Jira Agile 7 and later versions.
You can check [JSWSERVER-15321](https://jira.atlassian.com/browse/JSWSERVER-15321) for progress.
{{% /note %}}

Jira Service Desk Java API:

*   [Latest production version](https://docs.atlassian.com/jira-servicedesk/server/).
*   [All versions](https://docs.atlassian.com/jira-servicedesk/).

## Java API policy for Jira

This page is a friendly statement of the Atlassian policy for Jira's Java APIs. The intention of this document
is to let developers know about the APIs that are safe to use, and to indicate the internal components that
are subject to change without notice.

This policy is applicable to **Jira 5.0 and later**.

### Terms and definitions

*   **Major release**  
    A major release is a '.0' release.  
    For example: 5.0, 6.0, 7.0, ...
*   **Minor release**  
    Minor releases are the "children" of a major release that introduce new features.  
    For example: 5.1, 5.2, 5.3, ...
*   **Bug fix release**  
    A bug fix release is a child of a minor or major release and is primarily intended to fix problems without altering behavior.  
    For example: 5.0.1, 5.0.2, 5.1.1, ...

### The `jira-api` Maven artifact

In Jira 5.0, we separated the Java components that we consider to be part of our API from the internal components
that are subject to change without notice.

The API components live in a Maven module called `jira-api`.

Apps can depend on this module that means that they will use only the safe Jira components.

``` xml
    <dependency>
        <groupId>com.atlassian.jira</groupId>
        <artifactId>jira-api</artifactId>
        <version>${atlassian.product.version}</version>
        <scope>provided</scope>
    </dependency>
```

For more information, see [Working with Maven](/server/framework/atlassian-sdk/working-with-maven/#atlassian-3rdparty-repository) page.

### @PublicApi, @PublicSpi, and @ExperimentalApi

In general, any class or interface in the `jira-api` module can be considered a part of Jira's Java API, and
we will strive to maintain binary compatibility for these classes and interfaces. There is a distinction between
different types of API, however, and these are indicated by the presence of the following annotations.

#### @PublicApi

API includes interfaces/classes that apps need to use to get things done. Normally Jira will produce instances
of these things, which apps will consume but not implement/extend. Apps can safely use these and we will guarantee
binary compatibility (subject to our deprecation policy). If apps implement/extend these classes, we can not
guarantee binary compatibility (we will be adding methods to these interfaces, etc).

#### @PublicSpi

SPI [Service Provider Interface](http://en.wikipedia.org/wiki/Service_Provider_Interface) includes interfaces/classes
that are designed specifically for apps to implement or extend. In this case we have to be even more careful
than with @PublicApi, since even adding methods to interfaces will break binary compatibility.

#### @ExperimentalApi

If we know that something is likely to change but is still useful enough to move into the API in its preliminary
form, we'll mark it with @ExperimentalApi. There is no guarantee of binary compatibility for experimental API.

#### @Internal

Internal classes and interfaces are not to be used by clients other than Jira itself. There is no guarantee of
binary compatibility for internal types.

### Compatibility policy

The Jira API (all classes and other components in the `jira-api` module) will remain *forward-compatible* throughout
all the minor releases within a major release family.

For example, an app that is built against Jira 5.0 will continue to work with Jira 5.1, 5.2, and so on, without
being recompiled.

#### Major releases

A major release may change the existing API.

Normally, we will attempt to mark the method or class as deprecated in a previous minor release, but this is not guaranteed.

In general, it may not be possible to create a single app artifact that is compatible with different major release families.

For example, it may not be possible to create a single JAR for your app that is compatible with both Jira 5.0 and Jira 4.4.

#### Minor releases

A minor release may add a new API, but it will be binary compatible with the API from previous releases in the
same major release family.

For example, an app built against Jira 5.0 will still work with Jira 5.1, 5.2, and so on, but may not be
compatible with Jira 6.0.

In turn, an app using new functionality introduced in Jira 5.1 will still work in Jira 5.2, 5.3, and so on,
but may not be compatible with Jira 6.0.

### Deprecation policy

Types or methods that are deprecated in the Java API will have the `@deprecated` tag added to their Javadoc.

The Javadoc will explain how to replace usage of the deprecated component and list the version in which it
was set to deprecated.

Example:

``` java
    /**
     * Returns the logged in user.
     *
     * @deprecated Use {@link #getLoggedInUser()} instead. Since v4.3
     */
    public User getRemoteUser()
```

Any Jira API classes or methods that exist but are deprecated in a particular release will continue to exist and
work in the following minor releases, but will be removed in the next major release.

For example, a method that is marked as deprecated in Jira 6.1 will still work in Jira 6.2, but will be deleted
in Jira 7.0

### Getting access to the internal classes

The internal classes (sometimes called "implementation classes") exist in a module called `jira-core`.

App developers may have a dependency on `jira-core` and use any of these implementation classes, but the underlying
code may change even in bug-fix releases.

`jira-core` has a transitive dependency on `jira-api`, so depending on `jira-core` will also bring `jira-api` into your app.
