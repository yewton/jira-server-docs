---
aliases:
- /server/jira/platform/44797470.html
- /server/jira/platform/44797470.md
category: devguide
confluence_id: 44797470
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44797470
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44797470
date: '2017-12-08'
legacy_title: JIRA Core 7.3 release candidate (RC02) release notes
platform: server
product: jira
subcategory: updates
title: JIRA Core 7.3 release candidate (RC02) release notes
---
# JIRA Core 7.3 release candidate (RC02) release notes

**5 December 2016**

Atlassian is proud to present **JIRA Core 7.3 RC02**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Core 7.3** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.3, read the developer change management guide: [Preparing for JIRA 7.3](/server/jira/platform/preparing-for-jira-7-3). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade:* Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.0.x+release+notes" class="external-link">latest official release</a> instead.*

{{% /warning %}}
{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10240" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.3 EAP`

{{% /note %}}

## Rich text editing

We released rich text editing as an opt-in labs feature in JIRA Core 7.2, and now we're pleased to announce that rich text editing has graduated to the default editing experience in JIRA Core. This means when you upgrade or install JIRA Core 7.3, the rich text editor will be turned on by default. This gives your users the option to either use Visual mode (what you see is what you get) or Text mode (wiki markdown). We've retained the ability for administrators to disable rich text editing if they wish, this will disable it for the entire instance, and all your users. 

To disable the rich text editor, select ![](/server/jira/platform/images/bluecog-icon.png)**&gt; System**, and select **Rich text editor** from the sidebar.

### For developers

We've added API's for the rich text editor, and our developers have written some tutorials on how you can create an add-on to work with the rich text editor, from basic use cases to more advanced use cases. [Check out the tutorials](/server/jira/platform/rich-text-editor-44053984.html) to get yourself up to speed.

## Project level administration

We've extended the project administrators permission, so that project administrators can now edit their projects workflow under certain conditions:

-   The workflow must not be shared with any other projects
-   The workflow cannot be the JIRA default system workflow, which cannot be edited at all

The project admins will not be able to edit the workflow to the same extent as a JIRA administrator. The restrictions are:

-   To add a status, the status must already exist in the JIRA instance i.e. the project admin can't create new statuses or edit existing statuses
-   The project admin isn't allowed to delete a status from the workflow
-   The project admin can create, update or delete transitions, but they can't select or update a screen used by the transition, or edit a transition's properties, conditions, validators or post-functions

This feature is enabled by default. When you upgrade to JIRA Core 7.3, all your project administrators will have access to this feature immediately. To edit a workflow, a project administrator should select **Project settings** in their project's sidebar, and select an issue type. This will present the workflow, and **Edit** will be available.

## Starting a JIRA instance

We've added two features that relate to starting JIRA:

1.  We've added an extra step to the JIRA install/upgrade process that means you need to actively choose to start JIRA.
2.  We've added a feature that lets you start JIRA manually with all non-system add-ons disabled.

#### 1. Installing and upgrading start up

Now when you install or upgrade a JIRA instance, you need to actively select to start JIRA. Previously, once JIRA ran the installation/upgrade, it would start JIRA automatically, before asking you if you'd like to launch JIRA in a browser. Now, there's an additional step where JIRA will ask if you want to start JIRA, and if you choose to start it, it will ask if you want to launch JIRA in a browser, This change was required to allow us to work with Amazon Web Services (AWS) to provide a<a href="https://aws.amazon.com/quickstart/architecture/jira/" class="external-link">Quick Start</a> guide using CloudFormation templates to allow you to deploy JIRA Software Data Center in an AWS environment.

#### 2. Disabled non-system add-ons

We've added a feature that allows you to start a JIRA instance with all user installed and custom add-ons disabled, or with a specified list of these add-ons disabled:

1.  `--disable-all-addons` (or `/disablealladdons` for Windows users)

2.  `--disable-addons=<addon keys>` or (`/disableaddons=<addon keys>` for Windows users)

This feature is designed to help with upgrades and installation issues whereby an add-on failing during JIRA startup stops your JIRA instance from starting. You'll be able to disable the add-ons, start JIRA and manually remove the add-on through the add-on manager. The parameters are designed to be specified at system startup when JIRA is started using the start-jira.sh script (or start-jira.bat for Windows), for example:

``` bash
./bin/start-jira.sh --disable-all-addons --disable-addons=com.atlassian.test.plugin
```

If a customer does not use start-jira.sh for starting JIRA but still wishes to use this feature, they can use these features by adding the following JVM parameter to the invocation of the servlet container that's running JIRA:

``` bash
-Datlassian.plugins.startup.options="--disable-all-addons --disable-addons=com.atlassian.test.plugin"
```

To disable **multiple plugins**, use a colon separated list of plugins. Regex/wildcards are not permitted, the full key of the plugin must be provided, for example:

``` bash
-Datlassian.plugins.startup.options=
"--disable-all-addons --disable-addons=com.atlassian.test.plugin:com.atlassian.another.test.plugin"
```

The parameters do not persist i.e. when you shut JIRA down and start it up again, the parameters are removed and JIRA will attempt to start all your add-ons as per a normal start up.

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review *all* applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.2.x+upgrade+notes" class="external-link">Upgrading to JIRA Core 7.2</a>
-   <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Core 7.1</a>
-   <a href="https://confluence.atlassian.com/migration/jira-7/server_jira_upgrade" class="external-link">Upgrading to JIRA Core 7.0</a>
-   <a href="https://confluence.atlassian.com/display/JIRA064/Important+Version-Specific+Upgrade+Notes" class="external-link">Upgrading from JIRA 6.3 or earlier</a>
