---
aliases:
- /server/jira/platform/design-guide-jira-project-centric-view-31508340.html
- /server/jira/platform/design-guide-jira-project-centric-view-31508340.md
category: devguide
confluence_id: 31508340
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=31508340
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=31508340
date: '2017-12-08'
guides: guides
legacy_title: Design guide - JIRA project-centric view
platform: server
product: jira
subcategory: learning
title: Designing for the JIRA project-centric view
---
# Designing for the JIRA project-centric view

This page provides you with design guidelines for adding your add-on to the new project-centric view in JIRA. Read this **before** you read the [Development guide - JIRA project-centric view](/server/jira/platform/developing-for-the-jira-project-centric-view), which provides the technical information required to implement the changes described below.

The guidelines on this page show how the new project-centric navigation will affect users using JIRA and your add-on. Following these guidelines will help you provide users with the best user experience for your add-on.

## Overview

The project-centric view will be introduced in JIRA 6.4, but can be disabled on both the user level or on the global level by an administrator.

For both JIRA Cloud and JIRA Server, individual users will be able to switch it on or off. Administrators will also be able to override this setting (i.e. enable/disable for all users). This means that from JIRA 6.4,** your add-on will need to cater for instances where the project-centric view is enabled, as well as instances where it is disabled.**

The project-centric view provides a new way for users to navigate through the *structure of a project, *which includes issues, collections of issues (like backlogs and boards), reports, and other project-related features (like versions, components, and add-on views provided by add-ons). The goal of this change is to make JIRA's structure understandable to every user, by simplifying the navigation of a JIRA project and making it easy to learn. In turn, this will help open up JIRA to users who may struggle with JIRA's current complexity and terminology.

*Screenshot: Example JIRA project-centric view  
<img src="/server/jira/platform/images/jira-project-annotated.png" width="700" /> *

The project-centric view can be accessed by navigating to **Projects** in the header and selecting a project.  The main feature of this new project centric home is the new sidebar navigation through which all information important for a particular project can be accessed.

## Guidelines

We strongly recommend that you follow the guidelines below when adding your add-on to the new project-centric view. Following these guidelines will help us build an approachable and stable UX platform, which will *help attract many new users to JIRA and its add-ons* in the long-term.

The instructions in this section tell you how to add your add-on to the sidebar, which is the main point of integration for add-ons in the project-centric view. In addition, we will show you how to design for navigation between your add-on's screens.

*Tip: When developing your add-on, consider how your add-on fits into a customer's day-to-day JIRA experience. When do they access your add-on's features? How often? How does they get there? Remember that prominence does not equal usability! *

### Adding your add-on to the sidebar

*Before you begin: the navigation in the project-centric view is reserved exclusively for navigation within the project context. If your add-on requires the user to leave the project, you should not add it to the project-centric view and consider the alternate pluggable interface points instead.*

#### Position

**Guidelines**

-   The add-on link must be in the project navigation section of the sidebar.
-   The link must be added below the default items in the project navigation section.  
    *Why? This provides a more consistent user experience.* 

| Good examples                                                                                                | Bad examples                                                                                                |
|--------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| <img src="/server/jira/platform/images/sidebar-goodposition.png" class="confluence-thumbnail" width="150" /> | <img src="/server/jira/platform/images/sidebar-badposition.png" class="confluence-thumbnail" width="150" /> |

#### Label

**Guidelines**

-   The label should unambiguously describe what the add-on does.
-   The label should be a noun, not a verb.
-   US English spelling and sentence case, as per the <a href="https://design.atlassian.com/latest/product/" class="external-link">Atlassian Design Guidelines</a>.
-   Do not use your company, brand or product name, unless it also explicitly calls out the functionality of your add-on.  
    *Why? This is confusing to new users or any user who is not familiar with your brand, particularly when multiple add-ons are installed,  
    plus it does not fit with the default menu items.*** **

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Good examples</th>
<th>Bad examples</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>&quot;Timesheets&quot;</p></td>
<td><p>&quot;Log time&quot;, &quot;Time&quot;, &quot;Tempo&quot;</p></td>
</tr>
</tbody>
</table>

#### Icon

**Guidelines**

-   Use an icon that aligns well with the functionality of your add-on
-   Do not simply use your company brand or logo
-   All icons must be designed according to the <a href="https://design.atlassian.com/latest/product/foundations/iconography/" class="external-link">Atlassian Design Guidelines</a>:
    -   20px by 20px
    -   Single pixel stroke
    -   \#333 only
    -   Must support retina displays

| Good examples                                                                                        | Bad examples                                                                                                    |
|------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| <img src="/server/jira/platform/images/icon---ok---3.png" class="confluence-thumbnail" width="20" /> | ![](/server/jira/platform/images/icon---not-ok---3.png) ![](/server/jira/platform/images/icon---not-ok---2.png) |

#### Sub-navigation

**Guidelines**

-   Do not add sub-navigation items for your add-on to the sidebar
-   Use the JIRA sub-navigation pattern, as shown in the example.

| Good examples                                                                                               | Bad examples                                                                                             |
|-------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| <img src="/server/jira/platform/images/jira-sidebar-subnav.png" class="confluence-thumbnail" width="220" /> | <img src="/server/jira/platform/images/anti-03---nested.png" class="confluence-thumbnail" width="100" /> |

## Need help?

Try the following resources:

-   Post your question on our forums: <a href="http://answers.atlassian.com/" class="external-link">Atlassian Answers</a>. This is probably the fastest way to get help, as many of our customers and staff are active on Atlassian Answers.
-   Contact the developer relations team: [Contact us](https://developer.atlassian.com/help#contact-us). Note, you will need to create a <a href="http://developer.atlassian.com" class="external-link">developer.atlassian.com</a> account if you don't already have one.

