---
title: JIRA 7.2 Eap Release Notes 39989415
aliases:
    - /server/jira/platform/jira-7.2-eap-release-notes-39989415.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39989415
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39989415
confluence_id: 39989415
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.2 EAP release notes

Find the release notes for the JIRA 7.2 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.2 EAP 01 (m01) release notes](/server/jira/platform/jira-core-7-2-eap-01-m01-release-notes)
-   [JIRA Software 7.2 EAP 01 (m01) release notes](/server/jira/platform/jira-software-7-2-eap-01-m01-release-notes)
-   [JIRA Core 7.2 Release Candidate (RC01) release notes](/server/jira/platform/jira-core-7-2-release-candidate-rc01-release-notes)
-   [JIRA Software 7.2 Release Candidate (RC01) release notes](/server/jira/platform/jira-software-7-2-release-candidate-rc01-release-notes)
-   [JIRA Service Desk 3.2 Release Candidate (RC01) release notes](/server/jira/platform/jira-service-desk-3-2-release-candidate-rc01-release-notes)
