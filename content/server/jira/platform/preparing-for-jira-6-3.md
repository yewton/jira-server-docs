---
aliases:
- /server/jira/platform/preparing-for-jira-6.3-26935339.html
- /server/jira/platform/preparing-for-jira-6.3-26935339.md
category: devguide
confluence_id: 26935339
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26935339
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26935339
date: '2017-12-08'
legacy_title: Preparing for JIRA 6.3
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 6.3
---
# Preparing for JIRA 6.3

## Overview

This page is intended to be read by JIRA plugin developers to inform them of changes in JIRA 6.3 that could affect their plugins. JIRA users should consult the documentation <a href="https://confluence.atlassian.com/display/JIRA/JIRA+Documentation" class="external-link">here</a>.

It is our goal to notify you, our plugin developers, **as far in advance as possible** of everything we know about that can possibly affect your plugins. Where possible, we will attach release targets.

We will update this page as the release progresses. Please **watch this page** **or check back regularly** to keep on top of the changes.

### About the Early Access Program

The JIRA development team release an EAP milestone every two weeks for customers and plugin developers to keep abreast of upcoming changes. These EAP releases are available on the <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">JIRA Early Access Program Downloads</a> page.

## Summary of Changes

The risk level indicates the level of certainty we have that things will break if you are in the "Who is affected?" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th>What is the change?</th>
<th>When will this happen?*</th>
<th>Who is affected?</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Identifier of global permissions has changed</td>
<td>6.3-OD1 (OnDemand)<br />
6.3-m1 (JIRA EAP)</td>
<td><p>Plugins that rely on experimental APIs</p>
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#changes-affecting-experimental-apis">Changes affecting experimental APIs</a> below</p></td>
</tr>
<tr class="even">
<td>Introduction of atlassian-scheduler API</td>
<td><p>6.3-OD1 (OnDemand)<br />
6.3-m1 (JIRA EAP)<br />
6.3 (BTF) </p></td>
<td>Plugins that schedule background jobs using Quartz or PluginScheduler
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="/server/jira/platform/plugin-guide-to-jira-high-availability-and-clustering-27567201.html#scheduledtasksandbackgroundprocesses">Plugin Guide to JIRA High Availability and Clustering</a> for information about this change, even if you have no plans to support JIRA Data Center.</p></td>
</tr>
<tr class="odd">
<td><p>New project permission TRANSITION_ISSUE</p>
<p> </p></td>
<td><p>6.3-OD3 (OnDemand)</p>
<p>6.3-m3 (JIRA EAP)</p>
<p>6.2.2 (BTF)</p></td>
<td>Plugins that grant BROWSE permission
<p><strong>Risk level: medium</strong></p>
<p><strong>Details:</strong> See <a href="#new-project-permission-transition-issue">New project permission TRANSITION_ISSUE</a> below</p></td>
</tr>
<tr class="even">
<td>Earliest support for JIRA Data Center features</td>
<td>6.3-m04 (JIRA EAP)<br />
6.3 (BTF) </td>
<td>Plugins that need to support JIRA Data Center
<p><strong>Risk level: medium</strong></p>
<p><strong>Details:</strong> See <a href="/server/jira/platform/plugin-guide-to-jira-high-availability-and-clustering-27567201.html#scheduledtasksandbackgroundprocesses">Plugin Guide to JIRA High Availability and Clustering</a> for details</p></td>
</tr>
<tr class="odd">
<td>AUI Messages now have a default image</td>
<td><p>6.3-OD7 (OnDemand)</p>
<p>6.3 (BTF)</p></td>
<td><p>Plugins using JIRA's implementation of AUI</p>
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#aui-messages-now-have-a-default-image">AUI Messages now have a default image</a> below</p></td>
</tr>
<tr class="even">
<td>JIRA 6.3 will support Java 7 &amp; Java 8</td>
<td>6.3 final</td>
<td> Any plugin using Java is <em>potentially</em> affected
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> Plugins should compile against Java 7, but run tests against both Java 7 and Java 8</p></td>
</tr>
</tbody>
</table>

## Detailed Explanations of Changes

**In this section:**

-   [Changes affecting experimental APIs](#changes-affecting-experimental-apis)
-   [New project permission TRANSITION_ISSUE](#new-project-permission-transition_issue)
-   [AUI Messages now have a default image](#aui-messages-now-have-a-default-image)
-   [JIRA support for Java 8](#jira-support-for-java-8)

### Changes affecting experimental APIs

As part of the work to allow plugins to define new global permissions, we have changed the identifier of global permissions from `id` to `key`. 

You will be affected if:

-   Your plugin listens for the `GlobalPermissionAdded` or `GlobalPermissionDeleted` events -- instead of `getPermissionId`, you will need to use `getGlobalPermissionType`. You can still get a global permission by `id` using the `getGlobalPermission` method. However, its return type has been changed to `Option<GlobalPermissionType>`.
-   Your plugin uses the `GlobalPermission` class. This has been replaced with `GlobalPermissionType`.

### New project permission TRANSITION\_ISSUE

To allow better control over who is able to transition issues in JIRA new project permission TRANSITION\_ISSUE was introduced. This permission was previously controlled by BROWSE permission.

You will be affected if:

-   Your plugin grants user BROWSE project permission and you are expecting that the user will have ability to transition issues.
-   Your plugin add group with BROWSE permission and you are expecting that the user will have ability to transition issues.
-   Your plugin depends on the behaviour that the users with BROWSE permission can transition issues.
-   Your plugin does **not use** IssueService or WorkflowTransitionUtil to execute transitions on issues. The behaviour of the plugin may be not consistent with JIRA experience.

#### Versions affected by TRANSITION\_ISSUE

-   **6.2.2+** The TRANSITION\_ISSUE permission was partially introduced in JIRA 6.2.2. In this release it is only used in invocations of ProjectPermissionOverride. In all other permission checks the TRANSITION\_ISSUE will effectively check for BROWSE permission.
-   **6.3-OD3+, 6.3-m3+** The TRANSITION\_ISSUE permission is checked when JIRA is supposed to transition issues. Please note that existing and new installations will have sightly different defaults.
    -   For **existing** installations all users/groups/project roles that previously had BROWSE permission will have TRANSITION\_ISSUE permission granted. This behaviour is intended to keep the old behaviour for current users.
    -   For **new** installations only jira-developers will have by default the ability to transition issues (this affects default permission scheme). So effectively all the users that previously had EDIT permission will have the ability to transition issues.

### AUI Messages now have a default image

AUI message containers now have an associated icon added via a CSS pseudo element. Provision to hide the following hard coded icons ( e.g. &lt;span class="aui-icon icon-success"&gt;&lt;/span&gt; or &lt;span class="aui-icon aui-icon-success"&gt;&lt;/span&gt; ) from aui messages has been made, but if you have used your own status icon, this will not be hidden. Icons that are included are:

-   .icon-generic / .aui-icon-generic
-   .icon-error / .aui-icon-error
-   .icon-hint / .aui-icon-hint
-   .icon-info / .aui-icon-info
-   .icon-success / .aui-icon-success
-   .icon-warning / .aui-icon-warning

More information on [AUI Messages](https://developer.atlassian.com/design/1.4/components/messages/)

<a href="https://docs.atlassian.com/aui/5.5.1/docs/messages.html" class="external-link">Implementation documentation</a> for Messages

### JIRA support for Java 8

JIRA 6.2 only supported running on Java 7. JIRA 6.3 will support running on either Java 7 or Java 8.

Plugin devs should build their plugin with Java 7, but it is recommended to test running against both Java 7 and Java 8.  
It is considered unlikely that Java 8 will cause problems, but it is possible under certain advanced or unlucky use cases.

{{% note %}}

Known bug for running plugins compiled with Java 8

{{% /note %}}

Customers who write in-house plugins and run JIRA 6.3 on Java 8 may be tempted to compile the plugin against Java 8.  
It seems that this will not work because of the following bug:  
<a href="https://ecosystem.atlassian.net/browse/PLUG-1098?src=confmacro" class="jira-issue-key"><img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" class="icon" />PLUG-1098</a> - The plugin system fails to load plugins compiled with a java 8 source level Resolved
