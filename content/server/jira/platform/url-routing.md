---
aliases:
- /server/jira/platform/url-routing-plugin-module-30621767.html
- /server/jira/platform/url-routing-plugin-module-30621767.md
category: reference
confluence_id: 30621767
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=30621767
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=30621767
date: '2017-12-08'
legacy_title: URL Routing Plugin Module
platform: server
product: jira
subcategory: modules
title: URL routing
---
# URL routing

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The URL routing plugin module is available in JIRA 6.0 and later.</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

The URL routing plugin module allows add-ons to map any URL to one of their own choosing.   For more information, please see [Guide - Using pretty URLs in a JIRA plugin](/server/jira/platform/using-pretty-urls-in-a-jira-plugin).

## Configuration

The root element for the URL routing plugin module is `routing`. It allows the following attributes and child elements for configuration:

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>path</p></td>
<td><p>The top level URL path that will be matched for further routing. No route rules will be executed if the start of the URL does not string match this path</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the routing module.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
</tbody>
</table>

**Elements**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="#route">route</a></p></td>
<td><p>Defines the routes you want to have in place</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
</tbody>
</table>

#### Route

Routes are how you switch from from a desired URL pattern to an actual internal one.

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>from</p></td>
<td><p>The URL pattern you want to route from. This is your desired <em>pretty</em> url pattern</p>
<p>You can use <a href="http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/UriInfo.html" class="external-link">jax-ws syntax here</a> in the URL pattern</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="even">
<td>to</td>
<td><p>The existing URL to which you want the pretty URL to go.</p>
<p>You can use <a href="http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/UriInfo.html" class="external-link">jax-ws syntax here</a> in the URL pattern</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td>verbs</td>
<td><p>The HTTP verbs that will be selectively routed. if you don't specify anything then all HTTP verbs will be routed.</p>
<p>The allowable verbs are <code>get, put, post, head, delete, options</code> and <code>patch</code>.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default:</strong> All HTTP verbs are routed</p></td>
</tr>
</tbody>
</table>

#### URL Routing example

This is a `routing` entry that causes a url like `http:``//localhost:2990/jira/prettyurls/are/cool to be routed to http://localhost:2990/jira/secure/HorribleName.jspa?variableGoesHere=cool`

``` xml
<routing key="prettyurls-key" path="/prettyurls">
    <route from="/are/{variableGoesHere}" to="/secure/HorribleName.jspa"/>
    <route from="/can/{variableGoesHere}" to="/secure/HorribleName.jspa"/>
</routing>
```

For more examples see [Guide - Using pretty URLs in a JIRA plugin](/server/jira/platform/using-pretty-urls-in-a-jira-plugin)















































































