---
title: "JIRA Core 7.9 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-03-23"
---
# JIRA Core 7.9 EAP release notes

*21 March 2018*

Atlassian is proud to present *JIRA Core 7.9 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *JIRA Core 7.9* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.9, read the developer change management guide: [Preparing for JIRA 7.9](/server/jira/platform/preparing-for-jira-7-9). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.8.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### Searching through versions with a wildcard

Searching through version fields with a wildcard was one of the most demanded features suggested by our users, and today we’re making it happen. Whether it’s 9, 9.1, or any other derivative you’re looking for, you can now find it with a simple query that uses the ~ operator and an asterisk to indicate the location of unknown characters (regardless of how many there are).

To illustrate this with an example, if you’d like to find all issues within the 9.x line, your search query would look like this: fixVersion ~ “9.*”. Go ahead, try it out, and bring some beauty to your queries!

### More improvements

#### Delimiters for CSV

You can choose one of the four most commonly used delimiters when exporting your issues to CSV. These are comma, semicolon, vertical bar, and caret (this one: ^). If you’d like to try it out, just search for some issues in JIRA, and then hit **Export > CSV** in the top-right corner. If you're a JIRA admin, you can also disable this feature in global settings.

#### Disabling empty JQL queries

When you use a filter with an empty JQL query, it will retrieve all possible issues, which can have a serious impact on performance if you throw a couple of these on your boards and gadgets. To avoid that, we’ve added a global setting that lets you decide how an empty JQL query behaves: either returning all issues (like it is now), or no results at all.
