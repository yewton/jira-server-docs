---
aliases:
- /server/jira/platform/4227111.html
- /server/jira/platform/4227111.md
category: devguide
confluence_id: 4227111
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227111
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227111
date: '2017-12-08'
legacy_title: Can I have a different Excel template for each report?
platform: server
product: jira
subcategory: other
title: Can I have a different Excel template for each report?
---
# Can I have a different Excel template for each report?

Excel templates of reports are controlled by the file `\secure\views\browser\report-excel.jsp`. While you can't easily have different `report-excel.jsp` for each report, you can easily edit the `report-excel.jsp` file to behave differently for each report (or if you want, use a different include for each report).

The report-excel.jsp has full access to methods available in `com.atlassian.jira.web.action.browser.ConfigureReport`. This means that you can figure out what report it is from the `getReportKey()` and  
`getReport()` methods, and make the page behave accordingly.

