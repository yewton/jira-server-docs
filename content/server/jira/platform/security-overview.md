---
aliases:
- /server/jira/platform/security-overview-32345317.html
- /server/jira/platform/security-overview-32345317.md
category: devguide
confluence_id: 32345317
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32345317
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32345317
date: '2018-06-25'
legacy_title: Security overview
platform: server
product: jira
subcategory: security
title: "Security overview"
---
# Security overview

Implementing security is an essential part of integrating with Jira Server. It lets Atlassian applications
protect customer data from unauthorized access and from malicious or accidental changes. It also allows
administrators to install apps with confidence, letting users enjoy the benefits of apps in a secure manner.

There are two parts to securing your Jira app or integration: authentication and authorization. Authentication
tells Jira Server the identity of your app or integration, authorization determines what actions it can take within Jira.

## Authentication

Authentication is the process of identifying your app or integration to the Atlassian application and is the
basis for all other security. The Jira platform, Jira Software, and Jira Service Desk REST APIs can use one of
the following two methods to authenticate clients directly. 

<table>
<thead>
<tr class="header">
<th> </th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>OAuth (1.0a) authentication</td>
<td>OAuth is a token-based authentication method that uses request tokens generated from Jira Server to authenticate
the client. We recommend that you use OAuth in most cases. It takes more effort to implement, but it is more
flexible and secure compared to the other two authentication methods. <br />
Read the <a href="/server/jira/platform/oauth">OAuth tutorial</a>.</td>
</tr>
<tr class="even">
<td>Basic authentication</td>
<td>Basic authentication uses a predefined set of user credentials to authenticate the client. We recommend that
you don't use basic authentication, except for tools like personal scripts or bots. It may be easier to implement,
but it is much less secure. <br />
Read the <a href="/server/jira/platform/basic-authentication">Basic authentication tutorial</a>.</td>
</tr>
<tr class="odd">
<td>Cookie-based authentication</td>
<td>It's recommended to use OAuth or Basic authentication for API calls, however, if you want to reproduce behavior
when user logs in to Jira, you can use cookie-based authentication. <br />
Read the <a href="/server/jira/platform/cookie-based-authentication">Cookie-based authentication tutorial</a>.
</td>
</tr>
</tbody>
</table>

Note:

*   Jira uses cookie-based authentication in the browser, so you can call REST from the browser (for example,
  via JavaScript) and rely on the authentication that the browser has established. 

*   Keep in mind that you cannot use Jira REST API to authenticate with a Jira site once Jira's CAPTCHA upon
login feature has been triggered. Check the response to confirm this: if the `X-Seraph-LoginReason` header has
value of `AUTHENTICATION_DENIED`, it means the application rejected the login without even checking the password,
which usually means that CAPTCHA has been triggered.

### Form token handling

Form token handling is an optional additional authentication mechanism that lets Jira validate the origin and
intent of requests. This is used to provide an another level of security against XSRF. For details, see 
[Form token handling](/server/jira/platform/form-token-handling/).

## Authorization

Authorization is the process of allowing your app or integration to take certain actions in the Atlassian
application after it has been authenticated. Authorization for direct calls to the Jira Server REST APIs is based
on the user used in the authentication process.

<table>
<thead>
<tr class="header">
<th> </th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>OAuth (1.0a) authentication</td>
<td>The user who authorizes the request token, in the authentication process for the client, is used to make
requests to Jira Server.</td>
</tr>
<tr class="even">
<td>Basic authentication</td>
<td>The actions your client can perform are based on the permissions of the user credentials you use.
For example, if you use basic authentication, your user must have Edit Issue permission on an issue
 to make a PUT request to the <code>/issue</code> resource.</td>
</tr>
</tbody>
</table>
