---
aliases:
- /server/jira/platform/--existing-user-name-drop-down-web-sections-4227204.html
- /server/jira/platform/--existing-user-name-drop-down-web-sections-4227204.md
category: devguide
confluence_id: 4227204
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227204
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227204
date: '2017-12-08'
legacy_title: __Existing_User_Name_Drop_Down_Web_Sections
platform: server
product: jira
subcategory: other
title: Existing username dropdown web sections
---
# Existing username dropdown web sections

<img src="/server/jira/platform/images/4391015.png" class="gliffy-macro-image" />
