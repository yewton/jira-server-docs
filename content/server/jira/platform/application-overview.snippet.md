---
aliases:
- /server/jira/platform/application-overview-4227092.html
- /server/jira/platform/application-overview-4227092.md
category: devguide
confluence_id: 4227092
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227092
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227092
date: '2017-12-08'
legacy_title: Application Overview
platform: server
product: jira
subcategory: other
title: Application overview
---
# Application overview

<img src="/server/jira/platform/images/4391004.png" class="gliffy-macro-image" />

