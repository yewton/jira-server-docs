---
title: JIRA 7.3 Eap Release Notes 44057319
aliases:
    - /server/jira/platform/jira-7.3-eap-release-notes-44057319.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44057319
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44057319
confluence_id: 44057319
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.3 EAP release notes

Find the release notes for the JIRA 7.3 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.3 release candidate (RC02) release notes](/server/jira/platform/jira-core-7-3-release-candidate-rc02-release-notes)
-   [JIRA Software 7.3 release candidate (RC02) release notes](/server/jira/platform/jira-software-7-3-release-candidate-rc02-release-notes)
-   [JIRA Service Desk 3.3 release candidate (RC02) release notes](/server/jira/platform/jira-service-desk-3-3-release-candidate-rc02-release-notes)
-   [JIRA Core 7.3 EAP4 release notes](/server/jira/platform/jira-core-7-3-eap4-release-notes)
-   [JIRA Software 7.3 EAP4 release notes](/server/jira/platform/jira-software-7-3-eap4-release-notes)
-   [JIRA Service Desk 3.3 EAP4 release notes](/server/jira/platform/jira-service-desk-3-3-eap4-release-notes)
