---
title: JIRA Xml Rpc Overview 4227112
aliases:
    - /server/jira/platform/jira-xml-rpc-overview-4227112.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227112
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227112
confluence_id: 4227112
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA XML-RPC Overview

{{% note %}}

 

 

 

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

 

 

***
***


*JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](https://developer.atlassian.com/display/JIRADEV/JIRA+SOAP+to+REST+Migration+Guide)).*

{{% /note %}}

## Introduction

This page provides basic documentation on JIRA's XML-RPC capabilities. For latest methods available through the XML-RPC please refer to the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/index.html?com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">latest Javadoc for XmlRpcService</a>.

Notes:

-   The URL for XML-RPC requests is http://`jira-install`/rpc/xmlrpc.
-   All methods must be prefixed by `jira1` - to indicate this is version 1 of the API. We might introduce another version in the future.
-   All keys in structs are case sensitive.
-   All strings are passed as UTF-8, and not ASCII per the XML-RPC
-   When reading the APInywhere you see the word Vector, you can interchange it with "Array" or "List" depending on what language you prefer. This is the array data type as defined in the XML-RPC spec.
-   Anywhere you see the word Hashtable, you can interchange it with "Struct" or "Dictionary" or "Map" depending on what language you prefer. This is the struct data type as defined in the XML-RPC spec.
-   The default XML-RPC session lifetime is 60 minutes, it will be editable in the future.

{{% warning %}}

You may also wish to see the [Creating an XML-RPC Client](/server/jira/platform/creating-an-xml-rpc-client-4227133.html) or [Creating a JIRA SOAP Client](/server/jira/platform/creating-a-jira-soap-client-4227095.html) if you're interested in creating a JIRA remote client.

{{% /warning %}}

## Enable the RPC plugin

{{% note %}}

 

 

 

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

 

 

***
***


*JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](https://developer.atlassian.com/display/JIRADEV/JIRA+SOAP+to+REST+Migration+Guide)).*

{{% /note %}}

To invoke JIRA operations remotely, you should ensure that the RPC plugin is enabled on the JIRA installation you are targeting. If you simply want to create a client to <a href="http://jira.atlassian.com/" class="uri external-link">http://jira.atlassian.com/</a> then you can skip this step. First you need to check if the Accept Remote API Calls has been enabled in '**General Configuration**' under '**Global Settings**' in the left-hand menu:

<img src="/server/jira/platform/images/rpc-remote-calls.png" class="image-center" />

Then you need to enable the JIRA RPC Plugin in '**Plugins**' under '**System**' in the left-hand menu:

<img src="/server/jira/platform/images/plugins-rpcplugin.png" class="image-center" />

![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) To get the source code of the RPC plugin, see <a href="https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin" class="uri external-link">https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin</a>

Your server should now be ready to accept remote procedure calls.

## Remote Methods

The most recent and up-to-date source of information of available exposed methods is the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/" class="external-link">Javadoc for the RPC plugin</a>, specifically those on the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">XmlRpcService</a>.

The Javadoc will often refer to "hashtables with fields from RemoteObject". The hashtable will contain keys that map to the fields available <a href="http://jakarta.apache.org/commons/beanutils/apidocs/org/apache/commons/beanutils/BeanUtils.html#describe(java.lang.Object)" class="external-link">through reflection</a> of the particular RemoteObject. For example, the object `RemoteVersion`, has the methods getReleaseDate(), getSequence(), isArchived() and isReleased(). This will be converted into a Hashtable with keys releaseDate, sequence, archived and released.

## Data Objects

Most returned structs have a summary and a detailed form:

-   The summary form is a primary key (ie project key) and a representative form (ie name)
-   The detailed form will have all of the entity details as might be needed for the client.

Unless otherwise specified, all returned structs are in detailed form.

### Project

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of the project</p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>String</p></td>
<td><p>the project key</p></td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the project</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>String</p></td>
<td><p>the url to view this project online</p></td>
</tr>
<tr class="odd">
<td><p>projectUrl</p></td>
<td><p>String</p></td>
<td><p>the url of this project in your organisation (ie not a JIRA URL)</p></td>
</tr>
<tr class="even">
<td><p>lead</p></td>
<td><p>String</p></td>
<td><p>the username of the project lead</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>String</p></td>
<td><p>a description of this project</p></td>
</tr>
</tbody>
</table>

### Component

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of the component</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the component</p></td>
</tr>
</tbody>
</table>

### Version

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of the version</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the version</p></td>
</tr>
<tr class="odd">
<td><p>released</p></td>
<td><p>boolean</p></td>
<td><p>whether or not this version is released</p></td>
</tr>
<tr class="even">
<td><p>archived</p></td>
<td><p>boolean</p></td>
<td><p>whether or not this version is archived</p></td>
</tr>
</tbody>
</table>

### IssueType / Status / Resolution

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of this constant</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the constant</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>String</p></td>
<td><p>the description of this constant</p></td>
</tr>
<tr class="even">
<td><p>icon</p></td>
<td><p>String</p></td>
<td><p>the URL to retrieve the icon of this constant</p></td>
</tr>
</tbody>
</table>

### Priority

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of this constant</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the constant</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>String</p></td>
<td><p>the description of this constant</p></td>
</tr>
<tr class="even">
<td><p>icon</p></td>
<td><p>String</p></td>
<td><p>the URL to retrieve the icon of this constant</p></td>
</tr>
<tr class="odd">
<td><p>colour</p></td>
<td><p>String</p></td>
<td><p>the colour of this constant</p></td>
</tr>
</tbody>
</table>

### Filter

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>String</p></td>
<td><p>the id of this filter</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the name of the filter</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>String</p></td>
<td><p>the description of this filter</p></td>
</tr>
<tr class="even">
<td><p>author</p></td>
<td><p>String</p></td>
<td><p>the username of this filter's owner</p></td>
</tr>
<tr class="odd">
<td><p>project</p></td>
<td><p>String</p></td>
<td><p>the id of the project this search relates to (null if the search is across projects)</p></td>
</tr>
<tr class="even">
<td><p>xml</p></td>
<td><p>String</p></td>
<td><p>a complete XML representation of this search request - I don't recommend you use this for now, it's complex <img src="/server/jira/platform/images/icons/emoticons/smile.png" alt="(smile)" class="emoticon-smile" /></p></td>
</tr>
</tbody>
</table>

### User

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Type</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>name</p></td>
<td><p>String</p></td>
<td><p>the username of this user</p></td>
</tr>
<tr class="even">
<td><p>fullname</p></td>
<td><p>String</p></td>
<td><p>the full name of this user</p></td>
</tr>
<tr class="odd">
<td><p>email</p></td>
<td><p>String</p></td>
<td><p>the email address of this user</p></td>
</tr>
</tbody>
</table>
