---
title: JIRA Webwork Actions 4227099
aliases:
    - /server/jira/platform/jira-webwork-actions-4227099.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227099
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227099
confluence_id: 4227099
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Webwork Actions

### Webwork Actions and `actions.xml`

A <a href="http://en.wikipedia.org/wiki/Comparison_of_web_application_frameworks#Java" class="external-link">web application framework</a> defines what happens when you visit a particular URL in a web application. For example, the URL for a simple static page with no dynamic content could end in ".html". A ".jspa" suffix indicates that the URL is referring to a page whose content was created using Java Server Pages (JSP). JSP files are templates that can contain both HTML and commands to create HTML. The commands refer to a Java object and the object's methods are called just as in an ordinary Java program. The mapping of the URL to a Java class in JIRA is done using the Webwork 1.x web application framework. The original documentation for Webwork 1.x can be found at <a href="http://opensymphony.com/webwork_old/src/docs/manual" class="uri external-link">http://opensymphony.com/webwork_old/src/docs/manual</a> and <a href="http://wiki.opensymphony.com/display/WW1/Home" class="uri external-link">http://wiki.opensymphony.com/display/WW1/Home</a>. However, this framework has been superseded by Webwork 2, which is used by Confluence, Bamboo and Crowd

The mappings between a URL and classes are declared in the actions.xml file (`src/webapp/WEB-INF/classes/actions.xml`). A typical element of this file looks like:

![](/server/jira/platform/images/actions.xml.png)

Each action element has an `alias` attribute, which is the part of the URL that you see in a web browser. The `name` element is the name of the Java class that is used by the `alias`.

Command elements are optional, and are used when several interactions belong to the same Action. A command name is specified on the URL like this:

``` javascript
SomeAction!myCommand.jspa
```

The command is implemented in a method in the Action class with the corresponding name:

``` javascript
public String doMyCommand() {

    // implement the command logic here

    return "someview";
}
```

The doExecute method is run when no command is requested i.e. the bare `/path/to/MyAction.jspa`.

{{% warning %}}

Security

When adding an action to the actions.xml You must ensure the appropriate `roles-required` value is specified. This will ensure only users in the authorised role can execute the action. For actions that can be handled by application level security, such as those actions that can be given to project administrators identified through the course of administering JIRA, no role may be required, likewise setup actions and others that manage their own permissions. For some actions, the **use** role must be present. This ensures the user is logged in and identified.

The **admin** role is required for all administration actions so you must be sure when adding an action that your new action has **roles-required="admin"** or confident that it doesn't need it. The **sysadmin** role requires the user be a system administrator, and the **use** role requires that they just be logged in. (Other definitions can be found in `Permissions.java` in the source).

Actions don't care about the path of the URI, just the ActionName.jspa and optionally the !commandName suffix.

{{% /warning %}}

### Webwork Plugins

JIRA can have new actions defined using the [Webwork plugin module](/server/jira/platform/webwork). These actions can also override existing actions in JIRA.
However, jsp files cannot currently be bundled in the plugin jar file and have to be installed in a separate step when deploying such a plugin.

There is a <a href="https://bitbucket.org/mdoar/webwork-sample" class="external-link">Webwork Sample plugin</a> that contains example actions and classes that can be used to understand this topic more fully.
