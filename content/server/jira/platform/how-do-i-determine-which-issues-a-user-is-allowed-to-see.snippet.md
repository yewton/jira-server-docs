---
aliases:
- /server/jira/platform/4227148.html
- /server/jira/platform/4227148.md
category: devguide
confluence_id: 4227148
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227148
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227148
date: '2017-12-08'
legacy_title: How do I determine which Issues a user is allowed to see?
platform: server
product: jira
subcategory: other
title: How do I determine which Issues a user is allowed to see?
---
# How do I determine which Issues a user is allowed to see?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I determine which Issues a user is allowed to see?

Given a list of issues, use

``` javascript
IssueUtils.filterIssues(issueGVs, new PermissionsParameter(user))
```

to filter the list of issues down to only those that the user has permission to view.
