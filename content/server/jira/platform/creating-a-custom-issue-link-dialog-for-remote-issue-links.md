---
aliases:
- /server/jira/platform/creating-a-custom-issue-link-dialog-for-remote-issue-links-6848565.html
- /server/jira/platform/creating-a-custom-issue-link-dialog-for-remote-issue-links-6848565.md
category: devguide
confluence_id: 6848565
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848565
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848565
date: '2017-12-08'
guides: guides
legacy_title: Creating a Custom Issue Link Dialog for Remote Issue Links
platform: server
product: jira
subcategory: learning
title: Creating a custom issue link dialog for remote issue links
---
# Creating a custom issue link dialog for remote issue links

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

This guide will show you how to create a custom dialog for creating issue links, which makes it easier for users to create remote links to a third-party application in JIRA.

## Steps

1.  Insert a [web item](/server/jira/platform/web-item) in your `atlassian-plugin.xml` with a section attribute having a value of 'create-issue-link-types'.

    ``` xml
    <web-item key="add-myapp-link" section="create-issue-link-types" weight="100">
      <label key="add.myapp.link.label" />
      <link linkId="add-myapp-link-link">
        /secure/LinkMyAppEntity!default.jspa?id=${issueId}
      </link>
    </web-item>
    ```

2.  Insert a [webwork action](/server/jira/platform/webwork) in your `atlassian-plugin.xml` with an alias attribute matching the link destination defined in the web item above.

    ``` xml
    <webwork1 key="link-myapp-entity" name="Link My Application Entities" class="java.lang.Object">
      <actions>
        <action name="com.mycompany.myapp.LinkMyAppEntity" alias="LinkMyAppEntity">
          <view name="error">/templates/dialog/linkmyappentity.vm</view>
          <view name="input">/templates/dialog/linkmyappentity.vm</view>
        </action>
      </actions>
    </webwork1>
    ```

3.  Write a matching webwork action and a velocity template. Examples:
    -   `com.atlassian.jira.plugin.link.remotejira.LinkRemoteJiraIssue`
    -   `linkremotejiraissue.vm`

