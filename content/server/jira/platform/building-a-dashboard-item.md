---
aliases:
- /server/jira/platform/guide-building-a-dashboard-item-for-a-jira-p2-add-on-33745889.html
- /server/jira/platform/guide-building-a-dashboard-item-for-a-jira-p2-add-on-33745889.md
category: devguide
confluence_id: 33745889
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33745889
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33745889
date: '2018-06-13'
guides: guides
legacy_title: Guide - Building a dashboard item for a JIRA P2 add-on
platform: server
product: jira
subcategory: learning
title: "Building a dashboard item"
---
# Building a dashboard item

This guide will help you build a [dashboard item](/server/jira/platform/dashboard-item-module/) for a *Jira P2 add-on*.
You need to be familiar with building Jira P2 add-ons to use this guide. 

If you build a dashboard item for a Jira Connect
app, see this guide instead: 
[Building a dashboard item for Jira Connect add-on](/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254.html).

A dashboard item is similar to a gadget since it provides discrete dynamic content that user can
use to customize their dashboard. However, dashboard items use modern rendering technologies like AMD,
Soy, LESS, and so on. Dashboard items are also able to share web-resources, which greatly improves page load
performance by reducing the number of requests and the overall size of data to be transferred on a dashboard load.

Dashboard items will eventually replace gadgets in Jira. They are easy to build and are faster. Gadgets
are rendered in `iframes` and cannot share web-resources, which makes them slow to load. Gadgets are also
based on the OpenSocial specification that is no longer championed by Google. 

## Before you begin

*   If you replace a gadget with a dashboard item for your app, **do not modify the existing
    OpenSocial gadget code**. This provides you with a fallback, in case something goes wrong with your dashboard item —
    you can turn off the app module for the dashboard item and the old OpenSocial gadget will render instead.
*   Unlike gadgets, dashboard items do not support remoting, for example, embedding an OpenSocial gadget from Jira
on a Confluence page. We don't have plans to support remoting for dashboard items at this point in time.

## Defining the dashboard item

The [Dashboard item](/server/jira/platform/dashboard-item-module/) page shows how you should define
a dashboard item in your app descriptor.

## General development guidelines

We recommend that you read all of the guidelines below before developing a dashboard item. These are
high level guidelines, as this guide is intended for experienced app developers.

### Setting the correct context

Unlike OpenSocial gadgets, dashboard items are not sandboxed in iframes. Hence, you need to set the
correct context for the CSS/LESS and JavaScript of your dashboard item to ensure that it doesn't interfere
with other dashboard items.

*   **Ensure the CSS/LESS is properly namespaced.** This ensures that the styling of your dashboard item does
not interfere with other dashboard items on the dashboard.
*   **Ensure all JavaScript operates in the context of the passed `<div />`.** All JavaScript must operate within
the context of the passed `<div />` when the dashboard item is initialized. This ensures that event handlers,
for example, only get bound within the current dashboard item and don't affect other dashboard items.

### Performance

*   **Only use server-side rendering for mostly static information.** Anything that might be expensive
    (for example, running a JQL query, generating data for a chart, and so on) should not be done server-side,
    otherwise it will delay the rendering of the entire dashboard. Instead, the data should be requested via an
    AJAX request and then rendered client-side.

### Sharing resources

*   **Using shared AMD modules and soy templates.** You can introduce shared AMD modules and soy
    templates where this makes sense for common configuration form elements (for example, filter pickers)

### Testing your dashboard item

*   **Unit testing** — we recommend testing all new code with a JavaScript unit testing framework, like QUnit.
*   **Testing "replacement" dashboard items** — we recommend that you test your "replacement" dashboard
    item by creating an instance of the old gadget and then enabling your new dashboard item. You can
    then check whether your dashboard item is compatible with the preferences of the old gadget.

## Design guidelines

There are no design guidelines specific to dashboard items. We recommend that you consult the
[Atlassian Design Guidelines](https://atlassian.design/server/), as you should do when developing for any Atlassian product.

Here are a few topics that may be relevant to your dashboard item:

*   [Tables](https://design.atlassian.com/latest/product/components/tables/)
*   [Charts](https://design.atlassian.com/latest/product/foundations/charts/)
*   [Forms](https://design.atlassian.com/latest/product/components/forms/)
