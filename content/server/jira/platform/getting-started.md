---
aliases:
- /server/jira/platform/getting-started-32344047.html
- /server/jira/platform/getting-started-32344047.md
category: devguide
confluence_id: 32344047
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32344047
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32344047
date: '2017-12-08'
legacy_title: Getting started
platform: server
product: jira
subcategory: intro
title: Getting started
---
# Getting started

Welcome to the Jira developer community! This page provides some resources which will help you get started developing for Jira Server.

*Note, these resources are relevant for Jira Server only. If you are using* ***Jira Cloud****, check out our [Jira Cloud documentation](https://developer.atlassian.com/display/jiracloud/Getting+started) instead.*

### Developing apps for Jira Server

Jira's plugin system allows you to build apps that customize and extend Jira. An app (also known as a plugin or add-on) is a bundle of code, resources and a special configuration file that can be installed on a Jira site to add new functionality, or change the behavior of existing features.

There are two main types of apps:

- **system apps** which are bundled with Jira and provide core Jira functions.
- **custom and third-party apps** which are developed specifically for your Jira site or available from the [Atlassian Marketplace](https://marketplace.atlassian.com). These can be free or paid. [Learn how to make your app available on Marketplace](https://developer.atlassian.com/platform/marketplace/)

The term app, add-on, and plugin are often used interchangeably. We'll mostly stick to the term 'plugin'.

##### Get hands-on with the Atlassian Plugin SDK

Never built a Plugins2 plugin before? Try our Atlassian Plugin SDK tutorial. This will help you set up the Atlassian Plugin SDK and lead you through the creation of a Jira plugin module. You can also bring your plugin into an Eclipse IDE project, if you choose.

Learn more: [Atlassian Plugin SDK tutorial](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) *(SDK documentation)*

##### Check out the Jira APIs

Jira has REST APIs and Java APis that you can use to interact with Jira. For example, you may want to integrate your existing application with Jira or write a script to get information from Jira.

Learn more: [Jira APIs](/server/jira/platform/jira-apis-32344053.html)

##### Try a few Jira tutorials

A great way to get acquainted with the Jira platform is to try a few of our hands-on tutorials. The Jira platform section has quick reference pages for the major components of Jira (e.g. projects, issues, etc), On each page, you'll find links to step-by-step tutorials. The beginner tutorials, like [this one](/server/jira/platform/adding-menu-items-to-jira), are a good place to start.

Learn more: [Jira Platform](/server/jira/platform/integrating-with-jira-server)

##### Learn about the Jira architecture

If you're ready to learn more about the nuts and bolts of Jira development, check out the [Jira architecture](/server/jira/platform/jira-architecture-32344051.html) section. You'll find articles on topics like Jira's dependencies, webhooks, web panels, plugin modules, and more.

Learn more: [Jira architecture](/server/jira/platform/jira-architecture-32344051.html)
