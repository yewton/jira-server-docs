---
aliases:
- /server/jira/platform/project-configuration-locations-4227077.html
- /server/jira/platform/project-configuration-locations-4227077.md
category: reference
confluence_id: 4227077
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227077
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227077
date: '2018-06-18'
legacy_title: Project Configuration Locations
platform: server
product: jira
subcategory: modules
title: "Project settings UI locations"
---
# Project settings UI locations

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 4.4 and later</p></td>
</tr>
</tbody>
</table>

## Project configuration administration location

{{% tip %}}
[Web Fragment Finder](https://marketplace.atlassian.com/apps/1211656/web-fragment-finder) can be
useful when looking for Jira UI locations. Note that it is a third-party tool that is not supported by Atlassian.
{{% /tip %}}

The `atl.jira.proj.config` location defines web sections (for groups of tabs) and web items (for individual tabs
within a group) that are available when viewing a project's configuration in Jira's administration area.

You can add new web items to predefined web sections (that is, groups of tabs) or add new sections along with their own items.

<img src="/server/jira/platform/images/web-fragments-admin-project-config-admin.png" width="330" height="250" />

### Adding new items to existing web sections

To add your own web item to an existing web section (that is, groups of tabs) of Jira project configuration
in the administration area, your web item must include a section attribute with the value
`"atl.jira.proj.config/existing_web_section_name"`, where `existing_web_section_name` is the existing group
of tabs to which the web item will be added.

The following table lists these existing web sections (by `existing_web_section_name`) and the existing web items
associated with these web sections. The `weight` attribute values associated with each web section and item are
indicated in brackets.

For more information about the purpose of this attribute, see "Customizing the positions of items and sections"
section later on this page.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Group of tabs <code>existing_web_section_name</code></p></th>
<th><p>Web items associated with this group of tabs</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>projectgroup1 (10)</p></td>
<td><p>Summary (10) <br/>
Details (11) <br/>
Re-index project (13) <br/>
Delete project (15)</p></td>
</tr>
<tr class="even">
<td><p>projectgroup2 (20)</p></td>
<td><p>Workflows (20)<br/>
Screens (30)<br/>
Fields (40)<br/>
Priorities (50)</p></td>
</tr>
<tr class="odd">
<td><p>projectgroup3 (30)</p></td>
<td><p>Versions (10)<br />
Components (20)</p></td>
</tr>
<tr class="even">
<td><p>projectgroup4 (40)</p></td>
<td><p>Users and roles (15)<br />
Permissions (20)<br />
Issue Security (30)<br />
Notifications (40)</p></td>
</tr>
</tbody>
</table>

For example, to add a web item module that defines a new item in the `projectgroup3` section, which appears between
the existing `Versions` and `Components` web items, your web item module should contain the following:

``` xml
<web-item key="my_new_tabbed_item_in_an_existing_project_group" ... section="atl.jira.proj.config/projectgroup3" ... weight="15">
    ...
</web-item>
```

### Adding custom sections and items

To add your own section to the username drop-down menu, define a web section that includes a `location` attribute
with the value `"atl.jira.proj.config"`.

For example, your web section module should contain the following:

``` xml
<web-section key="my-custom-group-of-tabs-section" ... location="atl.jira.proj.config" ...>
...
</web-section>
```

Adding new web items to your own sections is similar to the method for adding new web items to existing web sections.
However, you need to specify the value of your web section's `key` attribute in the value of your web item's
`section` attribute (preceded by the location). So, based on the previous example, your web item should contain this:

``` xml
<web-item key="my_new_tabbed_item_in_a_custom_project_group" ... section="atl.jira.proj.config/my-custom-group-of-tabs-section" ...>
...
</web-item>
```

### Ensuring a web item is displayed by default in a project configuration

To ensure that a particular web item (that is, tabbed page) is displayed after a Jira administrator accesses a
project's configuration by selecting the project's menu item, do the following:

1.  Define a `meta` element in your HTML template file (such as a Velocity template file) with the attribute
`name="admin.active.section"`.

    {{% note %}}

This element links your HTML template to the menu item that you defined previously in either "Adding new items
to existing web sections" or "Adding custom sections and items" section of this page.

    {{% /note %}}

    *   This `meta` tag should also contain a `content` attribute whose value is the project configuration
    administration location (`"atl.jira.proj.config"`).

    For example:

    ``` xml
    <meta name="admin.active.section" content="atl.jira.proj.config"/>
    ```

1.  Define another `meta` tag in the HTML template file with the attribute `name="admin.active.tab"`.

    {{% note %}}

This element specifies the tabbed page that is displayed by default after a Jira administrator selects
the project menu item.

    {{% /note %}}

    *   This `meta` tag should also contain a `content` attribute whose value is the `key` attribute's value specified
    in the `web-item` module you defined above for either an existing or custom group of tabs.
    *   For example, following on from the procedure of adding custom sections and items for defining a web item
    module for a custom group of tabs:

    ``` xml
    <meta name="admin.active.tab" content="my_new_tabbed_item_in_a_custom_project_group"/>
    ```

### Customizing the positions of items and sections

To customize the position of your own web items or sections in this location, add a `weight` attribute to your
`web-item` or `web-section` and adjust its value with respect to Jira's existing web items or sections.
Lower weight values result in these items or sections appearing higher up the page.

### Source File

To find the values of Jira's existing web items and sections for the `atl.jira.proj.config` location,
check the following file in Jira's source archive:

`<source-installation-directory>/jira-project/jira-components/jira-plugins/jira-project-config-plugin/src/main/resources/atlassian-plugin.xml`

## Project configuration administration – summary tab location

The `webpanels.admin.summary.left-panels` and `webpanels.admin.summary.right-panels` locations define web panels for
individual panels/segments of an HTML page when viewing the summary tab of a project's configuration in Jira
administration area.

The two `webpanels.admin.summary.left-panels` and `webpanels.admin.summary.right-panels` locations define web
panels for the respective left and right hand sides of this summary tab.

`webpanels.admin.summary.left-panels`

<img src="/server/jira/platform/images/project-configuration-administration-summary-tab-left.png" />

`webpanels.admin.summary.right-panels`

<img src="/server/jira/platform/images/project-configuration-administration-summary-tab-right.png" />

### Adding custom panels

To add your own web panel to the summary tab location of Jira project configuration administration, your web
panel must include a location attribute with one of the following values:

*   `"webpanels.admin.summary.left-panels"` – to add a panel to the left hand side of the summary tab page.
*   `"webpanels.admin.summary.right-panels"` – to add a panel to the right hand side of the summary tab page.

For example, to add a web panel module that defines a new section on the right of the summary tab page,
 your web panel module should contain this:

``` xml
<web-panel ... location="webpanels.admin.summary.right-panels" ...>
    ...
        <label key="my-new-panel.title"/>
    ...
</web-panel>
```

### Panel Headings

If you want to create a "panel label" (that is, the collapsible heading that appears at the top of the panel),
define a `label` element with a `key` attribute whose value defines a unique property (that is, `my-new-panel.title`
based on the example above). The string value of this unique property (defined in your plugin's `i18n.properties` file)
will be shown in bold text at the top of your section.

If you do not want a heading to appear at the top of the panel, include the following child element in your
`web-panel` module:

``` xml
<param name="headless">true</param>
```

### Customizing the positions of panels

To customize the position of your own web panels in this location, add a `weight` attribute to your `web-panel` and
adjust its value with respect to Jira's existing web panels.
Lower weight values result in these panels appearing higher up the summary tab page.

### Source file

To find the values of Jira's existing web panels for the `webpanels.admin.summary.left-panels` and
`webpanels.admin.summary.right-panels` locations, check the following file in Jira's source archive:

`<source-installation-directory>/jira-project/jira-components/jira-plugins/jira-project-config-plugin/src/main/resources/atlassian-plugin.xml`

## Project configuration operations location

In Jira 4.4 and later, the `system.view.project.operations` location defines web items of the "Actions" drop-down menu
when viewing a project's configuration in Jira's administration area.

![](/server/jira/platform/images/web-fragments-project-configuration-operations-post-4-4.png)

### Adding custom items

To add your own web item to Jira project configuration operations location, your web item must include a
`section` attribute with the value `"system.view.project.operations"`.

For example, to add a web item module that defines a new project operation link on a project configuration page,
your web item module should contain:

``` xml
<web-item ... section="system.view.project.operations" ...>
    ...
</web-item>
```
