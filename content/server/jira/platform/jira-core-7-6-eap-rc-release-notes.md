---
aliases:
- /server/jira/platform/jira-core-7.6-eap-rc-release-notes-52429404.html
- /server/jira/platform/jira-core-7.6-eap-rc-release-notes-52429404.md
category: devguide
confluence_id: 52429404
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52429404
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52429404
date: '2017-12-08'
legacy_title: JIRA Core 7.6 EAP RC release notes
platform: server
product: jira
subcategory: updates
title: JIRA Core 7.6 EAP RC release notes
---
# JIRA Core 7.6 EAP RC release notes

**30 October 2017**

Atlassian is proud to present **JIRA Core 7.6 EAP Release Candidate**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Core 7.6** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.6, read the developer change management guide: [Preparing for JIRA 7.6](/server/jira/platform/preparing-for-jira-7-6). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Priority schemes

No two projects are the same, and no two teams tackle a problem in the same way. With that in mind, we've introduced a new approach to priorities to accommodate the diversity of teams working with JIRA. Now, you can choose different sets of priorities for each of your projects by using priority schemes.

### What is a priority scheme? 

A priority scheme works like a mapping that lets you associate a subset of priorities with particular projects. You can use it to achieve the following goals:

-   restrict the set of available priorities for a project,
-   control the order in which priorities are displayed for users trying to assign them to issues,
-   select a default priority that will be assigned to all newly created issues in a project.

A single priority scheme can be used across multiple projects so that a group of similar projects can share the same priorities. It's also easier to add or remove priorities for these projects because all you need to modify is a single priority scheme. Whether you want two schemes with ten priorities each, or ten schemes with a smaller number of priorities--we'll leave that one for you to decide. 

### Managing priority schemes

To create priority schemes, or to associate them with your projects, go to <img src="https://confluence.atlassian.com/download/attachments/939516800/image2017-10-24%2016%3A38%3A29.png?version=1&amp;modificationDate=1508855910332&amp;api=v2" class="confluence-external-resource" /> **&gt; Issues**, and choose **Priority schemes**. You need to be a JIRA administrator to get there. The page where priority schemes live looks similar to the one below.

1.  This is where all **priority schemes** live. 
2.  **Priorities** used by a scheme.
3.  **Projects** that a scheme is associated with. In other words, projects that use this set of priorities.
4.  **Actions** that you can perform on a scheme: **Edit**, **Associate**, and **Delete**.

Project priorities are in the final stage, and that's exactly how they're going to look like when we release them with JIRA 7.6. 

### Priority schemes in JIRA Service Desk

Because of differences between JIRA Service Desk and JIRA platform, we couldn't introduce priority schemes for JSD projects in this release. These projects will use the default priority scheme, which works like a global list of priorities--you can still add and remove priorities, like in earlier versions, but you can't restrict them to specific projects. If you're using both JIRA Core and JIRA Service Desk, you can manage priority schemes only for Core projects; the JSD ones will be hidden on the Priority schemes page.

------------------------------------------------------------------------

## Dragging and dropping subtasks

Until now, the only way to order your subtasks was to move them up and down by using arrows. We're perfectly aware how difficult that was, so we've introduced a small improvement that allows you to drag your subtasks and drop them anywhere on the list. Regardless of whether you usually create 3 or 33 subtasks under your stories, it'll be easier now to reorder them.

<img src="/server/jira/platform/images/subtasks-dragdrop.png" width="1078" height="137" />

------------------------------------------------------------------------

## JMX monitoring

JMX (<a href="https://docs.oracle.com/javase/8/docs/technotes/guides/jmx/" class="external-link">Java Management Extensions</a> API) monitoring brings live monitoring of your JIRA instance, including such metrics as the number of issues, all active users, and many others. JMX uses objects called MBeans (Managed Beans) to expose data and resources from your application. It might be useful to determine what causes performance issues, and where JIRA is struggling with a lot of data. We've introduced several metrics, but they're not collected by default. First, you'll need to enable JMX monitoring in JIRA, and then connect to it over JConsole.

To enable and use JMX monitoring:

1.  In JIRA, go to ![](/server/jira/platform/images/image2017-10-26-14:15:36.png) **&gt; System &gt; JMX monitoring**, and toggle **Enable JMX monitoring**.
2.  Connect to JIRA over JConsole.

    **To monitor locally:**

    1.  Start JConsole (you'll find it in the `bin` directory of the JDK installation directory).
    2.  Select **Local Process**.
    3.  Select the JIRA process. It will be called something like `org.apache.catalina.startup.Bootstrap start`

    See <a href="http://docs.oracle.com/javase/8/docs/technotes/guides/management/jconsole.html" class="external-link">Using JConsole</a> for more information on local monitoring.

    **To monitor remotely:**

    1.  Add the following properties to your `setenv.sh` / `setenv.bat` file. The port can be any port that is not in use.

        ``` bash
        set CATALINA_OPTS=-Dcom.sun.management.jmxremote %CATALINA_OPTS% set CATALINA_OPTS=-Dcom.sun.management.jmxremote.port=8099 %CATALINA_OPTS%
        ```

    2.  Decide how you will secure your remote connection. See <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html#gdenl" class="external-link">Remote Monitoring and Management</a> for more information.   
        Although it is possible to disable authentication, we do not recommend doing this on a production system.
    3.  Start JConsole (you'll find it in the `bin` directory of the JDK installation directory).
    4.  Select **Remote Process**.
    5.  Enter your hostname and port (this is the port you specified earlier, not the JIRA port).
    6.  Click **Connect**.

    See <a href="http://docs.oracle.com/javase/8/docs/technotes/guides/management/jconsole.html" class="external-link">Using JConsole</a> for more information on remote monitoring.

3.  After connecting to JIRA over JConsole, you'll find all of the following metrics in the `com.atlassian.jira` property.

    | Metric                    | Description                                                                                                                  |
    |---------------------------|------------------------------------------------------------------------------------------------------------------------------|
    | dashboard.view.count      | The number of times all dashboards were viewed by users.                                                                     |
    | entity.attachments.total  | The number of attachments.                                                                                                   |
    | entity.components.total   | The number of components.                                                                                                    |
    | entity.customfields.total | The number of custom fields.                                                                                                 |
    | entity.filters.total      | The number of filters.                                                                                                       |
    | entity.groups.total       | The number of user groups.                                                                                                   |
    | entity.issues.total       | The number of issues.                                                                                                        |
    | entity.users.total        | The number of users.                                                                                                         |
    | entity.versions.total     | The number of versions created.                                                                                              |
    | issue.assigned.count      | The number of times issues were assigned or reassigned to users (counts each action).                                        |
    | issue.created.count       | The number of issues that you created after installing this JIRA instance.                                                   |
    | issue.link.count          | The total number of issue links.                                                                                             |
    | issue.search.count        | The number of times you searched for issues.                                                                                 |
    | issue.updated.count       | The number of times you updated issues (each update after adding or changing some information).                              |
    | issue.worklogged.count    | The number of times you logged work on issues.                                                                               |
    | jira.license              | The types of licenses you have, the number of active users, and the maximum number of users available for each license type. |
    | web.requests              | The number of requests (invocation.count), and the total response time (total.elapsed.time).                                 |
