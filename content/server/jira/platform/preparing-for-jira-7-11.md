---
title: "Preparing for Jira 7.11"
product: jira
category: devguide
subcategory: updates
date: "2018-06-22"
---
# Preparing for Jira 7.11

This page covers changes in *Jira 7.11* that can affect add-on compatibility and functionality. This includes changes to the Jira platform and the Jira applications (Jira Core, Jira Software, Jira Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the Jira Server 7.11 products. We also make release milestones available prior to the release:

- For *Jira Server*, the Jira development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [Jira EAP Releases](/server/jira/platform/).

- For *Jira Cloud*, the Jira team releases updates to the Jira Cloud products on a weekly basis. Add-ons that integrate with the Jira Cloud products via Atlassian Connect should use [Jira REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the Jira news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Risk level/Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **IPv6 support**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Low<br>Affects: Plugin developers, Jira admins<br>
| **Apache Tomcat upgrade**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Medium<br>Affects: Plugin developers, Jira admins<br>
| **Rebranding Jira**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Low<br>Affects: Plugin developers, Jira admins<br>
| **Projects: new columns**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Low<br>Affects: Plugin developers, Jira admins<br>

## Changes

#### IPv6 support

We now support running Jira in an IPv6 environment. In most cases there's nothing you need to do, however there are a few known issues and limitations that you need to be aware of: 

- **Must allow IPv4 traffic** 

Some parts of Jira (e.g. End of Life check) need to be able to connect to IPv4 sites, such as Atlassian Marketplace. The same applies to integrating with Atlassian Cloud products, as they require IPv4.

- **Avoid using raw IPv6 addresses anywhere in the Jira configuration**

We recommend that you use hostnames or domain names instead of IPv6 addresses. It's a more reliable way of configuring and accessing both Jira and other Atlassian products. 

- **Avoid using raw IPv6 addresses in the browser**

Rich Text Editor won't work if you access Jira through a plain IPv6 address. It will work when you use a hostname.

- **AWS RDS does not yet support IPv6**

For this reason, our CloudFormation template and Quick Start (deploying Jira Data Center into AWS) will continue to provision an IPv4 VPC.  

#### Apache Tomcat upgrade

Jira 7.11 will use Apache Tomcat 8.5.29.

#### Rebranding Jira

Following the new [Atlassian design direction](https://atlassian.design/server/), we're rebranding JIRA to Jira. Please make sure you update the product name in your plugins, so our users get a consistent look and feel. [See all products and logos](https://www.atlassian.com/software)

#### Projects: new columns

We're adding two new columns to the *Projects* page to make it easier to determine the importance of a project. *Issue count* and *Last update date* will show you the number of issues a project has and the date of a last update on any issue. Jira admins can view this info by going to **Administration > Projects**.


<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

