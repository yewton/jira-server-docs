---
aliases:
- /server/jira/platform/gadget-plugin-module-4227209.html
- /server/jira/platform/gadget-plugin-module-4227209.md
category: reference
confluence_id: 4227209
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227209
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227209
date: '2017-12-08'
legacy_title: Gadget Plugin Module
platform: server
product: jira
subcategory: modules
title: Gadget
---
# Gadget

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The Gadget plugin module is available only for OSGi-based plugins in JIRA 4.0 and later.</p></td>
</tr>
</tbody>
</table>

Atlassian gadgets provide the portlets that people can add to their JIRA dashboard in JIRA 4 onwards. To build a gadget, you need a gadget plugin module. The gadget module type can also be included in other Atlassian products.

See the [Atlassian Gadgets](https://developer.atlassian.com/display/GADGETS/Packaging+your+Gadget+as+an+Atlassian+Plugin) documentation for details.
