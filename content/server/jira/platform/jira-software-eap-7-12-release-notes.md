---
title: "Jira Software 7.12 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-08-08"
---
# Jira Software 7.12 EAP release notes

*08 August 2018*

Atlassian is proud to present *Jira Software 7.12 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *Jira Software 7.12* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for Jira 7.12, read the developer change management guide: [Preparing for Jira 7.12](/server/jira/platform/preparing-for-jira-7-12). If you are new to Jira, you should also read our [Java API Policy for Jira](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both *from* and *to* EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of Jira, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.9.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}


### EAP-01

## Changes

### Jira gets faster

You can get up to 25% faster if you disable the *Days in column* time indicator that normally displays on issue cards on your Kanban and Scrum boards. For Kanban boards the indicator is on by default and for Scrum boards it's off. However, you can now decide if you want to use it and tweak Jira performance accordingly. 

![Days in columns](/server/jira/platform/images/days-in-column.png)

### Custom fields optimizer (Jira Data Center)

Custom fields can have a huge impact on the performance of your Jira instance, especially if they're not configured in the most optimal way. Now, you can use the Custom fields optimizer that will scan and highlight custom fields whose configuration can be improved. 

You can find it in **Administration > System > Custom fields optimizer**, and it looks like in the following example.

![Custom fields optimizer](/server/jira/platform/images/cfomain_desc.png)

1. **Recommendation**: Shows why custom fields are highlighted, and tells you how many fields can be optimized.
2. **Manage custom fields**: Takes you to the page with highlighted custom fields, where you can optimize them.
3. **Scan**: Action to start the scan, or to run it again.

**Custom fields with global contexts**

Currently, the optimizer finds custom fields whose context is set to global (available to all projects), yet they're being used by fewer than 10 projects. To optimize these custom fields, it's better to change their context to project-specific, applying them only to relevant projects and freeing up resources. You can do it by clicking **Change context** next to each custom field. We'll take care of the rest.

![Custom fields with global contexts](/server/jira/platform/images/cfo_global_fields.png)

1. **Name** of a custom field.
2. **Used by**: View projects that are currently using this custom field.
3. **Actions**: Select **Change context** to change the configuration to an optimal one. Later, you can select **View change** to see the custom field's configuration page, and make any changes you need.

### Share edit rights for filters and dashboards

We’ve added the following improvements to filters and dashboards:

- **Share permission to view or to edit:** Choose who can only view the filter/dashboard, and who can also edit it.
- **Share with single users:** Apart from projects, groups, and roles, you can now share your filters/dashboards with single users.

Here’s how it looks in Jira:

![Filters UI](/server/jira/platform/images/share_filters.png)

*Filters view*

![Dashboards UI](/server/jira/platform/images/share_dashboards.png)

*Dashboards view*

#### Audit log

To keep you safe, we’ll notify you about every change to filters and dashboards through the following events in the audit log.

Filters:

- filter created
- filter updated (updates to name, description, JQL, and permissions)
- filter deleted

Dashboards:

- dashboard created
- dashboard updated (updates to name, description, and permissions)
- dashboard deleted

