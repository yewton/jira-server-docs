---
aliases:
- /server/jira/platform/4227154.html
- /server/jira/platform/4227154.md
category: devguide
confluence_id: 4227154
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227154
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227154
date: '2017-12-08'
legacy_title: How do I get a handle to the model (issues) then iterate?
platform: server
product: jira
subcategory: other
title: How do I get a handle to the model (issues) then iterate?
---
# How do I get a handle to the model (issues) then iterate?

All of the objects defined externally to these methods are available to a JIRA plugin via [dependency injection](/server/jira/platform/picocontainer-and-jira.html).

``` javascript
   /**
     * Retrieve a list of all the issues in the current project. Note that several of these objects are passed via
     * dependency injection as constructor parameters.
     *
     * @return list of Issue objects
     */
    public List<Issue> getAllIssuesInCurrentProject()
    {
        final  JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where().project(currentProjectId);
        Query query = builder.buildQuery();
        try
        {
            final SearchResults results = searchService.search(authenticationContext.getUser(),
                    query, PagerFilter.getUnlimitedFilter());
            return results.getIssues();
        }
        catch (SearchException e)
        {
            log.error("Error running search", e);
        }

        return Collections.emptyList();
    }
```
