---
title: JIRA 7.5 Eap Release Notes 51913984
aliases:
    - /server/jira/platform/jira-7.5-eap-release-notes-51913984.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51913984
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51913984
confluence_id: 51913984
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.5 EAP release notes

Find the release notes for the JIRA 7.5 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.5 EAP release notes](/server/jira/platform/jira-core-7-5-eap-release-notes)
-   [JIRA Software 7.5 EAP release notes](/server/jira/platform/jira-software-7-5-eap-release-notes)
-   [JIRA Service Desk 3.8 EAP release notes](/server/jira/platform/jira-service-desk-3-8-eap-release-notes)
