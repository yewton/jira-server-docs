---
aliases:
- /server/jira/platform/jira-rest-api-in-jira-5.0-6848598.html
- /server/jira/platform/jira-rest-api-in-jira-5.0-6848598.md
category: devguide
confluence_id: 6848598
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848598
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848598
date: '2017-12-08'
legacy_title: JIRA REST API in JIRA 5.0
platform: server
product: jira
subcategory: updates
title: JIRA REST API in JIRA 5.0
---
# JIRA REST API in JIRA 5.0

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

The REST API in JIRA 5.0 charges ahead with the ability to create and edit issues via the REST API. Here are some guidelines on how to take advantage of these new capabilities.

## A Simple Example of Creating an Issue

Follow the simple example of how to create a JIRA issue, in our [introduction to JIRA's REST APIs](/server/jira/platform/rest-apis).

## How to Edit and Update Issues

A guide to [how to update an issue via REST](/server/jira/platform/updating-an-issue-via-the-jira-rest-apis-6848604.html).

## More Tutorials

See our [JIRA REST API tutorials](/server/jira/platform/jira-rest-api-tutorials-6291593.html).

## Full REST API Documentation

See the <a href="http://docs.atlassian.com/jira/REST/5.0-m5/" class="external-link">details of each REST request and response</a>.

## Feedback?

Tell us what you think by <a href="https://jira.atlassian.com/browse/JRA-22139" class="external-link">commenting on this issue</a>.
