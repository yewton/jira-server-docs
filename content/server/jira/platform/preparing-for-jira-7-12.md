---
title: "Preparing for Jira 7.12"
product: jira
category: devguide
subcategory: updates
date: "2018-08-08"
---
# Preparing for Jira 7.12

This page covers changes in *Jira 7.12* that can affect add-on compatibility and functionality. This includes changes to the Jira platform and the Jira applications (Jira Core, Jira Software, Jira Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the Jira Server 7.12 products. We also make release milestones available prior to the release:

- For *Jira Server*, the Jira development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [Jira EAP Releases](/server/jira/platform/).

- For *Jira Cloud*, the Jira team releases updates to the Jira Cloud products on a weekly basis. Add-ons that integrate with the Jira Cloud products via Atlassian Connect should use [Jira REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the Jira news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **&lt;user-data&gt; removed from JSON**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>
| **Days in column optional for boards**<br>Jira Software | Affects: Plugin developers, Jira admins<br>
| **Custom fields optimizer**<br>Jira Data Center | Affects: Plugin developers, Jira admins<br>
| **Atlassian User Interface (AUI) upgrade**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>
| **Share edit rights for filters and dashboards**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>
| **Deprecated PostgreSQL 9.3**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>
| **Jira Service Desk is deprecating com.atlassian.fugue**<br>Jira Service Desk | Affects: Plugin developers<br>


## Changes

#### Jira Service Desk is deprecating com.atlassian.fugue in 3.15

 Jira Service Desk 3.15 is deprecating the use of com.atlassian.fugue. In Jira Service Desk 4.0, we'll be permanently removing com.atlassian.fugue and updating our APIs to use Core Java Data types and Exceptions instead. Read the [full deprecation notice](https://developer.atlassian.com/server/jira/platform/deprecation-notice-jira-service-desk-3-15-fugue/) for more information and post any questions in the [Atlassian Developer Community](https://community.developer.atlassian.com/t/jira-service-desk-is-deprecating-com-atlassian-fugue-in-3-15/22326).

#### PostgreSQL 9.3 deprecated in Jira 7.12 

Jira 7.12 has deprecated the use of PostgreSQL 9.3. In Jira 8.0, we'll be permanently removing the support for PostgreSQL 9.3. For more info, see [End of support announcements](https://confluence.atlassian.com/display/ADMINJIRASERVER/End+of+support+announcements).

#### &lt;user-data&gt; removed from JSON

For security reasons, we have removed the &lt;user-data&gt; attribute from the JSON that displays when you view page source for an issue. 

#### Days in column optional for boards

You can now decide if you want to have the *Days in column* time indicator disabled for all your Scrum and Kanban boards. Disabling *Days in column* can significantly improve Jira performance. For more info, see [Release notes](https://developer.atlassian.com/server/jira/platform/jira-software-eap-7-12-release-notes/).

#### Custom fields optimizer (Jira Data Center)

You can now find custom fields whose configuration is not optimal, and improve them with a click of a button. For more info, see [Release notes](https://developer.atlassian.com/server/jira/platform/jira-software-eap-7-12-release-notes/).

#### Atlassian User Interface (AUI) upgrade

Jira 7.12 will have a new version of Atlassian User Interface (AUI), upgrading from 7.8.0 to 7.9.6. The new version includes changes to multiple drop-down menus. For more info, see [AUI upgrade guide](https://docs.atlassian.com/aui/7.9.6/docs/upgrade-guide.html).

#### Share edit rights for filters and dashboards

We’ve added new permissions to filters and dashboards, allowing you to choose who can edit or view them. What’s more, you can now share filters and dashboards not only with projects, groups, and roles, like it was before, but also with single users. You can see some examples and screenshots in the [Release notes](https://developer.atlassian.com/server/jira/platform/jira-software-eap-7-12-release-notes/). Below you can also find some changes to REST APIs.

**REST API changes**

When improving the filters, we’ve modified some of the existing APIs, mostly adding new fields. We haven't made any changes to the APIs related to dashboards. Expand the sections below for more info.

<details><summary>**Retrieving/changing filter's details**</summary>

| Method                                                        | Changes             |
|------------------------------------------------------------------------------|------------------------------------|
| [GET filter/{id}][get-filter] <br> [PUT filter {id}][put-filter] <br> [POST filter {id}][post-filter] | Added a new field to the FilterBean class: *editable* (boolean). <br>It tells you whether the user retrieving information <br>about the filter has permission to edit it.

</details>

<details><summary>**Adding permissions to filters**</summary>

| Method                                                        | Changes             |
|------------------------------------------------------------------------------|------------------------------------|
| [POST filter/{id}/permission][post-filter-permission] | - new option for the *type* parameter: *user* (allowing you to grant permissions to a single user)<br> - new parameter: *userKey* (describing the user for whom you're changing the permissions) <br> - new parameters: *view* (boolean) and *edit* (boolean) (permissions you're granting)

**Note:** To give edit permission, you need to specify both *view* and *edit* as *true*. If you specify only one parameter, or none at all, *view* permission will be granted by default. 

</details>

<details><summary>**Retrieving filter's permissions**</summary>

| Method                                                        | Changes             |
|------------------------------------------------------------------------------|------------------------------------|
| [GET filter/{id}/permission][get-filter-permission] | The following 3 new fields are returned: <br> - *user* <br> - *view* (boolean)<br> - *edit* (boolean)

</details>


[get-filter]: https://docs.atlassian.com/software/jira/docs/api/REST/7.11.0/#api/2/filter-getFilter
[put-filter]: https://docs.atlassian.com/software/jira/docs/api/REST/7.11.0/#api/2/filter-editFilter
[post-filter]: https://docs.atlassian.com/software/jira/docs/api/REST/7.11.0/#api/2/filter-createFilter

[post-filter-permission]: https://docs.atlassian.com/software/jira/docs/api/REST/7.11.0/#api/2/filter-addSharePermission
[get-filter-permission]: https://docs.atlassian.com/software/jira/docs/api/REST/7.11.0/#api/2/filter-getSharePermissions
 
 <center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>