---
title: Earlier JIRA Eap Releases 51921380
aliases:
    - /server/jira/platform/earlier-jira-eap-releases-51921380.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921380
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921380
confluence_id: 51921380
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Earlier JIRA EAP releases

This page gathers information about earlier JIRA EAP releases. Choose a version you're interested in for more details.

## Available versions

-   [JIRA 7.1 EAP release notes](/server/jira/platform/jira-7.1-eap-release-notes-38442155.html)
-   [JIRA 7.0 EAP release notes](/server/jira/platform/jira-7.0-eap-release-notes-38441056.html)
