---
title: Guide Building a Dashboard Item for a JIRA Connect Add On 33746254
aliases:
    - /server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33746254
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33746254
confluence_id: 33746254
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Guide - Building a dashboard item for a JIRA Connect add-on

This guide will help you build a dashboard item for **JIRA Connect add-on**. You will need to be familiar with building JIRA add-ons on [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html) to use this guide. If you are building a dashboard item for a JIRA P2 add-on, see this guide instead: [Building a dashboard item for JIRA P2 add-on](/server/jira/platform/building-a-dashboard-item). 

A dashboard item provides discrete dynamic content that a user can use to customize their dashboard, via the gadget directory. Dashboard items use modern rendering technologies, like AMD, Soy, LESS, etc. Dashboard items can also share web-resources, which improves page load performance by reducing the number of requests and the overall size of data to be transferred on a dashboard load. 

Note, it is not possible to build OpenSocial gadgets on Atlassian Connect. You should build dashboard items instead.

## Before you begin

-   If you haven't built a Connect add-on before, try our [getting started guide for Connect](https://developer.atlassian.com/static/connect/docs/latest/guides/getting-started.html).
-   We also recommend checking out our sample Connect dashboard item, available here: <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/" class="external-link">atlassian-connect-jira-dashboard-item-example</a>. 
-   Unlike gadgets, dashboard items do not support remoting, e.g. embedding an OpenSocial gadget from JIRA on a Confluence page. We don't have plans to support remoting for dashboard items at this point in time. However, if you did want to surface JIRA functionality in Confluence, you could build an [Atlassian Connect Confluence macro](https://developer.atlassian.com/static/connect/docs/latest/modules/confluence/dynamic-content-macro.html) instead.

## Defining the dashboard item

The [Dashboard item](https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html) page in the Atlassian Connect documentation shows how you should define a dashboard item in your add-on descriptor. 

You can see an example of this in the source for the <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/" class="external-link">sample Connect dashboard item</a> that was referenced earlier. If you haven't built a Connect add-on before, here are a few things to take note of:

-   The `baseUrl` in the <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/src/1a60df7a18d083cfb7e9813dc6fe65b0402f23d3/atlassian-connect.json?at=master" class="external-link">atlassian-connect.json</a> describes where you host your add-on.
-   The routes for your add-on provide a definition of the end points and how they respond to client requests (as defined by the <a href="http://expressjs.com/guide/routing.html" class="external-link">Express documentation</a>). The route paths are built on the `baseUrl` defined in your atlassian-connect.json. In the <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/src/1a60df7a18d083cfb7e9813dc6fe65b0402f23d3/routes/index.js?at=master" class="external-link">index.js</a>, you'll notice that the 'issues-in-project' route is used to actually render the dashboard item.
-   To render the dashboard item, the route takes a template as a parameter. If you view the index.js again, you can see that the 'issues-in-project' route uses the 'issues-in-project' template. This corresponds to the <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/src/1a60df7a18d083cfb7e9813dc6fe65b0402f23d3/views/issues-in-project.hbs?at=master" class="external-link">issues-in-project.hbs template</a>.

## General development guidelines

We recommend that you read all of the guidelines below before developing a dashboard item. These are high level guidelines, as this guide is intended for experienced add-on developers. If you need help with a specific development issue, try browsing <a href="https://answers.atlassian.com/questions/topics/753681/jira-development" class="external-link">the JIRA development forums</a>.

**In this section:**

[Check out our sample dashboard item](#check-out-our-sample-dashboard-item) \| [Performance](#performance) \| [Testing your dashboard item](#testing-your-dashboard-item) \| [Sandboxing](#sandboxing)

### Check out our sample dashboard item

If you haven't seen our sample dashboard item yet, check it out here: <a href="https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/" class="uri external-link">https://bitbucket.org/atlassianlabs/atlassian-connect-jira-dashboard-item-example/</a>

You can browse the source, clone it, modify it, and even run it in a JIRA development instance yourself.

### Performance

-   **Only use server-side rendering for mostly static information** -- Anything that might be expensive (e.g. running a JQL query, generating data for a chart, etc) should not be done server-side. Instead, the data should be requested via an AJAX request and then rendered client-side.

### Testing your dashboard item

-   **Unit testing** -- We recommend testing all new code with a JavaScript unit testing framework, like QUnit.

### Sandboxing

Dashboard items are sandboxed in Atlassian Connect, as they run in iframes, so you don't need to worry about setting the correct context (e.g. namespacing CSS) as you would with a P2 dashboard item.

## Design guidelines

There are no design guidelines specific to dashboard items. We recommend that you consult the <a href="https://design.atlassian.com" class="external-link">Atlassian Design Guidelines</a>, as you should when developing for any Atlassian product. Here are a few topics that may be relevant to your dashboard item:

-   <a href="https://design.atlassian.com/latest/product/components/tables/" class="external-link">Tables</a>
-   <a href="https://design.atlassian.com/latest/product/foundations/charts/" class="external-link">Charts</a>
-   <a href="https://design.atlassian.com/latest/product/components/forms/" class="external-link">Forms</a>
