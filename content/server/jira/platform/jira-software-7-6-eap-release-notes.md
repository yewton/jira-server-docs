---
aliases:
- /server/jira/platform/jira-software-7.6-eap-release-notes-51921393.html
- /server/jira/platform/jira-software-7.6-eap-release-notes-51921393.md
category: devguide
confluence_id: 51921393
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921393
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921393
date: '2017-12-08'
legacy_title: JIRA Software 7.6 EAP release notes
platform: server
product: jira
subcategory: updates
title: JIRA Software 7.6 EAP release notes
---
# JIRA Software 7.6 EAP release notes

**3 October 2017**

Atlassian is proud to present **JIRA Software 7.6 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Software 7.6** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.6, read the developer change management guide: [Preparing for JIRA 7.6](/server/jira/platform/preparing-for-jira-7-6). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

## Priorities per project

We're currently working on a way to assign separate, unique priorities to each of your projects, as opposed to having a common set of priorities, like it is now in JIRA.

JIRA 7.6 will introduce a concept of priority schemes that let JIRA admins add new priorities and choose projects they're assigned to. Because of that, the global list of priorities will be deprecated, and replaced with a default priority scheme that contains all priorities and is assigned to all projects. We don't expect any problems for users in JIRA as the default priority scheme is an equivalent of the current global list, and works in the same way. We're still in the early stages of developing this feature, and this EAP milestone doesn't bring any changes to the UI.

What it brings is necessary changes to API. To manage priority schemes, we added new APIs, deleted some, and improved some more, so they can handle the new schemes. If you're an add-on developer and your add-on is using the priorities' API in any way, take a look at the complete list of changes you should be aware of: [Preparing for JIRA 7.6](/server/jira/platform/preparing-for-jira-7-6).
