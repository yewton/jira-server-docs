---
aliases:
- /server/jira/platform/how-do-i-develop-for-application-links-4227173.html
- /server/jira/platform/how-do-i-develop-for-application-links-4227173.md
category: devguide
confluence_id: 4227173
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227173
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227173
date: '2017-12-08'
legacy_title: How do I develop for Application Links
platform: server
product: jira
subcategory: other
title: How do I develop for Application Links
---
# How do I develop for Application Links

Resources for developers who want to develop for the Application Links plugin should refer to the [Application Links Development Hub](https://developer.atlassian.com/display/DOCS/Application+Links).
