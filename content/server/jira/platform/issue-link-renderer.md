---
aliases:
- /server/jira/platform/issue-link-renderer-plugin-module-6849912.html
- /server/jira/platform/issue-link-renderer-plugin-module-6849912.md
category: reference
confluence_id: 6849912
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6849912
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6849912
date: '2017-12-08'
legacy_title: Issue Link Renderer Plugin Module
platform: server
product: jira
subcategory: modules
title: Issue link renderer
---
# Issue link renderer

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

The issue link renderer plugin module allows you to add new *custom* renderers for issue links to JIRA.

When a Remote Issue Link is rendered, if there is no custom renderer for the application type of the link, a default renderer is used.

For background information, see the overview of [Guide - JIRA Remote Issue Links](/server/jira/platform/creating-remote-issue-links). If you are interested in creating Remote Issue Links, take a look at the [REST API Guide](/server/jira/platform/jira-rest-api-for-remote-issue-links).

## Example of an Issue Link Renderer Definition

Here is an example of `issue-link-renderer` defined in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/):

``` xml
<issue-link-renderer key="customIssueLinkRenderer" application-type="com.mycompany" class="com.atlassian.jira.plugin.viewissue.CustomIssueLinkRenderer">
    <resource name="view" type="velocity" location="viewissue/customissuelink.vm"/>
</issue-link-renderer>
```

## Issue Link Renderer Configuration

The root element for the issue link renderer plugin module is `issue-link-renderer`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The Java class of the issue link renderer plugin module. Classes must implement com.atlassian.jira.plugin.issuelink.IssueLinkRenderer. There is a com.atlassian.jira.plugin.issuelink.AbstractIssueLinkRenderer defined which issue link renderers can extend to minimise the number of methods that are required to be implemented.</p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The identifier of the plugin module. This key must be unique in the plugin where it is defined.<br />
Sometimes, in other contexts, you may need to uniquely identify a module. Do this with the <strong>complete module key</strong>. A module with key fred in a plugin with key com.example.modules will have a complete key of com.example.modules:fred.I.e. the identifier of the custom field type module.</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p></td>
</tr>
<tr class="odd">
<td><p>application-type</p></td>
<td><p>The application type that the issue link renderer will handle. You can only have one custom renderer map to a specific application-type.</p></td>
</tr>
</tbody>
</table>

**\*class, key and application-type attributes are required.**

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>resource</p></td>
<td><p><strong>Attribute:</strong> type.</p>
<p>Type of the resource. At the moment only &quot;velocity&quot; is supported.</p></td>
</tr>
<tr class="even">
<td><p>resource</p></td>
<td><p><strong>Attribute:</strong> name.</p>
<p>Name of the resource. Use either &quot;initial-view&quot; or &quot;final-view&quot; to specify a resource for the initial viewing and the async loading respectively.</p></td>
</tr>
</tbody>
</table>

## Issue Link Renderer

Custom issue link renderers may extend `com.atlassian.jira.plugin.issuelink.AbstractIssueLinkRenderer` to minimise the number of methods required to be implemented.

``` java
/**
 * Defines an issue link renderer to customise how issue links appear.
 *
 * @since v5.0
 */
public interface IssueLinkRenderer
{
    /**
     * Returns the context used by the template to render the initial HTML. Implementers of this method
     * should not make remote calls, use {@link #getFinalContext(RemoteIssueLink, Map)} for that purpose.
     *
     * The resulting HTML will be injected as follows:
     * {@code
     * <dl class="links-list">
     *    <dt>Relationship Text</dt>
     *    <!-- ... Other Issue Links ... -->
     *    <dd id="uniqueHtmlElementId" class="remote-link">
     *        <div class="link-content">
     *            <!-- ISSUE LINK RENDERER CONTENT HERE -->
     *        </div>
     *
     *        <div class="delete-link" id="delete_uniqueHtmlElementId">
     *            <a class="icon icon-delete" title="Delete Link" id="delete-link_uniqueHtmlElementId" href="delete_remote_issue_link_url"><span>Delete Link</span></a>
     *        </div>
     *     </dd>
     *    <!-- ... Other Issue Links ... -->
     * </dl>
     * }
     *
     * @param remoteIssueLink remote issue link
     * @param context the contextual information that can be used during
     *  rendering.
     * @return context used to render the initial HTML.
     */
    Map<String, Object> getInitialContext(RemoteIssueLink remoteIssueLink, Map<String, Object> context);

    /**
     * Returns the context used to render the final HTML. This method will only be called if
     * {@link #requiresAsyncLoading(RemoteIssueLink)} returns <tt>true</tt>.
     *
     * The resulting HTML will be injected as follows:
     * {@code
     * <dl class="links-list">
     *    <dt>Relationship Text</dt>
     *    <!-- ... Other Issue Links ... -->
     *    <dd id="uniqueHtmlElementId" class="remote-link">
     *        <div class="link-content">
     *            <!-- ISSUE LINK RENDERER CONTENT HERE -->
     *        </div>
     *
     *        <div class="delete-link" id="delete_uniqueHtmlElementId">
     *            <a class="icon icon-delete" title="Delete Link" id="delete-link_uniqueHtmlElementId" href="delete_remote_issue_link_url"><span>Delete Link</span></a>
     *        </div>
     *     </dd>
     *    <!-- ... Other Issue Links ... -->
     * </dl>
     * }
     *
     * @param remoteIssueLink remote issue link
     * @param context the contextual information that can be used during rendering.
     * @return velocity context used to render the final HTML
     */
    Map<String, Object> getFinalContext(RemoteIssueLink remoteIssueLink, Map<String, Object> context);

    /**
     * Returns <tt>true</tt> if an AJAX call is required to retrieve the final HTML. If <tt>true</tt> is returned,
     * then {@link #getFinalContext(com.atlassian.jira.issue.link.RemoteIssueLink, java.util.Map)} will be
     * called to retrieve the final HTML for the issue link.
     *
     * @param remoteIssueLink remote issue link
     * @return <tt>true</tt> if an AJAX call is required to retrieve the final HTML
     */
    boolean requiresAsyncLoading(RemoteIssueLink remoteIssueLink);

    /**
     * Returns <tt>true</tt> if the remote issue link should be displayed.
     *
     * @param remoteIssueLink remote issue link
     * @return <tt>true</tt> if the remote issue link should be displayed
     */
    boolean shouldDisplay(RemoteIssueLink remoteIssueLink);
}
```
