---
aliases:
- /server/jira/platform/adding-javascript-to-all-pages-for-google-analytics-4227131.html
- /server/jira/platform/adding-javascript-to-all-pages-for-google-analytics-4227131.md
category: devguide
confluence_id: 4227131
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227131
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227131
date: '2017-12-08'
guides: tutorials
legacy_title: Adding JavaScript to all pages for Google Analytics
platform: server
product: jira
subcategory: learning
title: Adding JavaScript to all pages for Google Analytics
---
# Adding JavaScript to all pages for Google Analytics

If you wish to add javascript to every page within JIRA (for example for use with <a href="http://www.google.com/analytics/" class="external-link">Google Analytics</a>), add the following javascript to the appropriate file:

-   `stylesheettag.jsp` (JIRA 3.13)
-   `<atlassian-jira>/includes/decorators/header.jsp` (JIRA 4.x)
-   `<atlassian-jira>/atlassian-jira/includes/decorators/aui-layout/header.jsp` (JIRA 5.x)

    ``` xml
    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
    _uacct = "UA-xxxxx";
    urchinTracker();
    </script>
    ```

Note that you can also add a noindex tag, like:

``` xml
<meta name="robots" content="noindex">
```

{{% note %}}

This page is outdated for JIRA7, please see <a href="https://jira.atlassian.com/browse/JRA-59587" class="uri external-link">jira.atlassian.com/browse/JRA-59587</a> for details

{{% /note %}}
