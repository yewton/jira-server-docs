---
title: JIRA 7.1 Eap Release Notes 38442155
aliases:
    - /server/jira/platform/jira-7.1-eap-release-notes-38442155.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38442155
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38442155
confluence_id: 38442155
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.1 EAP release notes

Find the release notes for the JIRA 7.1 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.1 EAP 01 (m01) Release Notes](/server/jira/platform/jira-core-7-1-eap-01-m01-release-notes)
-   [JIRA Software 7.1 EAP 01 (m02) Release Notes](/server/jira/platform/jira-software-7-1-eap-01-m02-release-notes)
