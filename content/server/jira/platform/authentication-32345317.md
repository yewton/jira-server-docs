---
title: Authentication 32345317
aliases:
    - /server/jira/platform/authentication-32345317.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32345317
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32345317
confluence_id: 32345317
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Authentication

This section provides you with information on how to implement authentication for your add-on (Plugins2 or Connect). This includes information on how to specify required JIRA permissions for your add-on.

-   [Available Permissions]
-   [Form Token Handling]

  [Available Permissions]: /server/jira/platform/available-permissions-4227135.html
  [Form Token Handling]: /server/jira/platform/form-token-handling
