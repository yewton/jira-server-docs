---
category: reference
date: '2018-06-13'
platform: server
product: jira
subcategory: modules
title: "Dashboard item"
---
# Dashboard item

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 6.4 and later.</p></td>
</tr>
</tbody>
</table>

## Purpose of this module type

Dashboard items allow apps to display a summary information data on the dashboard. Each `dashboard-item`
can be configured to display information relevant to a particular user.
Dashboard items will eventually replace [gadgets](/server/jira/platform/gadget/) in Jira.

## Configuration

You can define your dashboard item in the `atlassian-plugin.xml` file. For more details, see the
[Configuring the app descriptor](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/) page.

The root element for the Dashboard item plugin module is `dashboard-item`. It allows the following
attributes and child elements for configuration.

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td>The localization key for the human-readable name of the plugin module.</td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td>The unique identifier of the plugin module. You refer to this key to use the resource from other
contexts in your app, such as from the app Java code or JavaScript resources. <br/>
<code>&lt;dashboard-item key=&quot;new-dashboard-item&quot;&gt;
...
&lt;/dashboard-item&gt;</code>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p>Used only in the app's administrative user interface.</p></td>
</tr>
<tr class="even">
<td><p>configurable</p></td>
<td><p>Allows users to configure dashboard item.</p></td>
</tr>
</tbody>
</table>

**\*key attribute is required.**

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>definition</td>
<td>This information is used to populate the dashboard item's entry in the gadget directory.
The gadget directory is where users select gadgets to add to the dashboard.
This element is required if you don't specify <code>replace-gadget-spec-uri</code>.</td>
</tr>
<tr class="even">
<td>replace-gadget-spec-uri</td>
<td><p>The gadget that is being replaced by this dashboard item still needs to be defined in your app.<br />
Specify the definition of the gadget in this element.
This element is required if you don't specify <code>definition</code>.</p></td>
</tr>
<tr class="odd">
<td><p>amd-module</p></td>
<td><p>The <code>amd-module</code> parameter is optional.
You can use this to specify a JavaScript AMD (that is, Asynchronous Module Definition) module for your dashboard item.
When the dashboard renders, it will call this module and provide it with the dashboard item's surrounding as context.</p></td>
</tr>
<tr class="even">
<td><p>condition</p></td>
<td><p>Defines a condition that must be satisfied for the dashboard-item to be displayed.</p>
<p>If you want to "invert" a condition, add an attribute <code>invert=&quot;true&quot;</code> to it.</p>
<p>The web item will then be displayed if the condition returns false (not true).</p></td>
</tr>
<tr class="odd">
<td><p>conditions</p>
<p> </p></td>
<td><p>Defines the logical operator type to evaluate its condition elements. By default <code>AND</code> will be used.</p>
</td>
</tr>
<tr class="even">
<td><p>context-provider</p></td>
<td><p>Passes variables to the Soy template.</p>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>The description of the plugin module. The <code>key</code> attribute can be specified to declare a localization key
for the value instead of text in the element body.</p>
<p>That is, the description of the dashboard item.</p></td>
</tr>
<tr class="even">
<td><p>resource type="view"</p>
<p> </p></td>
<td><p>A resource element is used to provide a dashboard item with content. You can use this to render
a dashboard item entirely server-side, by using an optional <code>&lt;context-provider/&gt;</code>
to provide the data for rendering.</p>
</tr>
</tbody>
</table>

**\*definition or replace-gadget-spec-uri element is required.**

#### Definition elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>title</td>
<td>The name of your dashboard item that will be displayed in the gadget directory. You can use
<code>key</code> to reference i18n property or assign value to the body of element.</td>
</tr>
<tr class="even">
<td>categories</td>
<td>Specify categories for your dashboard item using one or multiple <code>category</code> elements.
Valid categories: Jira, Chart, Wallboard, Other, and Admin.</br>
<em>Default</em>: Other.</td>
</tr>
<tr class="odd">
<td><p>author</p></td>
<td><p>Specify author using <code>name</code> element. <br/>
<code>&lt;author&gt;&lt;name&gt;Atlassian&lt;/name&gt;&lt;/author&gt;</code>
</tr>
<tr class="even">
<td><p>thumbnail</p></td>
<td><p>Point its <code>location</code> attribute to a picture that will be displayed in gadget directory.</p></td>
</tr>
</tbody>
</table>

**\*title and author elements are required.**

## Creating a new "standalone" dashboard item

This is the definition for a "standalone" dashboard item:

``` xml
<dashboard-item key="new-dashboard-item" i18n-name-key="dashboard.item.name" configurable="true">
    <definition>
        <title key="dashboard.item.title"/>
        <categories>
            <category>Jira</category>
        </categories>
        <author>
            <name>Atlassian</name>
        </author>
        <thumbnail location="/download/resources/atlassian-plugin-key:web-resource-module-key/login-thumb.png"/>
    </definition>
    <description key="dashboard.item.description"/>
    <resource name="view" type="soy" location=":web-resource-module-key/Soy.DashboardItem.Templates.TemplateName"/>
    <amd-module>jira-dashboard-items/my-amd-module</amd-module>
    <context-provider class="com.example.plugins.dashboarditem.DashboardItemContextProvider"/>
    <condition class="com.atlassian.jira.plugin.webfragment.conditions.IsAnonymousUserCondition"/>
</dashboard-item>
```

## Replacing a gadget with a dashboard item

This is the definition for a dashboard item that replaces a gadget:

``` xml
<dashboard-item key="login-dashboard-item">
     <replace-gadget-spec-uri>rest/gadgets/1.0/g/com.atlassian.jira.gadgets/gadgets/login.xml</replace-gadget-spec-uri>
     <resource name="view" type="soy" location=":login-dashboard-item-resources/JIRA.DashboardItem.Login.Templates.Login"/>
     <amd-module>jira-dashboard-item/login</amd-module>
     <context-provider class="com.atlassian.jira.dashboarditem.login.LoginContextProvider"/>
 </dashboard-item>
```

## Example AMD module

If you want to specify a JavaScript AMD module for your dashboard item,
you can use the following example as a guideline:

``` javascript
define("jira-dashboard-items/sample-dashboard-item", [
    'underscore'
], function (_) {
    var DashboardItem = function (API) {
        this.API = API;
    };
    /**
     * Called to render the view for a fully configured dashboard item.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item (e.g. filter id, number of results...)
     */
    DashboardItem.prototype.render = function (context, preferences) {
        //TODO: render the view with the preferences provided
        this.API.once("afterRender", _.bind(function () {
            this.API.showLoadingBar();
        }, this));
    };
    /**
     * Called to render the configuration form for this dashboard item if preferences.isConfigured
     * has not been set yet.
     * This method will be called only if dashboard-item is configurable and editable
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item
     */
    DashboardItem.prototype.renderEdit = function (context, preferences) {
        //TODO: render a config form here using provided SOY templates and assign it
        //      to the 'form' variable, i.e.
        //      context.empty().html(My.Soy.Templates.Example({preferences: preferences}));
        form.on("submit", _.bind(function (e) {
            e.preventDefault();
            if (!validateFields()) {  //TODO: validateFields needs to implemented
                this.API.resize();
                return;
            }
            this.API.savePreferences({
                //provide parsed prefs from your config form here
                //to store them for this dashboard item
            });
        }, this));
        form.find("input.button.cancel").on("click", _.bind(function () {
            this.API.closeEdit();
        }, this));
    };
    return DashboardItem;
});
```

#### API interface

An `API` interface is passed to the AMD module in the example above. The table below describes this interface.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>isEditable</td>
<td><p>Checks if a dashboard item is configurable and if the current user has permission to edit this dashboard
item's configuration.<br />
Returns {boolean} true if the current user can edit the gadget.</p></td>
</tr>
<tr class="even">
<td>getRefreshFieldValue</td>
<td><p>Helper to extract the correct value to be persisted for a refresh field rendered with <code>.refreshInterval</code>.<br />
Returns {string} value that should be persisted.</p>
<p><em>Parameters:</em></p>
<ul>
<li>fieldName (string) — refresh field name, defaults to &quot;refresh&quot;.</li>
</ul></td>
</tr>
<tr class="odd">
<td>savePreferences</td>
<td><p>Tries to save the passed preferences on the server for this dashboard item.</p>
<p>If the user doesn't have permission to edit this dashboard item, no action is performed.</p>
<p><em>Parameters:</em></p>
<ul>
<li>preferences (object) — the new preferences object to save for this dashboard item.</li>
</ul></td>
</tr>
<tr class="even">
<td>setTitle</td>
<td><p>Sets the title for the dashboard item.</p>
<p><em>Parameters:</em></p>
<ul>
<li>title (string) — the new title for the dashboard item.</li>
</ul></td>
</tr>
<tr class="odd">
<td>initRefresh</td>
<td>Triggers a refresh of the dashboard item.</td>
</tr>
<tr class="even">
<td>showLoadingBar</td>
<td>Shows the loading bar for the dashboard item.</td>
</tr>
<tr class="odd">
<td>hideLoadingBar</td>
<td>Hides the loading bar for the dashboard item.</td>
</tr>
<tr class="even">
<td>getContext</td>
<td>Returns the context for the dashboard item.</td>
</tr>
<tr class="odd">
<td>closeEdit</td>
<td>Closes the edit dialog of the dashboard item.</td>
</tr>
<tr class="even">
<td>getGadgetId</td>
<td>Returns the ID for the dashboard item.</td>
</tr>
<tr class="odd">
<td>resize</td>
<td><p>When you add some content dynamically to your dashboard item and it causes the height to increase, a scroll appears.</p>
<p>Call this method to resize the content automatically and make the scroll go away.</p></td>
</tr>
</tbody>
</table>

## Related topic

* [Building a dashboard item](/server/jira/platform/building-a-dashboard-item/) — this reference guide provides
common recommendations on how to build dashboard items.
* [Writing a dashboard item app](/server/jira/platform/writing-a-dashboard-item-app)
