---
aliases:
- /server/jira/platform/jira-software-32344108.html
- /server/jira/platform/jira-software-32344108.md
category: devguide
confluence_id: 32344108
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32344108
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32344108
date: '2017-12-08'
legacy_title: JIRA Software
platform: server
product: jira
subcategory: intro
title: Integrating with Jira Software Server
---
# Integrating with Jira Software Server

Welcome to Jira Software Server development! This overview will cover everything you need to know to integrate with Jira Software Server. This includes the P2 framework for building plugins, as well as Jira Software features and services that you can use when integrating with Jira Software.

{{% tip %}}

Hello world

If you already know the theory and want to jump straight into development, read our [Getting started guide](/server/jira/platform/getting-started) to build your first Jira Server plugin.

{{% /tip %}}

## What is Jira Software?

Jira Software Cloud helps software teams plan, track, and release great software. It's a hosted service from Atlassian for agile software development, with features sprint planning, scrum and kanban boards, agile reports, dashboards, and more. You can also integrate Jira Software Cloud with source code management and continuous integration tools to streamline your development processes.

Thinking about building a plugin or integration for Jira Software? Jira Software is already used by thousands of companies, like NASA, Spotify, and Twitter; and there are over a thousand Jira-related plugins in the Atlassian marketplace. The opportunities for extending Jira Software are endless!

If you haven't used Jira Software before, check out the <a href="https://www.atlassian.com/software/jira" class="external-link">product overview</a> for more information.

## Building plugins for Jira Software Server

If you want to build a plugin for Jira Software Server, you should use the Atlassian development platform, which includes the Plugins2 framework. Add-ons are used to extend the functionality of Jira Software, which could be an an integration with another existing service, new features for Jira Software, or even a new product that runs within Jira Software.

The basic development flow for building a Jira Software plugin using the Atlassian SDK is:

1.  Create your plugin project as you would any other Jira plugin, for instance, by using the [atlas-create-jira-plugin](https://developer.atlassian.com/display/DOCS/atlas-create-jira-plugin) SDK command. This command creates the plugin project files and skeleton code.

2.  Add any Jira modules or common modules, e.g. web panel, by using [atlas-create-jira-plugin-module](https://developer.atlassian.com/display/DOCS/atlas-create-jira-plugin-module). Be aware that if you want to implement the LinkProvider module, you will need to add the module code by hand, as described [here](https://developer.atlassian.com/display/JIRADEV/JIRA+Agile+LinkProvider+Plugin+Module).

3.  When ready to build and test your plugin, run the [atlas-package](https://developer.atlassian.com/display/DOCS/atlas-package) command from the project root directory. This command creates a packaged JAR file that you can install into Jira. 
4.  Configure AMPS to run Jira with Jira Software installed. If you haven't done this before, see [Configure AMPS to run Jira Core with additional applications installed](https://developer.atlassian.com/display/DOCS/Configure+AMPS+to+run+JIRA+Core+with+additional+applications+installed).
5.  Start up Jira. For early development and testing, you can use the development instance that the SDK gives you when you run the following command *(set the version to 7.0.0 or later)*: `atlas-run-standalone --product jira --version 7.0.0.`
6.  Log in to Jira as an administrator, using the default username and password combination of admin/admin.
7.  Install the plugin you built in step \#2 with the `atlas-package` command. There are two ways that you can do this:
    1.  Go to the Administration Console and open the "Manage Add-ons" page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.
    2.  Using the SDK command line tools: using you terminal, step into your project home directory and simply run [atlas-install-plugin](https://developer.atlassian.com/display/DOCS/atlas-install-plugin) (after having packaged your plugin JAR using the `atlas-package` SDK command). This command will simply upload your plugin to UPM (and can considerably shorten your development flow).

8.  You should now be able to see your plugin in the Jira Software UI. Every time you change your plugin code or resource, reload your plugin by running the `atlas-package` command again, and then remove and reinstall the plugin JAR file from the Manage Add-ons page.

If you haven't built a plugin before, check out the [Getting started guide](/server/jira/platform/getting-started). This guide will help you learn how to set up a development environment and build a Jira Server app.

### Developing with Plugins2

If you are building a plugin with Plugins2, you'll also need to understand a number of key development processes:

-   **Implementing security:** Implementing security is a crucial part of integrating with Jira. To learn more about authentication and authorization for Jira Server, see the [Security overview](https://developer.atlassian.com/display/JIRASERVER/Security+overview).
-   **Listing your plugin on the Atlassian Marketplace and implementing licensing:** Listing your plugin on the Atlassian Marketplaces and licensing it are necessary, if you want to make your plugin available to the public. See the [Marketplace documentation](https://developer.atlassian.com/display/HOME/Marketplace) for more instructions.
-   **Designing your plugin:** Since Plugins2 plugins can insert content directly into the Atlassian host application, it is critical that plugins are visually compatible with the Atlassian application's design. Our designers and developers have created a number of resources to help you with this:
    -   <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> -- Our design guidelines define core interactions with the Atlassian applications.
    -   <a href="https://docs.atlassian.com/aui/" class="external-link">Atlassian User Interface (AUI)</a> -- AUI is a library of reusable front-end UI components.

## Building blocks for integrating with Jira Software Server

The three building blocks of integrating with Jira Software are the APIs, webhooks, and modules.

### Jira Software Server APIs

The Jira Software Server APIs lets your integration communicate with Jira Software Server. For example, using the REST API, you can write a script that moves issues from one sprint to another. For most other integrations, you should use the REST API. The Java API should only be used if you are building a Plugins2 plugin.

-   <a href="https://docs.atlassian.com/jira-software/REST/server/" class="external-link">Jira Software Server REST API</a> *(latest production version)*
-   <a href="https://docs.atlassian.com/jira-software/" class="external-link">Jira Software Server Java API</a> *(all versions)*

*Note, Jira Software is built on the Jira platform, so you can also use the <a href="https://docs.atlassian.com/jira/REST/server/" class="external-link">Jira Server Platform REST API</a> (latest) and <a href="https://docs.atlassian.com/jira/server/" class="external-link">Jira Server Platform Java API</a> (latest) to interact with Jira Software Server.*

### Webhooks and automation rules

Add-ons and applications can react to conditions/events in Jira Software via automation rules. You can implement an "automation action" that performs actions in a remote system as part of an automation rule. An automation rule can also be configured to fire a webhooks that notifies your plugin or application. For more information, see [Webhooks](/server/jira/platform/webhooks).

### Jira Software modules

A module is simply a UI element, like a tab or a menu. Jira Software UI modules allow plugins to interact with the Jira Software UI. For example, your plugin can use a Jira Software UI module to add a new dropdown menu to a board. For more information, see [About Jira modules](/server/jira/platform/about-jira-modules).

## Jira Software Server and the Jira platform

Jira Software is an application built on the Jira platform. The Jira platform provides a set of base functionality that is shared across all Jira applications, like issues, workflows, search, email, and more. A Jira application is an extension of the Jira platform that provides specific functionality. For example, Jira Software adds boards, reports, development tool integration, and other agile software development features.

This means that when you develop for Jira Software, you are actually integrating with the Jira Software application as well as the Jira platform. The Jira Software application and Jira platform each have their own REST APIs, webhook events, and web fragments.

Read the [Jira Server platform documentation](/server/jira/platform/integrating-with-jira-server) for more information.

## Looking for inspiration? <span id="inspiration"></span>

If you are looking for ideas on building the next Jira Software Cloud integration, here are a few examples of what you can build on top of Jira Software Cloud:

-   <a href="https://marketplace.atlassian.com/plugins/com.sngtec.jira.cloud.kanbanalytics/cloud/overview" class="external-link">Retrospective Tools for Jira</a> - This plugin provides a number of tools for reviewing your project history, and with the Jira Software API they're able to let you filter by board, or even overlay your swimlanes.
-   <a href="https://marketplace.atlassian.com/plugins/aptis.plugins.epicSumUp/cloud/overview" class="external-link">Epic Sum Up</a> - This plugin adds a panel to the view issue page that allows you to review your epic progress or every issue within the epic.
-   <a href="https://marketplace.atlassian.com/plugins/com.tempoplugin.tempo-planner/cloud/overview" class="external-link">Tempo Planner for Jira</a> - This plugin fetches information from boards, epics, and backlogs using the Jira Software API, and lets you plan your work by team member.

## More information

-   [Jira Software Cloud tutorials](https://developer.atlassian.com/cloud/jira/software/tutorials-and-guides/) - Learn more about Jira Software development by trying one of our hands-on tutorials.
-   <a href="https://community.developer.atlassian.com/tags/jira-software" class="external-link">jira-agile-development tag</a> on the Community forum - Join the discussion on Jira Software development.
