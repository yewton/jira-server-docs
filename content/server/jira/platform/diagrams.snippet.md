---
aliases:
- /server/jira/platform/diagrams-4227136.html
- /server/jira/platform/diagrams-4227136.md
category: devguide
confluence_id: 4227136
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227136
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227136
date: '2017-12-08'
legacy_title: Diagrams
platform: server
product: jira
subcategory: other
title: Diagrams
---
# Diagrams

-   [\_\_Web\_Fragments](/server/jira/platform/web-fragments.snippet)
-   [Application Overview](/server/jira/platform/application-overview.snippet)
