---
aliases:
- /server/jira/platform/working-with-inline-edit-in-jira-5.1-11927729.html
- /server/jira/platform/working-with-inline-edit-in-jira-5.1-11927729.md
category: devguide
confluence_id: 11927729
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11927729
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11927729
date: '2017-12-08'
legacy_title: Working with Inline Edit in JIRA 5.1
platform: server
product: jira
subcategory: updates
title: Working with Inline Edit in JIRA 5.1
---
# Working with Inline Edit in JIRA 5.1

With the introduction of inline edit in JIRA 5.1, it is important that all [plugin modules](/server/jira/platform/about-jira-modules) (issue operations, issue tab panels, web panels, etc.) are registered in `atlassian-plugin.xml` to ensure that these modules work correctly with the refreshed information on the 'view issue' page after a successfully submitted inline edit.

If these plugin modules are not registered in `atlassian-plugin.xml` but are simply injected into the page using JavaScript, the elements will disappear from the page. For example, the following pattern examples must be avoided:

``` javascript
// Injecting a web panel on DOM ready
//
// This panel will disappear after a successful inline edit. The correct
// approach is to register it in atlassian-plugin.xml and, if necessary,
// reconfigure it when the NEW_CONTENT_ADDED event is triggered.
AJS.$(function() {
    var html = [
        "<div class='module toggle-wrap'>",
            "<div class='mod-header'>",
                "<h3 class='toggle-title'>Custom Web Panel</h3>",
            "</div>",
            "<div class='mod-content'>",
                "Panel content.",
            "</div>",
        "</div>"
    ].join("");
 
    AJS.$("#details-module").after(html);
});
 
// Connecting event handlers on DOM ready
//
// This event handler may not work after a successful inline edit. If it is
// required, configuration should instead be performed when the
// NEW_CONTENT_ADDED event is triggered.
AJS.$(function() {
    AJS.$("#my-element").on("click", function() {
        // ...
    });
});
```

For more information about working with inline edit, see [Guide - Inline edit for JIRA plugins](/server/jira/platform/extending-inline-edit-for-jira-plugins).


