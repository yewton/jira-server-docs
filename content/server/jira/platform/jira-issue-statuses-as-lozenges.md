---
aliases:
- /server/jira/platform/jira-issue-statuses-as-lozenges-23299417.html
- /server/jira/platform/jira-issue-statuses-as-lozenges-23299417.md
category: devguide
confluence_id: 23299417
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23299417
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23299417
date: '2017-12-08'
legacy_title: JIRA issue statuses as lozenges
platform: server
product: jira
subcategory: updates
title: JIRA issue statuses as lozenges
---
# JIRA issue statuses as lozenges

### Overview

In JIRA 6.1, new infrastructure for issue statuses was introduced.

In JIRA 6.2, the status of an issue will no longer be represented by an icon. Instead, they will be rendered as lozenges.

### Rendering an issue status

With the infrastructure introduced in JIRA 6.1, there is now one unified approach to rendering an issue's status. The new rendering approach is backward-compatible with all previous versions of JIRA.

The assets required to render an issue's status live in a separate plugin: the <a href="https://bitbucket.org/atlassian/issue-status-plugin" class="external-link">Issue Status Plugin</a>. This plugin is written to be cross-product, and is included in JIRA 6.1 and 6.2. There are three essential assets:

-   CSS file
-   Soy file with templates, and
-   JS responsible for rendering tooltips for the status lozenges.

This plugin does not contain any Java code, but depends on AUI version 5.0 or later. Any JIRA plugins wishing to use these assets should check the version of JIRA they are using.

To use these assets, add the following dependency to your web resources:

-   `com.atlassian.plugins.issue-status-plugin:issue-status-resources`

### Visibility of status lozenges

The rendering of issue statuses as lozenges is controlled by a dark feature. The dark feature key is `jira.issue.status.lozenge`.

When using the Soy template provided by the Issue Status Plugin, an issue's status will be rendered as:

-   An AUI Lozenge when the dark feature is enabled, and
-   A combination of the (legacy) icon plus text when disabled, or when the JIRA instance does not provide the necessary data to render a lozenge.

### Issue status API

The major addition to the API is the `StatusCategory` object ([Java API](#java-api)). This object stores additional metadata about a `Status` which, when defined and present, will enable statuses to be rendered as lozenges.

#### Example usages

You can find examples of how to use all the assets in our <a href="https://bitbucket.org/atlassian/issue-status-demo-plugin" class="external-link">Issue Status Demo Plugin</a>.``

#### Issue status template API

We recommend using the following Soy template to render all JIRA issue statuses:

**status.soy**

``` java
{namespace JIRA.Template.Util.Issue.Status}

/**
 * Renders issue status depending on value of statusCategory property in $issueStatus.
 * When statusCategory property is defined and not empty status is rendered as a lozenge. Otherwise it will be displayed
 * as an icon and text.
 *
 * @param issueStatus {SimpleStatus} the JIRA issue status
 * @param? isSubtle {boolean} whether to render subtly or not. Default is false.
 * @param? isCompact {boolean} whether to render compact or not. Default is false.
 */
{template .issueStatusResolver}
    ...
{/template}
```

The `issueStatusResolver` template will switch between rendering a status as a lozenge or icon+text as appropriate.

The soy templates are accessible on both the client and server side.

`SimpleStatus` is a new object within the JIRA API that exposes just the information from a JIRA Status required to render an issue status. You do not have to use this object, however - you can provide a map of values in the same structure as the JSON response for a `Status` ([REST API](#rest-api)).

#### Java API

**StatusCategory.java**

``` java
/**
 * Represents single group of {@link com.atlassian.jira.issue.status.Status} which has similar positions in terms of
 * representing in workflows.
 *
 * @since v6.1
 */
@PublicApi
public interface StatusCategory extends Serializable, Comparable<StatusCategory>
{
    Long getId();

    /**
     * Unique {@link String} identifier of given category. Should not contain any spaces and non-standard ASCII
     * characters.
     */
    String getKey();

    /**
     * Returns one of the 6 defined ADG colors: "medium-gray", "green", "yellow", "brown", "warm-red", "blue-gray"
     * @return
     */
    String getColorName();

    Long getSequence();
}
```

The `StatusCategory` is made available through the Status object:

**Status.java**

``` java
public interface Status extends IssueConstant
{
    StatusCategory getStatusCategory();
    void setStatusCategory(StatusCategory statusCategory);
    SimpleStatus getSimpleStatus();
    SimpleStatus getSimpleStatus(I18nHelper i18nHelper);

    /**
     * @deprecated Use {@link #getStatusCategory()} instead. Since v6.1.
     */
    @Deprecated
    String getCompleteIconUrl();

    /**
     * @deprecated Use {@link #getStatusCategory()} instead. Since v6.1.
     */
    @Deprecated
    String getIconUrl();
}
```

**SimpleStatus.java**

``` java
/**
 * Simplified, immutable version of {@link Status}. Contains all data necessary to display status of an issue.
 */
@PublicApi
public interface SimpleStatus extends Serializable
{
    String getId();
    String getName();
    String getDescription();
    StatusCategory getStatusCategory();

    /**
     * Temporal addition of iconUrl in order to achieve fluent transition from icons to lozenges.
     */
    String getIconUrl();
}
```

#### REST API

The updated version of JIRA REST is already released and bundled with JIRA 6.1. This change adds two new endpoints (`/statuscategory` and `/statuscategory/{idOrKey}`). The JSON example shown below is an example of `/statuscategory` endpoint output.

In JIRA 6.1 there are four categories defined:

``` javascript
[
  {
    "self": "http://localhost:8090/jira/rest/api/2/statuscategory/1",
    "id": 1,
    "key": "undefined",
    "colorName": "medium-gray"
  },
  {
    "self": "http://localhost:8090/jira/rest/api/2/statuscategory/2",
    "id": 2,
    "key": "new",
    "colorName": "blue-gray"
  },
  {
    "self": "http://localhost:8090/jira/rest/api/2/statuscategory/3",
    "id": 3,
    "key": "done",
    "colorName": "green"
  },
  {
    "self": "http://localhost:8090/jira/rest/api/2/statuscategory/4",
    "id": 4,
    "key": "indeterminate",
    "colorName": "yellow"
  }
]
```

Every object (in every endpoint) representing a Status is updated to contain also information about its category:

``` javascript
{
    "self": "http://localhost:8090/jira/rest/api/2/status/1",
    "description": "The issue is open and ready for the assignee to start work on it.",
    "iconUrl": "http://localhost:8090/jira/images/icons/statuses/open.png",
    "name": "Open",
    "id": "1",
    "statusCategory": {
      "self": "http://localhost:8090/jira/rest/api/2/statuscategory/1",
      "id": 1,
      "key": "undefined",
      "colorName": "medium-gray"
    }
  }
```

{{% note %}}

Status Category information is only accessible if the JIRA instance's dark feature is enabled. If the dark feature is off, the entry point for status categories will respond with a 404 code and a standard JSON output with error messages.

{{% /note %}}

#### XML API

{{% warning %}}

We strongly recommend use of the REST API over the XML.

{{% /warning %}}

The updated version of JIRA REST is already released and bundled with JIRA 6.1. This change adds a new `<statusCategory>` node to the <a href="https://confluence.atlassian.com/display/JIRA/Displaying+Search+Results+in+XML" class="external-link">XML issue search results</a>:

``` xml
<item>
  <title>[TIS-1] Expand travel to destinations outside of The Solar System</title>
  <project id="10001" key="TIS">Teams in Space</project>
  <key id="10052">TIS-1</key>
  <summary>Expand travel to destinations outside of The Solar System</summary>
  <type id="6" iconUrl="...">Epic</type>
  <priority id="3" iconUrl="...">Major</priority>
  <status id="1" iconUrl="...">Open</status>
  <statusCategory id="2" key="new" colorName="blue-gray"/>
...
</item>
```

{{% note %}}

Status Category information is only accessible if the JIRA instance's dark feature is enabled. If the dark feature is off, the `<statusCategory>` node will not be present.

{{% /note %}}
