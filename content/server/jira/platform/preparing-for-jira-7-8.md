---
title: "Preparing for JIRA 7.8"
product: jira
category: devguide
subcategory: updates
date: "2018-02-16"
---
# Preparing for JIRA 7.8

This page covers changes in *JIRA 7.8* that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.8 products. We also make release milestones available prior to the release:

- For *JIRA Server*, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](https://developer.atlassian.com/display/JIRADEV/JIRA+EAP+Releases).

- For *JIRA Cloud*, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use [JIRA REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Risk level/Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **Quick search**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: Low<br>Affects: Plugin developers, JIRA admins<br>Improved quick search with instant results.
| **Oracle JDBC driver**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: Medium<br>Affects: Plugin developers, JIRA admins<br>Oracle JDBC driver is no longer bundled with JIRA.
| **Dutch language**<br>JIRA Core, JIRA Software | Risk level: Low<br>Affects: Plugin developers, JIRA admins<br>JIRA 7.8 allows you to choose Dutch as your language.


## JIRA platform changes
**All changes in JIRA platform are also relevant for JIRA Core, JIRA Software, and JIRA Service Desk.**

**Quick search**

An improved quick search allows you to quickly search through all your issues and projects, and shows instant result while you're typing your search term. You can read more about it in [JIRA EAP release notes](/server/jira/platform/jira-software-eap-7-8-release-notes).

What might be useful for JIRA admins and plugin developers is monitoring how your users are searching, and limiting the number of concurrent searches.

The quick search doesn’t affect performance in a significant way unless many users start searching at the same time. If you notice any slowdown, start monitoring the searches to find a limit that works best for your instance. 

- To monitor searching in real-time, use the *quicksearch.concurrent.search* metric in JMX. For more info on how to set up JMX, see [Live monitoring with JMX](https://confluence.atlassian.com/display/ADMINJIRASERVER/Live+monitoring+using+the+JMX+interface).
- To limit the number of searches, use the *jira.quicksearch.max.concurrent.searches* global setting. In JIRA, you can find it by going to **Settings (cog icon) > System**, and clicking **Advanced settings**.

**Oracle JDBC driver is no longer bundled with JIRA**

If you're using Oracle as the database, you'll need to download the driver from the [Oracle website](http://www.oracle.com/technetwork/database/features/jdbc/index-091264.html), and copy it to the /lib directory in the JIRA installation directory after the upgrade. The supported Oracle JDBC version is 12.1.0.1. Starting JIRA without the new driver will result in an error until you replace the driver, and restart.

If you're upgrading JIRA Data Center with ZDU (zero downtime upgrades), you'll need to copy the driver to your nodes before starting the upgraded JIRA instances. 
