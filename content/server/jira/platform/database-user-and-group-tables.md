---
aliases:
- /server/jira/platform/database-user-and-group-tables-4227143.html
- /server/jira/platform/database-user-and-group-tables-4227143.md
category: reference
confluence_id: 4227143
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227143
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227143
date: '2017-12-08'
legacy_title: Database - User and Group tables
platform: server
product: jira
subcategory: database
title: "Database – User and Group tables"
---
# Database – User and Group tables

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Changed:</p></td>
<td><p>Jira 4.3 and later uses &quot;Embedded Crowd&quot; as its user management framework.<br />
For the old user and group tables, see Database Schema v4.2.</p></td>
</tr>
</tbody>
</table>

## Users

Users are stored in the `APP\_USER` table.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>USER_KEY</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Unique user key</p></td>
</tr>
<tr class="odd">
<td><p>LOWER_USER_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Links to CWD_USER.LOWER_USER_NAME</p></td>
</tr>
</table>

`CWD_USER` table.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>DIRECTORY_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p>Links to CWD_DIRECTORY</p></td>
</tr>
<tr class="odd">
<td><p>USER_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>LOWER_USER_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>used for case-insensitive search</p></td>
</tr>
<tr class="odd">
<td><p>ACTIVE</p></td>
<td><p>NUMBER(9,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>CREATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>UPDATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>FIRST_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Not used</p></td>
</tr>
<tr class="odd">
<td><p>LOWER_FIRST_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Not used</p></td>
</tr>
<tr class="even">
<td><p>LAST_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Not used</p></td>
</tr>
<tr class="odd">
<td><p>LOWER_LAST_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Not used</p></td>
</tr>
<tr class="even">
<td><p>DISPLAY_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_DISPLAY_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>EMAIL_ADDRESS</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_EMAIL_ADDRESS</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>CREDENTIAL</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

See also `CWD\_USER\_ATTRIBUTES` that stores arbitrary "Attributes" against the User.

## Group tables

The groups are stored in the `CWD\_GROUP` table.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>GROUP_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_GROUP_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>used for case-insensitive search</p></td>
</tr>
<tr class="even">
<td><p>ACTIVE</p></td>
<td><p>NUMBER(9,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOCAL</p></td>
<td><p>NUMBER(9,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>CREATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>UPDATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>DESCRIPTION</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_DESCRIPTION</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>GROUP_TYPE</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>DIRECTORY_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p>Links to CWD_DIRECTORY</p></td>
</tr>
</tbody>
</table>

See also `CWD\_GROUP\_ATTRIBUTES` that stores arbitrary "Attributes" against the Group.

## Group membership

The `CWD\_MEMBERSHIP` table records which users belong to which groups.  
Note that it is also used to store parent/child relationships for nested groups.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>PARENT_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p>Parent Group</p></td>
</tr>
<tr class="odd">
<td><p>CHILD_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p>User or nested Group ID</p></td>
</tr>
<tr class="even">
<td><p>MEMBERSHIP_TYPE</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p>Indicates a Group-User membership or Group-Group membership</p></td>
</tr>
<tr class="odd">
<td><p>GROUP_TYPE</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p>not used</p></td>
</tr>
<tr class="even">
<td><p>PARENT_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>Parent Group</p></td>
</tr>
<tr class="odd">
<td><p>LOWER_PARENT_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>used for case-insensitive search</p></td>
</tr>
<tr class="even">
<td><p>CHILD_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>User or child Group</p></td>
</tr>
<tr class="odd">
<td><p>LOWER_CHILD_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p>used for case-insensitive search</p></td>
</tr>
<tr class="even">
<td><p>DIRECTORY_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p>Note that this must match the DirectoryId for the Group and User</p></td>
</tr>
</tbody>
</table>

## User Directories

Jira can have multiple "User Directories".  
The main config is stored in `CWD\_DIRECTORY`.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>DIRECTORY_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_DIRECTORY_NAME</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>CREATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>UPDATED_DATE</p></td>
<td><p>DATE</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>ACTIVE</p></td>
<td><p>NUMBER(9,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>DESCRIPTION</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>IMPL_CLASS</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>LOWER_IMPL_CLASS</p></td>
<td><p>VARCHAR(255)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>DIRECTORY_TYPE</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p>Distinguishes Internal, LDAP, Crowd, etc</p></td>
</tr>
<tr class="odd">
<td><p>DIRECTORY_POSITION</p></td>
<td><p>VARCHAR(18,0)</p></td>
<td><p>Hierarchy of directories</p></td>
</tr>
</tbody>
</table>

Details and custom settings are stored in `CWD\_DIRECTORY\_ATTRIBUTE`.  
Available operations (permissions) are stored in `CWD\_DIRECTORY\_OPERATION`.

## Shadowed users

Consider a query like the following:

``` sql
select user_name, directory_id, display_name, email_address
from cwd_user
where user_name = 'fred'
```

Normally this should return a single row, however, Jira allows you to set up multiple user directories (for
example, multiple LDAP directories, or a single LDAP directory mixed with local users).  
It is possible that two or more directories contain the same username.  
Now the User Directories have a sort hierarchy and Jira will only recognize the user in the highest priority directory.  
To find out which user is in effect, you can change the query to:

``` sql
select user_name, directory_id, display_name, email_address, dir.directory_position as position
from cwd_user usr
join cwd_directory dir on dir.id = usr.directory_id
where user_name = 'fred'
order by dir.directory_position
```

The first user in the list is the actual one that Jira will use.  
Any other users are considered as "shadowed" by the first and will be ignored by Jira.

## Watches and votes

Watches and votes are recorded in the `USERASSOCIATION` table.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>COLUMN_NAME</p></th>
<th><p>DATA_TYPE</p></th>
<th><p>COMMENTS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SOURCE_NAME</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p>username</p></td>
</tr>
<tr class="even">
<td><p>SINK_NODE_ID</p></td>
<td><p>NUMBER(18,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>SINK_NODE_ENTITY</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>ASSOCIATION_TYPE</p></td>
<td><p>VARCHAR(60)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>SEQUENCE</p></td>
<td><p>NUMBER(9,0)</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>CREATED</p></td>
<td><p>DATETIME</p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

For example:

``` sql
mysql> select * from userassociation;
+---------------+--------------+------------------+------------------+----------+---------------------+
| SOURCE_NAME   | SINK_NODE_ID | SINK_NODE_ENTITY | ASSOCIATION_TYPE | SEQUENCE | CREATED             |
+---------------+--------------+------------------+------------------+----------+---------------------+
| asmith        |     108433   | Issue            | WatchIssue       |     NULL | 2018-06-01 11:55:01 |
| droberts      |     100915   | Issue            | WatchIssue       |     NULL | 2018-06-01 12:08:01 |
| dfernandez    |     106387   | Issue            | VoteIssue        |     NULL | 2018-06-01 12:18:35 |
...
```

For example, here user `asmith` watches issue with ID 108433.
