---
title: Velocity Contexts 4227090
aliases:
    - /server/jira/platform/velocity-contexts-4227090.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227090
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227090
confluence_id: 4227090
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Velocity Contexts

The following table lists the Velocity contexts available for use in the XML descriptor and Velocity views.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Type</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>user</p></td>
<td><p>User</p></td>
<td><p>Currently logged in user</p></td>
</tr>
<tr class="even">
<td><p>helper</p></td>
<td><p>JiraHelper</p></td>
<td><p>Convenient class which holds information such as the current request and selected project</p></td>
</tr>
<tr class="odd">
<td><p>xmlutils</p></td>
<td><p>XMLUtils</p></td>
<td><p>Utilities for basic XML reading</p></td>
</tr>
<tr class="even">
<td><p>textutils</p></td>
<td><p><a href="http://www.opensymphony.com/oscore/api/com/opensymphony/util/TextUtils.html" class="external-link">TextUtils</a></p></td>
<td><p>Utilities for common String manipulations</p></td>
</tr>
<tr class="odd">
<td><p>urlcodec</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/util/URLCodec.html" class="external-link">JiraUrlCodec</a></p></td>
<td><p>Utility for encoding a string</p></td>
</tr>
<tr class="even">
<td><p>outlookdate</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/web/util/OutlookDate.html" class="external-link">OutlookDate</a></p></td>
<td><p>Class to give a nice String representation of a date</p></td>
</tr>
<tr class="odd">
<td><p>authcontext</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/security/JiraAuthenticationContext.html" class="external-link">JiraAuthenticationContext</a></p></td>
<td><p>User locale dependant utility class. Can get the current user, locale, I18nBean (for internationalisation) and OutlookDate</p></td>
</tr>
<tr class="even">
<td><p>dateutils</p></td>
<td><p>DateUtils</p></td>
<td><p>Utilities for displaying date/time</p></td>
</tr>
<tr class="odd">
<td><p>externalLinkUtil</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/web/util/ExternalLinkUtilImpl.html" class="external-link">ExternalLinkUtil</a></p></td>
<td><p>A simple utility class that lets you resolve external links that may need to change, because of partner sites and such</p></td>
</tr>
<tr class="even">
<td><p>requestContext</p></td>
<td><p>VelocityRequestContext</p></td>
<td><p>A context that allows for different implementations depending on whether it is running in the scope of a web request, or via email.</p></td>
</tr>
<tr class="odd">
<td><p>req</p></td>
<td><p><a href="http://java.sun.com/j2ee/1.4/docs/api/javax/servlet/http/HttpServletRequest.html" class="external-link">HttpServletRequest</a></p></td>
<td><p>current request</p></td>
</tr>
<tr class="even">
<td><p>baseurl</p></td>
<td><p>String</p></td>
<td><p>The base URL for this instance (velocityRequestContext.getBaseUrl())</p></td>
</tr>
</tbody>
</table>

























































































































































































































































