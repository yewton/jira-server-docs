---
title: Building JIRA Add Ons 32345314
aliases:
    - /server/jira/platform/building-jira-add-ons-32345314.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32345314
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32345314
confluence_id: 32345314
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Building JIRA add-ons

A JIRA add-on (also known as a plugin) does exactly what you think it might do: it adds to the functionality of JIRA. It might add a single feature, like a report, or it might provide enough features to constitute a product in its own right, like JIRA Agile. An add-on is installed separately to JIRA (via the [Universal Plugin Manager](https://developer.atlassian.com/display/UPM)). If you want to make it available for others, you can [list it on the Atlassian Marketplace](https://developer.atlassian.com/display/MARKET/Listing+step-by-step). 

There are two different frameworks for building Atlassian add-ons: Atlassian Connect and the Plugins2 framework. 

## Atlassian Connect

Atlassian Connect add-ons are essentially web applications that operate remotely over HTTP. Currently, Atlassian Connect add-ons will only run in JIRA Cloud, and are the recommended type of add-ons for JIRA Cloud.

For more information about Atlassian Connect, read [What is Atlassian Connect?](https://developer.atlassian.com/static/connect/docs/guides/introduction.html) *(Atlassian Connect documentation).*

## Plugins2

A Plugins2 plugin is a single JAR containing code, an plugin descriptor (XML) and usually some Velocity template files to render HTML. 

### Descriptor

The plugin descriptor is the only mandatory part of the plugin. It must be called **atlassian-plugin.xml** and be located in the root of your JAR file. Here is a sample of the descriptor with highlighted elements:

``` xml
<!-- the plugin key must be unique, think of it as the 'package' of the plugin -->
<atlassian-plugin key="com.atlassian.plugin.sample" name="Sample Plugin" plugins-version="2">
    <!-- a short block describing the plugin itself -->
    <plugin-info>
        <description>This is a brief textual description of the plugin</description>
        <!-- the version of the plugin -->
        <version>1.1</version>
        <!-- details of the plugin vendor -->
        <vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com"/>
    </plugin-info>

    . . . 1 or more plugin modules . . .
</atlassian-plugin>
```

### Modules and keys

Each plugin consists of one or more **plugin modules**. These are of different types (e.g. a report) and each has an individual XML element describing it.

Each plugin has a **plugin key** which is unique among all plugins (e.g. "com.atlassian.plugin.sample"). Typically the root package name of the primary Java class is used. Each module within the plugin also has a **module key** which is unique within the plugin (eg. "myreport"). Semantically this equates to the class name of a Java class.

The plugin key + module key are combined to make the **complete key** of the plugin module (combining the examples above, the complete key would be "com.atlassian.plugin.<a href="http://samplemyreport/" class="external-link">sample:myreport</a>"). Note: `a :` is used to separate the plugin key from the module key.

### Plugin lifecycle

Once you start building your own plugins, it is likely that you'll need to call on JIRA code to accomplish certain tasks; for example, to retrieve a list of users, make workflow changes or add new data to issues. We've compiled some resources about how JIRA works to help you know how to access that functionality.

See [JIRA Plugin Lifecycle](https://developer.atlassian.com/display/JIRADEV/JIRA+Plugin+Lifecycle) for more information on the JIRA environment from a plugin's perspective.

### Dependency injection

JIRA uses PicoContainer to manage object creation throughout the system. It is important to understand how <a href="http://picocontainer.com/injection.html" class="external-link">dependency injection</a> *(Picocontainer documentation)* works before trying to call JIRA functionality from your plugin. See the introduction to [PicoContainer and JIRA](/server/jira/platform/picocontainer-and-jira.html).

