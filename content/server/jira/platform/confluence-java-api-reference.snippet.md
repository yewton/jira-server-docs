---
aliases:
- /server/jira/platform/confluence-java-api-reference-24085297.html
- /server/jira/platform/confluence-java-api-reference-24085297.md
category: devguide
confluence_id: 24085297
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085297
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085297
date: '2017-12-08'
legacy_title: Confluence Java API Reference
platform: server
product: jira
subcategory: other
title: Confluence Java API reference
---
# Confluence Java API reference

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/yABg> in about 2 seconds.

{{% /note %}}

