---
title: "Jira Core 7.12 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-08-08"
---
# Jira Core 7.12 EAP release notes

*08 August 2018*

Atlassian is proud to present *Jira Core 7.12 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *Jira Core 7.12* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for Jira 7.12, read the developer change management guide: [Preparing for Jira 7.12](/server/jira/platform/preparing-for-jira-7-12). If you are new to Jira, you should also read our [Java API Policy for Jira](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both *from* and *to* EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of Jira, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.10.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes


### Share edit rights for filters and dashboards

We’ve added the following improvements to filters and dashboards:

- **Share permission to view or to edit:** Choose who can only view the filter/dashboard, and who can also edit it.
- **Share with single users:** Apart from projects, groups, and roles, you can now share your filters/dashboards with single users.

Here’s how it looks in Jira:

![Filters UI](/server/jira/platform/images/share_filters.png)

*Filters view*

![Dashboards UI](/server/jira/platform/images/share_dashboards.png)

*Dashboards view*

#### Audit log

To keep you safe, we’ll notify you about every change to filters and dashboards through the following events in the audit log.

Filters:

- filter created
- filter updated (updates to name, description, JQL, and permissions)
- filter deleted

Dashboards:

- dashboard created
- dashboard updated (updates to name, description, and permissions)
- dashboard deleted