---
title: Contents Of the Velocity Context 4227181
aliases:
    - /server/jira/platform/contents-of-the-velocity-context-4227181.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227181
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227181
confluence_id: 4227181
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Contents of the Velocity Context

This is the listing of the contents of the Velocity Context used to process web templates.

{{% note %}}

For JIRA 3.6.x and later - please refer to the [JIRA 3.6.x guide](/server/jira/platform/velocity-context-for-email-templates-4227144.html) for details on the velocity context used to process email templates.

{{% /note %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Velocity variable</p></th>
<th><p>JIRA Object/Value</p></th>
<th><p>Description</p></th>
<th><p>As of Version </p></th>
<th><p> Condition</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> constantsManager</p></td>
<td><p> com.atlassian.jira.config.ConstantsManager</p></td>
<td><p> Manager for issue types, statuses, priorities and resolutions.</p></td>
<td><p> 3.4</p></td>
<td><p> always present<br />
</p></td>
</tr>
<tr class="even">
<td><p> projectManager</p></td>
<td><p> com.atlassian.jira.project.ProjectManager</p></td>
<td><p> Implementations of this interface are responsible for all management of project entities within JIRA.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> customFieldManager</p></td>
<td><p>com.atlassian.jira.issue.CustomFieldManager </p></td>
<td><p> Functions for working with CustomFields</p></td>
<td><p> 3.5</p></td>
<td><p> always present<br />
</p></td>
</tr>
<tr class="even">
<td><p> applicationProperties</p></td>
<td><p> com.atlassian.jira.config.properties.ApplicationProperties</p></td>
<td><p> provides access to JIRA properties stored in the DB<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> jirautils </p></td>
<td><p> com.atlassian.jira.util.JiraUtils</p></td>
<td><p> Miscellaneous utility methods.<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present<br />
</p></td>
</tr>
<tr class="even">
<td><p> jirakeyutils</p></td>
<td><p> com.atlassian.jira.util.JiraKeyUtils</p></td>
<td><p> utilities to determine the validity of JIRA project/issue keys<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> buildutils</p></td>
<td><p> com.atlassian.jira.util.BuildUtils</p></td>
<td><p> provides information on the running version of JIRA<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="even">
<td><p> velocityhelper</p></td>
<td><p> com.atlassian.jira.util.JiraVelocityHelper</p></td>
<td><p> A simple class store methods we want to expose to velocity templates</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> userutils</p></td>
<td><p> com.atlassian.core.user.UserUtils</p></td>
<td><p> A utility class for operating on users.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> textutils</p></td>
<td><p> com.opensymphony.util.TextUtils</p></td>
<td><p> lots of utility methods for manipulating text<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> params</p></td>
<td><p> java.util.Map</p></td>
<td><p> parameters of the IssueEvent that triggered this email notification </p></td>
<td><p> 3.4</p></td>
<td><p> always present<br />
</p></td>
</tr>
<tr class="even">
<td><p> issue</p></td>
<td><p> org.ofbiz.core.entity.GenericValue</p></td>
<td><p> a GenericValue representing the issue which triggered this email notification </p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> issueObject</p></td>
<td><p> com.atlassian.jira.issue.MutableIssue</p></td>
<td><p>an Issue object representing the issue which triggered this email notification<br />
</p></td>
<td><p> 3.5.2<br />
</p></td>
<td><p> always present<br />
</p></td>
</tr>
<tr class="even">
<td><p> remoteUser</p></td>
<td><p> com.opensymphony.user.User</p></td>
<td><p> the logged in user if they exist </p></td>
<td><p> 3.4</p></td>
<td><p> remoteUser != null </p></td>
</tr>
<tr class="odd">
<td><p> renderedDescription</p></td>
<td><p> java.lang.String</p></td>
<td><p> the rendered description field, it a renderer has been specified </p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field </p></td>
</tr>
<tr class="even">
<td><p> renderedEnvironment</p></td>
<td><p> java.lang.String</p></td>
<td><p> the rendered environment field, it a renderer has been specified </p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field<br />
</p></td>
</tr>
<tr class="odd">
<td><p> timeoriginalestimate</p></td>
<td><p> java.lang.String</p></td>
<td><p>The DateUtils.getDurationPretty value of timeoriginalestimate from issue or &quot;None&quot; if null<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> timeestimate</p></td>
<td><p> java.lang.String</p></td>
<td><p> The DateUtils.getDurationPretty value of timeestimate from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> timespent</p></td>
<td><p> java.lang.String</p></td>
<td><p> The DateUtils.getDurationPretty value of timespent from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> duedate</p></td>
<td><p> java.sql.Timestamp</p></td>
<td><p> Duedate from the issue<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> versions</p></td>
<td><p> List(org.ofbiz.core.entity.GenericValue)</p></td>
<td><p> A list of GenericValues representing the Affected Versions of the issue<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> fixfors</p></td>
<td><p> List(org.ofbiz.core.entity.GenericValue)</p></td>
<td><p> A list of GenericValues representing the Fix Versions of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> components</p></td>
<td><p> List(org.ofbiz.core.entity.GenericValue)</p></td>
<td><p> A list of GenericValues representing the Components of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> assignee</p></td>
<td><p> com.opensymphony.user.User</p></td>
<td><p> The assignee of the issue<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> assignee != null </p></td>
</tr>
<tr class="odd">
<td><p> reporter</p></td>
<td><p> com.opensymphony.user.User</p></td>
<td><p> The reporter of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> reporter != null<br />
</p></td>
</tr>
<tr class="even">
<td><p> renderedComment</p></td>
<td><p> java.lang.String</p></td>
<td><p> the rendered comment field, it a renderer has been specified. The comment is the comment associated with the change<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field and comment created<br />
</p></td>
</tr>
<tr class="odd">
<td><p> comment</p></td>
<td><p> org.ofbiz.core.entity.GenericValue</p></td>
<td><p> generic value representing the comment associated with the change<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="even">
<td><p> commentauthor</p></td>
<td><p> com.opensymphony.user.User</p></td>
<td><p> the author of the comment<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="odd">
<td><p> timelogged</p></td>
<td><p> java.lang.String</p></td>
<td><p> The DateUtils.getDurationPretty value of timelogged from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> changelog</p></td>
<td><p> org.ofbiz.core.entity.GenericValue</p></td>
<td><p> generic value representing the changelog associated with the change</p></td>
<td><p> 3.4</p></td>
<td><p> changelog != null<br />
</p></td>
</tr>
<tr class="odd">
<td><p> changelogauthor</p></td>
<td><p> com.opensymphony.user.User</p></td>
<td><p> The change log author of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> changelog != null</p></td>
</tr>
<tr class="even">
<td><p> visibilitylevel</p></td>
<td><p> java.lang.String</p></td>
<td><p> The security level of the comment<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="odd">
<td><p> i18n</p></td>
<td><p> com.atlassian.jira.util.I18nHelper</p></td>
<td><p> Bean that searches for i18n text in JiraWebActionSupport.properties</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> dateutils</p></td>
<td><p><code>com.atlassian.core.util.DateUtils</code></p></td>
<td><p>methods for working with dates<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> attachments</p></td>
<td><p> List( org.ofbiz.core.entity.GenericValue)<br />
</p></td>
<td><p> list of generic values that represents the attachments associated with the issue<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> security</p></td>
<td><p> org.ofbiz.core.entity.GenericValue</p></td>
<td><p> generic value representing the security level, if any, associated with this issue<br />
</p></td>
<td><p> 3.4</p></td>
<td><p> if(isEnterpriseEdition) </p></td>
</tr>
<tr class="odd">
<td><p>mailPluginsHelper</p></td>
<td><p>com.atlassian.jira.mail.JiraMailPluginsHelperImpl</p></td>
<td><p>provides access to isPluginModuleEnabled()<br />
</p></td>
<td><p>3.7</p></td>
<td><p>always present</p></td>
</tr>
</tbody>
</table>

