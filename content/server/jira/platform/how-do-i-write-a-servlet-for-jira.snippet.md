---
aliases:
- /server/jira/platform/4227140.html
- /server/jira/platform/4227140.md
category: devguide
confluence_id: 4227140
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227140
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227140
date: '2017-12-08'
legacy_title: How do I write a servlet for JIRA?
platform: server
product: jira
subcategory: other
title: How do I write a servlet for JIRA?
---
# How do I write a servlet for JIRA?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I write a servlet for JIRA?

Please follow the instructions for writing a [Servlet Plugin Module](/server/jira/platform/servlet).
