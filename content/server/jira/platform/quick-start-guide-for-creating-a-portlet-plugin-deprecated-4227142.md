---
title: Quick Start Guide for Creating a Portlet Plugin Deprecated 4227142
aliases:
    - /server/jira/platform/quick-start-guide-for-creating-a-portlet-plugin-deprecated-4227142.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227142
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227142
confluence_id: 4227142
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Quick start guide for creating a portlet plugin - deprecated

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Deprecated:</p></td>
<td><p>In JIRA 4.0, portlets were replaced by gadgets. For details, please see <a href="https://developer.atlassian.com/display/GADGETS/Gadgets+and+JIRA+Portlets">Gadgets and JIRA Portlets</a>.<br />
In JIRA 5.0, all portlet-specific code has been removed from JIRA plugin points and APIs. See <a href="/server/jira/platform/java-api-changes-in-jira-5-0">Java API Changes in JIRA 5.0</a>.</p></td>
</tr>
</tbody>
</table>

# Goal

This page is intended to get you up and running with writing your own JIRA portlet within no time.  It actually is a combination of several examples included in the plugin development kit which were broken.   The portlet which is explained below lets you configure 3 parameters and will result in table of books that match the searchcriterium:

1.  subscriptionId (Needed to use an Amazon webservice)
2.  searchParameter  (What book property will we be looking for ? E.g. Title, Author, ...)
3.  searchArgument (The argument which will be searched for.  E.g. "Java"  or "Ruby") 
    ![](/server/jira/platform/images/portletconfiguration.gif)
    Screenshot 1: Configuration of portlet
     
    ![](/server/jira/platform/images/results-byauthor.gif)
    Screenshot 2: result of search (parameter = 'Author' & argument = 'Sierra')
     
    ![](/server/jira/platform/images/resultsbytitle.gif)
    Screenshot 3: result of search (parameter = 'Title' & argument = 'Java')

# Prequisites

My installed software:

1.  JDK 1.6.0\_03
2.  Eclipse 3.3.0 
3.  Maven 2.0.7
4.  Jira enterprise edition 3.12.2

# Configuration steps

You will need to make some changes to your $maven\_home/conf/settings.xml.

``` xml
   <profiles>
    <profile>
      <id>Ciber</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>

      <repositories>
        ...
        <!-- JIRA -->
        <repository>
          <id>atlassian-public</id>
          <url>https://maven.atlassian.com/repository/public</url>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
          </releases>
        </repository>
        <repository>
          <id>atlassian-contrib</id>
          <url>https://maven.atlassian.com/contrib</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
          </releases>
        </repository>
        <repository>
          <id>atlassian-m1-repository</id>
          <url>https://maven.atlassian.com/maven1</url>
          <layout>legacy</layout>
        </repository>
      </repositories>

      <pluginRepositories>
        ...
        <!-- JIRA -->
        <pluginRepository>
          <id>atlassian-public</id>
          <url>https://maven.atlassian.com/repository/public</url>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
          </releases>
        </pluginRepository>
        <pluginRepository>
          <id>atlassian-contrib</id>
          <url>https://maven.atlassian.com/contrib</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
          </releases>
       </pluginRepository>
      </pluginRepositories>

      <properties>
        <downloadSources>false</downloadSources>
        <downloadJavadocs>false</downloadJavadocs>
        <atlassian.pdk.server.url>http://localhost:8080</atlassian.pdk.server.url>
        <atlassian.pdk.server.username>********</atlassian.pdk.server.username>
        <atlassian.pdk.server.password>********</atlassian.pdk.server.password>
      </properties>
    </profile>
  </profiles>
```

See also [how to build an Atlassian plugin](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).

Next we will create a new plugin project based on a plugin archetype.  In a DOS-box, run following command:

``` xml
 mvn org.apache.maven.plugins:maven-archetype-plugin:1.0-alpha-7:create \
  -DarchetypeGroupId=com.atlassian.maven.archetypes \
  -DarchetypeArtifactId=jira-plugin-archetype \
  -DarchetypeVersion=10 \
  -DremoteRepositories=https://maven.atlassian.com/repository/public/ \
  -DgroupId=$MY_PACKAGE$ -DartifactId=$MY_PLUGIN$
```

and replace $MY\_PACKAGE$ and $MY\_PLUGIN$ according to your project. Make sure you remove the backslashes "\\" so the complete command is one 1 line before you execute it.

Maven will create a new folder $MY\_PLUGIN$ and provide some default files. Check the [JIRA:attachment](attachments/4227142/4390984.rar) for how the plugin works.

Cheers,

Robby

























































































































































































































































