---
title: "Deprecation notice – Jira Service Desk will be deprecating the use of com.atlassian.fugue in Jira Service Desk 3.15"
product: jira
category: devguide
subcategory: updates
date: "2018-08-13"
---
# Deprecation notice - Jira Service Desk is deprecating com.atlassian.fugue in 3.15

Jira Service Desk will be deprecating the use of **com.atlassian.fugue** in Jira Service Desk 3.15, release date mid August 2018. In Jira Service Desk 4.0, we'll be updating our APIs to use **Core Java Data types and Exceptions**. We're introducing this change to make it easier to develop on Jira Service Desk.

We'll be permanently removing com.atlassian.fugue with the release of Jira Service Desk 4.0, in accordance with the [Java API Policy for Jira](https://developer.atlassian.com/server/jira/platform/java-api-policy-for-jira-4227213/). 

## What will I need to do?

You will need to update any scripts, integrations or apps that make requests to endpoints returning com.atlassian.fugue to use **Core Java Data types and Exceptions** instead. You can start testing this with the release of Jira's 8.0 Early Access Program (EAP). Watch the [Latest updates page](https://developer.atlassian.com/server/jira/platform/) for more information.

## What will happen if I do nothing?

When com.atlassian.fugue is removed, any apps or services that make a request to APIs using com.atlassian.fugue will break during build.