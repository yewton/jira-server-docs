---
aliases:
- /server/jira/platform/deprecating-junit3-test-framework-39366997.html
- /server/jira/platform/deprecating-junit3-test-framework-39366997.md
category: devguide
confluence_id: 39366997
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39366997
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39366997
date: '2017-12-08'
legacy_title: Deprecating JUnit3 test framework
platform: server
product: jira
subcategory: updates
title: Deprecating JUnit3 test framework
---
# Deprecating JUnit3 test framework

## 1. Deprecating JUnit3 test frameowrk

JIRA is moving away from its JUnit 3 test framework entirely. JIRA will continue to support exiting tests for at least 7.2 time frame. There are three major changes that will affect how JIRA functional test looks like.

-   Switching from JUnit3 to JUnit4,
-   Change in base classes and test utility class injection,
-   Minor behaviour changes.

## 2. Staying with JUnit3

The JUnit3 base classes are being pulled and since 7.2 will not be available in the standard JIRA functional test library. In particular, the following base classes are being deprecated:

-   `com.atlassian.jira.webtests.JIRAWebTest`
-   `com.atlassian.jira.functest.framework.FuncTestCase`
-   `com.atlassian.jira.webtests.ztests.bundledplugins2.rest.RestFuncTest`

Utility classes that are now deprecated:

-   `com.atlassian.jira.functest.framework.FuncTestHelperFactory`
-   `com.atlassian.jira.functest.framework.FuncTestCaseJiraSetup`
-   `com.atlassian.jira.functest.framework.AbstractFuncTestUtil` (removed completely)
-   `com.atlassian.jira.functest.framework.AbstractNavigationUtil` (removed completely) 

You can continue using JUnit3 as your default test runner. In order to keep using the existing JUnit3 base and utility classes that have been deprecated, add the following maven artefact in test scope to your project:

``` xml
  <dependency>
        <groupId>com.atlassian.jira</groupId>
        <artifactId>jira-func-tests-legacy</artifactId>
        <version>${jira.version}</version>
        <scope>test</scope>
    </dependency>
```

This library will not be supported and should be used only to conveniently plan and execute the migration of existing tests.

## 3. Moving on to JUnit4

### 3.1. Creating a functional test

In order to create a new functional test in our new JUnit4 framework, we recommend to create a test class with the following rule: 

``` java
    @Rule
    public TestRule funcTest = FuncTestRuleChain.forTest(this);
```

Alternatively, if you want to migrate existing tests, you may find deriving from the following classes easier in your case: 

-   `com.atlassian.jira.functest.framework.BaseJiraFuncTest`
-   `com.atlassian.jira.functest.framework.BaseJiraRestTest`

Any and all test utility classes that you need should be declared as fields and will be injected by the framework. 

### 3.2. Utility classes injection in tests

All utility classes (like `Administration`, `Navigation`, etc) are now injectable directly into the test:

``` java
    @com.javax.Inject
    private Administration administration;
```

Injected components will be available in methods annotated with `@org.junit.Before`. 

### 3.3. Converting tests

Conversion of tests between JUnit3 and JUnit4 is straightforward and contains these steps:

-   Removing a JUnit3 base class (a class inheriting from `junit.framework.TestCase`),
-   Adding static imports to call JUnit assertions,
-   Annotating test methods with the `@org.junit.Test` annotation, annotating setup and tear down methods with `@org.junit.Before` and `@org.junit.After` annotations, respectively.

There may be other changes required, an overview that describes what need to be done can be found <a href="http://stackoverflow.com/questions/264680/best-way-to-automagically-migrate-tests-from-junit-3-to-junit-4" class="external-link">in this stackoverflow question</a>.

### 3.4. Differences in test behavior

Tests will behave differently in the following regard: 

-   Tests no longer log the user in by default, use the `@com.atlassian.jira.functest.framework.LoginAs` annotation on the test class or method level. There is no need to explicitly log out at the start of the test in order to test with anonymous access.
-   Tests can be set up using the `@com.atlassian.integrationtesting.runner.restore.Restore` or `@com.atlassian.jira.functest.framework.RestoreBlankInstance` annotations on the test class or method level. You can also use `@com.atlassian.integrationtesting.runner.restore.RestoreOnce` on class level to restore data once before running all test methods.
-   The HttpUnit settings can be safely declared and will be automatically torn down by the `@com.atlassian.jira.functest.framework.HttpUnitConfiguration` annotation on the test class or method level

Annotations on method level override annotations on class level.

### 3.5. Accessing JIRA from multiple sessions in functional tests

Since 7.2 there is a new functional test component called `SessionFactory` that can be used to create a separate HttpUnit session:

``` java
    @Inject
    private SessionFactory sessionFactory;
 
    @Inject
    private Navigation navigation;
 
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void systemShouldDoX() {
        final Session otherUser = sessionFactory.begin();
        otherUser.withSession(() -> {
            navigation.login("fred");
            // do things in the other session
        });
    }
```

The context and session is preserved as long as you have access to the `Session` object acquired this way. Each separate session, including your main default one, keeps the browsing contexts. You can create multiple sessions this way and reuse the injected utility classes with different contexts.

## 4. Changes in testing API

Constructors of test utility classes are no longer part of the API. Creating test utility classes in the tests using constructors is **not supported** and may change in a breaking way **without further notice** at any time after the next release post JIRA 7.2.
