---
aliases:
- /server/jira/platform/guide-jira-workflow-designer-23299083.html
- /server/jira/platform/guide-jira-workflow-designer-23299083.md
category: devguide
confluence_id: 23299083
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23299083
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23299083
date: '2017-12-08'
guides: guides
legacy_title: Guide - JIRA workflow designer
platform: server
product: jira
subcategory: learning
title: Implementing the JIRA workflow designer
---
# Implementing the JIRA workflow designer

## Summary

JIRA's workflow designer JavaScript component renders an editable visual representation of a workflow in the browser. Users can add, edit, and remove statuses and transitions, inspect the workflow by panning and zooming, and position elements on the canvas. Changes are periodically saved to the server without prompting the user.  
   
To edit a workflow, the user must be an administrator and have an active [WebSudo](https://developer.atlassian.com/display/DOCS/Adding+WebSudo+Support+to+your+Plugin) session. If the user's WebSudo session expires an error message appears prompting them to refresh the page. The workflow designer also supports a read-only mode in which the workflow can be inspected but not modified -- this requires the user to be a project administrator or have the view workflow permission.

<img src="/server/jira/platform/images/workflow-designer.png" class="image-center" width="500" />

## Availability

The workflow designer component is available in JIRA 6.1 and above, it works in all <a href="https://confluence.atlassian.com/display/JIRA060/Supported+Platforms" class="external-link">supported browsers</a>, except Internet Explorer 8, and on mobile devices.

## Limitations

The workflow designer component has the following limitations:

-   it will not display an editable workflow designer in a dialog as JIRA doesn't support dialog stacking, and
-   prior to JIRA 6.2 it doesn't support creation of common transitions (though they are rendered correctly).

## Usage

To use the workflow designer in your plugin you must add a dependency on one of the web resources listed in the table below. You can then create an instance of the `JIRA.WorkflowDesigner.Application` class, passing appropriate options for the desired behavior.

| Web resource key                                                                | Description                                                                                                                                    |
|---------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `com.atlassian.jira.plugins.jira-workflow-designer:workflow-designer`           | The full workflow designer, including resources related to modifying workflows (dialogs, etc.).                                                |
| `com.atlassian.jira.plugins.jira-workflow-designer:workflow-designer-read-only` | The resources required for a read-only workflow designer. Use this if you don't need to modify workflows as it results in a smaller page size. |

### Methods

| Method      | Description                                                                                                                                                                                                 |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `destroy()` | Destroy the workflow designer, removing it from the page. You must call this method when you no longer need the workflow designer; it stops periodically executing jobs, unbinds event handlers, and so on. |

### Options

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Option</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>actions</code></td>
<td><p>Whether to show the actions bar (add/create status, etc.)</p>
<p><strong>Possible values:</strong> boolean.</p>
<p><strong>Default:</strong> true.</p></td>
</tr>
<tr class="even">
<td><code>currentStepId</code></td>
<td><p>The step ID of the status that is to be highlighted as &quot;current&quot;.</p>
<p><strong>Possible values:</strong> number.</p>
<p><strong>Default:</strong> undefined.</p></td>
</tr>
<tr class="odd">
<td><code>draft</code></td>
<td><p>Whether the draft version of the workflow should be loaded.</p>
<p><strong>Possible values:</strong> boolean.</p>
<p><strong>Default:</strong> false.</p></td>
</tr>
<tr class="even">
<td><code>element</code></td>
<td><p>The element to show the workflow designer in.</p>
<p><strong>Possible values:</strong> element.</p>
<p><strong>Default:</strong> undefined.</p></td>
</tr>
<tr class="odd">
<td><code>fullScreenButton</code></td>
<td><p>Whether the full screen button should be shown (providing <code>actions</code> is <code>true</code>).</p>
<p><strong>Possible values:</strong> boolean.</p>
<p><strong>Default:</strong> true.</p></td>
</tr>
<tr class="even">
<td><code>immutable</code></td>
<td><p>Whether the workflow designer should be read-only.</p>
<p><strong>Possible values:</strong> boolean.</p>
<p><strong>Default:</strong> false.</p></td>
</tr>
<tr class="odd">
<td><code>workflowId</code></td>
<td><p>The ID of the workflow to load (its name as shown on the global workflows page).</p>
<p><strong>Possible values:</strong> string.</p>
<p><strong>Default:</strong> undefined.</p></td>
</tr>
</tbody>
</table>

**\*element and workdlowId options are required.**

## Examples

### Creating a read-only workflow designer

``` javascript
new JIRA.WorkflowDesigner.Application({
  element: AJS.$("#workflow-designer"),
  immutable: true,
  workflowId: "My Workflow"
});
```

### Creating an editable workflow designer

``` javascript
new JIRA.WorkflowDesigner.Application({
  element: AJS.$("#workflow-designer"),
  workflowId: "My Workflow"
});
```

### Showing a workflow designer in a dialog

Because it is not possible to display an editable workflow designer in a dialog ([Limitations](#limitations)), the `immutable` option must be `true`. We must also ensure that the workflow designer is destroyed when the dialog is hidden.

``` javascript
function createDialogContent(callback) {
  callback([
    "<div class='aui-dialog-content'>",
      "<h2 class='dialog-title'>My Workflow</h2>",
      "<div class='form-body'></div>",
    "</div>"
  ].join(""));
}

function showWorkflowDesigner() {
  this._workflowDesigner = new JIRA.WorkflowDesigner.Application({
    actions: false,
    element: this.get$popup().find(".form-body"),
    immutable: true,
    workflowId: "My Workflow"
  });
 
  this._positionInCenter();
}
 
var dialog = new JIRA.FormDialog({
  content: createDialogContent,
  onContentRefresh: showWorkflowDesigner,
  widthClass: "large"
});

AJS.$(dialog).one("Dialog.hide", function () {
  this._workflowDesigner.destroy();
});
 
dialog.show();
```
