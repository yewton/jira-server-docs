---
aliases:
- /server/jira/platform/preparing-for-earlier-jira-releases-33128537.html
- /server/jira/platform/preparing-for-earlier-jira-releases-33128537.md
category: devguide
confluence_id: 33128537
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33128537
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33128537
date: '2017-12-08'
legacy_title: Preparing for earlier JIRA releases
platform: server
product: jira
subcategory: updates
title: Preparing for earlier JIRA releases
---
# Preparing for earlier JIRA releases

This page gathers important developer changes for earlier JIRA versions. These are versions that were made available via our <a href="https://confluence.atlassian.com/display/JIRA/EAP+Releases" class="external-link">Early Access Program</a>, while they were under development, but have since been released to the public.

If you are looking for the developer changes for the JIRA version currently under development, see [Latest updates](/server/jira/platform/index).

## Available versions

-   [Preparing for JIRA 7.1](/server/jira/platform/preparing-for-jira-7-1)
-   [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0)
-   [Preparing for JIRA 6.4](/server/jira/platform/preparing-for-jira-6-4)
-   [Preparing for JIRA 6.3](/server/jira/platform/preparing-for-jira-6-3)
-   [Preparing for JIRA 6.2](/server/jira/platform/preparing-for-jira-6-2)
-   [Preparing for JIRA 6.1](/server/jira/platform/preparing-for-jira-6-1)
-   [Preparing for JIRA 6.0](/server/jira/platform/preparing-for-jira-6-0)
-   [Preparing for JIRA 5.2](/server/jira/platform/preparing-for-jira-5-2)
-   [Preparing for JIRA 5.1](/server/jira/platform/preparing-for-jira-5-1)
-   [Preparing for JIRA 5.0](/server/jira/platform/preparing-for-jira-5-0)
