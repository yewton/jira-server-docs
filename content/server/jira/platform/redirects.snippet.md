---
aliases:
- /server/jira/platform/-redirects-22020518.html
- /server/jira/platform/-redirects-22020518.md
category: devguide
confluence_id: 22020518
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22020518
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22020518
date: '2017-12-08'
legacy_title: _Redirects
platform: server
product: jira
subcategory: other
title: _Redirects
---
# \_Redirects

-   [About JIRA Plugin Development](/server/jira/platform/about-jira-plugin-development.snippet)
-   [Confluence Java API Reference](/server/jira/platform/confluence-java-api-reference.snippet)
-   [How to create a JIRA Report](/server/jira/platform/how-to-create-a-jira-report.snippet)
-   [How to search in a plugin](/server/jira/platform/how-to-search-in-a-plugin.snippet)
-   [Jelly Examples](/server/jira/platform/jelly-examples)
-   [JIRA Remote API Reference](/server/jira/platform/jira-remote-api-reference.snippet)
-   [JIRA Tutorials](/server/jira/platform/jira-tutorials-24085274.html)
-   [Retrieving issue's links](/server/jira/platform/retrieving-issues-links.snippet)























































