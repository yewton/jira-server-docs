---
aliases:
- /server/jira/platform/jira-core-7.5-eap-release-notes-51914114.html
- /server/jira/platform/jira-core-7.5-eap-release-notes-51914114.md
category: devguide
confluence_id: 51914114
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51914114
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51914114
date: '2017-12-08'
legacy_title: JIRA Core 7.5 EAP release notes
platform: server
product: jira
subcategory: updates
title: JIRA Core 7.5 EAP release notes
---
# JIRA Core 7.5 EAP release notes

**9 August 2017**

Atlassian is proud to present **JIRA Core 7.5 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Core 7.5** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.5, read the developer change management guide: [Preparing for JIRA 7.5](/server/jira/platform/preparing-for-jira-7-5). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## New events and webhooks

Long awaited, events for creating and deleting issue links have landed in JIRA. You can now use them while building your plugins, or to get informed whenever an issue link is created or deleted. The new events are <a href="https://docs.atlassian.com/jira/7.5.0-EAP01/com/atlassian/jira/event/issue/link/IssueLinkCreatedEvent.html" class="external-link">IssueLinkCreatedEvent</a> and <a href="https://docs.atlassian.com/jira/7.5.0-EAP01/com/atlassian/jira/event/issue/link/IssueLinkDeletedEvent.html" class="external-link">IssueLinkDeletedEvent</a>.

When creating webhooks, you can use the following events. The string in parentheses is the name of the webhookEvent in the response.

-   create (*issuelink\_created*)
-   deleted (*issuelink\_deleted*)

For more info about creating webhooks in JIRA, see [Webhooks](https://developer.atlassian.com/display/JIRADEV/Webhooks).
