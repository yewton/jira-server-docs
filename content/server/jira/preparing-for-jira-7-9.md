---
title: "Preparing for JIRA 7.9"
product: jira
category: devguide
subcategory: updates
date: "2018-03-23"
---
# Preparing for JIRA 7.9

This page covers changes in *JIRA 7.9* that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.9 products. We also make release milestones available prior to the release:

- For *JIRA Server*, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](https://developer.atlassian.com/display/JIRADEV/JIRA+EAP+Releases).

- For *JIRA Cloud*, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use [JIRA REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Risk level/Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **Improved Kanban boards**<br>JIRA Software | Risk level: None<br>Affects: Plugin developers, JIRA admins<br>
| **Microsoft SQL Server 2016**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: None<br>Affects: Plugin developers, JIRA admins<br>
| **Wildcard search for version fields**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: Low<br>Affects: Plugin developers, JIRA admins<br>
| **Empty JQL queries**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: Medium<br>Affects: Plugin developers, JIRA admins<br>
| **Audit log - new events**<br>JIRA Software | Risk level: None<br>Affects: Plugin developers, JIRA admins<br>
| **Delimiters for CSV**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: Low<br>Affects: Plugin developers, JIRA admins<br>


## Changes

**Improved Kanban boards** (JIRA Software)

We're limiting the number of issues displayed in the **Done** column on the Kanban board. Instead of cluttering the board with issues that are already completed, we're showing only issues that were updated in any way within the last 2 weeks. Any project admin can disable this feature, or change the retention period (**Board > Configuration > General**). Limiting the number of displayed issues should speed up your board significantly (depending on how many issues you have).

**Microsoft SQL Server 2016**

We've added another supported platform - Microsoft SQL Server 2016.

**Wildcard search for version fields**

JIRA 7.9 brings searching through version fields with a wildcard. Here are some details about this feature:

Operators:

- *CONTAINS (~)* operator. It will behave differently than for the text fields (finding an exact match, not a fuzzy match).
- Asterisk (*) to specify the location of unknown characters.

Fields:

- Fix version
- Affected version
- Custom fields using the Version Picker

Examples:

- *fixVersion ~ "9.\*"* will find all issues from the 9.x line.
- *fixVersion ~ "\*9\*"* will find all issues that contain "9".

**Empty JQL queries**

Some of our users reported performance problems with their boards and gadgets, which were caused by filters using empty JQL queries. Running an empty JQL query retrieves all issues existing in your instance, which can seriously affect performance in large instances. To avoid that, we've added an option that lets you decide how an empty JQL query behaves: either returning all possible issues (like it is now), or no results at all. This option is available as one of the global settings (**Administration > System > General configuration**).

*Important:* If users choose to disable empty JQL queries, it might affect plugins that use such queries to "get all" on purpose. If you're a plugin developer and your plugin uses empty queries, you should change them to proper ones that retrieve accurate results. If you're a JIRA admin planning to disable empty JQL queries, make sure to check that plugins in your instance are not affected. 

**New events in the audit log** (JIRA Software)

We've added the following events to the audit log:

- boards (created, deleted)
- sprints (deleted)

**Delimiters for CSV** (JIRA Software)

We've added an option that lets you choose one of the four most commonly delimiters when exporting your issues to CSV. These are *comma*, *semicolon*, *vertical bar*, and *carret*. An extra dialog showing these delimiters will be displayed every time you export search results to CSV (run a search, and then click **Export > CSV**). If you've automated exports or are running a lot of them, you can disable the extra dialog in global settings. If you disable it, *comma* will be used as the default delimiter.

