---
aliases:
- /server/jira/platform/index-document-configuration-plugin-module-26936506.html
- /server/jira/platform/index-document-configuration-plugin-module-26936506.md
category: reference
confluence_id: 26936506
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26936506
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26936506
date: '2017-12-08'
legacy_title: Index Document Configuration Plugin Module
platform: server
product: jira
subcategory: modules
title: Index document configuration
---
# Index document configuration

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The Index Document Configuration plugin module is available in JIRA 6.2 and later.</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

The Index Document Configuration plugin module allows add-ons to add key/value stores to JIRA entities, such as issues or projects. These values are indexed by JIRA and can be queried via a REST API or through JQL. For more information, please see the <a href="https://docs.atlassian.com/jira/REST/ondemand/#d2e529" class="external-link">JIRA documentation on entity properties</a>.

## Configuration

The root element for the index document configuration plugin module is `index-document-configuration`. It allows the following attributes and child elements for configuration:

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>entity-key</p></td>
<td><p>The type of the property. Currently, the only available value is <code>IssueProperty</code></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import
  key=&quot;appProps&quot;
  interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. That is, the identifier of the index document configuration module.</p></td>
</tr>
</tbody>
</table>

**\*all the attributes above are required.**

**Elements**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>References to the values of the JSON object which will be indexed, and the types of the referenced values (<a href="#key-elements">Key elements</a>).</p></td>
</tr>
</tbody>
</table>

**\*key element is required.**

#### Key elements

Keys of the entity properties can be added to `index-document-configuration` as elements. Each element defines the key of the property from which the data is indexed (`property-key`) and a list that has references to values of the JSON object which will be indexed (`extract`).

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>property-key</p></td>
<td><p>The key of the property from which the data is indexed.</p></td>
</tr>
</tbody>
</table>

**\*property-key attribute is required.**

**Elements**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="#e">e</a>xtract</p></td>
<td><p>References to values of the JSON object which will be indexed, and the type of referenced value (<a href="#extract-elements">Extract elements</a> ).</p></td>
</tr>
</tbody>
</table>

**\*name element is required.**

#### Extract elements

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>path</p></td>
<td><p>The path of the JSON data that will be indexed. The path will be the key of a flattened JSON object that is delimited by '.'</p>
<p>For instance, for JSON shown below, the valid path referencing the <code>color</code> is <code>label.color</code>.</p>
<pre><code>{&quot;label&quot;: {&quot;color&quot;: &quot;red&quot;, &quot;name&quot;:&quot;jira-6.2&quot;}}</code></pre>
<p>Currently, specifying indexes for JSON arrays is not supported.</p></td>
</tr>
<tr class="even">
<td><p>type</p></td>
<td><p>The type of the referenced value. The type can be one of the following values:</p>
<ul>
<li>number, which is indexed as a number and allows range ordering and searching on this field.</li>
<li>text, which is tokenized before indexing and allows searching for particular words.</li>
<li>string, which is indexed as is and allows searching for the exact phase only.</li>
<li>date, which is indexed as a date and allows date range searching and ordering. The expected date format is <code>[YYYY]-[MM]-[DD]</code>. The expected date time format is <code>[YYYY]-[MM]-[DD]T[hh]:[mm]</code> with optional offset from UTC: <code>+/-[hh]:[mm]</code> or `<code>Z</code>` for no offset. For reference, please see [ISO_8601 standard](<a href="http://www.w3.org/TR/NOTE-datetime" class="uri external-link">http://www.w3.org/TR/NOTE-datetime</a>).</li>
</ul></td>
</tr>
<tr class="odd">
<td>alias</td>
<td><p>The name under which the property will appear in JQL search.</p>
<p><strong>Since:</strong> JIRA 6.4</p></td>
</tr>
</tbody>
</table>

**\*path and type attributes are required.**

## Index Document Configuration example

An index configuration that indexes labels by the exact label text:

``` xml
<index-document-configuration entity-key="IssueProperty" key="jira-issue-label-indexing">
  <key property-key="label">
    <extract path="name" type="text" alias="labelName"/>
  </key>
</index-document-configuration>
```

This code should be placed in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/).

A configuration like this makes issues searchable by their label names (assuming that the property `label` is
  stored against the issue).

For a property with the value:

``` javascript
{"color": "red", "name":"jira-6.2"}
```

stored under the `label` key, the JQL to search for the issues are: 

-   **issue.property\[label\].name = "jira-6.2". **This is a fully qualified name of the issue.property. Fully qualified names do not appear in autocompletion.
-   **labelName = "jira-6.2". **This is the alias of this issue.property. Aliases appear in JQL autocompletion.
