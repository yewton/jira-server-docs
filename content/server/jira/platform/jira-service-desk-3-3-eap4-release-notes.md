---
aliases:
- /server/jira/platform/jira-service-desk-3.3-eap4-release-notes-44058814.html
- /server/jira/platform/jira-service-desk-3.3-eap4-release-notes-44058814.md
category: devguide
confluence_id: 44058814
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44058814
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44058814
date: '2017-12-08'
legacy_title: JIRA Service Desk 3.3 EAP4 release notes
platform: server
product: jira
subcategory: updates
title: JIRA Service Desk 3.3 EAP4 release notes
---
# JIRA Service Desk 3.3 EAP4 release notes

**17 November 2016**

Atlassian is proud to present **JIRA Service Desk 3.3 EAP4**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Service Desk 3.3** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

JIRA Service Desk 3.3 also includes JIRA 7.3 platform changes, and for the full list of changes for JIRA 7.3, read the developer change management guide: [Preparing for JIRA 7.3](/server/jira/platform/preparing-for-jira-7-3). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10240" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.3 EAP`

{{% /note %}}

## Portal updates

### Agents can add announcements to a portal or help center

Administrators can enable announcements for agents, and agents can keep customers informed about outages, updates, and holiday hours. 

-   To allow agents to add announcements to the help center, JIRA administrators can go to ![](/server/jira/platform/images/bluecog-icon.png)** &gt; Applications &gt; JIRA Service Desk Configuration**.
-   To allow agents to add announcements to the portal, project administrators can go to **Project settings &gt; Portal settings**. Both project administrators and agents can now add portal announcements from the **portal &gt; Edit announcement**, not **Portal settings**.

### Customers can select their language

Customers can change their language preference in the portal by selecting **Profile** under their profile avatar, and then choosing to **Edit** their profile. This means the portal and future notifications will be sent to them in their chosen language.

## Group customers in organizations

Organizations are groups of customers that you can add to multiple projects. Customers can be members of multiple organizations, and can:

-   raise requests in all projects that use the organization.  
-   view and search the organization's requests from the **My Requests** page in the portal.
-   receive notifications about the organization's requests.
-   share requests with the organization.

If you have the Service Desk Team role for a project, you can manage organizations from its **Customers** list ( <img src="/server/jira/platform/images/orgs.png" class="confluence-thumbnail" width="28" /> ).

### Other updates for organizations

-   In the customer portal, **People involved** is now called **Shared with**. In issues, organizations that customers share with display in the new **Organizations** custom field.
-   In Project settings, **Request security** is now called **Customer permissions**.
-   In the Customers list, **Invite customers** is now called **Add customers**.
-   Agents don't display on the Customers list.
-   In the customer portal, approval notifications display in the **Requests** menu.

There are two new JQL queries that you can use to create queues, SLAs and reports for organizations:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Query
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Organizations = &quot;<em>organization name</em>&quot; </td>
<td><p>Returns all issues shared with an organization.</p>
<p><strong>Example:</strong> Organizations = &quot;Charlie Cakes Melbourne&quot;.</p></td>
</tr>
<tr class="even">
<td><em>single user field</em> in organizationMembers(&quot;<em>organization name</em>&quot;)</td>
<td><p>Returns all issues where the user is a member of the organization.</p>
<p><strong>Example:</strong> Reporter in organizationMembers(&quot;Charlie Cakes Melbourne&quot;).</p></td>
</tr>
</tbody>
</table>

### For developers

<a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/#servicedeskapi/organization" class="external-link">Public APIs for organizations</a><br>
<a href="https://statlas.atlassian.io/jsdrest/index.html" class="external-link">Public Java service for organizations</a>

## Customize your email notifications with style

You <a href="https://jira.atlassian.com/browse/JSD-4418?filter=-3&amp;jql=issue%20in%20(jsd-4418%2C%20jsd-218)" class="external-link">asked for it</a>, we built it! We've added a new page to your project settings called **Customer notifications**. Here, you can edit the messages that are sent as your project's notifications. You can:

-   create your own custom rules in automation and send your customers very specific notifications.
-   change the default subject line and content of your notifications.
-   add custom styling and use markdown formatting.
-   manage languages and add translations if required

This has been a very popular request, and we're happy to put more power into the hands of our customers. Now you can send very specific email notifications, that match the voice and tone of your organization.

## Rich text editing

We released rich text editing as an opt-in labs feature in JIRA Service Desk 7.2, and now we're pleased to announce that rich text editing has graduated to the default editing experience in JIRA Service Desk. This means when you upgrade or install JIRA Service Desk 7.3, the rich text editor will be turned on by default. This gives your users the option to either use Visual mode (what you see is what you get) or Text mode (wiki markdown). We've retained the ability for administrators to disable rich text editing if they wish, this will disable it for the entire instance, and all your users. 

To disable the rich text editor, select ![](/server/jira/platform/images/bluecog-icon.png) **&gt; System**, and select **Rich text editor** from the sidebar.

### For developers

We've added API's for the rich text editor, and our developers have written some tutorials on how you can create an add-on to work with the rich text editor, from basic use cases to more advanced use cases. [Check out the tutorials](/server/jira/platform/rich-text-editor-44053984.html) to get yourself up to speed.

## Project level administration

We've extended the project administrators permission, so that project administrators can now edit their projects workflow under certain conditions:

-   The workflow must not be shared with any other projects
-   The workflow cannot be the JIRA default system workflow, which cannot be edited at all

The project admins will not be able to edit the workflow to the same extent as a JIRA administrator. The restrictions are:

-   To add a status, the status must already exist in the JIRA instance i.e. the project admin can't create new statuses or edit existing statuses
-   The project admin isn't allowed to delete a status from the workflow
-   The project admin can create, update or delete transitions, but they can't select or update a screen used by the transition, or edit a transition's properties, conditions, validators or post-functions

This feature is enabled by default. When you upgrade to JIRA Service Desk 7.3, all your project administrators will have access to this feature immediately. To edit a workflow, a project administrator should select **Project settings** in their project's sidebar, and select an issue type. This will present the workflow, and **Edit** will be available.

## Starting a JIRA instance

We've added two features that relate to starting JIRA:

1.  We've added an extra step to the JIRA install/upgrade process that means you need to actively choose to start JIRA.
2.  We've added a feature that lets you start JIRA manually with all non-system add-ons disabled.

#### 1. Installing and upgrading start up

Now when you install or upgrade a JIRA instance, you need to actively select to start JIRA. Previously, once JIRA ran the installation/upgrade, it would start JIRA automatically, before asking you if you'd like to launch JIRA in a browser. Now, there's an additional step where JIRA will ask if you want to start JIRA, and if you choose to start it, it will ask if you want to launch JIRA in a browser, This change was required to allow us to work with Amazon Web Services (AWS) to provide a<a href="https://aws.amazon.com/quickstart/architecture/jira/" class="external-link"> Quick Start</a> guide using CloudFormation templates to allow you to deploy JIRA Software Data Center in an AWS environment.

#### 2. Disabled non-system add-ons

We've added a feature that allows you to start a JIRA instance with all user installed and custom add-ons disabled, or with a specified list of these add-ons disabled:

1.  `--disable-all-addons` (or `/disablealladdons` for Windows users)

2.  `--disable-addons=<addon keys>` or (`/disableaddons=<addon keys>` for Windows users)

This feature is designed to help with upgrades and installation issues whereby an add-on failing during JIRA startup stops your JIRA instance from starting. You'll be able to disable the add-ons, start JIRA and manually remove the add-on through the add-on manager. The parameters are designed to be specified at system startup when JIRA is started using the start-jira.sh script (or start-jira.bat for Windows), for example:

``` bash
./bin/start-jira.sh --disable-all-addons --disable-addons=com.atlassian.test.plugin
```

If a customer does not use start-jira.sh for starting JIRA but still wishes to use this feature, they can use these features by adding the following JVM parameter to the invocation of the servlet container that's running JIRA:

``` bash
-Datlassian.plugins.startup.options="--disable-all-addons --disable-addons=com.atlassian.test.plugin"
```

To disable **multiple plugins**, use a colon separated list of plugins. Regex/wildcards are not permitted, the full key of the plugin must be provided, for example:

``` bash
-Datlassian.plugins.startup.options=
"--disable-all-addons --disable-addons=com.atlassian.test.plugin:com.atlassian.another.test.plugin"
```

The parameters do not persist i.e. when you shut JIRA down and start it up again, the parameters are removed and JIRA will attempt to start all your add-ons as per a normal start up.

## Other improvements

-   We added a trigger to automate events when an issue comment is edited.
-   We added a new metric for reporting on success of your SLAs. Go to **Project settings&gt; Reports** and try making a report with the new % met series.
-   You can now create and link to a Confluence knowledge base directly from your project sidebar.
-   We expanded the set of fields you can add using automation rules. You can now edit components or labels in the **Edit issue** THEN action. 

## JIRA Core changes that affect JIRA Service Desk

All of the changes in the JIRA Core 7.3 EAP4 are also in the JIRA Service Desk 3.3 EAP4 unless otherwise stated. Read the [JIRA Core 7.3 EAP4 release notes](/server/jira/platform/jira-core-7-3-eap4-release-notes) for details.
