---
aliases:
- /server/jira/platform/4227138.html
- /server/jira/platform/4227138.md
category: devguide
confluence_id: 4227138
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227138
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227138
date: '2017-12-08'
legacy_title: How do I find issues with field X?
platform: server
product: jira
subcategory: other
title: How do I find issues with field X?
---
# How do I find issues with field X?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I find issues with field X?

You can find more information about searching in JIRA at [How to search in a plugin](/server/jira/platform/how-to-search-in-a-plugin.snippet).
