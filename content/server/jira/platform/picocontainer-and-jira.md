---
title: Picocontainer and JIRA
aliases:
    - /server/jira/platform/picocontainer-and-jira-4227118.html
    - /server/jira/platform/picocontainer-and-jira-4227118
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227118
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227118
confluence_id: 4227118
platform: server
product: jira
category: devguide
subcategory:
date: "2018-06-19"
---
# JIRA Developer Documentation: PicoContainer and JIRA

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Status:</p></td>
<td><p>This feature is <strong>DEPRECATED</strong> in favor of performing Dependency Injection via
<a href="https://bitbucket.org/atlassian/atlassian-spring-scanner/src/1.2.x/?_ga=2.95556572.1511644511.1529317752-1447739025.1521018839">Atlassian Spring Scanner</a>.</p></td>
</tr>
</tbody>
</table>

## Picocontainer

JIRA uses [Picocontainer](http://picocontainer.com/project.html) as a central object
factory. Picocontainer is responsible for instantiating objects and resolving their constructor dependencies.
This greatly simplifies code, in that any Picocontainer-instantiated object (eg. a Webwork action) can obtain an
instance of another (eg. a Manager class) simply by requesting one in its constructor. PicoContainer will ensure
each object required in the constructor is passed in (aka dependency injection). Eg. the ViewIssue action:

**ViewIssue.java**

``` javascript
public class ViewIssue extends AbstractViewIssue
{
    ....
    public ViewIssue(RepositoryManager repositoryManager, PermissionManager permissionManager, TrackbackManager trackbackManager,
                     ThumbnailManager thumbnailManager, SubTaskManager subTaskManager, IssueLinkManager issueLinkManager,
                     IssueLinkTypeManager issueLinkTypeManager, VoteManager voteManager, WatcherManager watcherManager,
                     PluginManager pluginManager)
   {
        super(issueLinkManager, subTaskManager);
        this.trackbackManager = trackbackManager;
        this.thumbnailManager = thumbnailManager;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.pluginManager = pluginManager;
        this.pagerManager = new PagerManager(ActionContext.getSession()); 
        this.repositoryManager = repositoryManager;
        this.permissionManager = permissionManager;
        this.voteManager = voteManager;
        this.watcherManager = watcherManager;
    }
    ....
}
```

### Non-managed classes

Unable to render {include} The included page could not be found.

### Register new Picocontainer-managed classes

Picocontainer-managed classes need to be registered with Picocontainer. This happens automatically for Webwork
actions, but other classes need to be registered manually. This is done in ComponentRegistrar's registerComponents() method:

**ComponentManager.java**

``` javascript
public void registerComponents(final ComponentContainer register, final boolean startupOK)
{
    ...
        register.implementation(INTERNAL, EntityUtils.class);
        register.implementation(PROVIDED, AttachmentManager.class, DefaultAttachmentManager.class);
        register.implementation(PROVIDED, AttachmentService.class, DefaultAttachmentService.class);
        register.implementation(PROVIDED, ProjectService.class, DefaultProjectService.class);
        register.implementation(PROVIDED, FieldManager.class, DefaultFieldManager.class);
        register.implementation(PROVIDED, CustomFieldManager.class, DefaultCustomFieldManager.class);
        register.implementation(PROVIDED, CustomFieldService.class, DefaultCustomFieldService.class);
        register.implementation(PROVIDED, FieldScreenManager.class, DefaultFieldScreenManager.class);
        register.implementation(INTERNAL, DefaultFieldScreenStore.class);
        register.implementation(PROVIDED, MailThreadManager.class, MailThreadManagerImpl.class);
        register.implementation(PROVIDED, CvsRepositoryUtil.class, CvsRepositoryUtilImpl.class);
        register.implementation(INTERNAL, DefaultWebAttachmentManager.class);
        register.implementation(INTERNAL, I18nBean.class);// this is a candidate for removal (may not be used - SF 08/Oct/04)
        register.implementation(PROVIDED, I18nHelper.class, I18nBean.class);
        register.implementation(PROVIDED, I18nHelper.BeanFactory.class, I18nBean.CachingFactory.class);
        register.implementation(INTERNAL, JiraLocaleUtils.class);
        register.implementation(PROVIDED, LocaleManager.class, DefaultLocaleManager.class);
        register.implementation(INTERNAL, PingUrlFilterer.class);
    ...
}
```

Components can either by **INTERNAL** meaning that they will be available only to JIRA itself or **PROVIDED**
in which case they will also be available to plugins2 plugins.

Components are generally only registered in the ComponentRegistrar, if they are required in JIRA internally.
Plugin writers who wish to write to write their own components that can be injected in their plugin's classes
should use the [component plugin module](/server/jira/platform/component).

If you wanted to register your overridden version of a pico-registered class, you could just register yours instead
of the default in ComponentRegistrar above.

## Overriding components in JIRA with an extension pico-container

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Deprecated:</p></td>
<td><p>Please note that this technique is deprecated and unsupported from JIRA 4.3 onwards.</p></td>
</tr>
</tbody>
</table>

Sometimes it may be necessary for a plugin writer to override a component that JIRA ships with to provide some
custom behaviour. You can do this by providing an extension pico container via a jira-application.properties property.
In jira-application.properties, register an *extension container provider*:

``` javascript
jira.extension.container.provider = com.mycompany.jira.MyContainerProvider
```

In this class, you can register your own implementations of interfaces, which will be used in preference to the
defaults in ComponentManager:

**MyContainerProvider.java**

``` javascript
package com.mycompany.jira;

import org.picocontainer.PicoContainer;
import org.picocontainer.defaults.DefaultPicoContainer;
import com.atlassian.jira.config.component.ProfilingComponentAdapterFactory;
import com.atlassian.jira.web.action.issue.BugAssociatorPrefs;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.permission.PermissionSchemeManager;

import com.mycompany.jira.MyBugAssociatorPrefs;
import com.mycompany.jira.MyPermissionManager;
import com.mycompany.jira.MyPermissionSchemeManager;

public class MyContainerProvider implements ContainerProvider
{
    private DefaultPicoContainer container;

    public PicoContainer getContainer(PicoContainer parent)
    {
        if (container == null)
            buildContainer(parent);
        return container;
    }

    private void buildContainer(PicoContainer parent)
    {
        this.container = new DefaultPicoContainer(new ProfilingComponentAdapterFactory(), parent);
        container.registerComponentImplementation(BugAssociatorPrefs.class, MyBugAssociatorPrefs.class);
        container.registerComponentImplementation(PermissionManager.class, MyPermissionManager.class);
        container.registerComponentImplementation(PermissionSchemeManager.class, MyPermissionSchemeManager.class);
    }
}
```

Here we have registered our own implementations of three classes, after delegating to the default (so ours will
  take precedence). You can now keep MyContainerProvider and your modified com.mycompany.jira.\* classes in their
  own jar, which can be dropped into any JIRA instance to customise it to your needs.
