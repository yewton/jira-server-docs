---
title: "Jira Core 7.10 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-04-26"
---
# Jira Core 7.10 EAP release notes

*26 April 2018*

Atlassian is proud to present *Jira Core 7.10 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *Jira Core 7.10* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for Jira 7.10, read the developer change management guide: [Preparing for Jira 7.10](/server/jira/platform/preparing-for-jira-7-10). If you are new to Jira, you should also read our [Java API Policy for Jira](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both *from* and *to* EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of Jira, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.9.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### Jira gets a fresh, new look

We're bringing a refreshed look and feel to many of our Server and Data Center products, including the whole Jira family. This work is based on the new Atlassian design and includes things like updated colors, typography and icons. In Jira 7.10, we're mostly updating the following pages, but you'll also notice some smaller tweaks all around Jira. 

- Issue view
- Issue Navigator
- Global Issue Navigator
- Dashboard
- Projects list
- Login page
- Updated buttons, icons, and typography

**Things to watch out for if you're an app developer:**

- *Issue view:* We're now using AUI tabs instead of custom Jira elements.
- *Global Issue Navigator:* Some AUI overrides were removed from Jira.
- *Project Issue Navigator:* We've replaced the deprecated Toolbar1 with the new Toolbar2, removed the 'legacyIssueHeader' template, and replaced deprecated drop-downs with the Dropdown2 web component.
- *Browse projects:* We've changed the vertical navigation to the AUI one.

You can find more changes in the [AUI upgrade guide](https://docs.atlassian.com/aui/7.8.0/docs/upgrade-guide.html).

We haven't made any changes to the navigation, like you may have seen in our Cloud products, so you should still know your way around. As for the pages listed above, just have a look at these screenshots, or better yet install the EAP and see for yourself!

*Dashboard*

![Dashboard](/server/jira/platform/images/jira_dashboard.png)

*Global Issue Navigator*

![Global issue navigator](/server/jira/platform/images/jira_issue_navigator.png)

*Issue view*

![Issue view](/server/jira/platform/images/issue_view.png)
