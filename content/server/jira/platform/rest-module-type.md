---
aliases:
- /server/jira/platform/rest-plugin-module-type-4227207.html
- /server/jira/platform/rest-plugin-module-type-4227207.md
category: reference
confluence_id: 4227207
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227207
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227207
date: '2017-12-08'
legacy_title: REST Plugin Module Type
platform: server
product: jira
subcategory: modules
title: REST module type
---
# REST module type

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The REST plugin module is available only for OSGi-based plugins in JIRA 4.0 and above.</p></td>
</tr>
</tbody>
</table>

The REST plugin module allows plugin developers to create their own REST API for JIRA.

This module type is shared with other Atlassian products. See the common [REST Plugin Module](https://developer.atlassian.com/display/DOCS/REST+Plugin+Module) documentation for details.
