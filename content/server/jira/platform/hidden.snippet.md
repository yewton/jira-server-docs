---
aliases:
- /server/jira/platform/-hidden-24805494.html
- /server/jira/platform/-hidden-24805494.md
category: devguide
confluence_id: 24805494
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805494
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805494
date: '2017-12-08'
legacy_title: _Hidden
platform: server
product: jira
subcategory: other
title: _Hidden
---
# \_Hidden

**![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) The pages in this section need to be deleted. They are not visible to customers under Viewport.**

The documentation in this section does not apply to the latest version of JIRA (Cloud and Server) and has been archived. The functionality (e.g. APIs, plugin modules, etc) described on these pages may be unsupported or may not be present at all. 

Pages:

-   [Creating and Editing an Issue](/server/jira/platform/creating-and-editing-an-issue.snippet)
-   [Customising JIRA code](/server/jira/platform/customising-jira-code.snippet)
-   [DRAFTS or Problem Pages](/server/jira/platform/drafts-or-problem-pages.snippet)
-   [FAQ](/server/jira/platform/faq.snippet)
-   [The Shape of an Issue in JIRA REST APIs](/server/jira/platform/the-shape-of-an-issue-in-jira-rest-apis.snippet)
