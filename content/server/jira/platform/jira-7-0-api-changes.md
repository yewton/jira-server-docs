---
aliases:
- /server/jira/platform/jira-7.0-api-changes-34669640.html
- /server/jira/platform/jira-7.0-api-changes-34669640.md
category: devguide
confluence_id: 34669640
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=34669640
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=34669640
date: '2017-12-08'
legacy_title: JIRA 7.0 - API changes
platform: server
product: jira
subcategory: updates
title: JIRA 7.0 - API changes
---
# JIRA 7.0 - API changes

This page covers the **API changes** for **JIRA 7.0**. 

The changes are part of a larger set of developer changes to JIRA for 7.0. You should also read [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0) for an overview of the release, as well as [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-platform-changes) and [JIRA 7.0 - General changes](/server/jira/platform/jira-7-0-general-changes).

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<thead>
<tr class="header">
<th>Change*</th>
<th>Affected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="#soap-and-xml-rpc-apis-removed">SOAP and XML-RPC APIs removed</a></p></td>
<td><p>Add-ons/scripts that use the SOAP and XML-RPC APIs</p>
<p><strong>Risk level:</strong> high.</p></td>
</tr>
<tr class="even">
<td><p><a href="#user-replaced-with-applicationuser">User replaced with ApplicationUser</a></p></td>
<td><p>All add-ons.</p>
<p><strong>Risk level:</strong> high.</p></td>
</tr>
<tr class="odd">
<td><a href="#new-method-for-user-counts">New method for user counts</a></td>
<td><p>Add-ons that were using <code>UserUtil.getActiveUserCount</code> or <code>UserUtil.getTotalUserCount.</code></p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><a href="#methods-to-read-all-users-have-been-deprecated">Methods to read all users have been deprecated</a></td>
<td><p>Add-ons that read all users through <code>getAllUsers()</code> methods.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><p><a href="#jiralicensestore-removed">JiraLicenseStore removed</a></p></td>
<td><p>Add-ons that look up JIRA's license.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><p><a href="#licensedetails-changes">LicenseDetails changes</a> &ndash; getLicenseStatusMessage signature of deprecated methods has changed </p></td>
<td><p>Add-ons that call the deprecated methods.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><p><a href="#licensedetails-changes">LicenseDetails changes</a> &ndash; getMaximumNumberOfUsers() and isUnlimitedNumberOfUsers() removed </p></td>
<td><p>Add-ons that look up JIRA's license.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><p><a href="#removal-of-accessor-methods-for-managerfactory-and-componentmanager-classes">Removal of accessor methods for ManagerFactory and ComponentManager classes</a></p></td>
<td><p>Add-ons that call the deprecated methods.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><a href="#version-picker-classes-removed">Version picker classes removed</a>  </td>
<td><p>Add-ons that were using any of VersionCustomFieldRenderer, VersionCustomFieldSearchInputTransformer or VersionSearcher classes.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><p><a href="#userutil-hasexceededuserlimit-deleted">UserUtil::hasExceededUserLimit deleted</a></p></td>
<td><p>Add-ons which care about the number of users in the system or look up JIRA's license.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><p><a href="#user-creation-methods-in-userutil-deprecated">User creation methods in UserUtil deprecated</a> in favor of UserService::createUser</p></td>
<td><p>Add-ons that create users.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><a href="#version-object-changed-from-mutable-to-immutable">Version object changed from mutable to immutable</a></td>
<td><p>Add-ons that use the Version class.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><a href="#project-types-required-for-projects">Project types required for projects</a></td>
<td><p>Add-ons that create or update projects.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="even">
<td><a href="#changes-to-the-project-validation-creation-apis">Changes to the project validation/creation APIs</a></td>
<td><p>Add-ons that create projects.</p>
<p><strong>Risk level:</strong> medium.</p></td>
</tr>
<tr class="odd">
<td><p><a href="#licensedetails-changes">LicenseDetails changes</a> &ndash; hasLicenseTooOldForBuildConfirmationBeenDone(), isLicenseSet() and getSupportRequestMessage() have been removed without alternative</p></td>
<td><p>Add-ons that call these methods.</p>
<p><strong>Risk level:</strong> low.</p></td>
</tr>
<tr class="even">
<td><a href="#strings-removed-from-apkeys-interface">Strings removed from APKeys interface</a></td>
<td><p>Nobody should be affected &ndash; this has not been used, except in setup, since JIRA 3.13.x.</p>
<p><strong>Risk level:</strong> low.</p></td>
</tr>
<tr class="odd">
<td><a href="#newlicenseevent-renamed-to-licensechangedevent">NewLicenseEvent renamed to LicenseChangedEvent</a></td>
<td><p>Add-ons that listens to NewLicenseEvent.</p>
<p><strong>Risk level:</strong> low.</p></td>
</tr>
<tr class="even">
<td><p><a href="#licensedetails-changes">LicenseDetails changes</a> &ndash; new method added: hasApplication(ApplicationKey application)</p></td>
<td><strong>Risk level:</strong> low.</td>
</tr>
<tr class="odd">
<td><p><a href="#atlassian-plugins-webresourcemodule-deprecated-and-may-be-removed-in-7-x">atlassian-plugins-webresourcemodule deprecated and may be removed in 7.x</a></p></td>
<td><p>Add-ons that use web-resource APIs in Java.</p>
<p><strong>Risk level:</strong> low.</p></td>
</tr>
</tbody>
</table>

**\*all the changes above start from the 7.0 version.**

## Details

### SOAP and XML-RPC APIs removed

As previously advised, the SOAP and XML-RPC APIs that were deprecated in JIRA 6.0 will be removed in JIRA 7.0. We are committed to providing feature parity for these APIs in the REST APIs by 7.0 and recommend that you use the [About the JIRA REST APIs](/server/jira/platform/rest-apis). See our [JIRA SOAP to REST Migration Guide](/server/jira/platform/jira-soap-to-rest-migration-guide) for help.

For further details, see the [original announcement](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice).

###  User replaced with ApplicationUser

Any JIRA API that uses the `com.atlassian.crowd.embedded.api.User` class will either be removed or replaced with a new version that uses `com.atlassian.jira.user.ApplicationUser` instead.

Many existing methods will have their return types changed to `ApplicationUser`. `User` is deprecated in jira-api and will likely be removed in a future version.

###  New method for user counts

We've deprecated the method `UserUtil.getActiveUserCount` and introduced a replacement interface and method: `LicenseCountService.totalBillableUsers`. In the future, we plan to add more support for Connect-style add-ons, the implementation of which requires the consumption of a user. The `totalBillableUsers` excludes these users and in our Cloud offering will also exclude Atlassian's support user. This method is intended to get a count of the "real" users, for example for checking that your add-on's license user count matches the number of users in JIRA. Note that the existing `UserUtil.getActiveUserCount` method now delegates to this method; its behaviour will be slightly different in 7.0 due to this.

The `UserUtil.getTotalUserCount` method has also been deprecated. Consumers should use `UserManager.getTotalUserCount`.

### Methods to read all users have been deprecated

-   com.atlassian.jira.user.util.UserManager.getAllApplicationUsers()
-   com.atlassian.jira.user.util.UserManager.getUsers()
-   com.atlassian.jira.user.util.UserManager.getAllUsers()
    -   Only retrieve users you really need.  Use `com.atlassian.jira.bc.user.search.UserSearchService` to search users.  
-   com.atlassian.jira.security.groups.GroupManager.getAllGroups()
    -   Only retrieve users and groups you really need. Use `com.atlassian.jira.bc.group.search.GroupPickerSearchService` to search groups.  
-   com.atlassian.jira.user.UserUtils.getAllUsers()
    -   Only retrieve users you really need.  Use `com.atlassian.jira.bc.user.search.UserSearchService` to search users.

In previous versions of JIRA, these methods retrieved users from an in-memory cache of all users, which was a relatively fast operation.  From JIRA 7.0, these methods will now retrieve lists of users from the database every call and will have degraded performance.

Additionally, the class `com.atlassian.jira.bc.user.search.UserPickerSearchService` has been deprecated, its functionality now exists within `com.atlassian.jira.bc.user.search.UserSearchService` with enhancements.

**Alternatives:**

As an alternative to retrieving all users, additional methods were added to `com.atlassian.jira.bc.user.search.UserSearchService` to allow more flexible searching of users:

-   `findUserKeysByFullName()`
-   `findUsersByFullName()`
-   `findUserKeysByEmail()`
-   `findUsersByEmail()`

The `findUsers(String nameQuery, String emailQuery, UserSearchParams userSearchParams)` can be used for more flexible searching.  `UserSearchParams` has the following additional properties:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>maxResults</code></td>
<td><p>Limits the number of results returned. </p>
<p>If only a small number of results are required, such as when displaying a user picker, using a small value here can dramatically increase performance as only a small amount of data will be retrieved from the database.</p></td>
</tr>
<tr class="even">
<td><code>postProcessingFilter</code></td>
<td><p>Filter results using arbitrary Java logic.  Set to this to a predicate that will filter out or keep particular users.</p>
<p>This filter is applied after results are retrieved from the database.  Using a filter is better than retrieving a large <code>List&lt;User&gt;</code> and filtering yourself since</p>
<ul>
<li>Not all users retrieved from the database need to be present in memory at the same time</li>
<li>It can be used in combination with maxResults to retrieve a particular number of users <em>after</em> filtering.</li>
</ul>
<p>It is only recommended to use the post processing filter when user filtering logic cannot be expressed using a <code>UserFilter</code> (which can filter by group) or by the <code>nameQuery</code> and <code>emailQuery</code> parameters of <code>findUsers()</code> as it is less efficient.</p></td>
</tr>
</tbody>
</table>

The user search service also retrieves users from the database and does not use an in-memory user cache.  For the best performance, ensure you retrieve only the users you need, and use parameters to filter results as necessary.

### `JiraLicenseStore` removed

The `JiraLicenseStore` class will be removed. No replacement class will be provided. If you need to look up specific license information, your add-on can do so via the `JiraLicenseManager` or `JiraLicenseService` classes.

### `LicenseDetails `changes

#### Methods removed without alternative

-   JIRA will not start if the license is not set. As a result, we have removed the `isLicenseSet`() method from the API.

-   `hasLicenseTooOldForBuildConfirmationBeenDone`() was not really intended for public usage and it does not match the multi-license world, so it has been removed.

-   `getSupportRequestMessage` does not have a direct substitute but new messages getters have been added.

#### Signature changes

| Old Signature                                                                                      | New Signature                                                                                                |
|----------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| `String getLicenseStatusMessage(@Nullable User user, String delimiter)`                            | `String getLicenseStatusMessage(@Nullable ApplicationUser user, String delimiter, UserManager userManager);` |
| `String getLicenseStatusMessage(I18nHelper i18n, @Nullable OutlookDate ignored, String delimiter)` | `LicenseStatusMessage getLicenseStatusMessage(I18nHelper i18n, UserManager userManager)`                     |

Also, `LicenseDetails` will not use the deprecated `User` class anymore. This has been replaced by the `ApplicationUser `class, as described in [User replaced with ApplicationUser](#user-replaced-with-applicationuser) above.

#### Methods removed with an alternative

The `isUnlimitedNumberOfUsers()` and `getMaximumNumberOfUsers()` methods will be removed from the `LicenseDetails `class. You will need to check the number of users per application in the license instead:

``` javascript
applications = licenseDetails.getLicensedApplications();
for (ApplicationKey applicationKey : applications.getKeys())
{
    userLimit = applications.getUserLimit(applicationKey);
}  
```

#### New methods

Refer to the [API documentation](/server/jira/platform/jira-apis-32344053.html) for more information about these new methods:

-   `getLicensedApplications()`
-   `LicenseStatusMessage getMaintenanceMessage(@Nonnull I18nHelper i18n, String applicationName);`
-   `String getLicenseExpiryStatusMessage(@Nullable ApplicationUser user);`
-   `Date getMaintenanceExpiryDate();`
-   `LicenseType getLicenseType();`
-   `String getApplicationDescription();`
-   `LicenseStatusMessage::hasAnyMessages();`

###  Removal of accessor methods for `ManagerFactory` and `ComponentManager` classes

The `ManagerFactory` and `ComponentManager` classes have been deprecated for some time. These classes were not intended to be part of the API, and in JIRA 7.0, the accessor methods for these classes will be removed.

Historically, add-ons have used these classes to get to JIRA components statically. If your add-on uses these classes, you should inject your components via the constructor injection instead. If this is not possible or practical, the <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/component/ComponentAccessor.html" class="external-link">ComponentAccessor</a> class provides the supported way to access components statically.

### Version picker classes removed

Some of the classes that implement the internal behaviour of the Version Picker custom fields were exposed on the public API. These will be removed in JIRA 7.0. The classes that will be removed are:

-   `com.atlassian.jira.issue.customfields.searchers.renderer.VersionCustomFieldRenderer`
-   `com.atlassian.jira.issue.customfields.searchers.transformer.VersionCustomFieldSearchInputTransformer`
-   `com.atlassian.jira.issue.customfields.searchers.VersionSearcher`

If your add-on is using these classes and needs to maintain that behaviour, you need to get your add-on to provide the definition of these classes. This means that your add-on needs to define these classes with the exact same behaviour that they have on JIRA 6.3.

### `UserUtil::hasExceededUserLimit` deleted

`UserUtil::hasExceededUserLimit` deleted in favor of `ApplicationAuthorizationService::isAnyRoleLimitExceeded`. 

### User creation methods in `UserUtil` deprecated

All user creation methods has been deprecated in `UserUtil` in favor of `UserService::createUser`.

``` javascript
final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(currentApplicationUser, username, password, email, displayName)
result = userService.validateCreateUser(createUserRequest);
if(result.isValid())
{
   userService.createUser(createUserRequest);
}
```

### `Version` object changed from mutable to immutable

If objects allow unnecessary modifications, it is harder to cache them or pass them between different components. To simplify the `Version` object, it will be changed from a mutable object to an immutable object in JIRA 7.0, and the respective builders provided.

### Project types required for projects

Every project in JIRA now has a project type. This means that you must provide a project type key when creating a project. 

-   If you are using the JIRA Java API, see the [Changes to the project validation/creation APIs](#changes-to-the-project-validation-creation-apis) section below for instructions.
-   If you are using the JIRA REST API, you must specify a `projectTypeKey` when calling the <a href="https://docs.atlassian.com/jira/REST/7.0.0-m01b/#api/2/project-createProject" class="external-link">Create project</a> method.

Valid values for the project type key are "`business`",  "`software`", and  "`service_desk`".

We've also added a new method to both the Java API and REST API that let you update the project type for a project. This is the only way to update the project type for an existing project, i.e. the update project methods won't modify the project type.

-   JIRA REST API: use the <a href="https://docs.atlassian.com/jira/REST/7.0.0-m01b/#api/2/project-updateProjectType" class="external-link">Update project type</a> method

-   JIRA Java API: use `com.atlassian.jira.project.ProjectManager#updateProjectType (ApplicationUser user, Project project, ProjectTypeKey newProjectType)`

### Changes to the project validation/creation APIs

In JIRA 7.0, the APIs used to validate the creation of a project and to actually create a project have changed. The biggest change is that all of the methods related with this functionality now take a single parameter object of type `com.atlassian.jira.bc.project.ProjectCreationData.`

The exact list of methods that have been changed are:

-   `com.atlassian.jira.bc.project.ProjectService#validateCreateProject(ApplicationUser user, ProjectCreationData projectCreationData)`
-   `com.atlassian.jira.bc.project.ProjectService#isValidAllProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData)`
-   `com.atlassian.jira.bc.project.ProjectService#isValidRequiredProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData)`
-   `com.atlassian.jira.project.ProjectManager#createProject(ProjectCreationData projectCreationData)`

Another very important change around project creation is that a project type key must now be specified. There are two ways to specify the project type when creating a project:

-   You can specify the type directly on `ProjectCreationData`
-   You can specify a project template to be used for the project in `ProjectCreationData`. In this case, the project type will be the one defined for the project template.

Validation will be performed against the project type. The identifier of the type must correspond with one of the project types available on the JIRA instance where the project is being created.   
*Note, validation isn't turned on by default in JIRA 7.0 EAP 01 (m01), so it will be possible to create a project without a valid project type if you are using this EAP milestone.*

Before 7.0, project creation had normally two "phases": Creating the project and applying a template over the new project. On 7.0, we are merging this two things in a such way that the project will be created and the template applied with a single call to `com.atlassian.jira.bc.project.ProjectService#createProject`.

### Strings removed from APKeys interface

The Strings `JIRA_LICENSE_V1_MESSAGE` ( *License Message Text* ), `JIRA_LICENSE_V1_HASH` ( *License Hash 1 Text* ), `JIRA_OLD_LICENSE_V1_MESSAGE` ( *License Message* ), and `JIRA_OLD_LICENSE_V1_HASH` ( *License Hash 1* ) will be removed from APKeys interface. These strings represent the old location of licenses in JIRA in versions prior to 4.0 and as they are no longer used they will be removed.

### NewLicenseEvent renamed to LicenseChangedEvent 

`com.atlassian.jira.license.NewLicenseEvent` will be renamed to `com.atlassian.jira.license.LicenseChangedEvent`

This change is to support new licenses as well as updated and removed licenses.

## `atlassian-plugins-webresource`module deprecated and may be removed in 7.x

The deprecated `atlassian-plugins-webresource` module is no longer supported and will be removed during 7.x. This includes the WebResourceManager interface. Add-ons should use the official API in `atlassian-plugins-webresource-api`. Since WebResources 3.0.0, the new web resource API has revolved around `PageBuilderService` in the `atlassian-plugins-webresource-api` module. This API module deprecates and replaces the `WebResourceManager` in `atlassian-plugins-webresource`. This new WebResource API has been supported since JIRA 6.1.

Note that this only impacts the Java API for WebResources. The `<web-resource>` module descriptor has not changed in 7.0 and is backwards compatible. 

In JIRA 7.0, we no longer guarentee that the old Java API will work. While `WebResourceManager` is still available in JIRA 7.0, it may be removed in a later 7.x release. Add-ons should use the new `PageBuilderService` service and depend on `atlassian-plugins-webresource-api` instead.

The following 8 classes are currently in the old `atlassian-plugins-webresource` module, but are considered stable and will be moved as-is to `atlassian-plugins-webresource-api`.

-   com.atlassian.plugin.webresource.QueryParams
-   com.atlassian.plugin.webresource.condition.UrlReadingCondition
-   com.atlassian.plugin.webresource.transformer.TransformableResource
-   com.atlassian.plugin.webresource.transformer.TransformerParameters
-   com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder
-   com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer
-   com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory
-   com.atlassian.plugin.webresource.url.UrlBuilder
