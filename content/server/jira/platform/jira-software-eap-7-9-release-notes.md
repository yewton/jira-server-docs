---
title: "JIRA Software 7.9 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-03-22"
---
# JIRA Software 7.9 EAP release notes

*21 March 2018*

Atlassian is proud to present *JIRA Software 7.9 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *JIRA Software 7.9* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.9, read the developer change management guide: [Preparing for JIRA 7.9](/server/jira/platform/preparing-for-jira-7-9). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.8.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### Kanban boards just got faster

Every issue on your board affects performance in some way, but not every one is really needed there. Often, completed issues clutter up the 'Done' column, which can slow down the board and make you scroll way too much. To fix that, we’re bringing what we’ve codenamed “Fast Kanban”—a way to keep your board fresh and clean, and as quick as a flash. 

<img src="/server/jira/platform/images/fastkanban.png"/>

The idea behind it is simple. The ‘Done’ column will now only show issues that were updated in any way within the last 2 weeks, hiding the rest. This should clear up a lot of noise and make your work environment more enjoyable. Any project admin can change the retention period (**Board > Configure > General**), or choose to display all issues, if they prefer. 

### Searching through versions with a wildcard

Searching through version fields with a wildcard was one of the most demanded features suggested by our users, and today we’re making it happen. Whether it’s 9, 9.1, or any other derivative you’re looking for, you can now find it with a simple query that uses the ~ operator and an asterisk to indicate the location of unknown characters (regardless of how many there are).

To illustrate this with an example, if you’d like to find all issues within the 9.x line, your search query would look like this: fixVersion ~ “9.*”. Go ahead, try it out, and bring some beauty to your queries!

### More improvements

#### Delimiters for CSV

You can choose one of the four most commonly used delimiters when exporting your issues to CSV. These are comma, semicolon, vertical bar, and caret (this one: ^). If you’d like to try it out, just search for some issues in JIRA, and then hit **Export > CSV** in the top-right corner. If you're a JIRA admin, you can also disable this feature in global settings.

#### Disabling empty JQL queries

When you use a filter with an empty JQL query, it will retrieve all possible issues, which can have a serious impact on performance if you throw a couple of these on your boards and gadgets. To avoid that, we’ve added a global setting that lets you decide how an empty JQL query behaves: either returning all issues (like it is now), or no results at all.

#### New events in the audit log

We've added new events for boards (created, deleted) and sprints (deleted). While riding this wave, we've also added info about who started a sprint to Burndown Chart and Sprint Report.