---
aliases:
- /server/jira/platform/preparing-for-jira-6.4-29956208.html
- /server/jira/platform/preparing-for-jira-6.4-29956208.md
category: devguide
confluence_id: 29956208
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29956208
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29956208
date: '2017-12-08'
legacy_title: Preparing for JIRA 6.4
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 6.4
---
# Preparing for JIRA 6.4

## Overview

This page is to inform JIRA plugin developers of changes in the upcoming JIRA 6.4 release that could affect their plugins. JIRA users should consult the documentation <a href="https://confluence.atlassian.com/display/JIRA/JIRA+Documentation" class="external-link">here</a>.

It is our intention to notify you, our plugin developers, **as far in advance as possible** of everything we know that could possibly affect your plugins. Where possible, we will attach release targets. EAP releases are available on the <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">JIRA Early Access Program Downloads</a> page.

We will update this page as the release progresses, so please **watch this page** **or check back regularly** to keep on top of the changes.

## Summary of changes

The risk level indicates the level of certainty we have that things will break if you are in the "What is affected?" column and you don't make the necessary changes. The changes listed in this section affect only Plugins 2 (not Atlassian Connect).

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 20%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>What is the change?</th>
<th>When will this happen?</th>
<th>What is affected?</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>JIRA Report Plugin Module changes</td>
<td>6.4</td>
<td><p>Plugins that expose Reports</p>
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#jira-report-plugin-module">JIRA Report Plugin Module</a> below</p></td>
</tr>
<tr class="even">
<td>NewLicenseEvent deprecated in favor of LicenseChangedEvent</td>
<td>Deprecated in 6.4 and to be removed in 7.0</td>
<td>Plugins that listen to NewLicenseEvent
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#license-event-renamed">License event renamed</a> below</p></td>
</tr>
<tr class="odd">
<td>The way JIRA and AUI messages are rendered is changing</td>
<td><p>6.4-OD-09 (Cloud)</p>
<p>6.4-m09 (JIRA EAP)</p></td>
<td><p>Plugins that:</p>
<ul>
<li>use JIRA.Messages to display messages; and</li>
<li>use hardcoded AUI messages markup (rather than the provided templates).</li>
</ul>
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#message-rendering-changes">Message rendering changes</a> below</p></td>
</tr>
<tr class="even">
<td>JIRA File based APIs for attachments deprecated</td>
<td>6.4</td>
<td><p>Plugins that:</p>
<ul>
<li>store attachments in JIRA; and</li>
<li>plugins that use TemporaryAttachmentsMonitor to upload temporary attachments and want to convert them to normal attachments.</li>
</ul>
<p><strong>Risk level: moderate</strong></p>
<p><strong>Details:</strong> See <a href="#jira-file-based-apis-for-attachments-deprecated">JIRA File based APIs for attachments deprecated</a> below</p></td>
</tr>
<tr class="odd">
<td>JIRA project-centric view</td>
<td>6.4</td>
<td>Plugins that use project navigation.
<p><strong>Risk level: moderate</strong></p>
<p><strong>Details:</strong> See <a href="/server/jira/platform/designing-for-the-jira-project-centric-view">Design guide - JIRA project-centric view</a></p></td>
</tr>
<tr class="even">
<td>Dropped support for Jelly scripts</td>
<td>6.4</td>
<td>Instances using this feature.
<p><strong>Risk level: moderate</strong></p>
<p><strong>Details:</strong> See the <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201411jelly" class="external-link">end of support announcement</a></p></td>
</tr>
<tr class="odd">
<td>Send Head Early for Dashboard and IssueNav</td>
<td>6.4</td>
<td><p>Plugins that manipulate the HTTP response (changing the HTTP status code, setting headers, etc) - either directly or indirectly - while the Dashboard, Issue Navigator or View Issue page is loading.</p>
<p><strong>Risk level: moderate</strong></p>
<p><strong>Details:</strong> See <a href="#send-head-early-for-dashboard-and-issuenav">Send Head Early for Dashboard and IssueNav</a> below</p></td>
</tr>
</tbody>
</table>

## Detailed Explanations of Changes

**In this section:**

-   [JIRA Report Plugin Module](#jira-report-plugin-module)
-   [License event renamed](#license-event-renamed)
-   [Message rendering changes](#message-rendering-changes)
-   [JIRA File based APIs for attachments deprecated](#jira-file-based-apis-for-attachments-deprecated)
-   [Send Head Early for Dashboard and IssueNav](#send-head-early-for-dashboard-and-issuenav)

### JIRA Report Plugin Module

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>category</td>
<td><p>In JIRA 6.4 reports are grouped in 4 categories:</p>
<ul>
<li>Agile - <code>agile</code></li>
<li>Issue analysis -<code> issue.analysis</code></li>
<li>Forecast &amp; management - <code>forecast.management</code></li>
<li>Other - <code>other</code></li>
</ul>
<p>Each report can be placed in a category. When no category is provided, the report will be placed in &quot;Other&quot;.</p>
<pre><code>&lt;category key=&quot;forecast.management&quot; /&gt;</code></pre>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td>thumbnail</td>
<td><p>In JIRA 6.4 reports will have the ability to provide a thumbnail, which will be displayed on the reports page. Thumbnails are controlled by css class, which can be defined in the Plugin Report Module and must have the dimensions of: <strong><code>268px</code></strong> x <strong><code>138px</code></strong></p>
<p>Corresponding web resources should be placed in context <code>com.atlassian.jira.project.reports.page.</code></p>
<pre><code>&lt;thumbnail cssClass=&quot;report-css-class&quot; /&gt;</code></pre>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td>url</td>
<td><p><code>url</code> element can contain the <code>provider</code> attribute which points to the class which will provide URLs for the given report depending on the context in which the report is presented. The class has to implement the <code>com.atlassian.jira.plugin.report.ReportUrlProvider</code> interface.</p>
<pre><code>&lt;url
  provider=&quot;com.atlassian.sample.plugin.MyUrlProvider&quot; /&gt;</code></pre>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

### License event renamed

`com.atlassian.jira.license.NewLicenseEvent` is scheduled to be replaced by `com.atlassian.jira.license.LicenseChangedEvent` in 7.0. This is to not only support new licenses but also updated and removed licenses. The new class LicenseChangedEvent has been introduced in 6.4 to allow for easier migration. NewLicenseEvent extends LicenseChangedEvent, so listeners should listen for LicenseChangedEvent where possible. In JIRA 6.4, all raised LicenseChangedEvents are also NewLicenseEvents, so there is no functional difference between listening to either one.

### Message rendering changes

#### Message icons

AUI message containers now have an associated icon added via a CSS pseudo element, implemented as an icon font character. Including a hard coded icon ( e.g. &lt;span class="aui-icon icon-info"&gt;&lt;/span&gt; ) in your AUI message markup will result in a double icon effect:

<img src="/server/jira/platform/images/screen-shot-2014-09-29-at-5.34.51-pm.png" width="500" />

Please use the AUI-provided message templates for building message markup, e.g. (as part of a soy template):

``` javascript
{call aui.message.info}
    {param closeable : false /}
    {param content}
        <p>Message content</p>
    {/param}
{/call}
```

For more information, please see <a href="https://docs.atlassian.com/aui/5.7.0/docs/messages.html" class="external-link">AUI Messages</a>.

#### JIRA messages & flags

Using the JIRA.Messages (or 'jira/message' via AMD) methods to show a global message will now result in an <a href="https://docs.atlassian.com/aui/latest/docs/flag.html" class="external-link">AUI flag</a> being displayed. Methods for displaying a message inside a target element will work the same as before. JIRA.Messages is deprecated and slated for removal in 8.0, though, so please prefer either 'jira/flag' or 'aui/flag' modules (obtained via the global require() method).

'jira/flag' adds the option to prevent flags from being re-shown once dismissed, by providing a dismissalKey like this:

``` javascript
flag.showMsg('Title', 'Body', { dismissalKey: 'dismiss.me' })
```

The following REST endpoint can be used to reset dismissals for a given flag:

``` javascript
JIRA.SmartAjax.makeRequest({type: 'PUT', url: '/rest/flags/1.0/flags/dismiss.me/reset'});
```

### JIRA File based APIs for attachments deprecated

Since JIRA 6.4 only stream based API should be used for manipulating attachments. Plugins should also not depend on structure of directories on the file system and should not assume that attachments are files stored on local files system. 

To read attachments you should use  `com``.atlassian.jira.issue.AttachmentManager#streamAttachmentContent` which accepts `InputStreamConsumer` that should be used to process stream of attachment content. You should never attempt to buffer/store content of attachments in memory.

#### Deprecated classes/method

##### `AttachmentManager`

All methods that accepted <a href="http://java.io/" class="external-link">java.io</a>`.File` are now deprecated, new methods that accept `CreateAttachmentParamsBean` with `InputStream` where added. New added methods does not throw exceptions in case of error but rather return `Either<AttachmentError, ChangeItemBean>` which defines result of the operation. 

##### `TemporaryAttachmentsMonitor` **and** WebAttachmentManager

You should be using `TemporaryWebAttachmentManager` instead to store temporary attachments in user session. Please note that attachments previously where identified by integer id, now they are identified by `TemporaryAttachmentId` which contains string id.

In regard to clearing temporary attachments, only a part of previous behavior was preserved. Temporary attachments are still automatically cleared when the user session expires.

Previously, a common practice was to identify a set of temporary attachments by issue id when attaching to an existing issue or by project id when attaching to a new issue. The server then could clear all attachments associated with such issue, e.g. when displaying a new "edit issue" or create issue" form. Such behavior interfered with multiple simultaneous forms. In order to isolate forms, it is recommended to generate a unique token for each form via `XsrfTokenGenerator` and pass it to `TemporaryWebAttachmentManager` methods: `createTemporaryWebAttachment` or `clearTemporaryAttachmentsByFormToken`.

### Send Head Early for Dashboard and IssueNav

JIRA's Dashboard, Issue Navigator and View Issue pages now employ the "Send Head Early" performance optimization strategy. Instead of constructing the HTML response in a buffer and only sending it when complete, they optimistically send HTTP 200 OK, all HTTP headers, and the start of the HTML page (most of the &lt;head&gt; section) as early as possible. This has the benefit of allowing the client to start loading and parsing resources referenced in the &lt;head&gt; section early, in parallel with server-side page construction. It also means that the HTTP response is committed much sooner - code that modifies the HTTP response during page construction is likely to cause a  `"java.lang.IllegalStateException: Response already committed`" exception in JIRA 6.4. This may happen indirectly. For example: an attempt to redirect (implies modification of the HTTP status code); an attempt to set a cookie (implies modification of the HTTP headers.)
