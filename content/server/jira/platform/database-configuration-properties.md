---
aliases:
- /server/jira/platform/database-configuration-properties-4227098.html
- /server/jira/platform/database-configuration-properties-4227098.md
category: reference
confluence_id: 4227098
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227098
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227098
date: '2017-12-08'
legacy_title: Database - Configuration properties
platform: server
product: jira
subcategory: database
title: Database - Configuration properties
---
# Database - Configuration properties

There are some global JIRA configuration settings, such as:

-   Search index path
-   Attachments path
-   Base URL
-   Settings configured in Administration -&gt; General Configuration
-   License info

These are all stored in a <a href="http://www.opensymphony.com/propertyset/" class="external-link">propertyset</a> on a virtual 'jira.properties' entity:

``` sql
mysql> select * from propertyentry where ENTITY_NAME='jira.properties';
+-------+-----------------+-----------+--------------------------------------+--------------+
| ID    | ENTITY_NAME     | ENTITY_ID | PROPERTY_KEY                         | propertytype |
+-------+-----------------+-----------+--------------------------------------+--------------+
| 10001 | jira.properties |         1 | webwork.i18n.encoding                |            5 | 
| 10000 | jira.properties |         1 | jira.i18n.language.index             |            5 | 
| 10002 | jira.properties |         1 | jira.title                           |            5 | 
| 10008 | jira.properties |         1 | jira.option.allowattachments         |            1 | 
| 10003 | jira.properties |         1 | jira.baseurl                         |            5 | 
| 10005 | jira.properties |         1 | jira.path.index                      |            5 | 
| 10006 | jira.properties |         1 | jira.option.indexing                 |            1 | 
| 10007 | jira.properties |         1 | jira.path.attachments                |            5 | 
| 10004 | jira.properties |         1 | jira.mode                            |            5 | 
| 10011 | jira.properties |         1 | jira.path.backup                     |            5 | 
| 10012 | jira.properties |         1 | License Message                      |            5 | 
| 10013 | jira.properties |         1 | License Hash 1                       |            5 | 
| 10018 | jira.properties |         1 | jira.option.user.externalmanagement  |            1 | 
| 10019 | jira.properties |         1 | jira.option.voting                   |            1 | 
| 10016 | jira.properties |         1 | jira.setup                           |            5 | 
| 10022 | jira.properties |         1 | jira.version.patched                 |            5 | 
| 10017 | jira.properties |         1 | jira.option.allowunassigned          |            1 | 
| 10020 | jira.properties |         1 | jira.option.watching                 |            1 | 
| 10021 | jira.properties |         1 | jira.option.issuelinking             |            1 | 
| 10023 | jira.properties |         1 | jira.option.cache.issues             |            1 | 
| 10024 | jira.properties |         1 | jira.issue.desc.environment          |            5 | 
| 10025 | jira.properties |         1 | jira.issue.desc.timetrack            |            5 | 
| 10027 | jira.properties |         1 | jira.timetracking.hours.per.day      |            5 | 
| 10028 | jira.properties |         1 | jira.issue.desc.original.timetrack   |            5 | 
| 10050 | jira.properties |         1 | jira.option.allowsubtasks            |            1 | 
| 10080 | jira.properties |         1 | jira.option.allowthumbnails          |            1 | 
| 10101 | jira.properties |         1 | jira.constant.default.resolution     |            5 | 
| 10026 | jira.properties |         1 | jira.timetracking.days.per.week      |            5 | 
| 10100 | jira.properties |         1 | jira.scheme.default.issue.type       |            5 | 
| 10120 | jira.properties |         1 | jira.option.emailvisible             |            5 | 
| 10150 | jira.properties |         1 | jira.sid.key                         |            5 | 
| 10161 | jira.properties |         1 | jira.trackback.exclude.pattern       |            5 | 
| 10151 | jira.properties |         1 | jira.lf.edit.version                 |            5 | 
| 10160 | jira.properties |         1 | jira.comment.level.visibility.groups |            1 | 
+-------+-----------------+-----------+--------------------------------------+--------------+
34 rows in set (0.02 sec)
```

The 'propertytype' column indicates which table stores the actual value for this property. 1 means 'propertynumber' and 5 means 'propertystring':

``` sql
mysql> select * from propertyentry pe, propertynumber ps where pe.id=ps.id and pe.ENTITY_NAME='jira.properties' and propertytype='1' ;
+-------+-----------------+-----------+--------------------------------------+--------------+-------+---------------+
| ID    | ENTITY_NAME     | ENTITY_ID | PROPERTY_KEY                         | propertytype | ID    | propertyvalue |
+-------+-----------------+-----------+--------------------------------------+--------------+-------+---------------+
| 10006 | jira.properties |         1 | jira.option.indexing                 |            1 | 10006 |             1 | 
| 10008 | jira.properties |         1 | jira.option.allowattachments         |            1 | 10008 |             1 | 
| 10018 | jira.properties |         1 | jira.option.user.externalmanagement  |            1 | 10018 |             0 | 
| 10017 | jira.properties |         1 | jira.option.allowunassigned          |            1 | 10017 |             0 | 
| 10019 | jira.properties |         1 | jira.option.voting                   |            1 | 10019 |             1 | 
| 10020 | jira.properties |         1 | jira.option.watching                 |            1 | 10020 |             1 | 
| 10021 | jira.properties |         1 | jira.option.issuelinking             |            1 | 10021 |             1 | 
| 10023 | jira.properties |         1 | jira.option.cache.issues             |            1 | 10023 |             0 | 
| 10050 | jira.properties |         1 | jira.option.allowsubtasks            |            1 | 10050 |             1 | 
| 10080 | jira.properties |         1 | jira.option.allowthumbnails          |            1 | 10080 |             1 | 
| 10160 | jira.properties |         1 | jira.comment.level.visibility.groups |            1 | 10160 |             1 | 
+-------+-----------------+-----------+--------------------------------------+--------------+-------+---------------+
11 rows in set (0.01 sec)
```

Here you can see that indexing is enabled (1), external user management off (0), subtasks enabled (1), etc.

``` sql
mysql> select PROPERTY_KEY, propertyvalue 
         from propertyentry pe, propertystring ps 
         where pe.id=ps.id and pe.ENTITY_NAME='jira.properties' and propertytype='5';
+-------+-----------------+-----------+--------------------------+--------------+-------+---------------------------------------------------+
| ID    | ENTITY_NAME     | ENTITY_ID | PROPERTY_KEY             | propertytype | ID    | propertyvalue                                     |
+-------+-----------------+-----------+--------------------------+--------------+-------+---------------------------------------------------+
| 10004 | jira.properties |         1 | jira.mode                |            5 | 10004 | public                                            | 
| 10000 | jira.properties |         1 | jira.i18n.language.index |            5 | 10000 | english                                           | 
| 10001 | jira.properties |         1 | webwork.i18n.encoding    |            5 | 10001 | UTF-8                                             | 
| 10002 | jira.properties |         1 | jira.title               |            5 | 10002 | Your Company JIRA                                 | 
| 10003 | jira.properties |         1 | jira.baseurl             |            5 | 10003 | http://localhost:8080/jira                        | 
| 10005 | jira.properties |         1 | jira.path.index          |            5 | 10005 | /home/jturner/jira/cleandb/ent/3.7.2/index       | 
| 10007 | jira.properties |         1 | jira.path.attachments    |            5 | 10007 | /home/jturner/jira/cleandb/ent/3.7.2/attachments | 
| 10011 | jira.properties |         1 | jira.path.backup         |            5 | 10011 | /home/jturner/jirabackups                                            
...
| 10022 | jira.properties |         1 | jira.version.patched               |            5 | 10022 | 186 
...
mysql> 
```

### JIRA build versions and upgrading

One important property stored is the build number:

``` sql
mysql> select * from propertyentry pe, propertystring ps where pe.id=ps.id and pe.ENTITY_NAME='jira.properties' and pe.PROPERTY_KEY='jira.version.patched';
+-------+-----------------+-----------+----------------------+--------------+-------+---------------+
| ID    | ENTITY_NAME     | ENTITY_ID | PROPERTY_KEY         | propertytype | ID    | propertyvalue |
+-------+-----------------+-----------+----------------------+--------------+-------+---------------+
| 10022 | jira.properties |         1 | jira.version.patched |            5 | 10022 | 186           | 
+-------+-----------------+-----------+----------------------+--------------+-------+---------------+
1 row in set (0.00 sec)
```

The build number corresponds to a JIRA version. In the footer of JIRA pages you'll see this build number after the version, eg. "Version: 3.7.2-\#186".

The build number is mostly relevant when upgrading. JIRA will run "upgrade tasks" when it detects that the JIRA's data is from an older version, and it does this by comparing the build number in the database with its own.

When an upgrade task is successfully run, it is recorded in the upgradehistory table:

``` sql
mysql> select * from upgradehistory;
+-------+-----------------------------------------------------------------+
| ID    | UPGRADECLASS                                                    |
+-------+-----------------------------------------------------------------+
| 10000 | com.atlassian.jira.upgrade.tasks.UpgradeTask1_2                 | 
| 10001 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build10            | 
| 10080 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build100           | 
| 10081 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build101           | 
| 10082 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build102           | 
| 10085 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build105           | 
| 10002 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build11            | 
| 10083 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build103           | 
| 10084 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build104           | 
| 10101 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build133           | 
....
| 10074 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build96            | 
| 10010 | com.atlassian.jira.upgrade.tasks.enterprise.UpgradeTask_Build47 | 
| 10040 | com.atlassian.jira.upgrade.tasks.enterprise.UpgradeTask_Build69 | 
| 10071 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build93            | 
| 10073 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build95            | 
| 10075 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build98            | 
| 10061 | com.atlassian.jira.upgrade.tasks.enterprise.UpgradeTask_Build84 | 
| 10065 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build89            | 
| 10140 | com.atlassian.jira.upgrade.tasks.UpgradeTask_Build186           | 
+-------+-----------------------------------------------------------------+
52 rows in set (0.02 sec)
```
