---
aliases:
- /server/jira/platform/39989419.html
- /server/jira/platform/39989419.md
category: devguide
confluence_id: 39989419
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39989419
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39989419
date: '2017-12-08'
legacy_title: JIRA Software 7.2 EAP 01 (m01) release notes
platform: server
product: jira
subcategory: updates
title: JIRA Software 7.2 EAP 01 (m01) release notes
---
# JIRA Software 7.2 EAP 01 (m01) release notes

**30 May 2016**

Atlassian is proud to present **JIRA Software 7.2 EAP 01 (m01)**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA 7.2** release. We are making these EAP milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.2, read the developer change management guide: [Preparing for JIRA 7.2](/server/jira/platform/preparing-for-jira-7-2). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

[<img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" />](#downloads)

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.2 EAP`

{{% /note %}}

## Editable issue detail view

When you view a JIRA Software board and select an issue, the details of the issue display in the issue detail view to the right of your screen. Previously, you were limited to the fields you could edit in the issue detail view, now we've added the ability to edit the issue from this view. This keeps you in the context of your board, which lets you work that little bit more efficiently and effectively. This also allows us to close a long running feature request with over <a href="https://jira.atlassian.com/browse/JSW-7893" class="external-link">340 votes</a>! 

## Copy sprint value

When cloning an issue that has already been added to sprint, you can now decide whether you'd like to copy the sprint value (the sprint it's assigned to) when you create the clone, effectively adding the cloned issue to the sprint. Simply select the relevant check box when cloning the issue.

<img src="/server/jira/platform/images/clonesprintvalue7.2.png" class="image-center" width="400" height="153" />

## Improvements

We've made a number of improvements which allow you to use JIRA Software that little bit more efficiently. You can now:

-   Search your backlog by assignee
-   Delete issue from on a board via the right-click menu
-   Base your board's swim lanes on projects

## JIRA Core changes that affect JIRA Software

All of the changes in the JIRA Core 7.2 EAP are also in the JIRA Software 7.2 EAP unless otherwise stated below. Read the [JIRA Core 7.2 EAP 01 (m01) Release Notes](/server/jira/platform/jira-core-7-2-eap-01-m01-release-notes) for details. 

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review *all* applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Core 7.1</a>
-   <a href="https://confluence.atlassian.com/migration/jira-7/server_jira_upgrade" class="external-link">Upgrading to JIRA Core 7.0</a>
-   <a href="https://confluence.atlassian.com/display/JIRA064/Important+Version-Specific+Upgrade+Notes" class="external-link">Upgrading from JIRA 6.3 or earlier</a>

## Downloads

-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712-x32.exe" class="external-link">JIRA Software 7.2.0-m01 (Windows 32 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712-x64.exe" class="external-link">JIRA Software 7.2.0-m01 (Windows 64 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712-x32.bin" class="external-link">JIRA Software 7.2.0-m01 (Linux 32 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712-x64.bin" class="external-link">JIRA Software 7.2.0-m01 (Linux 64 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712.zip" class="external-link">JIRA Software 7.2.0-m01 (ZIP Archive)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.2-EAP20160530074712-jira-7.2-EAP20160530074712.tar.gz" class="external-link">JIRA Software 7.2.0-m01 (TAR.GZ Archive)</a>
