---
aliases:
- /server/jira/platform/jira-build-information-45520501.html
- /server/jira/platform/jira-build-information-45520501.md
category: devguide
confluence_id: 45520501
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=45520501
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=45520501
date: '2017-12-08'
legacy_title: JIRA build information
platform: server
product: jira
subcategory: blocks
title: Jira build information
---
# Jira build information

Sometimes it is helpful to know the exact versions and build numbers of Jira releases. 

You can view a list of all the build information we've collected about Jira in [this kb article](https://confluence.atlassian.com/display/JIRAKB/JIRA+Build+and+Version+Numbers+Reference).
