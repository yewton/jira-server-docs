---
title: JIRA 7.4 Eap Release Notes 48991263
aliases:
    - /server/jira/platform/jira-7.4-eap-release-notes-48991263.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=48991263
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=48991263
confluence_id: 48991263
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.4 EAP release notes

Find the release notes for the JIRA 7.4 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.4 EAP release notes](/server/jira/platform/jira-core-7-4-eap-release-notes)
-   [JIRA Software 7.4 EAP release notes](/server/jira/platform/jira-software-7-4-eap-release-notes)
