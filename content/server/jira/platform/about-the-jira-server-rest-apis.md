---
aliases:
- /server/jira/platform/about-the-jira-rest-apis-7372883.html
- /server/jira/platform/about-the-jira-rest-apis-7372883.md
category: reference
confluence_id: 7372883
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7372883
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7372883
date: '2017-12-08'
legacy_title: About the JIRA REST APIs
platform: server
product: jira
subcategory: api
title: About the JIRA Server REST APIs
---
# About the JIRA Server REST APIs

The JIRA REST APIs are used to interact with the JIRA Server applications remotely, for example, when configuring webhooks. The JIRA Server platform provides a REST API for common features, like issues and workflows. Read the reference documentation to get started:

<a href="https://docs.atlassian.com/jira/REST/server/" class="external-link">JIRA Server platform REST API</a>

The JIRA Software and JIRA Service Desk applications have REST APIs for their application-specific features, like sprints (JIRA Software) or customer requests (JIRA Service Desk).

-   <a href="https://docs.atlassian.com/jira-software/REST/server/" class="external-link">JIRA Software Server REST API</a>
-   <a href="https://docs.atlassian.com/jira-servicedesk/REST/server/" class="external-link">JIRA Service Desk Server REST API</a>

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

*Note, the JIRA Server platform REST API resources are also documented in this WADL file: <a href="https://docs.atlassian.com/jira/REST/server/jira-rest-plugin.wadl" class="external-link">jira-rest-plugin.wadl</a>.*

## Authentication and authorization

-   **Authentication:** The following authentication methods are supported for the JIRA REST APIs: [OAuth 1.0a](/server/jira/platform/oauth) and [basic authentication](/server/jira/platform/basic-authentication).
-   **Authorization:** Authorization is based on the user used in the authentication process when you are calling the JIRA REST APIs.

For more information on authentication and authorization, read the [Security overview](/server/jira/platform/security-overview).

## URI structure

JIRA's REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. The JIRA REST API uses JSON as its communication format, and the standard HTTP methods like `GET`, `PUT`, `POST` and `DELETE`. URIs for JIRA's REST API resource have the following structure:

``` bash
http://host:port/context/rest/api-name/api-version/resource-name
```

Currently there are two API names available, which will be discussed further below:

-   `auth:` For authentication-related operations, and
-   `api:` For everything else.

The current API version is `2`. However, there is also a symbolic version, called `latest`, which resolves to the latest version supported by the given JIRA instance. As an example, if you wanted to retrieve the JSON representation of issue JRA-9 from Atlassian's public issue tracker, you would access:

``` bash
https://jira.atlassian.com/rest/api/latest/issue/JRA-9
```

## Using the REST APIs

The following topics describe how the JIRA REST APIs are structured and how you can interact with them.

### Expansion

In order to simplify API responses, the JIRA REST API uses resource expansion. This means the API will only return parts of the resource when explicitly requested. This helps you avoid problems that can occur when you request too little information (e.g. you need to make many requests) or too much information (e.g. performance impact on JIRA).

You can use the `expand` query parameter to specify a comma-separated list of entities that you want expanded, identifying each of them by name. For example, appending `?expand=names,renderedFields` to an issue's URI requests the inclusion of the translated field names and the HTML-rendered field values in the response. The example below expands the `name` and `renderedFields` fields for issue JRA-9:

``` bash
https://jira.atlassian.com/rest/api/latest/issue/JRA-9?expand=names,renderedFields
```

To find out which fields are expandable, look at the `expand` property in the parent object. In the example below, the `widgets` field is expandable:

``` javascript
{
    "expand": "widgets",
    "self": "http://jira.atlassian.com/rest/api/resource/KEY-1",
    "widgets": { "widgets": [], "size": 5 } 
}
```

You can use the dot notation to specify expansion of entities within another entity. For example `?expand=widgets.fringels` would expand the `widgets` collection and also the fringels property on each widget.

### Pagination

JIRA uses pagination to limit the response size for resources that return a potentially large collection of items. A request to a paged API will result in a values array wrapped in a JSON object with some paging metadata, for example:

``` javascript
{
    "startAt" : 0,
    "maxResults" : 10,
    "total": 200,
    "values": [
        { /* result 0 */ },
        { /* result 1 */ },
        { /* result 2 */ }
    ]
}
```

-   `startAt: `The item used as the first item in the page of results.
-   `maxResults:` Number of items to return per page.
-   `total:` Total number of items to return, subject to server-enforced limits. This number *may change* as the client requests the subsequent pages. A client should always assume that the requested page can be empty. REST API consumers should also consider the field to be optional. In cases, when calculating this value is too expensive it may not be included in the response.
-   `isLastPage: `Indicates whether the current page returned is the last page of results.

Clients can use the `startAt,` `maxResults, `and` total` parameters to retrieve the desired number of results. Note, each API resource or method may have a different limit on the number of items returned, which means you can ask for more than you are given. The actual number of items returned is an implementation detail and this can be changed over time.

### Ordering

Some resources support ordering by a specific field. This is provided by the **orderBy** query parameter.

Ordering can be ascending or descending. To specify the type of ordering, use the "+" or "-" symbols for ascending or descending respectively. By default, ordering is ascending. For example, `?orderBy=+name` will order the results by name in ascending order. 

### Self links

Many fields have a "self" link, which takes you to the canonical location for that resource. See the example below:

``` javascript
"reporter": {
    "self": "http://jira.atlassian.com/rest/api/2/user?username=admin",
    "name": "admin",
    "emailAddress": "admin@example.com",
    "displayName": "Administrator",
    "active": true
},
```

Making a GET request to the self link can sometimes provide you with additional information about the field. For example, if we make a GET request for the "self" link for the `reporter` field above, the response will contain additional information about the user, including the timezone and groups. 

### Special request and response headers

-   **X-AUSERNAME:** Response header which contains either username of the authenticated user or 'anonymous'.
-   **X-Atlassian-Token:** Methods which accept multipart/form-data will only process requests with 'X-Atlassian-Token: no-check' header.

### Error responses

Most resources will return a response body in addition to the status code. Usually, the JSON schema of the entity returned is the following:

``` javascript
{
    "id": "https://docs.atlassian.com/jira/REST/schema/error-collection#",
    "title": "Error Collection",
    "type": "object",
    "properties": {
        "errorMessages": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "errors": {
            "type": "object",
            "patternProperties": {
                ".+": {
                    "type": "string"
                }
            },
            "additionalProperties": false
        },
        "status": {
            "type": "integer"
        }
    },
    "additionalProperties": false
}
```

### Field input formats

**Summary**: A system field that is a single line of text.

``` javascript
"summary": "This is an example summary"
```

**Description:** A system field that is multiple lines of text.

``` javascript
"description": "This is an example description with multiples lines of text\n separated by\n line feeds"
```

**Components:** A system field that is multiple values addressed by 'name'.

``` javascript
"components" : [ { "name": "Active Directory"} , { "name": "Network Switch" } ]
```

**Due date:** A system field that is a date in 'YYYY-MM-DD' format.

``` javascript
"duedate" : "2015-11-18"
```

**Labels:** A system field that is an array of string values.

``` javascript
"labels" : ["examplelabelnumber1", "examplelabelnumber2"]
```

**Checkbox custom field:** A custom field that allows you to select a multiple values from a defined list of values. You can address them by 'value' or by 'id'.

``` javascript
"customfield_11440" : [{ "value" : "option1"}, {"value" : "option2"}]
```

or

``` javascript
"customfield_11440" : [{ "id" : 10112}, {"id" : 10115}]
```

**Date picker custom field:** A custom field that is a date in 'YYYY-MM-DD' format.

``` javascript
"customfield_11441" : "2015-11-18"
```

**Date time picker custom field:** A custom field that is a datetime in ISO 8601 `YYYY-MM-DDThh:mm:ss.sTZD` format.

``` javascript
"customfield_11442" : "2015-11-18T14:39:00.000+1100"
```

**Labels custom field:** A custom field that is an array of strings.

``` javascript
"customfield_11443" : [ "rest_label1", "rest_label2" ]
```

**Number custom field:** A custom field that contains a number.

``` javascript
"customfield_11444" : 123
```

**Radio button custom field:** A custom field that allows you to select a single value from a defined list of values. You can address them by 'value' or by 'id'.

``` javascript
"customfield_11445" : { "value": "option2" }
```

or

``` javascript
"customfield_11445" : { "id": 10112 }
```

**Cascading select custom field:** A custom field that allows you to select a single parent value and then a related child value. You can address them by 'value' or by 'id'.

``` javascript
"customfield_11447" : { "value": "parent_option1", "child": { "value" : "p1_child1"} }
```

or

``` javascript
"customfield_11447" : { "id": 10112, "child": { "id" : 10115 } }
```

**Multi-select custom field:** A custom field that allows you to select a multiple values from a defined list of values. You can address them by 'value' or by 'id'.

``` javascript
"customfield_11448" : [ { "value": "option1" }, { "value": "option2" } ]
```

or

``` javascript
"customfield_11448" : [ { "id": 10112 }, { "id": 10115 } ]
```

**Single-select custom field:** A custom field that allows you to select a single value from a defined list of values. You can address them by 'value' or by 'id'.

``` javascript
"customfield_11449" : { "value": "option3" }
```

or

``` javascript
"customfield_11449" : { "id": 10112 }
```

**Multi-line text custom field**: A custom field that allows multiple lines of text.

``` javascript
"customfield_11450": "An example of multiples lines of text\n separated by\n line feeds"
```

**Text custom field:** A custom field that allows a single line of text.

``` javascript
"customfield_11450": "An example of a single line of text"
```

**URL custom field:** A custom field that allows a URL to be entered.

``` javascript
"customfield_11452" : "http://www.atlassian.com"
```

**Single-user picker custom field:** A custom field that allows a single user to be selected.

``` javascript
"customfield_11453" : { "name":"tommytomtomahawk" }
```

**Multi-user picker custom field:** A custom field that allows multiple users to be selected.

``` javascript
"customfield_11458" : [ { "name":"inigomontoya" }, { "name":"tommytomtomahawk" }]
```

## Examples

The [JIRA REST API examples](/server/jira/platform/jira-rest-api-examples) guide contains a range of examples, including examples requests for creating issues, updating issues, searching for issues and more.

We've also provided a simple example below to get you started. The example shows you how to create an issue using the JIRA REST API. The sample code uses <a href="https://curl.haxx.se" class="external-link">curl</a> to make requests, but you can use any tool you prefer.

Note:

-   The input file is denoted by the '`--data @filename`' syntax. The data is shown separately, and uses the JSON format.
-   Make sure the content type in the request is set to '`application/json`', as shown in the example.
-   POST the JSON to your JIRA server. In the example, the server is `http://localhost:8080/jira/rest/api/2/issue/`.
-   The example uses basic authentication, with a username of admin and password of admin.
-   You'll need to add a project to the instance before running, and get the project ID of the project to which you want to add the issue beforehand.

To create an issue using the JIRA REST API:

1.  Create the data file that contains the POST data. For this example, we'll assume the file is named `data.txt`. Add the JSON shown below to the file.  
    In this data, the project ID is 10000 and the issue type in our case is 3, which represents a task. You should pick an ID of a project in your instance and whichever issue type you prefer.

    ``` javascript
    {
        "fields": {
           "project":
           {
              "id": "10000"
           },
           "summary": "No REST for the Wicked.",
           "description": "Creating of an issue using ids for projects and issue types using the REST API",
           "issuetype": {
              "id": "3"
           }
       }
    }
    ```

    Note, you can also use the key and name for the `project` and `issuetype` respectively, instead of the `id`. For example,` "key": "TEST"` for the project and `"name": "Task"` for the `issuetype`.

2.  From the command line enter the following command:

    ``` bash
    curl -u admin:admin -X POST --data @data.txt -H "Content-Type: application/json" http://localhost:8080/jira/rest/api/2/issue/ 
    ```

    As before, adjust details for your environment, such as the hostname or port of the JIRA instance. Note that a cloud instance or most public instances would require the use of HTTPS and of course valid credentials for the instance.

3.  Your issue will be created. Check the response, which will look something like this: 

    ``` javascript
    {
       "id":"10009",
       "key":"TEST-10",
        "self":"http://localhost:8080/jira/rest/api/2/issue/10009"
    } 
    ```

    That's it! You can use the issue ID, issue key, and the URL to the issue for additional requests, if you wish.

## Webhooks

Webhooks are user-defined callbacks over HTTP, which are specific to the JIRA REST API. JIRA webhooks allow the JIRA REST API to inform a remote application when changes have occurred, e.g. an issue transition. This saves the application from having to periodically poll JIRA (via the REST API). Read the webhooks page to learn more: [Webhooks](/server/jira/platform/webhooks).

## JIRA REST clients

-   <a href="https://ecosystem.atlassian.net/wiki/display/JRJC/Home" class="external-link">JIRA REST Java Client Library</a> is a sample open source implementation (Apache license) of JIRA REST client developed by Atlassian. This implementation evolves together with JIRA REST API itself, helps driving, validating and testing it. We encourage everyone willing to use JIRA REST API capabilities from a Java or any JVM-based language program to try it out and give us feedback.
