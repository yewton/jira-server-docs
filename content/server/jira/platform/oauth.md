---
aliases:
- /server/jira/platform/oauth-52430795.html
- /server/jira/platform/oauth-52430795.md
category: devguide
confluence_id: 52430795
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430795
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430795
date: '2018-06-27'
legacy_title: OAuth
platform: server
product: jira
subcategory: security
title: "OAuth"
---
# OAuth

This page shows you how to authenticate clients against the Jira REST API using 
[OAuth](https://en.wikipedia.org/wiki/OAuth) (1.0a). 
We'll explain how OAuth works with Jira and walk you through an example of how to use OAuth to authenticate a
Java application (*consumer*) against the Jira (*resource*) REST API for a user (*resource owner*).

Authentication is a key process when integrating with Jira. OAuth is one of a number of authentication methods
used with Jira and it is typically used for server-to-server authentication, especially for behind the firewall
applications.

### Show me the code!

{{% note %}}

You can find the code for the final output of this tutorial on [Bitbucket](https://bitbucket.org/atlassian_tutorial/atlassian-oauth-examples)
(look for the `Java` directory).

{{% /note %}}

If you prefer to figure things out from the source, feel free to check the code first.
Otherwise, the guide below will explain what's going on.

Also notice that the repository contains directories with examples for Node.js, PHP, Python, Perl, and Ruby.

## Pick the authentication method

Have you picked the right authentication method?

Considering [Basic Authentication](/server/jira/platform/basic-authentication)? 
We recommend that you *don't use Basic Authentication at all*, except for tools like personal scripts or bots.
It may be easier to implement, but it is much less secure. The username and password are sent repeatedly with
requests and cached on the web browser. Even if the credentials are sent via SSL/TLS, these protocols can still
be compromised.

{{% note %}}

This tutorial was last tested with Jira Server 7.0.

{{% /note %}}

## Overview

OAuth is an authentication protocol that allows a user (that is, resource owner) to grant a third-party application
(that is, consumer/client) access to their information on another site (that is, resource). Jira uses 3-legged OAuth (3LO),
which means that the user is involved in the authentication process by authorizing access to their data on the
resource (as opposed to 2-legged OAuth, where the user is not involved).

### Roles

These are the roles in the OAuth authentication process, and how they relate to authenticating with Jira:

**Resource**  

<img src="/server/jira/platform/images/atlassian-software-26.png" class="image-center confluence-thumbnail" width="80" />

If you integrate a client application with Jira, then Jira is considered to be the "resource".

**Resource owner**

<img src="/server/jira/platform/images/meeple-84-@2x.png" class="image-center confluence-thumbnail" width="80" />

As Jira is the "resource", the Jira user is considered to be the "resource owner" for an authentication request. 

**Consumer**

<img src="/server/jira/platform/images/atlassian-software-01.png" class="image-center confluence-thumbnail" width="80" />

The client application is registered as a consumer, by creating an application link (that uses OAuth) in Jira
that links to the client. 

### Process

The authentication process, commonly known as the "OAuth dance", works by getting the resource owner to grant
access to their information on the resource by authenticating a request token. This request token is used by
the consumer to obtain an access token from the resource. Once the client has an access token, it can use it
to make authenticated requests to the resource until the token expires or is revoked. 

This process is shown in more detail in the following diagram.

<img src="/server/jira/platform/images/52430788.png" class="gliffy-macro-image" />

## See it in action

Before we look at the code, let's see how the OAuth authentication process actually works. This will be a
hands-on walkthrough where you'll configure Jira so that the sample client can authenticate against it using
OAuth. You'll also use the sample client to initiate the "OAuth dance", and then finally make an authenticated
request to the Jira REST API.

### Before you begin

To complete this tutorial, you need to know and do the following: 

1.   A basic knowledge of how to use REST APIs, for example, requests, responses, headers.
1.   A basic understanding of Jira.
1.   If you want to run the Java example, you'll need a Jira development instance, Maven (3.x), and a recent Java
version (for example, Oracle JDK 1.8).
1.   If you haven't got the sample OAuth client application code yet, clone it from here:
https://bitbucket.org/atlassian_tutorial/atlassian-oauth-examples (in the `Java` directory).
1.   After you have the code, build the client by running the following command in the root of the project: 

    ``` bash
    mvn clean compile assembly:single
    ```

1.   Navigate to the `target` directory in the project and run: 

    ``` bash
    java -jar OAuthTutorialClient-1.0.jar requestToken
    ```

    Ignore the exception in the output for now. You just need to do this to generate the `config.properties`
    file that you'll use later.

## Step 1. Configure client application as an OAuth consumer

In Jira, OAuth consumers are represented by *application links*. Application links use OAuth with RSA-SHA1 signing
for authentication. This means that a private key is used to sign requests, rather than the OAuth token secret/consumer
secret. In the following steps, you'll generate an RSA public/private key pair, and then create a new application
link in Jira that uses the key.

### Generate an RSA public/private key pair

1.  In Terminal window, run the following [openssl](https://www.openssl.org/docs/manmaster/apps/) commands.
You can do this anywhere in your file system.

    ``` bash
    openssl genrsa -out jira_privatekey.pem 1024
    openssl req -newkey rsa:1024 -x509 -key jira_privatekey.pem -out jira_publickey.cer -days 365
    openssl pkcs8 -topk8 -nocrypt -in jira_privatekey.pem -out jira_privatekey.pcks8
    openssl x509 -pubkey -noout -in jira_publickey.cer  > jira_publickey.pem
    ```

    This generates a 1024-bit private key, creates an X509 certificate, and extracts the private key (PKCS8 format)
    to the `jira\_privatekey.pcks8` file. It then extracts the public key from the certificate to the 
    `jira\_publickey.pem` file.

1.  Copy the private key from the `jira\_privatekey.pcks8` file to your clipboard.
1.  Navigate to the `target` directory in the sample OAuth client project. Open the `config.properties` file 
and make the following changes:
    1.   Paste the private key from your clipboard over the value of `private_key` field. Remove all line breaks.
    1.   Change the `jira_home` to the URL of your Jira development instance, for example, `https://example-dev1.atlassian.net`
    1.   *Don't change the `consumer_key`, which should be set to 'OauthKey'.*
1.  Save the `config.properties` file.

### Configure the client app as a consumer in Jira

1.  In Jira, click ![](/server/jira/platform/images/cog.png) > **Applications** > **Application links**.
1.  In the "Enter the URL of the application you want to link" field, enter any URL, for example, 
http://example.com/, and then click **Create new link**.  
    You'll get a warning that "No response was received from the URL you entered". Ignore it and click **Continue**.
1.  On the first screen of the **Link applications** dialog, enter anything you want in the fields. However,
make sure you select the **Create incoming link** checkbox.

    <img src="/server/jira/platform/images/jdev-oauth-applink1.png" width="400" />

    {{% note %}}
In this example, it doesn't matter what you enter for the client application details (URL, name, type, and so on).
This is because we only want to retrieve data from Jira, therefore we only need to set up a one-way (incoming)
link from the client to Jira.
    {{% /note %}}

1.  On next screen of the **Link applications** dialog, enter the consumer details for the sample client:

    *   **Consumer key** = OauthKey
    *   **Consumer name** = Example Jira app 
    *   **Public key** = Copy the public key from the `jira\_publickey.pem` file you generated previously and
    paste it into this field.

        <img src="/server/jira/platform/images/jdev-oauth-applink2.png" width="400" />

1.  Click **Continue**. You should end up with an application link that looks like this:  
    <img src="/server/jira/platform/images/jdev-oauth-applink3.png" width="850" /> 

That's it! You've now configured the sample client as an OAuth consumer in Jira.

## Step 2. Do the "OAuth dance"

The "OAuth dance" is a term that's used to describe the process of getting an access token from the resource,
that the consumer can use to access information on the resource. This involves a "dance" where different tokens
are passed between the consumer, resource owner, and resource (see OAuth overview at the beginning of the page).

1.  In your Terminal, navigate to the `target` directory of the sample OAuth client project, if you are not already there. 
1.  Run the following command: 

    ``` bash
    java -jar OAuthTutorialClient-1.0.jar requestToken
    ```

    This command requests an unauthorized request token from your Jira instance.

    You'll see the following output with the details of your new request token:

    ``` bash
    Token:          ec3dj4byySM5ek3XW7gl7f4oc99obAlo
    Token Secret:   OhONj0eF7zhXAMKZLbD2Rd3x7Dmxjy0d
    Retrieved request token. go to https://jira101.atlassian.net/plugins/servlet/oauth/authorize?oauth_token=ec3dj4byySM5ek3XW7gl7f4oc99obAlo to authorize it
    ```

    The request token will persist for 10 minutes. If it expires, you'll need to request a new one.

1.  In your browser, go to the URL specified in the Terminal output. You'll see the following dialog.  
    <img src="/server/jira/platform/images/jdev-oauth-authrequesttoken.png" width="400" />
1.  Click **Allow**. This will authorize the request token.  
    You'll see the following output in your browser:

    ``` bash
    Access Approved
    You have successfully authorized 'Example JIRA app'. Your verification code is 'qTJkPi'. You will need to
    enter this exact text when prompted. You should write this value down before closing the browser window.
    ```

    Copy the verification code to your clipboard or somewhere else where you can get it.

1.  In your Terminal, run the following command:

    ``` bash
    java -jar OAuthTutorialClient-1.0.jar accessToken qTJkPi
    ```

    You'll need to replace the example verification code (`qTJkPi`) with your own verification code from the
    previous step.

    You'll see the following output in your terminal:

    ``` bash
    Access Token:           W1jjOV4sq2iEqxO4ZYZTJVdgbjtKc2ye
    ```

    Note that to get the access token in OAuth, you need to pass the consumer key, request token,
    verification code, and private key. However, in the sample client, information like the consumer key, request
    token, private key, and so on, are stored in the `config.properties` file when they are generated (have a
    look at it as you complete this tutorial and you'll see the new values added). You probably don't want
    to do this for a production implementation, but this makes the sample client easier to use for this example.

We now have what we wanted from the OAuth dance: an access token that we can use to make an authenticated
request to the Jira REST API. 

## Step 3. Make an authenticated request to the Jira REST API

An access code is all that we need to make an authenticated request to the Jira REST API. Requests are made as
the user who authorized the initial request token. The access token will persist for 5 years, unless it is revoked. 

1.  The sample OAuth client only makes GET requests. To use it, run the following command:

``` bash
java -jar OAuthTutorialClient-1.0.jar request <URL for GET method>
```

You'll need to substitute `<URL for REST method>` with the URL of the REST method that you try to call. 

In the code, the sample OAuth client actually stores the access code in the `config.properties` file when it
is obtained. When a request is made, the sample client passes the stored access code rather than you having
to enter it. 

Here's an example of a request to GET an issue, using the sample OAuth client. The following command gets the
JJ-2 issue via the Jira REST API:

``` bash
java -jar OAuthTutorialClient-1.0.jar request http://localhost:8080/rest/api/latest/issue/JJ-2
```

The above method returns an issue like this:

``` javascript
 {
   "expand": "renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
   "self": "<a href="http://localhost:8080/rest/api/latest/issue/10300">http://localhost:8080/rest/api/latest/issue/10300</a>",
   "id": "10300",
   "fields": {
     "issuetype": {
       "avatarId": 10803,
       "name": "Bug",
       "self": "<a href="http://localhost:8080/rest/api/2/issuetype/1">http://localhost:8080/rest/api/2/issuetype/1</a>",
       "description": "A problem which impairs or prevents the functions of the product.",
       "id": "1",
       "iconUrl": "<a href="http://localhost:8080/secure/viewavatar?size=xsmall&amp;avatarId=10803&amp;avatarType=issuetype">http://localhost:8080/secure/viewavatar?size=xsmall&amp;avatarId=10803&amp;avatarType=issuetype</a>",
       "subtask": false
     },
     "timespent": null,
     "project": {
       "avatarUrls": {
         "48x48": "<a href="http://localhost:8080/secure/projectavatar?pid=10200&amp;avatarId=10700">http://localhost:8080/secure/projectavatar?pid=10200&amp;avatarId=10700</a>",
         "24x24": "<a href="http://localhost:8080/secure/projectavatar?size=small&amp;pid=10200&amp;avatarId=10700">http://localhost:8080/secure/projectavatar?size=small&amp;pid=10200&amp;avatarId=10700</a>",
         "16x16": "<a href="http://localhost:8080/secure/projectavatar?size=xsmall&amp;pid=10200&amp;avatarId=10700">http://localhost:8080/secure/projectavatar?size=xsmall&amp;pid=10200&amp;avatarId=10700</a>",
         "32x32": "<a href="http://localhost:8080/secure/projectavatar?size=medium&amp;pid=10200&amp;avatarId=10700">http://localhost:8080/secure/projectavatar?size=medium&amp;pid=10200&amp;avatarId=10700</a>"
       },
       "name": "JIRA Junior",
       "self": "<a href="http://localhost:8080/rest/api/2/project/10200">http://localhost:8080/rest/api/2/project/10200</a>",
       "id": "10200",
       "key": "JJ"
     },
     "fixVersions": [],
     "aggregatetimespent": null,
     "resolution": null,
     "customfield_10500": null,
     "customfield_10700": "com.atlassian.servicedesk.plugins.approvals.internal.customfield.ApprovalsCFValue@c8d588",
     "resolutiondate": null,
     "workratio": -1,
     "lastViewed": "2016-08-01T11:23:39.481+1000",
     "watches": {
       "self": "<a href="http://localhost:8080/rest/api/2/issue/JJ-2/watchers">http://localhost:8080/rest/api/2/issue/JJ-2/watchers</a>",
       "isWatching": true,
       "watchCount": 1
     },
     "created": "2013-05-29T13:56:24.224+1000",
     "customfield_10020": null,
     "customfield_10021": "Not started",
     "priority": {
       "name": "Major",
       "self": "<a href="http://localhost:8080/rest/api/2/priority/3">http://localhost:8080/rest/api/2/priority/3</a>",
       "iconUrl": "<a href="http://localhost:8080/images/icons/priorities/major.svg">http://localhost:8080/images/icons/priorities/major.svg</a>",
       "id": "3"
     },
     "customfield_10300": null,
     "customfield_10102": null,
     "labels": [],
     "customfield_10016": null,
     "customfield_10017": null,
     "customfield_10018": null,
     "customfield_10019": null,
     "timeestimate": null,
     "aggregatetimeoriginalestimate": null,
     "versions": [],
     "issuelinks": [],
     "assignee": {
       "emailAddress": "alana@<a href="http://example.com">example.com</a>",
       "avatarUrls": {
         "48x48": "<a href="https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=48">https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=48</a>",
         "24x24": "<a href="https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=24">https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=24</a>",
         "16x16": "<a href="https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=16">https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=16</a>",
         "32x32": "<a href="https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=32">https://secure.gravatar.com/avatar/b259e2a7fd37a83b02015192ee247e96?d=mm&amp;s=32</a>"
       },
       "displayName": "Alana Example",
       "name": "alana",
       "self": "<a href="http://localhost:8080/rest/api/2/user?username=alana">http://localhost:8080/rest/api/2/user?username=alana</a>",
       "active": true,
       "timeZone": "Australia/Sydney",
       "key": "alana"
     },
     "updated": "2016-08-01T11:23:38.022+1000",
     "status": {
       "name": "Open",
       "self": "<a href="http://localhost:8080/rest/api/2/status/1">http://localhost:8080/rest/api/2/status/1</a>",
       "description": "The issue is open and ready for the assignee to start work on it.",
       "iconUrl": "<a href="http://localhost:8080/images/icons/statuses/open.png">http://localhost:8080/images/icons/statuses/open.png</a>",
       "id": "1",
       "statusCategory": {
         "colorName": "blue-gray",
         "name": "To Do",
         "self": "<a href="http://localhost:8080/rest/api/2/statuscategory/2">http://localhost:8080/rest/api/2/statuscategory/2</a>",
         "id": 2,
         "key": "new"
       }
     },
     "components": [],
     "timeoriginalestimate": null,
     "description": "The logo is currently a light cerise. I'd like to see it with a deep pink color.",
     "customfield_10012": null,
     "customfield_10013": null,
     "customfield_10014": null,
     "timetracking": {},
     "customfield_10015": null,
     "customfield_10600": null,
     "customfield_10006": "10",
     "customfield_10601": null,
     "customfield_10007": [
       "com.atlassian.greenhopper.service.sprint.Sprint@dc6300[id=1,rapidViewId=&lt;null&gt;,state=CLOSED,name=Sprint 1,goal=&lt;null&gt;,startDate=2013-07-26T11:31:09.530+10:00,endDate=2013-08-09T11:31:09.530+10:00,completeDate=2013-07-26T11:31:46.489+10:00,sequence=1]",
       "com.atlassian.greenhopper.service.sprint.Sprint@6b3e17[id=2,rapidViewId=&lt;null&gt;,state=ACTIVE,name=Sprint 2,goal=&lt;null&gt;,startDate=2013-08-22T11:35:33.759+10:00,endDate=2013-12-12T11:35:00.000+11:00,completeDate=&lt;null&gt;,sequence=2]"
     ],
     "customfield_10008": null,
     "attachment": [],
     "aggregatetimeestimate": null,
     "summary": "JIRA Junior logo is not pink enough",
     "creator": {
       "emailAddress": "admin@<a href="http://example.com">example.com</a>",
       "avatarUrls": {
         "48x48": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48</a>",
         "24x24": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24</a>",
         "16x16": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16</a>",
         "32x32": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32</a>"
       },
       "displayName": "Administrator",
       "name": "admin",
       "self": "<a href="http://localhost:8080/rest/api/2/user?username=admin">http://localhost:8080/rest/api/2/user?username=admin</a>",
       "active": true,
       "timeZone": "Australia/Sydney",
       "key": "admin"
     },
     "subtasks": [],
     "reporter": {
       "emailAddress": "admin@<a href="http://example.com">example.com</a>",
       "avatarUrls": {
         "48x48": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48</a>",
         "24x24": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24</a>",
         "16x16": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16</a>",
         "32x32": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32</a>"
       },
       "displayName": "Administrator",
       "name": "admin",
       "self": "<a href="http://localhost:8080/rest/api/2/user?username=admin">http://localhost:8080/rest/api/2/user?username=admin</a>",
       "active": true,
       "timeZone": "Australia/Sydney",
       "key": "admin"
     },
     "customfield_10000": null,
     "aggregateprogress": {
       "total": 0,
       "progress": 0
     },
     "customfield_10001": null,
     "customfield_10200": "0|10001s:",
     "customfield_10002": null,
     "customfield_10003": null,
     "customfield_10400": null,
     "environment": null,
     "duedate": null,
     "progress": {
       "total": 0,
       "progress": 0
     },
     "comment": {
       "total": 1,
       "comments": [{
         "author": {
           "emailAddress": "admin@<a href="http://example.com">example.com</a>",
           "avatarUrls": {
             "48x48": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48</a>",
             "24x24": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24</a>",
             "16x16": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16</a>",
             "32x32": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32</a>"
           },
           "displayName": "Administrator",
           "name": "admin",
           "self": "<a href="http://localhost:8080/rest/api/2/user?username=admin">http://localhost:8080/rest/api/2/user?username=admin</a>",
           "active": true,
           "timeZone": "Australia/Sydney",
           "key": "admin"
         },
         "created": "2013-06-04T16:11:24.505+1000",
         "updateAuthor": {
           "emailAddress": "admin@<a href="http://example.com">example.com</a>",
           "avatarUrls": {
             "48x48": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=48</a>",
             "24x24": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=24</a>",
             "16x16": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=16</a>",
             "32x32": "<a href="https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32">https://secure.gravatar.com/avatar/4beac5df66a475580809e0?d=mm&amp;s=32</a>"
           },
           "displayName": "Administrator",
           "name": "admin",
           "self": "<a href="http://localhost:8080/rest/api/2/user?username=admin">http://localhost:8080/rest/api/2/user?username=admin</a>",
           "active": true,
           "timeZone": "Australia/Sydney",
           "key": "admin"
         },
         "self": "<a href="http://localhost:8080/rest/api/2/issue/10300/comment/10100">http://localhost:8080/rest/api/2/issue/10300/comment/10100</a>",
         "id": "10100",
         "body": "Hi [~william] JIRA is *super fun* (see JJ-1). Alana is going to fix the logo.",
         "updated": "2013-06-12T21:55:34.882+1000"
       }],
       "maxResults": 1,
       "startAt": 0
     },
     "votes": {
       "hasVoted": false,
       "self": "<a href="http://localhost:8080/rest/api/2/issue/JJ-2/votes">http://localhost:8080/rest/api/2/issue/JJ-2/votes</a>",
       "votes": 0
     }
   },
   "key": "JJ-2"
 }
```

Congratulations! You now know how to use OAuth to make an authenticated request to the Jira REST API.

## Development tips

You can implement OAuth in a number of ways, depending on what you build. However, we encourage you to
browse the source code for our sample Java OAuth client to get an idea of how it is implemented, regardless of
the technologies that you use. 

In addition, you'll find a number of general tips below that should be helpful, regardless of how you implement OAuth.

*   Use OAuth libraries. Rather than implementing everything yourself, there are a number of OAuth libraries
that you can use at [Code at OAuth.net](https://oauth.net/1/). The sample OAuth client uses the
[Google OAuth Client Library for Java](https://developers.google.com/api-client-library/java/google-oauth-java-client/).
*   OAuth 2.0 is not supported. Currently only OAuth 1.0a is supported for authenticating to the Jira REST APIs.
*   Requests for tokens must be made using HTTP POST.
*   Set the callback to 'oob' (that is, out of band) when creating the request token if you want to show the
token secret to the user.
See `getAndAuthorizeTemporaryToken` in `JIRAOAuthClient.java` in the sample OAuth client for an example of this. 
You can then set the callback to some other URL after the user authorizes the token so that Jira sends the token
and secret to that URL.
*   When making requests, pass the OAuth access token in the request header, do not pass it in the request body. If you pass
the OAuth data in the request body, it will return a "400 Bad Request" error for most methods. You must pass
the data in the header instead, for example, `Authorization: Bearer {access_token}`.

## Next steps

*   Read the [Security overview](/server/jira/platform/security-overview) to learn the difference between authentication
and authorization and how users are authorized in Jira.
*   Check out the Jira REST APIs:
    *   [Jira platform Server REST API](https://docs.atlassian.com/software/jira/docs/api/REST/latest/)
    *   [Jira Software Server REST API](https://docs.atlassian.com/jira-software/REST/server/)
    *   [Jira Service Desk Server REST API](/cloud/jira/service-desk/rest/)
