---
title: "JIRA Core 7.8 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-02-16"
---
# JIRA Core 7.8 EAP release notes

*16 February 2018*

Atlassian is proud to present *JIRA Core 7.8 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *JIRA Software 7.8* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.8, read the developer change management guide: [Preparing for JIRA 7.8](/server/jira/platform/preparing-for-jira-7-8). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.7.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### Find your work faster

With an improved quick search, searching through all your issues and projects will be nothing else but a breeze. Whether you know the full issue key, part of the issue name, or just have a distant memory of a project from a year ago, start typing the words, and we’ll do the rest for you. The quick search instantly shows the most relevant results, and refreshes them whenever you change your search term.

<img src="/server/jira/platform/images/quicksearch.png"/>

If you’ve already found what you were looking for, just treat quick search as a handy work diary. Click anywhere in the box to see the issues and projects you’ve been working on recently, and have the most important work always at your fingertips. 

### Small improvements to make your day

#### We speak your language

You can now choose Dutch as your language in JIRA. Check out the [languages available](https://confluence.atlassian.com/display/AdminJIRAServer/Choosing+a+default+language) by default after you upgrade to JIRA Core 7.8.
