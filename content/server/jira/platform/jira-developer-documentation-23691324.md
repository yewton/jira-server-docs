---
title: JIRA Developer Documentation 23691324
aliases:
    - /server/jira/platform/jira-developer-documentation-23691324.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23691324
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23691324
confluence_id: 23691324
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA developer documentation

### Developing for JIRA Cloud?

Head over to our new [JIRA Cloud documentation](https://developer.atlassian.com/cloud/jira/platform/).
You'll find information on the Connect framework, the JIRA REST APIs, the JIRA platform, the JIRA applications, and more.

Creating a JIRA add-on is easier than you think. Learn the basic building blocks: [Atlassian Connect](https://connect.atlassian.com), [JIRA REST APIs](https://developer.atlassian.com/display/JIRADEV/JIRA+APIs), and [webhooks](https://developer.atlassian.com/display/JIRADEV/Webhooks); and you'll be able to extend JIRA to do whatever you want.

[Build an add-on](https://developer.atlassian.com/display/JIRADEV/Getting+started)

<img src="/server/jira/platform/images/objects-77.png" class="image-center confluence-thumbnail" width="250" />

Want to know more about the JIRA architecture and applications?
Check out our documentation on the [JIRA platform](https://developer.atlassian.com/display/JIRADEV/JIRA+platform), and the [JIRA applications](https://developer.atlassian.com/display/JIRADEV/JIRA+applications): JIRA Software, JIRA Service Desk, and JIRA Core.
