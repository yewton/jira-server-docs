---
aliases:
- /server/jira/platform/user-accessible-locations-4227075.html
- /server/jira/platform/user-accessible-locations-4227075.md
category: reference
confluence_id: 4227075
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227075
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227075
date: '2017-12-08'
legacy_title: User Accessible Locations
platform: server
product: jira
subcategory: modules
title: General navigation UI locations
---
# General navigation UI locations

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h6 id="UserAccessibleLocations-Available:">Available:</h6></td>
<td><p>JIRA 4.2 and later -- the following locations are available:<br />
    - <code>system.user.hover.links</code><br />
    - <code>jira.hints</code><br />
JIRA 4.0 and later -- the following locations are available:<br />
    - <code>system.user.options</code><br />
JIRA 3.7 and later -- the following locations are available:<br />
    - <code>system.top.navigation.bar</code><br />
    - <code>system.user.navigation.bar</code><br />
    - <code>system.user.profile.links</code><br />
    - <code>system.preset.filters</code></p></td>
</tr>
<tr class="even">
<td><h6 id="UserAccessibleLocations-Changed:">Changed:</h6></td>
<td><p>In JIRA 4.0 -- the method of defining web items and web sections for the <code>system.top.navigation.bar</code> changed and the <code>system.user.navigation.bar</code> location was removed and replaced by the <code>system.user.options</code> location.</p></td>
</tr>
</tbody>
</table>

## Top Navigation Bar Location

{{% note %}}

JIRA 4.0 and later versions.

{{% /note %}}

The **`system.top.navigation.bar`** location defines web sections and items in JIRA's top navigation bar, which are accessible from all JIRA areas (except JIRA's administration area/mode).

<img src="/server/jira/platform/images/web-fragments-top-navigation-bar.png" width="381" height="102" />

You can add new drop-down menus each with their own web sections and web items to this navigation bar, or add new items to JIRA's existing drop-down menus.

### Adding Custom Drop-down Menus, Sections and Items

Adding a new drop-down menu with its own sections and items to the top navigation bar can be broken down into three module definition steps.

**To add a new drop-down menu with its own sections and items:**

1.  #### **Define a `<web-item>` module for the drop-down menu itself**

    -   This web item module must include:
        -   A `section` attribute with the value `"system.top.navigation.bar"`.
        -   A child `<link>` element with a `linkId` attribute and a unique value `"linkId_for_my_dropdown_menu"`.
    -   For example:

        ``` xml
        <web-item ... section="system.top.navigation.bar" ...>
            ...
            <link linkId="linkId_for_my_dropdown_menu" ...>
                ...
            </link>
            ...
        </web-item>
        ```

    -   To make this drop-down menu accessible via a keyboard shortcut, add an i18n resource (as a property in any of your plugin's language resource files - for e.g. i18n.properties) whose property key is '`menu.accesskey.`' followed immediately by the value of the `linkID` attribute defined in your web item module above. For example (following on from above):  
        `menu.accesskey.linkId_for_my_dropdown_menu`  
        The value of this property is the actual keyboard shortcut you want to use to access this drop-down menu (along with the browser's <a href="https://confluence.atlassian.com/display/JIRA/Using+Keyboard+Shortcuts#UsingKeyboardShortcuts-modifierkeys" class="external-link">modifier/access keys</a>). E.g.

        ``` java
        # This property allows the 'linkId_for_my_dropdown_menu' drop-down menu to open when the user presses a combination of their browser's modifier keys + 'z':
        menu.accesskey.linkId_for_my_dropdown_menu=z
        ```

        {{% warning %}}

Avoid choosing a keyboard shortcut that is already built in to JIRA. For details on JIRA's existing keyboard shortcuts, refer to <a href="https://confluence.atlassian.com/display/JIRA/Using+Keyboard+Shortcuts" class="external-link">Using Keyboard Shortcuts</a> in the JIRA User's Guide.

        {{% /warning %}}

2.  **Define one or more `<web-section>` modules for this drop-down menu.**
    -   Each of these web section modules must include:
        -   A `key` attribute with a unique value.
        -   A `location` attribute whose value is that of the `linkId` attribute's value specified in the web item module for the drop-down menu (see [Define a &lt;web-item&gt; module for the drop-down menu itself.](#define-a-ltweb-itemgt-module-for-the-drop-down-menu-itself)).
    -   For example:

        ``` xml
        <web-section key="first_section_of_my_dropdown_menu" ... location="linkId_for_my_dropdown_menu" ...>
            ...
        </web-section>
        ```

3.  **Define a `<web-item>` module for each item that appears in one of these web sections in this drop-down menu.**
    -   Each of these web item modules must include a `section`attribute whose value is:
        -   The `linkId` attribute's value specified in the web item module for the drop-down menu (see [Define a &lt;web-item&gt; module for the drop-down menu itself.](#define-a-ltweb-itemgt-module-for-the-drop-down-menu-itself)), followed immediately by
        -   A slash symbol `/`, followed immediately by
        -   The `key` attribute's value specified in the relevant web section module (see [Define one or more &lt;web-section&gt; modules for this drop-down menu.](#define-one-or-more-ltweb-sectiongt-modules-for-this-drop-down-menu)).
    -   For example:

        ``` xml
        <web-item ... section="linkId_for_my_dropdown_menu/first_section_of_my_dropdown_menu" ...>
            ...
        </web-item>
        ```

### Adding Items to Existing Drop-down Menus

Based on the process [Adding Custom Drop-down Menus, Sections and Items](#adding-custom-drop-down-menus-sections-and-items), you can add your own item to an existing drop-down menu by defining a `<web-item>` module whose `section` attribute's value is:

-   The `linkId` attribute's value specified in the web item module for the drop-down menu (defined in the [JIRA source code](#source-file)), followed immediately by
-   A slash symbol `/`, followed immediately by
-   The `key` attribute's value specified in the relevant web section module (again found in the [JIRA source code](#source-file)).

For example, if you wanted to add a web item to the 'Dashboard' drop-down menu, the following diagram shows what values you would use for the `section` attribute of this web item, based on the web section of the drop-down menu.

![](/server/jira/platform/images/section-attribute-values-for-web-items-in-the-dashboard-drop-down.png)

### Source File

To find the values of JIRA's existing web items and sections for the `system.top.navigation.bar` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/system-top-navigation-plugin.xml`

### Customising the Positions of Menus, Items and Sections

To customise the position of your own web items or sections throughout JIRA's top navigation bar, add a weight attribute to your &lt;web-item&gt; or &lt;web-section&gt; and adjust its value with respect to JIRA's existing web items or sections.

-   For web items that define the drop-down menu itself (see [Define a &lt;web-item&gt; module for the drop-down menu itself.](#define-a-ltweb-itemgt-module-for-the-drop-down-menu-itself)), lower weight values result in menus appearing closer to the left of the user interface.
-   For web items and sections defined within a drop-down menu (see [Define one or more &lt;web-section&gt; modules for this drop-down menu.](#define-one-or-more-ltweb-sectiongt-modules-for-this-drop-down-menu) and [Define a &lt;web-item&gt; module for each item that appears in one of these web sections in this drop-down menu.](#define-a-ltweb-itemgt-module-for-each-item-that-appears-in-one-of-these-web-sections-in-this-drop-down-menu) above), lower weight values result in these items/sections appearing higher up the drop-down menu.

### Former Top Navigation Bar Location

{{% note %}}

Versions of JIRA prior to 4.0.

{{% /note %}}

The **`system.top.navigation.bar`** location specifies up to 2 optional URL prefixes (in web items with `section="system.top.navigation.bar"`), which are used to determine whether or not the link is selected.

This is done by adding a `param` element with names: `selected` and `selected2`. See [Web Item Plugin Module](/server/jira/platform/web-item) for details on how to add these elements.

![](/server/jira/platform/images/location-top-navigation-bar.png)

## User Name Drop-down Location

{{% note %}}

JIRA 4.0 and later versions.

{{% /note %}}

The **`system.user.options`** location defines web sections and items in JIRA's user name drop-down menu, which is accessible from all JIRA screens.

![](/server/jira/platform/images/web-fragments-user-name-drop-down.png)

You can add new web items to existing web sections in the user name drop-down menu (see [below](#existing-web-sections-of-the-user-name-drop-down-location)), or add new sections to this menu along with new items.

### Adding New Items to Existing Web Sections

To add your own web item to an existing web section of JIRA's user name drop-down location, your web item must include a `section` attribute with the value `"system.user.options/section_name"`, where `section_name` is the section of the user name drop-down menu to which the web item will be added.

For example, to add a web item module that defines a new item in the 'jira-help' section, your web item module would contain:

``` xml
<web-item ... section="system.user.options/jira-help" ...>
    ...
</web-item>
```

#### Existing Web Sections of the User Name Drop-down Location

<img src="/server/jira/platform/images/4391015.png" class="gliffy-macro-image" />

### Adding Custom Sections and Items

To add your own section to the user name drop-down menu, define a web section that includes a `location` attribute with the value `"system.user.options"`.

For example, your web section module would contain:

``` xml
<web-section key="my-custom-user-name-dropdown-section" ... location="system.user.options" ...>
    ...
</web-section>
```

Adding new web items to your own sections is similar to the method [Adding New Items to Existing Web Sections](#adding-new-items-to-existing-web-sections) for adding new web items to existing web sections. However, specify the value of your web section's `key` attribute in the value of your web item's `section` attribute (preceded by the location). Hence, following on from the previous example, your web item would contain:

``` xml
<web-item ... section="system.user.options/my-custom-user-name-dropdown-section" ...>
    ...
</web-item>
```

### Customising the Positions of Items and Sections

To customise the position of your own web items or sections in this location, add a `weight` attribute to your `<web-item>` or `<web-section>` and adjust its value with respect to JIRA's existing web items or sections. Lower weight values result in these items/sections appearing higher up the user name drop-down menu.

### Source File

To find the values of JIRA's existing web items and sections for the `system.user.options` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/webfragment/system-user-nav-bar-sections.xml`

## Navigation Bar User Options Location

{{% note %}}

Versions of JIRA prior to 4.0 *only*.

{{% /note %}}

The `system.user.navigation.bar` location, defines web items or sections for user options at the top-right of JIRA's navigation bar. 

{{% warning %}}

This location was replaced by the User Name Drop-down location in JIRA 4.0+ ([above](#user-name-drop-down-location)).

{{% /warning %}}

Custom web items must be added to either the 'links' or 'views' web sections of this location in JIRA:

-   `links`web section -- allows its links to be plain or pop-up links.
    -   To make a link to be a pop-up link, set a `param` element with name `isPopupLink` as true. Additional parameters can be set to dynamically set the height (`windowHeight`), width (`windowWidth`) and whether to have scrollbars (`scrollbars`) for the pop-up.
-   `views` web section -- displays all its links as icons and requires the `icon` element.

See [Web Item Plugin Module](/server/jira/platform/web-item) for details on how to add these elements.

![](/server/jira/platform/images/location-user-navigation-bar.png)

## Tools Drop-down on User Profile Location

The `system.user.profile.links` location defines web items of the 'Tools' drop-down menu on a JIRA user's user profile page.

![](/server/jira/platform/images/web-fragments-tools-user-profile-drop-down.png)

This location has only one section (`operations`) to which custom web items can be added.

### Adding Custom Items

To add your own web item to the Tools drop-down location, your web item must include a `section` attribute with the value `"system.user.profile.links/operations"`.

For example, to add a web item module that defines a new project operation link on a project configuration page, your web item module would contain:

``` xml
<web-item ... section="system.user.profile.links/operations" ...>
    ...
</web-item>
```

{{% note %}}

It is not possible to add your own web sections to the `system.user.profile.links` location.

{{% /note %}}

### Customising the Positions of Items

To customise the position of your own web items in this location, add a `weight` attribute to your `<web-item>` and adjust its value with respect to JIRA's existing web items. Lower weight values result in these items appearing higher up the drop-down menu.

### Source File

To find the values of JIRA's existing web items for the `system.user.profile.links` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/webfragment/system-user-profile-links.xml`

## Preset Issue Filters Drop-down on Project Summary Location

In JIRA 4.0 and later, the **`system.preset.filters`** location defines web items of the 'Filters' drop-down menu on the project summary page of a JIRA project.

![](/server/jira/platform/images/web-fragments-project-filters.png)

In versions of JIRA prior to 4.0, the **`system.preset.filters`** location defines links on the Project portlet of the dashboard and on the right-hand side of Browse Project page.

{{% note %}}

The filter links are only defined once and reused in both places.

{{% /note %}}

Links are displayed in two columns from left to right, then down (that is, the right column contains every second link).

<img src="/server/jira/platform/images/location-preset-filters.png" class="confluence-thumbnail" />

### Adding Custom Items

To add your own web item to JIRA's preset issue filters drop-down location, your web item must include a `section` attribute with the value `"system.preset.filters"`.

For example, to add a web item module that defines a new 'Filters' drop-down menu link (on a project summary page), your web item module would contain:

``` xml
<web-item ... section="system.preset.filters" ...>
    ...
</web-item>
```

{{% note %}}

It is not possible to add your own web sections to the `system.preset.filters` location.

{{% /note %}}

### Customising the Positions of Items

To customise the position of your own web items in this location, add a `weight` attribute to your `<web-item>` and adjust its value with respect to JIRA's existing web items. Lower weight values result in these items appearing higher up this 'Filters' drop-down menu.

### Source File

To find the values of JIRA's existing web items and sections for the `system.preset.filters` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/system-preset-filters-sections.xml`

## Hover Profile Links Location

{{% note %}}

Available in JIRA 4.2 and later.

{{% /note %}}

The `system.user.hover.links` location defines web items in JIRA's hover profile feature, which is accessible when a user hovers their mouse pointer over a JIRA user's name throughout JIRA's user interface.

![](/server/jira/platform/images/web-fragments-hover-profile.png)

### Adding Custom Items

To add your own web item to JIRA's hover profile links location, your web item must include a `section` attribute with the value `"system.user.hover.links"`.

For example, to add a web item module that defines a new hover profile link, your web item module would contain:

``` xml
<web-item ... section="system.user.hover.links" ...>
    ...
</web-item>
```

{{% note %}}

It is not possible to add your own web sections to the `system.user.hover.links` location.

{{% /note %}}

### Customising the Positions of Items

To customise the position of your own web items in this location, add a `weight` attribute to your `<web-item>` and adjust its value with respect to JIRA's existing web items. Lower weight values result in these items appearing to the left and then higher up the hover profile drop-down menu.

### Source File

To find the values of JIRA's existing web items and sections for the `system.user.hover.links` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/system-user-format-plugin.xml`

## Dialog Box Hint Location

{{% note %}}

Available in JIRA 4.2 and later.

{{% /note %}}

The **`jira.hints`** location defines web items that allow you to add hints on JIRA's dialog boxes. You can add hints to most JIRA dialog boxes.

![](/server/jira/platform/images/web-fragments-shortcut-hints.png)

### Adding Custom Items

To add your own web item to JIRA's dialog box hints location for a specific dialog box, your web item must include a `section` attribute with the value `"jira.hints/LOCATION_CONTEXT"`, where `LOCATION_CONTEXT` is a predefined 'context' in JIRA that determines on which dialog box your hints will appear.

The following table lists these predefined contexts available in JIRA's dialog box hint location.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Context</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>TRANSITION</p></td>
<td><p>Hints on a 'transition issue' dialog box.</p></td>
</tr>
<tr class="even">
<td><p>ASSIGN</p></td>
<td><p>Hints on the 'Assign' dialog box.</p></td>
</tr>
<tr class="odd">
<td><p>LABELS</p></td>
<td><p>Hints on the 'Labels' dialog box.</p></td>
</tr>
<tr class="even">
<td><p>COMMENT</p></td>
<td><p>Hints on 'Comment' dialog boxes.</p></td>
</tr>
<tr class="odd">
<td><p>CLONE</p></td>
<td><p>Hints on 'Clone Issue' dialog box.</p></td>
</tr>
<tr class="even">
<td><p>DELETE_FILTER</p></td>
<td><p>Hints on 'Delete Filter' dialog box.</p></td>
</tr>
<tr class="odd">
<td><p>ATTACH</p></td>
<td><p>Hints on 'Attach Files' dialog box (not the 'Attach Screenshot' one).</p></td>
</tr>
<tr class="even">
<td><p>DELETE_ISSUE</p></td>
<td><p>Hints on 'Delete Issue' dialog box.</p></td>
</tr>
<tr class="odd">
<td><p>LINK</p></td>
<td><p>Hints on 'Link Issue' dialog box.</p></td>
</tr>
<tr class="even">
<td><p>LOG_WORK</p></td>
<td><p>Hints on 'Log Work' dialog box.</p></td>
</tr>
</tbody>
</table>

For example, to add a web item module that defines hints on the 'Attach Files' dialog box, your web item module would contain:

``` xml
<web-item ... section="jira.hints/ATTACH" ...>
    ...
</web-item>
```

### Source File

To find out how existing web items for the `jira.hints` location are implemented in JIRA, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/webfragment/system-hints.xml`
