---
aliases:
- /server/jira/platform/jira-rest-api-for-remote-issue-links-6848577.html
- /server/jira/platform/jira-rest-api-for-remote-issue-links-6848577.md
category: reference
confluence_id: 6848577
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848577
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848577
date: '2018-06-26'
legacy_title: JIRA REST API for Remote Issue Links
platform: server
product: jira
subcategory: api
title: "Jira REST API for Remote Issue Links"
---
# Jira REST API for Remote Issue Links

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 5.0 and later.</p></td>
</tr>
</tbody>
</table>

This page gives a practical introduction to the Jira Remote Issue Links REST API for app developers. We assume
that you know what remote links are and the value they provide. If not, see the overview on
[Jira Remote Issue Links](/server/jira/platform/creating-remote-issue-links) page.

To use the REST API to manipulate remote links on an issue, you need to authenticate as a user that has the
permission to create links on the issue.

## Creating links

### The minimal Remote Link

There are only two required fields for creating a link to a remote object: a URL and a link title.

``` bash
POST http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink
```

``` javascript
{
    "object": {
        "url":"http://www.mycompany.com/support?id=1",
        "title":"Crazy customer support issue"
    }
}
```

The request will give you the successful response:

``` javascript
{
    "self":"http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink/100",
    "id":100
}
```

And it will give render simple link in the UI.  
![](/server/jira/platform/images/issuelinks1.png)

To ensure a good user experience (that is, UX), the ability to easily update link information and provide custom
link rendering apps in Jira, we suggest that you provide all the recommended fields defined in the guide to the
[Remote Issue Links fields](/server/jira/platform/using-fields-in-remote-issue-links).

### The complete Remote Link

It is possible to customize what is displayed on the Remote Link by providing more information about the link.
For a full definition of all the fields, both recommended and optional, see the guide to the
[Remote Issue Links fields](/server/jira/platform/using-fields-in-remote-issue-links).

Here is another example of request and response:

``` bash
POST http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink
```

``` javascript
{
    "globalId": "system=http://www.mycompany.com/support&id=1",
    "application": {                                            
         "type":"com.acme.tracker",                      
         "name":"My Acme Tracker"
    },
    "relationship":"causes",                           
    "object": {                                            
        "url":"http://www.mycompany.com/support?id=1",     
        "title":"TSTSUP-111",                             
        "summary":"Crazy customer support issue",        
        "icon": {                                         
            "url16x16":"http://www.openwebgraphics.com/resources/data/3321/16x16_voice-support.png",    
            "title":"Support Ticket"     
        },
        "status": {                                           
            "resolved": true,                                          
            "icon": {                                                       
                "url16x16":"http://www.openwebgraphics.com/resources/data/47/accept.png",
                "title":"Case Closed",                                     
                "link":"http://www.mycompany.com/support?id=1&details=closed"
            }
        }
    }
}
```

This example will give the same REST response as the earlier example, but it will render the following link in the UI.

<img src="/server/jira/platform/images/jira-rest-api-screenshot2.png" width="700" />

{{% note %}}
If you create a link with the same `globalId` as an existing Remote Link on an issue, you will update the
existing remote link instead of creating a new one. See about updating a Remote Link by global ID later on this page.
{{% /note %}}

## Updating links

### Updating a Remote Link by global ID

When you create a link, you can optionally supply `globalId` – an identifier that uniquely identifies the remote
application and the remote object within the remote system. This globally unique identifier can be used to update
or delete existing links on an issue.

``` bash
POST http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink
```

``` javascript
{
    "globalId": "system=http://www.mycompany.com/support&id=1",                          
    "object": {                                                     
        "url":"http://www.mycompany.com/support?id=1",
        "title":"Crazy customer support issue (RESOLVED)"                        
    }
}
```

If a remote link on the issue exists with the same URL, the remote link will be updated. Otherwise it will be created.

The update will replace all existing fields with the new values. Fields not present in the request are
interpreted as having a null value.

### Updating a Remote Link by internal ID

You can also update a Remote Link by referring to the internal ID returned after creation. You can get this ID
by sending a GET request for a list of remote links on an issue.

``` bash
PUT http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink/100
```

``` javascript
{   
    "object": {
        "url":"http://www.mycompany.com/support?id=1",
        "title":"Crazy customer support issue (RESOLVED)"
    }
}
```

The update will replace all existing fields with the new values. Fields not present in the request are interpreted
as having a null value.

This approach can be used to update the `globalId` on a remote link.

## Deleting links

### Deleting a Remote Link by global ID

When you create a link, you can optionally supply a `globalId` – an identifier that uniquely identifies the remote
application and the remote object within the remote system. This globally unique identifier can be used to update
or delete existing links on an issue. The `globalId` in the URL should be URL encoded.

``` bash
DELETE http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink?globalId=system%3Dhttp%3A%2F%2Fwww.mycompany.com%2Fsupport%26id%3D1
```

This will result in a 404 if the issue does not have a Remote Link matching the `globalId`.

### Deleting a Remote Link by internal ID

You can delete Remote Links by executing a DELETE method on a known Remote Link internal ID:

``` bash
DELETE http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink/100
```

This will result in a 404 if the link does not exist.

## Querying links

### Finding all Remote Links on an issue

You can find all the Remote Links associated with an issue by making a GET on the following resource:

``` bash
GET http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink
```

This will return an array of Remote Links:

``` javascript
[{
    "id":100,
    "self":"http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink/100",
    "object": {
        "url":"http://www.mycompany.com/support?id=1",
        "title":"Crazy customer support issue (RESOLVED)"    
    }
}]
```

### Finding a Remote Link by global ID

You can find a Remote Link on an issue by querying by the `globalId`.

``` bash
GET http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink?globalId=system%3Dhttp%3A%2F%2Fwww.mycompany.com%2Fsupport%26id%3D1
```

### Finding a Remote Link by internal ID

You can also find a Remote Link on an issue by entering the "self" URL that specifies the internal ID.

``` bash
GET http://localhost:8090/jira/rest/api/latest/issue/TST-1/remotelink/100
```
