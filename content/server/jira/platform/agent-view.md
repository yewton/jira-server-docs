---
aliases:
- /server/jira/platform/jira-service-desk-ui-plugin-points-agent-view-41234075.html
- /server/jira/platform/jira-service-desk-ui-plugin-points-agent-view-41234075.md
category: reference
confluence_id: 41234075
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41234075
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41234075
date: '2017-12-08'
legacy_title: JIRA Service Desk UI plugin points - Agent view
platform: server
product: jira
subcategory: modules
title: Agent view
---
# Agent view

This page lists the plugin points for the agent view in JIRA Service Desk (Server). 

## Queues

### Defining a queue section (group)

When you add a queue to the agent view, you have the option of providing a queues section (group) which contains one or more of your queues under it. If you don't provide a queues section, your queue will be added to the generic "Add-ons" section which is shared with all add-ons.

To define a queues section, use the following plugin point:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Plugin point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.agent.queues</code></pre></td>
<td><p>A queue group, the key of which will be used as the section for your queue link.</p>
<p><strong>Web fragment type:</strong> Web section.</p></td>
</tr>
</tbody>
</table>

### Defining a queue

To define a queue, you'll need both a web item and a web panel. The web item is used to display the link in the navigation menu and the web panel is for the actual contents of your queue.

In order for JIRA Service Desk to correctly associate your web item to your web panel and display them both, you'll need to do the following:

-   Set the "`section`" attribute of your web item to either of the following: the key of your queues web section (if you defined one) or "`sd-queues-addons`" (if you want your queue shown under the "Add-ons" section).
-   Set the "`link`" element of your web item to the following: "`/projects/$projectKey/queues/addon/<your-web-item-key>`", where "`<your-web-item-key>`" is the key of your web item.
-   Ensure that the "`location`" attribute of your web panel is the same as the key of your web item.
-   Ensure that there is only one web panel per web item.

For example, here is the web item and web panel definitions for a queue that will be displayed under the generic "Add-ons" section.
The following code should be placed in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/).

``` xml
<web-item key="my-queue" section="sd-queues-addons">
    <label key="my.queue.label" />
    <link>$baseurl/projects/$projectKey/queues/addon/my-queue</link>
</web-item>

<web-panel key="my-queue-content" location="my-queue">
    <resource name="view" type="static"><![CDATA[<b>Hello World!</b>]]></resource>
</web-panel>
```

## Reports

### Defining a reports section (group)

When you add a report to the agent view, you have the option of providing a reports section (group) which contains one or more of your reports under it. If you don't provide a reports section, your report will be added to the generic "Add-ons" section which is shared with all add-ons.

To define a reports section, use the following plugin point:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Plugin Point Location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.agent.reports</code></pre></td>
<td><p>A report group, the key of which will be used as the section for your report link.</p>
<p><strong>Web fragment type:</strong> Web section.</p></td>
</tr>
</tbody>
</table>

### Defining a report

To define a report, you'll need both a web item and a web panel. The web item is used to display the link in the navigation menu and the web panel is for the actual contents of your report.

In order for JIRA Service Desk to correctly associate your web item to your web panel and display them both, you'll need to do the following:

-   Set the "`section`" attribute of your web item to either of the following: the key of your reports web section (if you defined one) or "`sd-reports-addons`" (if you want your queue shown under the "Add-ons" section).
-   Set the "`link`" element of your web item to the following: "`/projects/$projectKey/reports/addon/<your-web-item-key>`", where "`<your-web-item-key>`" is the key of your web item.
-   Ensure that the "`location`" attribute of your web panel is the same as the key of your web item.
-   Ensure that there is only one web panel per web item.

For example, here is the web item and web panel definitions for a report that will be displayed under the generic "Add-ons" section:

``` xml
<web-item key="my-report" section="sd-reports-addons">
    <label key="my.report.label" />
    <link>$baseurl/projects/$projectKey/reports/addon/my-report</link>
</web-item>

<web-panel key="my-report-content" location="my-report">
    <resource name="view" type="static"><![CDATA[<b>Hello World!</b>]]></resource>
</web-panel>
```
