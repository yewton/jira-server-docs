---
title: JIRA REST API Example Edit Issues 6291632
aliases:
    - /server/jira/platform/jira-rest-api-example-edit-issues-6291632.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291632
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291632
confluence_id: 6291632
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - Edit issues

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

 

The Rest API allows you to edit an issue.

The JIRA REST api is very flexible in how it allows you to edit an issue. You can PUT a document, an updated version of the issue you received with a GET request, or you can send commands to set or modify fields of an issue.

{{% note %}}

The examples shown here use curl with an input file denoted by the "--data @filename" syntax and the file data is shown separately

{{% /note %}}

# Field meta-data

The editable fields and the operations they support can be *obtained* using the "editmeta" resource, e.g.

``` javascript
http://kelpie9:8081/rest/api/2/issue/XSS-13/editmeta
```

Note, the "editmeta" resource does not work with PUT operations. You should only use it to get data.

# Examples of updating an issue using fields.

### Example of assigning an issue to user "harry"

This simple edit

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
   "fields": {
       "assignee":{"name":"harry"}
   }
}
```

##### Response

You should just receive a response with a status of "204 No Content"

### Example of updating many fields at the same time

Here we update the assignee and also the summary, priority, and two custom fields.

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
    "fields" : {
        "customfield_10200" :
        {"value" : "Test 1"}
        ,
        "customfield_10201" :
        {"value" : "Value 1"}
    }
}
```

##### Response

You should just receive a response with a status of "204 No Content"

# Examples of updating a field using operations.

The fields of an issue may also be updated in more flexible ways using the SET, ADD and REMOVE operations. Not all fields support all operations, but as a general rule single value fields support SET, whereas multi-value fields support SET, ADD and REMOVE, where SET replaces the field contents while ADD and REMOVE add or remove one or more values from the the current list of values.

### Adding a component

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
   "update" : {
       "components" : [{"add" : {"name" : "Engine"}}]
   }
}
```

##### Response

You should just receive a response with a status of "204 No Content"

### Setting the components field

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
    "update" : {
        "components" : [{"set" : [{"name" : "Engine"}, {"name" : "Trans/A"}]}]
    }
}
```

##### Response

You should just receive a response with a status of "204 No Content"

### Adding a component and removing another

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
    "update" : {
        "components" : [{"remove" : {"name" : "Trans/A"}}, {"add" : {"name" : "Trans/M"}}]
    }
}
```

Note: The "Engine" component (if it exists) remains unaffected by this update.

##### Response

You should just receive a response with a status of "204 No Content"

### Manipulating multiple fields

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
    "update" : {
        "components" : [{"remove" : {"name" : "Trans/A"}}, {"add" : {"name" : "Trans/M"}}],
        "assignee" : [{"set" : {"name" : "harry"}}],
        "summary" : [{"set" : "Big block Chevy"}]
    }
}
```

##### Response

You should just receive a response with a status of "204 No Content"
