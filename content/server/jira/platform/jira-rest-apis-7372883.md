---
title: JIRA REST APIs 7372883
aliases:
    - /server/jira/platform/jira-rest-apis-7372883.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7372883
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7372883
confluence_id: 7372883
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST APIs

This page provides an overview of the JIRA REST APIs, which are the only remote APIs supported for JIRA. The examples on this page and in the tutorials use the JIRA platform REST API, but many of the principles can also be applied to the REST APIs for the JIRA applications.

-   *Looking for the full REST API documentation for JIRA? See the [JIRA APIs] page.*

## Overview

The Atlassian REST APIs provide a standard interface for interacting with JIRA and our other applications. 

REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. Your methods will be the standard HTTP methods like GET, PUT, POST and DELETE. REST APIs operate over HTTP(s) making it easy to use with any programming language or framework. The input and output format for the JIRA REST APIs is <a href="http://www.json.org/" class="external-link">JSON</a>.

JIRA uses the [Atlassian REST plugin] to implement the JIRA APIs. The REST plugin is bundled with JIRA. You can add your own REST APIs to JIRA by [creating a JIRA plugin] that includes the REST plugin module. 

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy].

## Browsing and Testing your REST APIs from the Application UI

Since the specifics of the REST API vary between Atlassian applications, the best way to learn about the REST API Browser for your product is to use the REST API Browser built into your JIRA instance. 

The REST API Browser lets you browse, inspect and try the REST API Browser for your application. It's a tool for discovering the REST APIs and other remote APIs available in Atlassian applications.

The browser is built into several types of Atlassian applications, including Stash and recent versions of Confluence and JIRA. It's also a part of the <a href="https://marketplace.atlassian.com/plugins/com.atlassian.devrel.developer-toolbox-plugin" class="external-link">Developer Toolbox plugin</a> (included by default in developer instances of Atlassian applications) or available <a href="https://marketplace.atlassian.com/plugins/com.atlassian.labs.rest-api-browser" class="external-link">separately</a>. Note, the Developer Toolbox plugin is not compatible with JIRA 7 and later.

For details on getting and using the RAB, see [Using the REST API Browser]. 

## A Simple Example: Creating an Issue in JIRA

Want to create an issue in JIRA via REST? Here's how easy it is.

The examples below use <a href="http://curl.haxx.se/" class="external-link">curl</a>. Ideally, you should use the REST API Browser to investigate how the API works, but this example is meant to give you a glimpse.  We'll create an issue in a local JIRA instance.

A few points to note about our example:

-   The input file is denoted by the '`--data @filename`' syntax. The data is shown separately, and uses the JSON format.
-   Make sure the content type in the request is set to '`application/json`', as shown in the example.
-   POST the JSON to your JIRA server. In the example, the server is `http://localhost:8080/jira/rest/api/2/issue/`.
-   The example uses basic authentication, with a username of admin and password of admin.
-   You'll need to add a project to the instance before running, and get the project ID of the project to which you want to add the issue beforehand.

To try a simple REST command with curl, follow these steps:

1.  Start by creating the data file that contains the POST data. In our command, we'll assume the file is named `data.txt`. Add the following content:

    ``` javascript
    {
        "fields": {
           "project":
           {
              "id": "10000"
           },
           "summary": "No REST for the Wicked.",
           "description": "Creating of an issue using ids for projects and issue types using the REST API",
           "issuetype": {
              "id": "3"
           }
       }
    }
    ```

    Our project ID is 10000. You should use a suitable ID of a project in your instance. Also, the issue type in our case is 3, which represents a task.

    Alternatively, find the api/2/issue resource in the REST API Browser, and paste the text above into the Request Body field to try it out.

2.  From the command line enter the following command:

    ``` javascript
    curl -u admin:admin -X POST --data @data.txt -H "Content-Type: application/json" http://localhost:8080/jira/rest/api/2/issue/
    ```

    As before, adjust details for your environment, such as the hostname or port of the JIRA instance. Note that a cloud instance or most public instances would require the use of HTTPS and of course valid credentials for the instance.

The response provides the issue ID, issue key, and the URL to the issue (which can then be used to GET additional data, PUT updates, etc).

``` javascript
{
   "id":"10009",
   "key":"TEST-10",
    "self":"http://localhost:8080/jira/rest/api/2/issue/10009"
} 
```

Instead of using numeric identifiers for the project and issue type, you could have used the key and name of the type instead. For example:

``` javascript
{
    "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "REST ye merry gentlemen.",
       "description": "Creating of an issue using project keys and issue type names using the REST API",
       "issuetype": {
          "name": "Task"
       }
   }
}
```

To learn more about creating an issue via REST, see:

-   Our tutorial about [creating an issue via REST].
-   Information about [updating an issue via REST] is equally applicable to creating an issue.

## Authentication

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. See the examples of [basic authentication], [cookie-based authentication], and [OAuth].

## How to Edit and Update Issues

A guide to [how to update an issue via REST][updating an issue via REST].

## Tutorials

Get hands-on with the JIRA REST API with our [JIRA REST API tutorials].

  [JIRA APIs]: /server/jira/platform/jira-apis-32344053.html
  [Atlassian REST plugin]: /server/jira/platform/rest-module-type
  [creating a JIRA plugin]: /server/jira/platform/about-jira-plugin-development-4227105.html
  [Atlassian REST API policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [Using the REST API Browser]: https://developer.atlassian.com/display/DOCS/Using+the+REST+API+Browser
  [creating an issue via REST]: /server/jira/platform/jira-rest-api-example-create-issue-7897248.html
  [updating an issue via REST]: /server/jira/platform/updating-an-issue-via-the-jira-rest-apis-6848604.html
  [basic authentication]: /server/jira/platform/jira-rest-api-example-basic-authentication-6291732.html
  [cookie-based authentication]: /server/jira/platform/jira-rest-api-example-cookie-based-authentication-37234858.html
  [OAuth]: /server/jira/platform/jira-rest-api-example-oauth-authentication-6291692.html
  [JIRA REST API tutorials]: /server/jira/platform/jira-rest-api-tutorials-6291593.html
