---
aliases:
- /server/jira/platform/preliminary-plugin-guide-to-jira-archiver-26935629.html
- /server/jira/platform/preliminary-plugin-guide-to-jira-archiver-26935629.md
category: devguide
confluence_id: 26935629
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26935629
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26935629
legacy_title: Preliminary Plugin Guide to JIRA Archiver
date: '2018-04-20'
platform: server
product: jira
subcategory: datacenter
title: Project archiving API
---

# Project archiving API

This page provides an overview of how to use API to archive your projects in Jira. Project archiving is available in Jira Data Center 7.10, or later. 

## What is archiving?

You can archive any project in Jira if you're no longer working on it. This helps you clean your instance of unused projects and also improve performance. After you archive a project, it will be removed from the list of projects, project pickers, and search results. Both you and your users won't be able to access the project itself, but you will be able to view all issues from an archived project if they're mentioned or linked in other projects. These issues will be read-only.

All data related to an archived project will remain in the database, and you can restore the project whenever you need it again.

## Archiving with REST API

Here are some examples of how to archive and restore your projects with REST API. For more info about these API calls, see [Archive a project](https://docs.atlassian.com/software/jira/docs/api/REST/7.9.0/#api/2/project-archiveProject) and [Restore a project](https://docs.atlassian.com/software/jira/docs/api/REST/7.9.0/#api/2/project-restoreProject) in the Jira REST API docs.

#### Archiving a project:
`curl -X PUT https://jira-instance1.net/rest/api/2/project/PROJ-1/archive`

#### Restoring a project:
After restoring a project, you need to re-index it to make it appear again in the search results. You can do it in Jira by going to **Project settings > Re-index**.

`curl -X PUT https://jira-instance1.net/rest/api/2/project/PROJ-1/restore`

<br>

## Archiving with JAVA API

Here are some examples of how to archive and restore your projects with JAVA API. For more info about these API calls, see [Jira JAVA API documentation](https://docs.atlassian.com/software/jira/docs/api/7.9.0/com/atlassian/jira/project/archiving/ArchivedProjectService.html).

#### Retrieving the list of archived projects:
`ComponentAccessor.getProjectManager().getArchivedProjects();`


#### Checking if a project is archived:
`curl -X PUT https://jira-instance1.net/rest/api/2/project/PROJ-1/restore`


#### Archiving a project:

```
ValidationResult validationResult =
        archivedProjectService.validateArchiveProject(authContext.getLoggedInUser(), project.getKey());

if (validationResult.isValid()) {
    archivedProjectService.archiveProject(validationResult);
}
```

#### Restoring a project:
After restoring a project, you need to re-index it to make it appear again in the search results. You can do it in Jira by going to **Project settings > Re-index**.

```
ValidationResult validationResult =
        archivedProjectService.validateRestoreProject(authContext.getLoggedInUser(), project.getKey());

if (validationResult.isValid()) {
    archivedProjectService.restoreProject(validationResult);
}
```

#### Notifying the plugins about archived projects:

You can use the ProjectArchivedEvent and ProjectRestoredEvent event listeners to notify your plugins whenever a project is archived or restored. Here's an example implementation:

```
public class ArchivedProjectEventListener {
    private static final Logger log = LoggerFactory.getLogger(ArchivedProjectEventListener.class);

    @EventListener
    public void onProjectArchived(ProjectArchivedEvent event) {
        // Removing the data related to event.getProjectId()
        // e.g. cache, or plugin data.
        log.info("A project has been archived.");
    }
}
```

We recommend that your plugins remove any non-critical data related to archived projects. An example of that is removing data from the index, which Jira does automatically when archiving a project. This data is preserved in the database and can be easily restored later, and there's no need to store it in Jira.
