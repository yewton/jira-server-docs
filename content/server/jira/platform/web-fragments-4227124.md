---
title: Web Fragments 4227124
aliases:
    - /server/jira/platform/web-fragments-4227124.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227124
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227124
confluence_id: 4227124
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Web fragments

A web fragment is a link, a section of links or an area of HTML content (or 'panel') in a particular location of the JIRA web interface. A web fragment could be a menu in JIRA's top navigation bar (which in itself may contain its own links and link sections), buttons on the issue operations bar or panels on the view issue page.

#### Plugin Modules Involved in Creating a Web Fragment

A web fragment can consist of three kinds of plugin module:

-   A [Web Item](/server/jira/platform/web-item) module defines an individual link that is displayed at a particular location or section of the JIRA user interface. A web item might define an individual item within a JIRA drop-down menu or a button on the issue operations bar.
-   A [Web Section](/server/jira/platform/web-section) module defines a collection of links that is displayed together at a particular location of the JIRA user interface. A web section might define a group of items within a JIRA drop-down menu (separated by lines) or a group of buttons on the issue operations bar.
-   A [Web Panel](/server/jira/platform/web-panel) module defines a section of HTML content displayed on a JIRA page.

Web items or web sections are utilised in a number of different ways, depending on the location of the web fragment you are creating.

##### Existing Web Fragments in JIRA

You can insert custom web fragments into existing ones in JIRA (e.g. web sections which are 'built in' to JIRA). However, you cannot redefine JIRA's existing web fragments.

We recommend downloading the <a href="https://my.atlassian.com/download/source/jira" class="external-link">JIRA source</a> (valid <a href="https://www.atlassian.com/licensing/purchase-licensing#source-1" class="external-link">JIRA license</a> required), so you can access the appropriate source files that define JIRA's existing web fragments. This will help you:

-   Alter the positions of your own web fragments
-   Help you ensure `key` values in your own `<web-items>` and `<web-sections>` remain unique.

{{% note %}}

You need to log in as a user with a commercial license to access the download page for the JIRA source archive.

{{% /note %}}

## Locations

Web fragments can be developed for a number of locations throughout the JIRA web interface. This section describes these locations and for each one:

-   where you can add web fragments for your plugin
-   how you implement web item, web section and web panel plugin modules to construct these web fragments

For details on how to create web fragments for the appropriate location of JIRA's user interface, refer to the appropriate documentation below:

-   [Administration Area Locations](/server/jira/platform/administration-ui-locations)
-   [Project Configuration Locations](/server/jira/platform/project-settings-ui-locations) -- including the:
    -   General project administration tabs
    -   Summary tab
    -   Operations buttons and menu items
-   [User Accessible Locations](/server/jira/platform/general-navigation-ui-locations) -- including:
    -   Top navigation bar
    -   User name drop-down menu
    -   Tools drop-down menu on the user profile
    -   Preset issue filters drop-down menu on the project summary tab
    -   Hover profile links
    -   Dialog box hints
-   [View Issue Page Locations](/server/jira/platform/issue-view-ui-locations) -- including:
    -   Issue operations bar
    -   Right side of the 'View Issue' page

























































































































































































































































