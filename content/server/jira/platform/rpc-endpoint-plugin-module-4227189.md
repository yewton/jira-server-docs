---
title: Rpc Endpoint Plugin Module 4227189
aliases:
    - /server/jira/platform/rpc-endpoint-plugin-module-4227189.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227189
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227189
confluence_id: 4227189
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : RPC Endpoint Plugin Module

{{% warning %}}

JIRA's SOAP and XML-RPC remote APIs were deprecated in JIRA 6.0.

[Read the announcement for more information.](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice)

{{% /warning %}}The RPC endpoint plugin modules enable you to publish new SOAP and XML-RPC endpoints within JIRA.

These endpoints allow you to expose your own remote web services to the outside world.

Here are two example RPC endpoint descriptors:

``` javascript
<rpc-soap key="soap" name="System SOAP Services"
    class="com.atlassian.jira.rpc.soap.JiraSoapServiceImpl">
    <description>The standard JIRA SOAP services.</description>
    <service-path>jiraservice-v1</service-path>
    <published-interface>
        com.atlassian.jira.rpc.soap.JiraSoapService
    </published-interface>
</rpc-soap>

<rpc-xmlrpc key="xmlrpc" name="System XML-RPC Services"
    class="com.atlassian.jira.rpc.xmlrpc.JiraXmlRpcService">
    <description>The standard JIRA XML-RPC services.</description>
    <service-path>jira1</service-path>
</rpc-xmlrpc>
```

The first defines a SOAP service, the second an XML-RPC service. These objects (class="x") will be published via Glue or Apache XML-RPC respectively. The **`service-path`** element defines where in the URL namespace the services will be published. The **`published-interface`** element for the SOAP module defines which interface will be published (XML-RPC publishes all methods of the object).

Please take note that you also have to create a [Component Plugin Module](/server/jira/platform/component) to avoid the client getting `NullPointerException`, e.g.

``` javascript
<component key="component-FoobarSoapService" name="Foobar SOAP Service Component" class="com.atlassian.jira.rpc.FoobarSoapServiceImpl">
    <interface>com.atlassian.jira.rpc.FoobarSoapService</interface>
</component>

<rpc-soap key="soap-foobar" name="Foobar SOAP Service" class="com.atlassian.jira.rpc.FoobarSoapServiceImpl">
    <description>Foobar SOAP service.</description>
    <service-path>foobarservice</service-path>
    <published-interface>com.atlassian.jira.rpc.FoobarSoapService</published-interface>
</rpc-soap>
```

You can learn more about RPC plugins from looking at JIRA's system RPC plugin. The source to this plugin is also freely available, and serves as an excellent learning resource.

{{% note %}}

You may also wish to see the [Creating an XML-RPC Client](/server/jira/platform/creating-an-xml-rpc-client-4227133.html) and [Creating a JIRA SOAP Client](/server/jira/platform/creating-a-jira-soap-client-4227095.html) if you're interested in creating a JIRA remote client.

{{% /note %}}

























































































































































































































































