---
aliases:
- /server/jira/platform/-archivenotice-35721552.html
- /server/jira/platform/-archivenotice-35721552.md
category: devguide
confluence_id: 35721552
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35721552
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35721552
date: '2017-12-08'
legacy_title: _ArchiveNotice
platform: server
product: jira
subcategory: other
title: _ArchiveNotice
---
# \_ArchiveNotice

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}
