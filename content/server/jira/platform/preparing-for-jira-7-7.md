---
category: devguide
subcategory: updates
date: '2017-12-19'
title: Preparing for JIRA 7.7
product: jira
platform: server
---
# Preparing for JIRA 7.7

This page covers changes in **JIRA 7.7** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.7 products. We also make release milestones available prior to the release:

- For **JIRA Server**, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](https://developer.atlassian.com/display/JIRADEV/JIRA+EAP+Releases).

- For **JIRA Cloud**, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Risk level/Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **Asynchronous Module Definition**<br>JIRA Core, JIRA Software, JIRA Service Desk | Risk level: None<br>Affects: Plugin developers<br>The JIRA development team has started working on converting the JIRA code to AMD. Read below for details.
| **API to manage priority schemes**<br>JIRA Core, JIRA Software | Risk level: None<br>Affects: JIRA administrators<br>JIRA 7.7 will bring experimental REST APIs to manage priority schemes outside of the user interface. Read below for details.|

## JIRA platform changes
**All changes in JIRA platform are also relevant for JIRA Core, JIRA Software, and JIRA Service Desk.**

#### Changes for plugin developers:

**Asynchronous Module Definition**

The JIRA development team has started converting the JIRA code to AMD (Asynchronous Module Definition) to be able to use modules and their dependencies, and to improve the code quality, and all our lives, in general. We’ve converted only some parts of the code, and no actions are required from you. This is just a heads up about our plans—eventually, we want to completely switch to AMD, in which case we’ll drop support for global variables.

We will be informing you about the progress in this area with each release. Despite the recent code changes, we’ve kept the backward compatibility, so you can develop your plugins the way you used to. 

If you encounter a JIRA page that still uses global variables (for objects that are provided as AMD modules), you’ll see a warning in the developer console saying that a global variable is deprecated and you should use an AMD module instead. You can either use the module, or stick to the global variable for now.

#### Changes for JIRA administrators:

**REST API to manage priority schemes**

JIRA 7.7 will bring experimental REST APIs to manage priority schemes outside of the user interface. **They are not applicable to JIRA Service Desk**. As a JIRA administrator, you’ll be able to perform the following actions on priority schemes:

- Create priority schemes (`POST /rest/api/2/priorityschemes`)
- Retrieve a list of priority schemes (`GET /rest/api/2/priorityschemes`)
- Retrieve a priority scheme (`GET /rest/api/2/priorityschemes/{schemeId}`)
- Delete priority schemes (`DELETE /rest/api/2/priorityschemes/{schemeId}`)
- Update priority schemes, e.g with new priorities (`PUT /rest/api/2/priorityschemes/{schemeId}`)
- Associate a project with a priority scheme (`PUT /rest/api/2/project/{projectKeyOrId}/priorityscheme`)
- Disassociate a project from a priority scheme (`DELETE /rest/api/2/project/{projectKeyOrId}/priorityscheme/{schemeId}`)
- Retrieve a priority scheme associated with a project (`GET /rest/api/2/project/{projectKeyOrId}/priorityscheme`)


