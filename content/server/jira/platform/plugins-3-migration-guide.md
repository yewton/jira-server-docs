---
aliases:
- /server/jira/platform/plugins-3-migration-guide-17563753.html
- /server/jira/platform/plugins-3-migration-guide-17563753.md
category: devguide
confluence_id: 17563753
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=17563753
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=17563753
date: '2017-12-08'
legacy_title: Plugins 3 Migration Guide
platform: server
product: jira
subcategory: updates
title: Plugins 3 Migration Guide
---
# Plugins 3 Migration Guide

## Breaking changes

### The version of Atlassian Plugins

The new major version number of Atlassian Plugins means some plugins that import `com.atlassian.plugin*` packages will break.

Most plugins will import with a version set like this: 

`com.atlassian.plugin;version="2.8"`

These will be fine. Some, however, will have their import set like this: 

`com.atlassian.plugin;version="[2.8,3.0)"`

These will break.

To avoid this, change the import version for the second pattern -  `[2.8,4.0)` - since we don't plan on breaking any APIs in the `3.x` cycle.

### The default constructor of `AbstractModuleDescriptor`

This was removed. A lot of our own module descriptors were using the default constructor, which means their modules were using the `LegacyModuleFactory,` which is not the module factory configured for their products. In most cases, fixing this is as simple as changing the constructor. Sometimes it requires small changes in the module descriptor when loading module classes, and also adding an actual `component-import` for the module factory exposed by the host product.

### Deprecated `com.atlassian.plugin.util.collect.CollectionUtil` removed

This class doesn't bring anything that G`uava` doesn't, it was removed in favor of using `Guava`.

## Web Resource and Web Fragment split

The following two modules have been split into their own repositories, with their own versioning:

`com.atlassian.plugins:atlassian-plugins-webresource`

`com.atlassian.plugins:atlassian-plugins-webfragment`

ThIs means that those libraries don't necessarily have the same version as Atlassian Plugins anymore. They are usually already declared in product `POM`s, if not you will need to do so.
