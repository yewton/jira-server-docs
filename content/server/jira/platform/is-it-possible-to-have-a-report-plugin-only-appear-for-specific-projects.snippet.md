---
aliases:
- /server/jira/platform/4227128.html
- /server/jira/platform/4227128.md
category: devguide
confluence_id: 4227128
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227128
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227128
date: '2017-12-08'
legacy_title: Is it possible to have a report plugin only appear for specific projects?
platform: server
product: jira
subcategory: other
title: Is it possible to have a report plugin only appear for specific projects?
---
# Is it possible to have a report plugin only appear for specific projects?

This isn't really possible at the moment. You could work around this by implementing the method

``` javascript
boolean showReport();
```

and utilise the fact that JIRA stores the current project in the session. For example, you can find the currently selected project ID by calling.

``` javascript
(Long) ActionContext.getSession().get(SessionKeys.SELECTED_PROJECT);
```

You can then use this in your `showReport` method to return true for false depending on the project.
