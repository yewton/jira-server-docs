---
aliases:
- /server/jira/platform/configuring-plugins-with-object-configurable-parameters-4227079.html
- /server/jira/platform/configuring-plugins-with-object-configurable-parameters-4227079.md
category: devguide
confluence_id: 4227079
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227079
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227079
date: '2017-12-08'
legacy_title: Configuring Plugins with Object Configurable Parameters
platform: server
product: jira
subcategory: blocks
title: Object configurable parameters
---
# Object configurable parameters

Jira Report plugins use the `com.atlassian.configurable.ObjectConfigurable` class to simplify the process of requesting configuration parameters from users. These parameters are specified in `atlassian-plugin.xml` as part of the report module. For example, the time tracking report that ships with Jira has the following input parameters:

``` xml
<properties>
    <property>
        <key>versionId</key>
        <name>common.concepts.version</name>
        <description>report.timetracking.version.description</description>
        <type>select</type>
        <values class="com.atlassian.jira.portal.VersionOptionalValuesGenerator"/>
    </property>
    <property>
        <key>sortingOrder</key>
        <name>report.timetracking.sortingorder</name>
        <description>report.timetracking.sortingorder.description</description>
        <type>select</type>
        <values class="com.atlassian.jira.portal.SortingValuesGenerator"/>
    </property>
    <property>
        <key>completedFilter</key>
        <name>report.timetracking.filter</name>
        <description>report.timetracking.filter.description</description>
        <type>select</type>
        <values class="com.atlassian.jira.portal.FilterValuesGenerator"/>
    </property>
</properties>
```

### Types

Types are defined in the `com.atlassian.configurable.ObjectConfigurationTypes` class. Available types are:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Type</p></th>
<th><p>Input HTML Type</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>string</p></td>
<td><p>text box</p></td>
</tr>
<tr class="even">
<td><p>long</p></td>
<td><p>text box</p></td>
</tr>
<tr class="odd">
<td><p>select</p></td>
<td><p>select box</p></td>
</tr>
<tr class="even">
<td><p>multiselect</p></td>
<td><p>multi-select box</p></td>
</tr>
<tr class="odd">
<td><p>hidden</p></td>
<td><p>hidden field</p></td>
</tr>
<tr class="even">
<td><p>date</p></td>
<td><p>text box with calendar pop-up</p></td>
</tr>
<tr class="odd">
<td><p>user</p></td>
<td><p>text box with user picker pop-up</p></td>
</tr>
<tr class="even">
<td><p>text</p></td>
<td><p>text area</p></td>
</tr>
<tr class="odd">
<td><p>checkbox</p></td>
<td><p>checkbox</p></td>
</tr>
<tr class="even">
<td><p>cascadingselect</p></td>
<td><p>cascading select boxes</p></td>
</tr>
</tbody>
</table>

### Values

Values can be provided by a value provider class that must subclass `com.atlassian.configurable.ValuesGenerator`. Acceptable values can also be hardcoded into the module descriptor:

``` xml
<values>
    <value>
        <key>KEY1</key>
        <value>somevalue</value>
    </value>
    <value>
        <key>KEY2</key>
        <value>someothervalue</value>
    </value>
</values>
```

### Defaults

You can specify a default for all types as well:

``` xml
<default>5</default>
<values>  
        <value><key>1</key><value>1</value></value>
        <value><key>2</key><value>2</value></value>
        <value><key>3</key><value>3</value></value>
        <value><key>4</key><value>4</value></value>
        <value><key>5</key><value>5</value></value>
        ...
</values>
```
