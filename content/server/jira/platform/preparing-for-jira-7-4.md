---
aliases:
- /server/jira/platform/preparing-for-jira-7.4-44801920.html
- /server/jira/platform/preparing-for-jira-7.4-44801920.md
category: devguide
confluence_id: 44801920
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44801920
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44801920
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.4
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.4
---
# Preparing for JIRA 7.4

This page covers changes in **JIRA 7.4** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.4 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Change: <a href="#extended-project-administration">Extended project administration</a></p>
<p> Platform/Application: JIRA platform</p></td>
<td><p>Risk level: medium</p>
<p> Affects: Project administrators will gain additional features. These changes are reflected in APIs.</p></td>
</tr>
<tr class="even">
<td><p>Change: <a href="#kanban-backlog-for-jira-sofware">Kanban backlog for JIRA Sofware</a></p>
<p> Platform/Application: JIRA Software</p></td>
<td><p>Risk level: medium</p>
<p> Affects: Backlog added to kanban boards.</p></td>
</tr>
<tr class="odd">
<td><p>Change: Spring 4.2</p>
<p> Platform/Application: JIRA platform</p></td>
<td><p>Risk level: medium</p>
<p> Affects: Spring 4.2 added a change that checks the interface hierarchy for spring annotations, if you don't include those dependencies as OSGi imports, there's a risk it could break your plugin.</p></td>
</tr>
</tbody>
</table>

## Supported platforms

The definitive list of supported platforms will be released with the official documentation for JIRA 7.4, here we hope to give you advanced warning of platforms we're adding or removing so you may add support to your add-on.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"> </th>
<th>Platform</th>
<th style="text-align: center;">Support added or<br />
removed  </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Databases</td>
<td><p>PostgreSQL 9.6</p></td>
<td style="text-align: center;">added</td>
</tr>
</tbody>
</table>

## JIRA platform changes

#### Extended project administration

In JIRA 7.4 we plan to allow project administrators to perform the following tasks:

-   ability to edit unshared workflows (removing statuses)
-   edit unshared screens.

We also plan to introduce the concept of "**Extended project administration**". This feature will allow JIRA administrators to disable the new capabilities added to project administrators for particular permission schemes.

 <img src="/server/jira/platform/images/image2017-3-31-8:14:12.png" class="image-center" width="553" height="226" />

 We also plan to extend Audit Log capabilities to record more events and improve the filtering options.

#### API Changes

-   there's new `com.atlassian.jira.bc.project.ProjectAction` -  `EDIT_PROJECT_CONFIG_EXTENDED` which is the suggested way to check extended project administration permission for given project
-   replaced  ~~`hasEditWorkflowPermission()`~~ method with `isWorkflowEditable()` in `com.atlassian.jira.bc.workflow``.``WorkflowService` class
-   added `getWorkflowPermission()` method in `com.atlassian.jira.bc.workflow``.``WorkflowService` class returning `WorkflowService.WorkflowPermission` which tells whether given workflow is editable for given user and, if it's not editable, it specifies the reason why it canot be edited
-   `com.atlassian.jira.auditing.AuditingFilter` class got `projectIDs` property
-   `com.atlassian.jira.bc.issue.fields.screen.FieldScreenService` got basic screen management methods (add/remove/rename)
-   added concept of `PermissionSchemeAttribute` - abstract data that can be stored along with permission schemes and added appropriate manager - `com.atlassian.jira.permission.PermissionSchemeAttributeManager`
-   `com.atlassian.jira.bc.project.ProjectAction#getPermissions` method is no longer `public`, the visibility was changed to `package-private`

#### Rest API changes

There is experimental `/rest/api/2/permissionscheme/{schemeId}/attribute/` endpoint to reflect support of permission scheme attributes.  

## JIRA Core changes

#### No specific JIRA Core application changes.

## JIRA Software changes

#### Kanban backlog for JIRA Sofware

In JIRA 7.4 we plan to deliver the first version of Kanban backlog - plan mode for kanban boards. It can be enabled for any kanban board from the **Column** configuration page.

#### Removal of RapidView\#isKanPlanEnabled method

This property was introduced in JIRA Sofware to provide the ability to check if Kanban backlog is enabled for given board. In case you need this property check if there is at least one status mapped to Kanban backlog column.

## JIRA Service Desk changes

#### No specific JIRA Service Desk application changes.
