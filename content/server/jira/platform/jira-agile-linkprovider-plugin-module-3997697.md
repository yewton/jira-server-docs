---
title: JIRA Agile Linkprovider Plugin Module 3997697
aliases:
    - /server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=3997697
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=3997697
confluence_id: 3997697
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Agile LinkProvider Plugin Module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA Agile 5.3 and later</p></td>
</tr>
</tbody>
</table>

 

This module allows you to add links to the JIRA Agile context menus (internally referred to as the cog actions).

{{% warning %}}

This plugin module only works with Classic Boards. As yet there is not an equivalent plugin point for the new boards.

{{% /warning %}}

 

## Configuration

The root element for the LinkProvider plugin module is `gh-link-provider`. It allows the following attributes and child elements for configuration:

### Attributes

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
<th><p>Default</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>Yes</p></td>
<td><p>Name of the class that implements the LinkProvider interface.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>Yes</p></td>
<td><p>The identifier of the plugin module.</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p> </p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the links this provider adds.</p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### Elements

No elements are supported currently.

## Java API

The actual logic has to be implemented in a Java class implementing the `LinkProvider` interface. Two other objects, `LinkContext` and `Link` are also of interest.

### LinkProvider interface

Following interface needs to be implemented by link providers:

``` java
package com.atlassian.greenhopper.plugin.link;

import java.util.List;

/**
 * Interface to be implemented by plugins that want to add links to the GreenHopper action context menus.
 *
 * @author miruflin
 *
 */
public interface LinkProvider
{
    /**
     * Get the list of links to be displayed for the given context.
     *
     * @param linkContext context information such as the current board and issue
     * @return a list of links to be displayed. null or an empty list if no links should be displayed.
     */
    public List<Link> getLinks(LinkContext linkContext);
}
```

### LinkContext object

Following context is provided to the LinkProvider in order to determine what element on the page the link is about:

``` javascript
package com.atlassian.greenhopper.plugin.link;

import com.atlassian.jira.project.Project;
import com.opensymphony.user.User;
/**
 * Context object provided to LinkProvider objects.
 *
 * @author miruflin
 *
 */
public interface LinkContext
{
    /**
     * Get the project.
     *
     * @return the project object
     */
    public Project getProject();
    /**
     * Get the current user.
     *
     * @return the user, might be null
     */
    public User getUser();
    /**
     * Get the id of the board.
     *
     * @return the id, commonly -1 for none, the username for type AssigneeBoard, the version for type VersionBaord, the component for type ComponentBoard
     */
    public String getBoardId();
    /**
     * Get the board type.
     *
     * @return the name of the board, e.g. versionBoard.
     */
    public String getBoardType();
    /**
     * Get the issue id.
     *
     * @return the issue id or null if not applicable
     */
    public Long getIssueId();
    /**
     * Get the issue key.
     *
     * @return the issue key or null if not applicable
     */
    public String getIssueKey();
    /**
     * Assignee "filter" value.
     * Note that this does not apply to the assignee board, in which case the board id is the selected assignee.
     *
     * @return the assignee drop down value
     */
    public String getByAssignee();
    /**
     * Version "filter" value.
     * Note that this does not apply to the version board, in which case the board id is the selected version.
     *
     * @return the version drop down value
     */
    public String getByVersion();
}
```

### Link object

The expected Link objects look as follows:

``` javascript
package com.atlassian.greenhopper.plugin.link;

/**
 * Link object displayed in one of the GreenHopper context menus.
 *
 * @author miruflin
 */
public class Link
{
    /** The href attribute content. */
    private String href;

    /** The title of the link. */
    private String title;

    /**
     * Get the href value
     */
    public String getHref()
    {
        return href;
    }

    /**
     * Set the href attribute value.
     * The value can be a full url, a relative url or even some javascript of the form "javascript:alert('hello');"
     *
     * @param href the value to set
     */
    public void setHref(String href)
    {
        this.href = href;
    }

    /**
     * Get the link title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Set the link title
     *
     * @param text the title of the link
     */
    public void setTitle(String text)
    {
        this.title = text;
    }

    /**
     * Should the href value be prefixed by the web application base url?
     *
     * The default implementation returns true if the href value starts with a slash.
     *
     * @return true if the base url should be added, false otherwise.
     */
    public boolean isAddBaseUrl()
    {
        return href != null && href.startsWith("/");
    }
}
```

## Example

Here is an example `atlassian-plugin.xml` file containing a single link provider:

``` javascript
    <gh-link-provider key="lhp-box-menu-item" name="LHP box menu item" class="com.pyxis.jira.greenhopper.GreenHopperMenuItems" />
```

class in one word (the attribute gets removed!).

The following code is the implementation of the `GreenHopperMenuItems` for the Link Hierarchy plugin:

``` javascript
package com.pyxis.jira.greenhopper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.greenhopper.plugin.link.Link;
import com.atlassian.greenhopper.plugin.link.LinkContext;
import com.atlassian.greenhopper.plugin.link.LinkProvider;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.pyxis.greenhopper.GreenHopper;
import com.pyxis.greenhopper.jira.boards.ArchivedChartBoard;
import com.pyxis.greenhopper.jira.boards.ChartBoard;
import com.pyxis.greenhopper.jira.boards.VersionBoard;

public class GreenHopperMenuItems implements LinkProvider
{
    public static final Logger log = Logger.getLogger(GreenHopperMenuItems.class);

    private final GreenHopper ghService;
    private final VersionManager versionManager;
    private final IssueManager issueManager;

    public GreenHopperMenuItems(VersionManager versionManager, IssueManager issueManager)
    {
        this.ghService = (GreenHopper)ComponentManager.getOSGiComponentInstanceOfType(GreenHopper.class);
        this.versionManager = versionManager;
        this.issueManager = issueManager;
    }

    public List<Link> getLinks(LinkContext linkContext)
    {
        try
        {
            if(!StringUtils.isEmpty(linkContext.getIssueKey())) // Display in the issue menu
            {
                Issue issue = issueManager.getIssueObject(linkContext.getIssueKey());
                if(issue != null)
                {
                    IssueLinkType linkType = ghService.getConfiguration(issue.getProjectObject()).getLinkType();
                    if(linkType != null)
                    {
                        return Arrays.asList(new Link[]{getLink(issue, linkType)});
                    }
                }
            }
            else if(isBoardSupported(linkContext.getBoardType())) // Display in the RHS column boxes menu
            {
                Version version = versionManager.getVersion(Long.valueOf(linkContext.getBoardId()));
                IssueLinkType linkType = ghService.getConfiguration(version.getProjectObject()).getLinkType();
                if(version != null && linkType != null)
                {
                    return Arrays.asList(new Link[]{getLink(version, linkType)});
                }
            }
        }
        catch (Exception e)
        {
            log.error(e);
        }

        return new ArrayList<Link>();
    }

    private boolean isBoardSupported(String boardType)
    {
        return VersionBoard.VIEW.equals(boardType) || ChartBoard.VIEW.equals(boardType) || ArchivedChartBoard.VIEW.equals(boardType);
    }

    private Link getLink(Version version, IssueLinkType linkType)
    {
        StringBuilder sb = new StringBuilder("/secure/ConfigureReport.jspa");
        sb.append("?selectedProjectId=").append(version.getProjectObject().getId());
        sb.append("&amp;pid=").append(version.getProjectObject().getId());
        sb.append("&amp;versionId=").append(version.getId());
        sb.append("&amp;linkTypeId=").append(linkType.getId());
        sb.append("&amp;reportKey=").append("com.pyxis.jira.links.hierarchy.reports:pyxis.hierarchy.report.version");
        sb.append("&amp;subtasks=true");
        sb.append("&amp;orphans=true");
        sb.append("&amp;hierarchyView=tree");
        sb.append("&amp;Next=Next");

        Link link = new Link();
        link.setHref(sb.toString());
        link.setTitle(linkType.getName());
        return link;
    }

    private Link getLink(Issue issue, IssueLinkType linkType)
    {
        StringBuilder sb = new StringBuilder("/secure/ConfigureReport.jspa");
        sb.append("?selectedProjectId=").append(issue.getProjectObject().getId());
        sb.append("&amp;pid=").append(issue.getProjectObject().getId());
        sb.append("&amp;issueKey=").append(issue.getKey());
        sb.append("&amp;linkTypeId=").append(linkType.getId());
        sb.append("&amp;reportKey=").append("com.pyxis.jira.links.hierarchy.reports%3Apyxis.hierarchy.report.issue");
        sb.append("&amp;subtasks=true");
        sb.append("&amp;orphans=true");
        sb.append("&amp;Next=Next");

        Link link = new Link();
        link.setHref(sb.toString());
        link.setTitle(linkType.getName());
        return link;
    }
}
```
