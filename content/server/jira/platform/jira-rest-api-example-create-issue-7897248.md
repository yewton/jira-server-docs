---
title: JIRA REST API Example Create Issue 7897248
aliases:
    - /server/jira/platform/jira-rest-api-example-create-issue-7897248.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7897248
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7897248
confluence_id: 7897248
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - Create Issue

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

 

The Rest API allows you to create an issue.

The JIRA REST api allows you to easily create an issue. You can POST a single JSON document.

{{% note %}}

The examples shown here use curl with an input file denoted by the "--data @filename" syntax and the file data is shown separately

{{% /note %}}

# Field meta-data for creating issues

The fields that are available when creating an issue are available via the REST API, e.g.

``` javascript
http://localhost:8090/rest/api/2/issue/createmeta
```

This will return all of the create metadata for all issue types across all projects.

You most likely want a subset of that information, for example for a specific project and issue type, which you can ask for by specifying the project ids, project names, issue type ids, or issue type names in the URL. For example, to get the create metadata for the Bug issue type in the JRA project:

``` javascript
http://localhost:8090/rest/api/2/issue/createmeta?projectKeys=JRA&issuetypeNames=Bug&expand=projects.issuetypes.fields
```

See the [Discovering meta-data for creating issues tutorial](https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+Discovering+meta-data+for+creating+issues) for more information

# Examples of creating an issue

### Example of creating an issue using project keys and field names.

This simple create request

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/
```

##### Data

``` javascript
{
    "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "REST ye merry gentlemen.",
       "description": "Creating of an issue using project keys and issue type names using the REST API",
       "issuetype": {
          "name": "Bug"
       }
   }
}
```

##### Response

``` javascript
{
   "id":"39000",
   "key":"TEST-101",
    "self":"http://localhost:8090/rest/api/2/issue/39000"
}
```

### Example of creating an issue using project ids and issue type ids.

Programs using REST may have queried the set of projects and issue ids (for example via createmeta above), and not want to use the names of projects or issue types. They can just as easily use ids, as in the example below:

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/
```

##### Data

``` javascript
{
    "fields": {
       "project":
       {
          "id": "10110"
       },
       "summary": "No REST for the Wicked.",
       "description": "Creating of an issue using ids for projects and issue types using the REST API",
       "issuetype": {
          "id": "1"
       }
   }
}
```

##### Response

The response provides the issue id, key, and the URL to the issue (which can then be used to GET additional data, PUT updates, etc) via the REST API.

``` javascript
{
   "id":"39001",
   "key":"TEST-102",
   "self":"http://localhost:8090/rest/api/2/issue/39001"
}
```

### Example of creating a sub-task

Creating a sub-task is similar to creating a regular issue, with two important differences:

-   the `issueType` field must correspond to a sub-task issue type (you can use `/issue/createmeta` to discover sub-task issue types), and
-   you must provide a `parent` field in the issue create request containing the `id` or `key` of the parent issue.

Because a sub-task is essentially a special type of issue, the sub-task creation request and response are very similar to issue creation, as you can see from the following examples.

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/
```

##### Data

``` javascript
{
    "fields":
    {
        "project":
        {
            "key": "TEST"
        },
        "parent":
        {
            "key": "TEST-101"
        },
        "summary": "Sub-task of TEST-101",
        "description": "Don't forget to do this too.",
        "issuetype":
        {
            "id": "5"
        }
    }
}
```

##### Response

The response provides the sub-task id, key, and the URL to the issue (which can then be used to GET additional data, PUT updates, etc) via the REST API.

``` javascript
{
   "id":"39002",
   "key":"TEST-103",
   "self":"http://localhost:8090/rest/api/2/issue/39002"
}
```

### Example of creating an issue using custom fields

Since custom field names are not unique within a JIRA instance, custom fields are referred to by the field ID, in the REST API. The same custom field name would have different ids across different JIRA instances. For example, on one JIRA instance, "Story Points" might have the id "10000" while on another instance the id might be "10101".

In REST in JIRA, customfields are referenced by "customfield\_" + the id of the custom field.

So the "Story Points" custom field, with id = "10000", would be referenced as "customfield\_10000" for the field name in the JIRA REST API.

If you're looking to always set a specific field, such as "Story Points", you should first GET the createmeta information for the issue type, and then get the appropriate custom field id for that field name.

In the example below, we have a field called "Explanation" that is a Free Text Field in JIRA, and the id of that field is 11050, so we reference the field as "customfield\_11050". Notice that the name of the field "Explanation" is not used anywhere.

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/
```

##### Data

``` javascript
{
    "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "Always do right. This will gratify some people and astonish the REST.",
       "description": "Creating an issue while setting custom field values",
       "issuetype": {
          "name": "Bug"
       },
       "customfield_11050" : "Value that we're putting into a Free Text Field."
   }
}
```

##### Response

Again, the issue created is returned in the response.

``` javascript
{
   "id":"39002",
   "key":"TEST-103",
    "self":"http://localhost:8090/rest/api/2/issue/TEST-103"
}
```

### Examples of how to set custom field data for other field types:

##### CascadingSelectField

``` javascript
 "customfield_10001": {"value": "green", "child": {"value":"blue"} }
```

The value associated with "name" ("green" in this example) is the parent option selected, then "blue" is the child option }

##### DatePickerField

``` javascript
 "customfield_10002": "2011-10-03"
```

The format is: YYYY-MM-DD

##### DateTimeField

``` javascript
 "customfield_10003": "2011-10-19T10:29:29.908+1100"
```

This format is ISO 8601: YYYY-MM-DDThh:mm:ss.sTZD

##### FreeTextField

``` javascript
 "customfield_10004": "Free text goes here.  Type away!"
```

##### GroupPicker

``` javascript
 "customfield_10005": { "name": "jira-developers" }
```

Like users, groups are specified by name (or id).

##### Labels

``` javascript
 "customfield_10006": ["examplelabelnumber1", "examplelabelnumber2"]
```

Labels are arrays of strings

##### MultiGroupPicker

``` javascript
 "customfield_10007": [{ "name": "admins" }, { "name": "jira-developers" }, { "name": "jira-users" }]
```

Like users, groups are specified by name, or id.

##### MultiSelect

``` javascript
 "customfield_10008": [ {"value": "red" }, {"value": "blue" }, {"value": "green" }]
```

##### MultiUserPicker

``` javascript
 "customfield_10009": [ {"name": "jsmith" }, {"name": "bjones" }, {"name": "tdurden" }]
```

Array of users

##### NumberField

``` javascript
 "customfield_10010": 42.07
```

Just a number (not a number in a string)

##### ProjectPicker

``` javascript
 "customfield_10011": { "key": "JRADEV" }
```

You can also specify the project by project ID

``` javascript
 { "id":"10000" }
```

##### RadioButtons

``` javascript
 "customfield_10012": { "value": "red" }
```

You can also specify the selection by id

##### SelectList

``` javascript
 "customfield_10013": { "value": "red" }
```

You can also specify the selection by id

##### SingleVersionPicker

``` javascript
 "customfield_10014": { "name": "5.0" }
```

You can also specify the version by id

##### TextField

``` javascript
 "customfield_10015": "Is anything better than text?"
```

##### URLField

``` javascript
 "customfield_10016": "http://www.atlassian.com"
```

##### UserPicker

``` javascript
 "customfield_10017": { "name":"brollins" }
```

##### VersionPicker

``` javascript
 "customfield_10018": [{ "name": "1.0" }, { "name": "1.1.1" }, { "name": "2.0" }]
```

You can also specify a version by id

### Adding a worklog entry during create

If you want to set the time tracking fields in a JIRA issue when creating the issue, the create data should include a section like the following:

``` javascript
       "timetracking":
        {
           "originalEstimate": "1d 2h",
           "remainingEstimate": "3h 25m"
        }
```

Time tracking must be enabled to set these fields. In addition, if you are using the JIRA "Legacy" time tracking mode (set by a JIRA Administrator), then only the remaining estimate can be set, so the "originalestimate" field should not be included in the REST request.

##### Request

No different from any of the other examples, the create is simply a POST:

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/
```

##### Data

Again, this data is if Legacy mode for time tracking is off.

``` javascript
{
   "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "Who seeks a faultless friend RESTs friendless.",
       "description": "Creating an issue and setting time tracking fields",
       "issuetype": {
          "name": "Bug"
        },
        "timetracking":
        {
           "originalEstimate": "1d 2h",
           "remainingEstimate": "3h 25m"
        }
    }
}
```

##### Response

``` javascript
{
   "id":"39003",
   "key":"TEST-104",
    "self":"http://localhost:8090/rest/api/2/issue/TEST-104"
}
```
