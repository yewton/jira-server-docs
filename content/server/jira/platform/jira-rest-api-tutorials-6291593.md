---
title: JIRA REST API Tutorials 6291593
aliases:
    - /server/jira/platform/jira-rest-api-tutorials-6291593.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291593
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291593
confluence_id: 6291593
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Tutorials

Take a look at the [overview of JIRA's REST APIs](/server/jira/platform/rest-apis), then follow a tutorial or two:

-   [JIRA REST API Version 2 Tutorial](/server/jira/platform/jira-rest-api-version-2-tutorial-8946379.html)
-   [JIRA REST API Example - Basic Authentication](/server/jira/platform/jira-rest-api-example-basic-authentication-6291732.html)
-   [JIRA REST API Example - Cookie-based Authentication](/server/jira/platform/jira-rest-api-example-cookie-based-authentication-37234858.html)
-   [JIRA REST API Example - OAuth authentication](/server/jira/platform/jira-rest-api-example-oauth-authentication-6291692.html)
-   [JIRA REST API Example - Create Issue](/server/jira/platform/jira-rest-api-example-create-issue-7897248.html)
-   [JIRA REST API Example - Edit issues](/server/jira/platform/jira-rest-api-example-edit-issues-6291632.html)
-   [JIRA REST API Example - Query issues](/server/jira/platform/jira-rest-api-example-query-issues-6291606.html)
-   [JIRA REST API Example - Discovering meta-data for creating issues](/server/jira/platform/jira-rest-api-example-discovering-meta-data-for-creating-issues-6291669.html)
-   [JIRA REST API Example - Add Comment](/server/jira/platform/jira-rest-api-example-add-comment-8946422.html)
-   [Updating an Issue via the JIRA REST APIs](/server/jira/platform/updating-an-issue-via-the-jira-rest-apis-6848604.html)
-   [JIRA REST API examples](/server/jira/platform/jira-rest-api-examples)
