---
aliases:
- /server/jira/platform/keyboard-shortcut-plugin-module-4227166.html
- /server/jira/platform/keyboard-shortcut-plugin-module-4227166.md
category: reference
confluence_id: 4227166
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227166
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227166
date: '2017-12-08'
legacy_title: Keyboard Shortcut Plugin Module
platform: server
product: jira
subcategory: modules
title: Keyboard shortcut
---
# Keyboard shortcut

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 4.1 and later</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In JIRA 4.2 and later, the <code>keyboard-shortcut</code> element accepts an optional child element (<code>hidden</code>), which prevents the keyboard shortcut from appearing on the keyboard shortcuts dialog box. Existing keyboard shortcuts can also be overridden.</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

A keyboard shortcut plugin module defines a keyboard shortcut within JIRA. A JIRA keyboard shortcut allows you to perform potentially any action in JIRA using one or more keyboard strokes - for example, going to the Dashboard, browsing a project, moving your cursor to a field on a JIRA form, or creating, editing or commenting on an issue.

## Configuration

The root element for the keyboard shortcut plugin module is `keyboard-shortcut`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import
  key=&quot;appProps&quot;
  interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="even">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p>
<p><strong><strong>Default: -</strong></strong></p></td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td>The human-readable name of the plugin module.
<p><strong>Required: -</strong></p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
<tr class="even">
<td><p>hidden</p></td>
<td><p>When <code>hidden='true'</code>, the keyboard shortcut will not appear in the <a href="http://confluence.atlassian.com/display/JIRA/Using+Keyboard+Shortcuts#UsingKeyboardShortcuts-AccessingtheKeyboardShortcutsDialogBox" class="external-link">Keyboard Shortcuts dialog box</a>.</p>
{{% note %}}
<div class="confluence-information-macro confluence-information-macro-note">
<div class="confluence-information-macro-body">
<p>Despite not appearing in the dialog box, hidden keyboard shortcuts can still be accessed via they keystrokes.</p>
</div>
</div>
{{% /note %}}{{% note %}}
<div class="confluence-information-macro confluence-information-macro-note">
<div class="confluence-information-macro-body">
<p>This attribute is only available in JIRA 4.2 and later.</p>
</div>
</div>
{{% /note %}}
<p><strong>Required: -</strong></p>
<p><strong>Default:</strong> false</p>
<p> </p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>order</p></td>
<td><p>A value that determines the order in which the shortcut appears on the <a href="http://confluence.atlassian.com/display/JIRA/Using+Keyboard+Shortcuts#UsingKeyboardShortcuts-AccessingtheKeyboardShortcutsDialogBox" class="external-link">Keyboard Shortcuts dialog box</a> (with respect to other <code>keyboard-shortcut</code> plugin modules). This element is also used to override existing shortcuts displayed on the Keyboard Shortcuts dialog box (see <a href="#overriding-existing-keyboard-shortcuts">Overriding Existing Keyboard Shortcuts</a>).</p>
{{% note %}}
<div class="confluence-information-macro confluence-information-macro-note">
<div class="confluence-information-macro-body">
<p>For each <code>keyboard-shortcut</code> plugin module, we recommend using gaps in <code>order</code> values (for example, <code>10</code>, <code>20</code>, <code>30</code>, etc.) rather than consecutive values. This will allow you to 'insert' new keyboard shortcuts more easily into the keyboard shortcuts dialog box.</p>
</div>
</div>
{{% /note %}}
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="even">
<td><p>description</p></td>
<td><p>A human-readable description of this keyboard shortcut module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td><p>shortcut</p></td>
<td><p>The sequence of keystrokes required to activate the keyboard shortcut's operation. These should be presented in the order that the keys are pressed on a keyboard. For example, <code>gb</code> represents a keyboard shortcut whose operation is activated after pressing '<code>g</code>', followed by '<code>b</code>' on the keyboard.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="even">
<td><p>operation</p></td>
<td><p>A <a href="http://api.jquery.com/category/selectors/" class="external-link">jQuery selector</a> that specifies the target of the keyboard shortcut. The target is typically a component of the current page that performs an action. The <code>operation</code> element is accompanied by a <code>type</code> attribute that specifies the type of keyboard shortcut operation. For more information on the available types, please refer to the <a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/plugin/keyboardshortcut/KeyboardShortcutManager.Operation.html#enum_constant_detail" class="external-link">JIRA API documentation</a>.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td><p>context</p></td>
<td><p>The section of the Keyboard Shortcuts dialog box on which the shortcut appears and defines where the keyboard shortcut can be used in JIRA. If this element contains:</p>
<ul>
<li><code>global</code> (or is omitted) -- the keyboard shortcut appears in the 'Global Shortcuts' section of this dialog box.</li>
<li><code>issueaction</code> -- the keyboard shortcut appears in the 'Issue Actions' section of the dialog box.</li>
<li><code>issuenavigation</code> -- the keyboard shortcut appears in the 'Navigating Issues' section of the dialog box.</li>
</ul>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

### Overriding Existing Keyboard Shortcuts

{{% note %}}

This feature is only available in JIRA 4.2 and later.

{{% /note %}}

You can override an existing keyboard shortcut defined either within JIRA itself or in another plugin.

To do this create a `keyboard-shortcut` plugin module with exactly the same `shortcut` element's keystroke sequence as that of the keyboard shortcut you want to override. Then, ensure that an `order` element is added, whose value *is greater than* that defined in the keyboard shortcut being overridden.

While the `order` element may affect the position of your overriding keyboard shortcut on the Keyboard Shortcuts dialog box, it will also prevent the overridden keyboard shortcut from:

-   being accessed via the keyboard and
-   appearing in the dialog box, on the proviso that you are overriding the keyboard shortcut within the same 'context' (i.e. with the same `context` element above).

### Internationalization

It is possible to include an i18n resource in your [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/)
to translate keyboard shortcut descriptions (via their '`key`' attributes) into multiple languages. For examples code on including i18n resources in your `atlassian-plugin.xml` file, refer to [Downloadable Plugin Resources](/server/jira/platform/downloadable-add-on-resources).

## Examples

These examples are taken from JIRA's pre-defined keyboard shortcuts for going to the Dashboard, commenting on an issue and moving to the next action item on an issue view.

``` javascript
...
    <keyboard-shortcut key="goto.dashboard" i18n-name="admin.keyboard.shortcut.goto.dashboard.name"
                       name="Goto Dashboard">
        <order>10</order>
        <description key="admin.keyboard.shortcut.goto.dashboard.desc">Go to Dashboard</description>
        <shortcut>gd</shortcut>
        <operation type="followLink">#home_link</operation>
    </keyboard-shortcut>
...

...
    <keyboard-shortcut key="comment.issue" i18n-name="admin.keyboard.shortcut.commentissue.name"
                       name="Comment Issue"
                       state='enabled'>
        <order>80</order>
        <description key="admin.keyboard.shortcut.commentissue.desc">Scrolls to comment input and focuses it</description>
        <shortcut>m</shortcut>
        <operation type="click">#comment-issue</operation>
        <context>issueaction</context>
    </keyboard-shortcut>
...

...
    <keyboard-shortcut key="move.to.next.action.item" i18n-name="admin.keyboard.shortcut.movetonextactionitem.name"
                       name="Move to next action item"
                       state='enabled'>
        <order>110</order>
        <description key="admin.keyboard.shortcut.movetonextactionitem.desc">Moves to the next action item on view issue page</description>
        <shortcut>n</shortcut>
        <operation type="moveToNextItem">.issue-data-block:visible</operation>
        <context>issueaction</context>
    </keyboard-shortcut>
...
```
