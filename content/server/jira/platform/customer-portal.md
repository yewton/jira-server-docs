---
aliases:
- /server/jira/platform/jira-service-desk-ui-plugin-points-customer-portal-41234066.html
- /server/jira/platform/jira-service-desk-ui-plugin-points-customer-portal-41234066.md
category: reference
confluence_id: 41234066
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41234066
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41234066
date: '2018-06-27'
legacy_title: JIRA Service Desk UI plugin points - Customer portal
platform: server
product: jira
subcategory: modules
title: "Customer portal"
---
# Customer portal

This page lists the app points for the customer portal in Jira Service Desk (Server). 

{{% tip %}}
[Web Fragment Finder](https://marketplace.atlassian.com/apps/1211656/web-fragment-finder/version-history) can be
useful when looking for Jira UI locations. Note that it is a third-party tool that is not supported by Atlassian.
{{% /tip %}}

## Headers and footers

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.header</code></pre></td>
<td><p>Header on all portal pages.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web panel</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalheader.png" width="850" height="509" />

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.subheader</code></pre></td>
<td><p>Sub-header on all portal pages.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web panel</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalsubheader.png" width="850" height="503" />

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.footer</code></pre></td>
<td><p>Footer on all portal pages.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web panel</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalfooter.png" width="850" height="481" />

Note that using the locations described above will show the web panel on all portal pages. If you want to restrict
the web panels to display on specific portal pages, use the following suffixes with the app point location. 
For example, a sub-header that will only be shown on the create request page will have the location:
`servicedesk.portal.subheader.create_request`.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Suffix</th>
<th>Page Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>.help_center</code></pre></td>
<td>The Service Desk Help Center page.</td>
</tr>
<tr class="even">
<td><pre><code>.portal</code></pre></td>
<td>The Portal page of a specific Service Desk.</td>
</tr>
<tr class="odd">
<td><pre><code>.create_request</code></pre></td>
<td>The create request page.</td>
</tr>
<tr class="even">
<td><pre><code>.view_request</code></pre></td>
<td>The view request page.</td>
</tr>
<tr class="odd">
<td><pre><code>.my_requests</code></pre></td>
<td>The &quot;My Requests&quot; page, which lists all the user's open requests.</td>
</tr>
<tr class="even">
<td><pre><code>.approvals</code></pre></td>
<td>The approvals page, which lists all the requests of which the user is the approver.</td>
</tr>
<tr class="odd">
<td><pre><code>.profile</code></pre></td>
<td>The user's profile page.</td>
</tr>
</tbody>
</table>

## View request page

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.request.panels</code></pre></td>
<td><p>Panel in the view request page.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web panel</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalrequestviewpanel.png" width="850" />

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.request.actions</code></pre></td>
<td><p>Action in the view request page.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web item</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalrequestviewaction.png" width="850" height="499" />

## Profile page

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.profile.panels</code></pre></td>
<td><p>Panel in the user profile page.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web panel</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalprofilepanel.png" width="850" height="475" />

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.profile.actions</code></pre></td>
<td><p>Action in the user profile page.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web item</p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalprofileaction.png" width="850" height="447" />

## Navigation bar

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>App point location</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.portal.user.menu.actions</code></pre></td>
<td><p>Action in the user drop-down menu.</p>
<p><strong>Web fragment type:</strong></p>
<p>Web item</p>
<p> </p></td>
</tr>
</tbody>
</table>

<img src="/server/jira/platform/images/jdev-servicedeskportalusermenuaction.png" width="850" height="452" />
