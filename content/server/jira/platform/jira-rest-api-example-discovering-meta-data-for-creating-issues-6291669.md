---
title: JIRA REST API Example Discovering Meta Data for Creating Issues 6291669
aliases:
    - /server/jira/platform/jira-rest-api-example-discovering-meta-data-for-creating-issues-6291669.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291669
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291669
confluence_id: 6291669
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - Discovering meta-data for creating issues

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

 

The JIRA Rest API allows you to discover the fields and data available and required for creating issues. For this we use the createmeta resource.

### Discovering project and issue type data

To create an issue in JIRA you first need to specify a Project and Issue Type. These together are referred to in JIRA as an Issue Context and are used to find the JIRA schemes that control what fields are available for an issue, what the default values are and what fields are mandatory.

Using the createmeta resource we can discover the project and issue types.

##### Request

``` javascript
curl -D- \
    -u fred:fred \
    -X GET \
    -H "Content-Type: application/json" \
    http://kelpie9:8081/rest/api/2/issue/createmeta
```

##### Response

The response consists of a array of projects and each project contains an array of issue types that apply to that project.

``` javascript
{
    "expand": "projects",
    "projects": [
        {
            "self": "http://kelpie9:8081/rest/api/2/project/XSS",
            "id": "10020",
            "key": "XSS",
            "name": "<iframe src=\"http://www.google.com\"></iframe>",
            "avatarUrls": {
                "16x16": "http://kelpie9:8081/secure/projectavatar?size=small&pid=10020&avatarId=10011",
                "48x48": "http://kelpie9:8081/secure/projectavatar?pid=10020&avatarId=10011"
            },
            "issuetypes": [
                {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/1",
                    "id": 1,
                    "name": "Bug",
                    "iconUrl": "http://kelpie9:8081/images/icons/bug.gif"
                },
                {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/2",
                    "id": 2,
                    "name": "New Feature",
                    "iconUrl": "http://kelpie9:8081/images/icons/newfeature.gif"
                },
.
.
.                {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                    "id": 5,
                    "name": "Sub-task",
                    "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif"
                }
            ]
        },
        {
            "self": "http://kelpie9:8081/rest/api/2/project/BULK",
            "id": "10000",
            "key": "BULK",
            "name": "Bulk Move 1",
            "avatarUrls": {
                "16x16": "http://kelpie9:8081/secure/projectavatar?size=small&pid=10000&avatarId=10020",
                "48x48": "http://kelpie9:8081/secure/projectavatar?pid=10000&avatarId=10020"
            },
            "issuetypes": [
                {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/1",
                    "id": 1,
                    "name": "Bug",
                    "iconUrl": "http://kelpie9:8081/images/icons/bug.gif"
                },
.
.
.
                {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                    "id": 5,
                    "name": "Sub-task",
                    "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif"
                }
            ]
        },
        {
            "self": "http://kelpie9:8081/rest/api/2/project/BLUK",
            "id": "10001",
            "key": "BLUK",
            "name": "Bulk Move 2",
            "avatarUrls": {
.
.
.
.
        }
    ]
}
```

If you know the projects and/or issue types you are interested in you can restrict the list using the "projectKeys", projectIds", "issuetypeNames" and "issuetypeIds" query parameters, e.g.

``` javascript
curl -D- \
    -u fred:fred \
    -X GET \
    -H "Content-Type: application/json" \
    http://kelpie9:8081/rest/api/2/issue/createmeta?projectKeys=QA,XSS
```

### Discovering issue field data

Once you have a project and issue type, then you can retrieve the information for the issue fields, by supplying the "expand" query parameter with the value "projects.issuetypes.fields"

##### Request

``` javascript
curl -D- \
    -u fred:fred \
    -X GET \
    -H "Content-Type: application/json" \
    http://kelpie9:8081/rest/api/2/issue/createmeta?projectKeys=QA&issuetypeNames=Bug&expand=projects.issuetypes.fields
```

##### Response

The response consists of a array of projects and each project contains an array of issue types that apply to that project.

``` javascript
{

    "expand": "projects",
    "projects": [
        {
            "expand": "issuetypes",
            "self": "http://kelpie9:8081/rest/api/2/project/QA",
            "id": "10010",
            "key": "QA",
            "name": "QA",
            "avatarUrls": {
                "16x16": "http://kelpie9:8081/secure/projectavatar?size=small&pid=10010&avatarId=10011",
                "48x48": "http://kelpie9:8081/secure/projectavatar?pid=10010&avatarId=10011"
            },
            "issuetypes": [
                {
                    "expand": "fields",
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/1",
                    "id": 1,
                    "name": "Bug",
                    "iconUrl": "http://kelpie9:8081/images/icons/bug.gif",
                    "fields": {
                        "summary": {
                            "required": true,
                            "schema": {
                                "type": "string",
                                "system": "summary"
                            },
                            "operations": [
                                "set"
                            ]
                        },
                        "timetracking": {
                            "required": false,
                            "operations": [ ]
                        },
                        "issuetype": {
                            "required": true,
                            "schema": {
                                "type": "issuetype",
                                "system": "issuetype"
                            },
                            "operations": [ ],
                            "allowedValues": [
                                {
                                    "id": "1",
                                    "name": "Bug",
                                    "description": "A problem which impairs or prevents the functions of the product.",
                                    "iconUrl": "http://kelpie9:8081/images/icons/bug.gif"
                                }
                            ]
                        },
                        "customfield_10080": {
                            "required": false,
                            "schema": {
                                "type": "array",
                                "items": "string",
                                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:labels",
                                "customId": 10080
                            },
                            "operations": [ ]
                        },
.
.
.
.
                        "customfield_10010": {
                            "required": false,
                            "schema": {
                                "type": "array",
                                "items": "string",
                                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:labels",
                                "customId": 10010
                            },
                            "operations": [ ]
                        },
                        "customfield_10071": {
                            "required": false,
                            "schema": {
                                "type": "array",
                                "items": "string",
                                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:textfield",
                                "customId": 10071
                            },
                            "operations": [ ]
                        }
                    }
                }
            ]
        }
    ]

}
```

If you prefer, by omitting the projectKeys and issuetypeNames parameters you can retrieve all the issue field data at once for all projects and issue types, but this could amount to a very large response and could take some time to build on systems with a large number of projects and issue types.
