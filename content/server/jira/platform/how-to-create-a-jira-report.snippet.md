---
aliases:
- /server/jira/platform/how-to-create-a-jira-report-4227177.html
- /server/jira/platform/how-to-create-a-jira-report-4227177.md
category: devguide
confluence_id: 4227177
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227177
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227177
date: '2017-12-08'
legacy_title: How to create a JIRA Report
platform: server
product: jira
subcategory: other
title: How to create a JIRA report
---
# How to create a JIRA report

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/iwIr> in about 3 seconds.

{{% /note %}}
