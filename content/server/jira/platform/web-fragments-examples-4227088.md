---
title: Web Fragments Examples 4227088
aliases:
    - /server/jira/platform/web-fragments-examples-4227088.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227088
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227088
confluence_id: 4227088
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Web Fragments - Examples

Here is a simple example of generating web fragments that uses both the web UI module and the [Webwork plugin module](/server/jira/platform/webwork).

A webwork plugin module defines a URL-addressable 'action', allowing JIRA's user-visible functionality to be extended or partially overridden. In this example, the action simply prints "Hello World" or greets a given name. However the action can only be executed by entering a specific URL that is not linked from JIRA. This is where the web UI plugin comes in play, by adding the specific URL as a link from the JIRA web interface.

The following plugin modules will:

-   Register a new webwork action that prints "Hello World" (or "Hello" to a specific name)
-   Add a new section to the administration menu called 'Example 1 Section'
    <img src="/server/jira/platform/images/newadminmenu.png" class="confluence-thumbnail" />
-   Add 3 links under the new section:
    -   <a href="http://google.com" class="external-link">Google Home</a> -- static link to <a href="http://google.com" class="uri external-link">http://google.com</a>
    -   Hello World -- static link to the HelloWorld action with no name argument
    -   Hello User -- dynamic link to the HelloWorld action with current user's login name

``` javascript
    <webwork1 key="HelloWorld" name="Hello World Action Example" i18n-name-key="action.hello.world.name">
        <description key="action.hello.world.desc">Webwork plugin example that prints hello world. Can also specify a name to say hello to.</description>
        <resource type="i18n" name="i18n" location="com.atlassian.jira.web.action.HelloWorldAction" />
        <actions>
            <action name="com.example.jira.web.action.HelloWorldAction" alias="Hello">
                <view name="input">/templates/example/helloworld_input.vm</view>
                <view name="success">/templates/example/helloworld.vm</view>
            </action>
        </actions>
    </webwork1>

    <web-section key="example1" name="Example Section" i18n-name-key="section.example.one.name" location="system.admin" weight="105">
        <description key="section.example.one.desc">Example section in the admin page with example links</description>
        <label key="section.example.one.label" />
    </web-section>

    <web-item key="google_home" name="Google Home" i18n-name-key="item.google.home.name" section="system.admin/example1" weight="10">
        <description key="item.google.home.desc">Static link to google.com.</description>
        <label key="item.google.home.label" />
        <link linkId="google_home">http://google.com</link>
    </web-item>

    <web-item key="hello_world" name="Greet world link" i18n-name-key="item.hello.world.name" section="system.admin/example1" weight="20">
        <description key="item.hello.world.desc">Static link to the HelloWorld action with no name parameter.</description>
        <label key="item.hello.world.label" />
        <link linkId="hello_world">/secure/Hello!default.jspa</link>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.UserLoggedInCondition" />
    </web-item>

    <web-item key="filter_closed" name="Closed Issues Filter" i18n-name-key="item.filter.closed.name" section="system.preset.filters" weight="25">
        <description key="item.filter.closed.desc">Custom preset-filter to find closed issues in current project</description>
        <label key="item.filter.closed.label" />
        <link linkId="filter_closed">/secure/IssueNavigator.jspa?reset=true&amp;pid=$helper.project.id&amp;status=6&amp;sorter/field=issuekey&amp;sorter/order=DESC</link>
    </web-item>
```

Here is the screenshot of the new administration menu:

{{% note %}}

The new section "Example 1 Section" appears in between the "Project" and "Users, Groups & Roles" as its [weight](#weight) is in between the two.

{{% /note %}}

![](/server/jira/platform/images/newadminmenu.png)

{{% note %}}

Get the full sample plugin from the <a href="http://confluence.atlassian.com/display/JIRA/JIRA+Plugin+Development+Kit" class="external-link">JIRA Plugin Development Kit</a>. The sample shows how to combine a simple <a href="http://confluence.atlassian.com/display/JIRA/Webwork+plugin+module" class="external-link">webwork module</a> with a web UI module to provide you with an interface to your plugin from Jira. You can find a real life application of this in the <a href="http://confluence.atlassian.com/display/JIRAEXT/JIRA+Subversion+plugin" class="external-link">JIRA Subversion plugin</a> (for Jira 3.7 compatible versions only).

{{% /note %}}

























































































































































































































































