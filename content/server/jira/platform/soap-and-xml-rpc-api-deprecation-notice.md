---
aliases:
- /server/jira/platform/soap-and-xml-rpc-api-deprecation-notice-17563864.html
- /server/jira/platform/soap-and-xml-rpc-api-deprecation-notice-17563864.md
category: devguide
confluence_id: 17563864
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=17563864
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=17563864
date: '2017-12-08'
legacy_title: SOAP and XML-RPC API Deprecation Notice
platform: server
product: jira
subcategory: updates
title: SOAP and XML-RPC API Deprecation Notice
---
# SOAP and XML-RPC API Deprecation Notice

{{% warning %}}

**SOAP and XML-RPC have been removed from both JIRA Cloud and JIRA Server (7.0 and later).**

{{% /warning %}}

Way back in <a href="https://confluence.atlassian.com/display/JIRA/JIRA+4.2+Release+Notes" class="external-link">October 2010 we released JIRA 4.2</a> and the first real REST API for JIRA. We continued to add to it over 4.3 and 4.4. By February 2012 we felt confident in the direction our REST API was going and <a href="https://confluence.atlassian.com/display/JIRA/JIRA+5.0+Release+Notes#JIRA5.0ReleaseNotes-restapiimprovements" class="external-link">removed the alpha and beta labels from it</a> in JIRA 5.0. The scope of the API continued to expand over subsequent releases: 5.1, 5.2, and now 6.0. With the continued growth of our REST APIs, we made the decision to officially **deprecate the SOAP and XML-RPC remote APIs in JIRA 6.0.**

## Why

Deprecating SOAP and XML-RPC means that developers have a clear and consistent message from us about how they should be interacting with JIRA remotely. They won't build up code relying on SOAP and then be disappointed when new features don't get added or bugs don't get fixed. No false hope and no bullshit: REST is the future.

{{% note %}}

Summary

-   SOAP and XML-RPC were  
    deprecated in JIRA 6.0
-   SOAP and XML-RPC will be  
    removed in JIRA 7.0 

{{% /note %}}

## When

Of course, we know that there is a lot of existing code out there so we're not going to remove SOAP immediately. Our intention is to remove SOAP from JIRA in release 7.0. 

{{% note %}}

**SOAP and XML-RPC will be removed from JIRA Cloud on July 6 2015 and in version 7.0 for JIRA Server.**

{{% /note %}}

SOAP and XML-RPC will continue to work without change during the JIRA 6.x, however we won't be spending any more effort on bugfixing or feature requests. When JIRA 7.0 is released the code will be removed from JIRA and they will become unsupported by Atlassian.

## What

-   The remote APIs provided by the **rpc-jira-plugin**. The full list can be found here: <a href="http://docs.atlassian.com/rpc-jira-plugin/latest/" class="uri external-link">docs.atlassian.com/rpc-jira-plugin/latest/</a>. The  _JiraSoapService_  and  _JiraXmlRpcService_  are the most commonly used interfaces from the deprecated plugin.
-   The **rpc-soap** and **rpc-xmlrpc** plugin module descriptors. These module descriptors can be used by plugins to provide custom SOAP and XML-RPC endpoints. If you want to extend JIRA with custom remote APIs you should use the [REST Plugin Module Type](/server/jira/platform/rest-module-type).

## Feature Parity

Right now the JIRA REST API has pretty good coverage of the SOAP API but it isn't 100% complete (we are at around 90% at the time this article is updated). Atlassian is absolutely committed to making every feature of the SOAP API available in the REST API. Making that happen is a precondition of removing the SOAP and XML-RPC APIs from JIRA. We will be working toward that goal during the 6.x development cycles.

For more details, see the [JIRA SOAP to REST Migration Guide](/server/jira/platform/jira-soap-to-rest-migration-guide).

