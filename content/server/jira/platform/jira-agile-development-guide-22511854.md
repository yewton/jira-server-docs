---
title: JIRA Agile Development Guide 22511854
aliases:
    - /server/jira/platform/jira-agile-development-guide-22511854.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22511854
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22511854
confluence_id: 22511854
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Agile development guide

 

 

 

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

 

 

***
***

About JIRA Agile Development

 JIRA Agile no longer exists as a standalone add-on in JIRA 7.0 and onwards. If you have an existing add-on or integration for JIRA Agile, we recommend that you migrate it to JIRA Software. JIRA Agile is one of the add-ons in the blob of add-ons that comprise the JIRA Software application (see JIRA applications for more information). Therefore, many of the JIRA Agile-specific integration points (e.g. JIRA Agile REST APIs, JIRA Agile Java APIs, JIRA Agile plugin modules) should still work in JIRA Software. You will have to make other changes to accommodate the new JIRA platform changes though, such as the changes to the licensing APIs. Read Preparing for JIRA 7.0 for more information.

If you need to develop for JIRA Agile 6.7.x and earlier, you can still use the old JIRA Agile Java APIs and REST APIs. You can also use the old development guide below.

## Guide

You can develop plugins for JIRA Agile (formerly known as GreenHopper) just as you can for other types of Atlassian applications. However, developing for JIRA Agile introduces a few wrinkles to the usual development process, as outlined below.

As you may know, JIRA Agile itself is a plugin to Atlassian JIRA. To extend JIRA Agile, therefore, you actually need to create a JIRA plugin, but one that interacts with or extends JIRA Agile in some way. Typically it does so by using the JIRA Agile APIs or accessing its plugin points.

JIRA Agile exposes a single module type, [JIRA Agile LinkProvider Plugin Module](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html). But you can use the modules in the underlying JIRA application with JIRA Agile locations as plugin points as well. The View Tab tutorial provides an example of this type of JIRA Agile plugin.

## JIRA Agile plugin development flow

The high level steps for developing JIRA Agile plugins with the SDK are as follows:

1.  Create your plugin project as you would any other JIRA plugin, for instance, by using the `atlas-create-jira-plugin` SDK command. This command creates the plugin project files and skeleton code.
2.  If adding a JIRA module or common module, such as a web panel, you may be able to use the Atlas SDK module generator to add it to the project. We'll take you through an example that adds the web-panel with the SDK module generator. To implement the LinkProvider module, you need to add the module code by hand, as described [here](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html).
3.  When ready to build and test your plugin, run the [atlas-package](https://developer.atlassian.com/display/DOCS/atlas-package) command from the project root directory. This command creates a packaged JAR file that you can install into JIRA. 
4.  Start up JIRA. For early development and testing, you can use the development instance that the SDK gives you when you run the following command: `atlas-run-standalone --product JIRA` ``
5.  Log in to JIRA as an administrator, using the default username and password combination of admin/admin.
6.  Add JIRA Agile to the JIRA instance. The quickest way to do this is to choose to create one of the Agile project types for your new project, such as Agile Scrum. (JIRA prompts you to create a new project at first login.) If you pick an agile project, JIRA asks you if you would like to install JIRA Agile. Follow the on-screen instructions to complete the installation, including licensing. If you log in to your Atlassian ID account, you'll get a temporary, 30-day trial license for JIRA Agile.  ` `
7.  Now install the plugin you built. Go to the Administration Console and open the Manage Add-ons page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.

You should now be able to see your add-on in the JIRA or JIRA Agile UI. Every time you change your plugin code or resource, reload your plugin by running the `atlas-package` command again, and then remove and reinstall the plugin JAR file from the Manage Add-ons page.

The View Tab tutorial takes you through these steps while building a sample JIRA Agile plugin.

## Resources

To learn more about developing for JIRA Agile, see the following resources:

-   <a href="https://answers.atlassian.com/tags/greenhopper-development/" class="external-link">JIRA Agile development on Atlassian Answers</a>
-   <a href="http://confluence.atlassian.com/display/AGILE/JIRA+Agile+Resources" class="external-link">JIRA Agile Resources</a> 

 

 

JIRA Agile no longer exists as a standalone add-on in JIRA 7.0 and onwards. If you have an existing add-on or integration for JIRA Agile, we recommend that you migrate it to [JIRA Software](/server/jira/platform/integrating-with-jira-software-server). JIRA Agile is one of the add-ons in the blob of add-ons that comprise the JIRA Software application (see [JIRA applications](https://developer.atlassian.com/display/JIRADEV/JIRA+applications) for more information). Therefore, many of the JIRA Agile-specific integration points (e.g. JIRA Agile REST APIs, JIRA Agile Java APIs, JIRA Agile plugin modules) should still work in JIRA Software. You will have to make other changes to accommodate the new JIRA platform changes though, such as the changes to the licensing APIs. Read [Preparing for JIRA 7.0](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.0) for more information.

If you need to develop for JIRA Agile 6.7.x and earlier, you can still use the old JIRA Agile <a href="https://docs.atlassian.com/greenhopper/" class="external-link">Java APIs</a> and <a href="https://docs.atlassian.com/greenhopper/REST/" class="external-link">REST APIs</a>. You can also use the old development guide below.

About JIRA Agile Development

You can develop plugins for JIRA Agile (formerly known as GreenHopper) just as you can for other types of Atlassian applications. However, developing for JIRA Agile introduces a few wrinkles to the usual development process, as outlined below.

As you may know, JIRA Agile itself is a plugin to Atlassian JIRA. To extend JIRA Agile, therefore, you actually need to create a JIRA plugin, but one that interacts with or extends JIRA Agile in some way. Typically it does so by using the JIRA Agile APIs or accessing its plugin points.

JIRA Agile exposes a single module type, [JIRA Agile LinkProvider Plugin Module](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html). But you can use the modules in the underlying JIRA application with JIRA Agile locations as plugin points as well. The View Tab tutorial provides an example of this type of JIRA Agile plugin.

## JIRA Agile plugin development flow

The high level steps for developing JIRA Agile plugins with the SDK are as follows:

1.  Create your plugin project as you would any other JIRA plugin, for instance, by using the `atlas-create-jira-plugin` SDK command. This command creates the plugin project files and skeleton code.
2.  If adding a JIRA module or common module, such as a web panel, you may be able to use the Atlas SDK module generator to add it to the project. We'll take you through an example that adds the web-panel with the SDK module generator. To implement the LinkProvider module, you need to add the module code by hand, as described [here](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html).
3.  When ready to build and test your plugin, run the [atlas-package](https://developer.atlassian.com/display/DOCS/atlas-package) command from the project root directory. This command creates a packaged JAR file that you can install into JIRA. 
4.  Start up JIRA. For early development and testing, you can use the development instance that the SDK gives you when you run the following command: `atlas-run-standalone --product JIRA` ``
5.  Log in to JIRA as an administrator, using the default username and password combination of admin/admin.
6.  Add JIRA Agile to the JIRA instance. The quickest way to do this is to choose to create one of the Agile project types for your new project, such as Agile Scrum. (JIRA prompts you to create a new project at first login.) If you pick an agile project, JIRA asks you if you would like to install JIRA Agile. Follow the on-screen instructions to complete the installation, including licensing. If you log in to your Atlassian ID account, you'll get a temporary, 30-day trial license for JIRA Agile.  ` `
7.  Now install the plugin you built. Go to the Administration Console and open the Manage Add-ons page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.

You should now be able to see your add-on in the JIRA or JIRA Agile UI. Every time you change your plugin code or resource, reload your plugin by running the `atlas-package` command again, and then remove and reinstall the plugin JAR file from the Manage Add-ons page.

The View Tab tutorial takes you through these steps while building a sample JIRA Agile plugin.

## Resources

To learn more about developing for JIRA Agile, see the following resources:

-   <a href="https://answers.atlassian.com/tags/greenhopper-development/" class="external-link">JIRA Agile development on Atlassian Answers</a>
-   <a href="http://confluence.atlassian.com/display/AGILE/JIRA+Agile+Resources" class="external-link">JIRA Agile Resources</a> 

 