---
title: Adding Custom Field to Issue Summary 4227210
aliases:
    - /server/jira/platform/adding-custom-field-to-issue-summary-4227210.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227210
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227210
confluence_id: 4227210
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Adding Custom Field to Issue Summary

On the View Issue page some of the issue's details are displayed in the top-left corner. With some minor customisation it is possible to display a custom field there as well. To do this please add the following code to the `includes/panels/issue/view_details.jsp` file found under the JIRA web application:

``` javascript
<webwork:iterator value="/fieldScreenRenderTabs" status="'status'">
    <%-- Show tab's fields --%>
    <webwork:iterator value="./fieldScreenRenderLayoutItems">
        <webwork:property value="./orderableField">
            <%-- don't display custom fields with no values --%>
            <webwork:if test="./id == 'customfield_<id>' && ./value(/issueObject) != null && ./customFieldType/descriptor/viewTemplateExists != false">
                <tr id="rowFor<webwork:property value="./id" />">
                    <td valign=top><b><webwork:property value="name" />:</b></td>
                    <td valign=top>
                        <webwork:property value="/customFieldHtml(../fieldLayoutItem,., /issueObject)" escape="false" />
                    </td>
                </tr>
            </webwork:if>
        </webwork:property>
    </webwork:iterator>
</webwork:iterator>
```

Note, that the `<id>` in the above code needs to be replaced with the valid numeric id of the custom field you wish to display. For example, 'customfield\_10000'.

On newer editions of JIRA (3.7+), *view\_details.jsp* is no longer used and has been supplanted by velocity templates. The file you'll want to modify will be: *WEB-INF/classes/templates/jira/issue/summary/issuesummary.vm*. The syntax for the changes will be different from above. You can see a community contributed example on our forums <a href="http://forums.atlassian.com/message.jspa?messageID=257247681" class="external-link">here</a>.
