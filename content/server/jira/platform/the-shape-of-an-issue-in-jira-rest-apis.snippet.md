---
aliases:
- /server/jira/platform/the-shape-of-an-issue-in-jira-rest-apis-6848607.html
- /server/jira/platform/the-shape-of-an-issue-in-jira-rest-apis-6848607.md
category: devguide
confluence_id: 6848607
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848607
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848607
date: '2017-12-08'
legacy_title: The Shape of an Issue in JIRA REST APIs
platform: server
product: jira
subcategory: other
title: The shape of an issue in JIRA REST APIs
---
# The shape of an issue in JIRA REST APIs

{{% note %}}

*This page is not visible to the public as it is incomplete and potentially inaccurate. It may be published again once it is reviewed.*

{{% /note %}}

 

This document describes the shape of the issue document in the JIRA REST APIs. There are slight differences between the shape of the issue document depending on the context of a single document and a search.

URL structure and operations on issues are not discussed here. The list of fields and their possible types are not described here.

In the tables below, <img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /> indicates elements that are not returned by default and require title expansion to return the expanded list.

## JSON Document for a Single Issue

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;id&quot;:</p></td>
<td><p>&quot;10001&quot;</p></td>
<td><p> </p></td>
<td><p>String issue id</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;key&quot;:</p></td>
<td><p>&quot;ABC-123&quot;</p></td>
<td><p> </p></td>
<td><p>String issue key</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;self&quot;:</p></td>
<td><p>&quot;<a href="http://host/.../issue/10001" class="uri external-link">http://host/.../issue/10001</a>&quot;</p></td>
<td><p> </p></td>
<td><p>Full self link (by id)</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;fields&quot;:</p></td>
<td><p>{...}</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;names&quot;:</p></td>
<td><p>{...}</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p>human-readable names of fields</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;schema&quot;:</p></td>
<td><p>{...}</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p>the types of the values of the fields</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;editmeta&quot;:</p></td>
<td><p>{...}</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p>edit metadata for fields</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;transitions&quot;:</p></td>
<td><p>[...]</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;changelog&quot;:</p></td>
<td><p> </p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>}</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

## JSON Document for Issue Search Results

The names and types elements are pulled out to a higher level in search results. The changelog is not available in search results. The editmeta and transitions elements are only available for the first 30 (?TODO?) results.

<table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;start-index&quot;:</p></td>
<td><p>0</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;size&quot;:</p></td>
<td><p>100</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;max-results&quot;:</p></td>
<td><p>20</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;names&quot;:</p></td>
<td><p>{...}</p></td>
<td><p> </p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;schema&quot;:</p></td>
<td><p>{...}</p></td>
<td><p> </p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;issues&quot;: [</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>{</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;id&quot;:</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;key&quot;:</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;self&quot;:</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;fields&quot;:</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;editmeta&quot;:</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>&quot;transitions&quot;:</p></td>
<td><p><img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>}</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>...</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>]</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>}</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

## Fields

The fields element is a map of field ids and field values. System fields have their usual name, custom fields are like customfield\_10001.

The fields present in any given issue can be filtered per request (not discussed here).

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>&quot;fields&quot;: {</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;summary&quot;:</p></td>
<td><p>&quot;blah&quot;</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;description&quot;:</p></td>
<td><p>{text: &quot;blah blah&quot;}</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;customfield_10001&quot;:</p></td>
<td><p>{....}</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>...</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>}</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

## Names

The human-readable, translated, names for each field.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p> </p></th>
<th><p> </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>&quot;names&quot;: {</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;summary&quot;:</p></td>
<td><p>&quot;Summary&quot;</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>&quot;description&quot;:</p></td>
<td><p>&quot;Description&quot;</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>&quot;customfield_10001&quot;:</p></td>
<td><p>&quot;Cheese&quot;</p></td>
</tr>
<tr class="odd">
<td><p>}</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

## Schema and types

The schema describes the shape of the fields element in an issue. For a search, the schema is a superset of the fields of all the matching issues. More details on the schema element is described below:

### About metadata

As well as sending/receiving the values of fields over REST, there are several usecases for providing metadata about those fields. There are 3 categories of metadata for a field:

-   "schema": Shape/interpretation of the JSON data. When to interpret a string as a date, when to expect arrays or compound objects. Is it a system field or a custom field?
-   "editmeta": Edit/create metadata. On a particular screen, is a field required? Autocomplete or valid-values?

### "schema"

JIRA borrows a few ideas from <a href="http://tools.ietf.org/html/draft-zyp-json-schema-03" class="external-link">JSON schema</a> to describe the types of each field in an issue. A "schema" object contains two parts: the first describes the "shape" or "type" of the issue, and the other indicates which system/custom field this field is.

Some examples:

``` javascript
"schema": { "system": "summary", "type": "string"}

"schema": { 
  "custom": "com.atlassian.jira.plugin.system.customfieldtypes:datepicker", "customId" : 10001,
  "type": "date"}
 
"schema": { 
  "custom": "com.atlassian.jira.plugin.system.customfieldtypes:select", "customId" : 10002,
  "type": "string"}
 
"schema": { 
  "custom": "com.atlassian.jira.plugin.system.customfieldtypes:select", "customId" : 10003,
  "type": "array", items: "string"} // TODO will this be a id/name object?
 
"schema": { 
  "custom": "com.atlassian.jira.plugin.system.customfieldtypes:multiversion", "customId" : 10003,
  "type": "array", items: "version"}
 
```

For a system field, the value of the "system" property will be id of the system field (as per the constants in com.atlassian.jira.issue.IssueFieldConstants).

For a custom field, the value of the "custom" property will be the custom field type key, and "customId" will be the ID of the custom field. 

The "type" can be:

-   "string", "boolean", "number" (simple json types)
-   "date" or "datetime" (as per the formats in JSON Schema)
-   "array", the "items" property will describe the type of the items in the array.
-   One of the built-in types "component", "version", "priority", "status", "project", "issuetype".
-   "option" indicating an object that will have at least an "id" and "name" properties (for example, this is used by select dropdown fields, "option" is like &lt;option&gt; in html). "name" must be a string, but "id" could be a number or a string. "option" objects may also have an "iconUrl".
-   "any" indicating that the shape is not specified or hinted at in this schema.
-   Some other string signifying the shape of some Plugin's custom field.

The schema information does not indicate whether or not a field can be null.

### "editmeta"

The "editmeta" data describes how an issue can be mutated in a particular context. It is used to describe editing an issue, creating an issue, and also to describe the screens required for workflow transitions.

``` javascript
// editmeta for the summary field
"summary": {
  "required": true,
  "schema": {"system": "summary", "type", "string"}
  "operations": ["SET", "ADD"]
}
```

Edit meta can contain:

-   required: the field is required on this screen.
-   autoCompleteUrl: ![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) doco format of url
-   allowedValues: An array of values that the field may take. The shape of these values will be the same JSON shape of the field itself (or of the items of the field if the field is an array e.g. in the case of a multiselect).  ![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) TODO improve, what about multi select, or is that implicit in the field shape already.
-   operations: the list of operations (verbs) that apply to this field.
-   type: the type object of this field (as above)

### Common pattern for some objects.

Many fields follow a pattern of having some common data such as an id and name, like Components, Versions, and other "drop-down like" fields. Such fields should base themselves on this pattern:

    {
      "id": {"type": "string", "required": true},
      "name": {"type": "string"},
      "iconUrl": {"type": "string"} 
    }

## editmeta

The schema element contains information on how to interpret the JSON values in the issue. The editmeta element contains information on how to update an issue, and further hints to help build an edit/create screen for an issue.

This editmeta information is reused to describe creating an issue, editing an issue, and transition screens.

Fields can support a variety of mutation "verbs". For example, you can add/delete individual tags in the Labels field, as well as setting the whole set of tags.

Editmeta is described further elsewhere.

## Transitions

(coming soon)

## Changelog

(coming soon)








