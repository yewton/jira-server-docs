---
aliases:
- /server/jira/platform/inline-create-for-jira-issues-27558471.html
- /server/jira/platform/inline-create-for-jira-issues-27558471.md
category: devguide
confluence_id: 27558471
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27558471
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27558471
date: '2017-12-08'
legacy_title: Inline create for JIRA issues
platform: server
product: jira
subcategory: updates
title: Inline create for JIRA issues
---
# Inline create for JIRA issues

## Overview

JIRA 6.3 will introduce the ability to create issues "inline" while you are in plan or work mode on a board. This feature is designed to help you quickly add issues to a sprint or backlog by adding a minimum amount of information and without leaving the board.

<img src="/server/jira/platform/images/inline.png" width="700" />

You can optionally add more details to the issue without leaving the context of the board, or you can continue to create issues to fill out a backlog or sprint.

## Details

When you create a new issue using this feature, JIRA pre-sets many of the issue settings based on the board where the issue is created, default project settings, and other data. For example, JIRA might automatically set the Reporter (as the user creating the issue), the Epic (as defined by the JQL for the board), and the Priority (as the default issue priority). You can override any of these pre-filled settings by clicking <img src="/server/jira/platform/images/dots.png" class="confluence-thumbnail" width="30" /> to open the **Create Issue** dialog.

If the JQL associated with a board is too complex for JIRA to determine certain information or there isn't enough information for JIRA to determine the setting for a field, it leaves those fields blank. (For example, if the board includes issues from multiple projects, JIRA can't determine which project to place the issue in.)

JIRA admins can disable this feature from the JIRA administration settings.
