---
title: "Preparing for Jira 7.10"
product: jira
category: devguide
subcategory: updates
date: "2018-05-11"
---
# Preparing for Jira 7.10

This page covers changes in *Jira 7.10* that can affect add-on compatibility and functionality. This includes changes to the Jira platform and the Jira applications (Jira Core, Jira Software, Jira Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the Jira Server 7.10 products. We also make release milestones available prior to the release:

- For *Jira Server*, the Jira development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [Jira EAP Releases](/server/jira/platform/).

- For *Jira Cloud*, the Jira team releases updates to the Jira Cloud products on a weekly basis. Add-ons that integrate with the Jira Cloud products via Atlassian Connect should use [Jira REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the Jira news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" section and you don't make the necessary changes.

| Change/Application                                                           | Risk level/Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **Archiving projects**<br>Jira Software DC, Jira Service Desk DC | Risk level: Low<br>Affects: Plugin developers, Jira admins<br>
| **Updated look and feel for Jira**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Medium<br>Affects: Plugin developers<br>
| **Support for IPv6 (postponed)**<br>Jira Core, Jira Software, Jira Service Desk | Risk level: Low<br>Affects: Plugin developers, Jira admins<br>

## Changes

#### Archiving projects (Data Center)

This release brings the possibility to archive inactive or completed projects, which you can already test in the second EAP. Project archiving is available for Jira Software Data Center and Jira Service Desk Data Center. The main goal of this feature is to improve performance and user experience by reducing the clutter and the amount of data stored directly in Jira. 

You can read more about project archiving in the [Jira Software EAP release notes](/server/jira/platform/jira-software-eap-7-10-release-notes). If you'd like to try out this feature, but don't have a DC license, you can generate a trial one after installing EAP.

Project archiving also introduces new REST APIs. You can see examples of how to use them in [Project archiving API](https://developer.atlassian.com/server/jira/platform/jira-archiver/).

#### Updated look and feel for Jira

This release brings the first wave of changes to the look and feel in Jira, following the new [Atlassian design direction](https://atlassian.design/server/). The changes in Jira 7.10 include the most frequently used pages like dashboards, issue navigators, issue view, and the list of projects. We’re also updating the branding to reflect the current Atlassian family identity. 

The changes are common for Jira Core, Jira Software, and Jira Service Desk, and have been implemented via an upgrade to AUI. If you’re a plugin developer, make sure you check the [AUI upgrade guide](https://docs.atlassian.com/aui/7.8.0/docs/upgrade-guide.html) to be up-to-date with the new version. You should also update any non-AUI components in your plugins to match the Atlassian design guidelines.

You can read more about the new “look and feel” in Jira 7.10 in the [EAP release notes](/server/jira/platform/jira-software-eap-7-10-release-notes). If you’re curious about what’s next, also have a look at [this blog post](https://developers.atlassian.com/blog/2018/03/coming-soon-updated-look-and-feel-jira-server/).

#### Support for IPv6 (postponed)

We've planned to support communication over IPv6 with Jira 7.10, but decided to postpone it to the next version to make sure we get everything right. Stay tuned for updates in the next development guide for Jira 7.11.
