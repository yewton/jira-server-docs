---
title: JIRA Software Development Guide 35718339
aliases:
    - /server/jira/platform/jira-software-development-guide-35718339.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35718339
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35718339
confluence_id: 35718339
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Software development guide

## About JIRA Software Development

You can develop add-ons for JIRA Software just as you can for other types of Atlassian applications. However, developing for JIRA Software introduces a few wrinkles to the usual development process, as outlined below.

JIRA Software is an application that is built on the JIRA platform and has its own APIs and plugin points. Therefore, to extend JIRA Software, you actually need to create a JIRA add-on that interacts with these integration points in some way.

JIRA Software exposes a single module type, [JIRA Agile LinkProvider Plugin Module](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html). But you can use the modules in the underlying JIRA application with JIRA Software locations as plugin points as well. The [Tutorial - Adding a Detail View Tab](/server/jira/platform/adding-a-detail-view-tab) tutorial provides an example of this type of JIRA Software add-on.

## JIRA Software add-on development flow

#### P2 add-ons

1.  Create your add-on project as you would any other JIRA add-on, for instance, by using the `atlas-create-jira-plugin` SDK command. This command creates the add-on project files and skeleton code.
2.  Add any JIRA modules or common modules, e.g. web panel, by using `atlas-create-jira-plugin-module`. Be aware that if you want to implement the LinkProvider module, you will need to add the module code by hand, as described [here](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html).
3.  When ready to build and test your add-on, run the [atlas-package](https://developer.atlassian.com/display/DOCS/atlas-package) command from the project root directory. This command creates a packaged JAR file that you can install into JIRA. 
4.  Configure AMPS to run JIRA with JIRA Software installed. If you haven't done this before, see [Configure AMPS to run JIRA Core with additional applications installed](https://developer.atlassian.com/display/DOCS/Configure+AMPS+to+run+JIRA+Core+with+additional+applications+installed).
5.  Start up JIRA. For early development and testing, you can use the development instance that the SDK gives you when you run the following command *(set the version to 7.0.0 or later)*: `atlas-run-standalone --product jira --version 7.0.0.` ``
6.  Log in to JIRA as an administrator, using the default username and password combination of admin/admin.``
7.  Install the add-on you built in step \#2 with the `atlas-package` command. There are two ways that you can do this:
    1.  Go to the Administration Console and open the "Manage Add-ons" page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.
    2.  Using the SDK command line tools: using you terminal, step into your project home directory and simply run `atlas-install-plugin` (after having packaged your add-on JAR using the `atlas-package` SDK command). This command will simply upload your add-on to UPM (and can considerably shorten your development flow).

You should now be able to see your add-on in the JIRA Software UI. Every time you change your add-on code or resource, reload your add-on by running the `atlas-package` command again, and then remove and reinstall the add-on JAR file from the Manage Add-ons page.

The [Tutorial - Adding a Detail View Tab](/server/jira/platform/adding-a-detail-view-tab) tutorial takes you through these steps while building a sample JIRA Software add-on.

#### Connect add-ons

As mentioned above, to build an add-on for JIRA Software, you essentially need to build a JIRA add-on. If you haven't built a Connect add-on for JIRA before, this tutorial will help you get started: [Tutorial: Display your projects in JIRA](https://developer.atlassian.com/static/connect/docs/latest/guides/project-activity-tutorial.html) *(Connect documentation)*

However, note the following differences:

-   Rather than starting a local JIRA instance, you will need to start a local JIRA Software instance. See the 'Start the local Atlassian application' section in [Developing locally](https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally.html) (Connect documentation).
-   Instead of modifying JIRA modules, you will probably want to modify JIRA Software web sections and web panels instead. This tutorial provides an example of how to do this: [Tutorial - Adding a dropdown to an agile board](/server/jira/platform/adding-a-dropdown-to-an-agile-board). 

## JIRA Software REST API

JIRA Service Desk has its own APIs, separate from the JIRA platform APIs. The REST API for **JIRA Software Cloud** is documented here: <a href="https://docs.atlassian.com/jira-software/REST/cloud/" class="uri external-link">https://docs.atlassian.com/jira-software/REST/cloud/</a>

You can find the links to the other JIRA Software API documentation here: [JIRA APIs](/server/jira/platform/jira-apis-32344053.html).

## Resources

To learn more about developing for JIRA Agile, see the following resources:

-   <a href="https://answers.atlassian.com/tags/greenhopper-development/" class="external-link">JIRA Agile development on Atlassian Answers</a>
-   <a href="https://docs.atlassian.com/greenhopper/REST/cloud/" class="external-link">JIRA Agile REST API (latest)</a>
