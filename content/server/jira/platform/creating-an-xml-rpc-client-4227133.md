---
title: Creating an Xml Rpc Client 4227133
aliases:
    - /server/jira/platform/creating-an-xml-rpc-client-4227133.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227133
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227133
confluence_id: 4227133
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Creating an XML-RPC Client

{{% warning %}}

JIRA's SOAP and XML-RPC remote APIs were deprecated in JIRA 6.0.

[Read the announcement for more information.](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice)

{{% /warning %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 3.0 and later</p></td>
</tr>
</tbody>
</table>

JIRA ships with the JIRA XML-RPC plugin which enables remote access through XML-RPC and SOAP. Using this feature with XML-RPC is easy with some help from the <a href="http://ws.apache.org/xmlrpc/index.html" class="external-link">Apache XML-RPC</a>package.In this tutorial, we write a basic XML-RPC client (using <a href="http://ws.apache.org/xmlrpc/index.html" class="external-link">Apache XML-RPC</a>) that logs in, retrieves projects and then logs out again. A Python client is also demonstrated.You may also be interested in the [Creating a JIRA SOAP Client](/server/jira/platform/creating-a-jira-soap-client-4227095.html). More methods are exposed via SOAP than XML-RPC.

{{% note %}}

Getting the latest XML-RPC client

You can download the latest XML-RPC client with the Atlassian Plugin SDK - see [Developing with the Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Developing+with+the+Atlassian+Plugin+SDK).

{{% /note %}}

The methods exposed via XML-RPC are listed in the <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">RPC plugin Javadoc for the XmlRpcService class</a>. The [JIRA XML-RPC Overview](/server/jira/platform/jira-xml-rpc-overview-4227112.html) has more information (though not guaranteed to be up to date).

To run the Java client in this tutorial, you'll need to <a href="http://ws.apache.org/xmlrpc/download.html" class="external-link">download the Apache XML-RPC libraries</a> and make them available in your classpath.

## Enabling the RPC plugin

{{% note %}}


{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}


*JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](https://developer.atlassian.com/display/JIRADEV/JIRA+SOAP+to+REST+Migration+Guide)).*

{{% /note %}}

To invoke JIRA operations remotely, you should ensure that the RPC plugin is enabled on the JIRA installation you are targeting. If you simply want to create a client to <a href="http://jira.atlassian.com/" class="uri external-link">http://jira.atlassian.com/</a> then you can skip this step. First you need to check if the Accept Remote API Calls has been enabled in '**General Configuration**' under '**Global Settings**' in the left-hand menu:

<img src="/server/jira/platform/images/rpc-remote-calls.png" class="image-center" />

Then you need to enable the JIRA RPC Plugin in '**Plugins**' under '**System**' in the left-hand menu:

<img src="/server/jira/platform/images/plugins-rpcplugin.png" class="image-center" />

![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) To get the source code of the RPC plugin, see <a href="https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin" class="uri external-link">https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin</a>

Your server should now be ready to accept remote procedure calls.

Now that your server is ready to accept remote procedure calls, we begin creating a Java XML-RPC client.

## Building a Python XML-RPC client

XML-RPC in Python is very easy. Here is a sample client that creates test issues on <a href="http://jira.atlassian.com" class="uri external-link">http://jira.atlassian.com</a>:

``` javascript
#!/usr/bin/python

# Sample Python client accessing JIRA via XML-RPC. Methods requiring
# more than basic user-level access are commented out.
#
# Refer to the XML-RPC Javadoc to see what calls are available:
# http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html

import xmlrpclib

s = xmlrpclib.ServerProxy('http://jira-stage.ironport.com/rpc/xmlrpc')
print "Proxy exited"
auth = s.jira1.login('opsengineer', 'opsengineer')
print "login exited"
newissue = s.jira1.createIssue(auth, { "project": "MONOPS",
                                       "type": 26,
                                       "summary": "Issue Created via XML-RPC",
                                       "description": "Issue created with Python client remotelly",
                                       "customFieldValues" :
                                       [
                                          {"customfieldId" : "customfield_10224", "values" : ["1 - Greater than 10,000 seats"]},
                                          {"customfieldId" : "customfield_10321", "values" : ["production"]},
                                          {"customfieldId" : "customfield_10320", "values" : ["capacity"]},
                                          {"customfieldId" : "customfield_10230", "values" : ["2 - Significant loss of customer productivity"]},
                                       ],
                                       "priority" : 1
                                      }
                               )
print "Created %s/browse/%s" % (s.jira1.getServerInfo(auth)['baseUrl'], newissue['key'])
```

## Building a Java client

The goal of this tutorial is to create a client that makes three simple remote calls to JIRA. Here we log in, retrieve the project information and then log out again. You can take a look at the full source code [here (xmlrpc-2.x)](attachments/4227133/4390944.java) or [here (xmlrpc-3.x)](attachments/4227133/4390986.java).

The first step is to configure your details.

``` javascript
public static final String JIRA_URI  = "http://jira.atlassian.com";
public static final String RPC_PATH  = "/rpc/xmlrpc";
public static final String USER_NAME = "enteryourlogin@atlassian.com";
public static final String PASSWORD  = "yourpassword";
```

All XML-RPC calls are invoked at with the path `/rpc/xmlrpc` by default. You need to configure your username and password appropriately.

``` javascript
// Initialise RPC Client
XmlRpcClient rpcClient = new XmlRpcClient(JIRA_URI + RPC_PATH);

// Login and retrieve logon token
Vector loginParams = new Vector(2);
loginParams.add(USER_NAME);
loginParams.add(PASSWORD);
String loginToken = (String) rpcClient.execute("jira1.login", loginParams);
```

Method calls to JIRA via XML-RPC need to be prefixed with "`jira1.`". Parameters to methods are passed as sequenced Objects in a Vector. In the above code, we log into jira.atlassian.com. We receive back a `loginToken` which will need to be passed to all subsequent method calls.

``` javascript
// Retrieve projects
Vector loginTokenVector = new Vector(1);
loginTokenVector.add(loginToken);
List projects = (List)rpcClient.execute("jira1.getProjectsNoSchemes", loginTokenVector);

// Print projects
for (Iterator iterator = projects.iterator(); iterator.hasNext();-)
{
    Map project =  (Map) iterator.next();
    System.out.println(project.get("name") + " with lead " + project.get("lead"));
}
```

The RPC client calls the getProjectsNoSchemes() method passing the loginToken. This returns with a Vector of *projects* which are represented by `HashTable` objects. For information on what methods are available as well as what properties are available on returned projects, you'll again need to look at the <a href="http://confluence.atlassian.com/pages/viewpage.action?pageId=1035" class="external-link">JIRA XML-RPC API Spec</a>.

``` javascript
// Log out
Boolean bool = (Boolean) rpcClient.execute("jira1.logout", loginTokenVector);
System.out.println("Logout successful: " + bool);
```

Lastly, we log out of the system, again passing the loginToken in a Vector form.

There it is! A simple client for JIRA XML-RPC. If you wish to extend or customise the JIRA XML-RPC plugin itself, you can download the latest source from the <a href="http://repository.atlassian.com/atlassian-jira-rpc-plugin/distributions/" class="external-link">repository</a>.

## Building a Perl client

Here's an XML-RPC client that uses the `XMLRPC::Lite` module (distributed with ActivePerl and available for free on <a href="http://cpan.org/" class="external-link">CPAN</a>).

``` javascript
#!/usr/bin/perl

# toy jira perl client using XMLRPC
# logs in, creates an issue
# handles failure or prints issue fields
# logs out.

use strict;
use warnings;

use XMLRPC::Lite;
use Data::Dumper;

my $jira = XMLRPC::Lite->proxy('http://localhost:8080/jira/rpc/xmlrpc');
my $auth = $jira->call("jira1.login", "admin", "admin")->result();
my $call = $jira->call("jira1.createIssue", $auth, {
    'project' => 'CEL',
    'type' => 2,
    'summary' => 'Issue created via XMLRPC',
    'assignee' => 'admin',
    'fixVersions' => [{'id' => '10000'},{'id' => '10001'}],
    'customFieldValues' => [{'customfieldId' => 'customfield_10000', 'values' => ['Blah', 'Bling']}],
    'description' => 'Created with a Perl client'});
my $fault = $call->fault();
if (defined $fault) {
    die $call->faultstring();
} else {
    print "issue created:n";
    print Dumper($call->result());
}
$jira->call("jira1.logout", $auth);
```

**Note:**
`XMLRPC::Lite` is poorly documented. Using it for this simple example required reading the code. We do not recommend it for newbie Perl hackers.

##### RELATED TOPICS

[Creating a JIRA SOAP Client](/server/jira/platform/creating-a-jira-soap-client-4227095.html)

