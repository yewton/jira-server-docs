---
aliases:
- /server/jira/platform/preparing-for-jira-7.3-44042060.html
- /server/jira/platform/preparing-for-jira-7.3-44042060.md
category: devguide
confluence_id: 44042060
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44042060
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44042060
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.3
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.3
---
# Preparing for JIRA 7.3

This page covers changes in **JIRA 7.3** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.3 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Change: <a href="#removal-of-the-java-screenshot-applet">Removal of the java screenshot applet</a></p>
<p>Platform/Application: JIRA platform</p>
<p> </p></td>
<td><p>Risk level: high</p>
<p> Affects: All IE10, IE9 users</p>
<p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#removal-of-jira-expandableblock">Change: Removal of JIRA.ExpandableBlock</a><br />
</p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: All consumers of the JavaScript global</p></td>
</tr>
<tr class="odd">
<td><p>Change: <a href="#new-workflow-editing-permission-checks">New workflow editing permission checks</a>  </p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: Extensions of Workflow Designer</p></td>
</tr>
<tr class="even">
<td><p>Change: <a href="#new-version-of-aui-6-0-0">New version of AUI: 6.0.0</a><br />
</p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: Add-on developers</p></td>
</tr>
<tr class="odd">
<td><p>Change: <a href="#tomcat-8-5-6">Tomcat 8.5.6</a><br />
</p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: medium</p>
<p> Affects: Add-on developers, sys admins</p></td>
</tr>
</tbody>
</table>

## Supported platforms

The definitive list of supported platforms will be released with the official documentation for JIRA 7.3, here we hope to give you advanced warning of platforms we're adding or removing so you may add support to your add-on.

**Databases:**

<table>
<thead>
<tr class="header">
<th>Platform</th>
<th>Support added or<br />
removed</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>PostGres 9.5</td>
<td>added</td>
</tr>
<tr class="even">
<td>MySQL 5.7</td>
<td>added</td>
</tr>
</tbody>
</table>

**Tomcat:**

<table>
<thead>
<tr class="header">
<th>Platform</th>
<th>Support added or<br />
removed</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>8.5.6</td>
<td>added</td>
</tr>
<tr class="even">
<td>8.0.33</td>
<td>removed</td>
</tr>
</tbody>
</table>

## JIRA platform changes

### Rich Text Editor for JIRA

The WYSIWYG editing experience introduced in JIRA 7.2 as a Labs feature is now enabled by default on all fields configured to use the wiki renderer. You can still opt out as a user or admin.

Interested in writing a plugin for Rich Text Editor? Check these tutorials:

-   [Editor Registry Documentation](/server/jira/platform/editor-registry)
-   [Tutorial - Customizing Rich Text Editor in JIRA](/server/jira/platform/customizing-rich-text-editor-in-jira)
-   [Tutorial - Customizing the TinyMCE within Rich Text Editor in JIRA](/server/jira/platform/customizing-the-tinymce-within-rich-text-editor-in-jira)
-   [Tutorial - Writing plugin for Rich Text Editor in JIRA](/server/jira/platform/extending-the-rich-text-editor-in-jira)

### Removal of the java screenshot applet

HTML5 copy-paste of images is supported in all browsers except IE10 and IE9. As we no longer support these browsers, we have removed the java applet from our codebase.

### Removal of JIRA.ExpandableBlock

This JavaScript global was a legacy component, deprecated in 2010.

The `jira/toggleblock/toggle-block` AMD module provides equivalent functionality to what the `ExpandableBlock` global provided.

### New version of AUI: 6.0.0

Minor bugfix, performance improvements, removed Raphael and eve.js libraries.

<a href="https://ecosystem.atlassian.net/browse/AUI-4328?jql=project%20%3D%20AUI%20AND%20fixVersion%20%3D%206.0.0%20%20" class="external-link">View in JIRA...</a>

What actually changed from AUI 5.10.1: https://bitbucket.org/atlassian/aui-adg/branches/compare/6.0.0..5.10.1

### New workflow editing permission checks

`com.atlassian.jira.bc.workflow.WorkflowService` has the new method: `hasEditWorkflowPermission(ApplicationUser, JiraWorkflow)` which should be used to check whether the given user can edit a workflow. Old checks based on global administrative permissions are no longer sufficient.

### Tomcat 8.5.6

We've upgraded the bundled version of Tomcat in JIRA to Tomcat 8.5.6. You can check the <a href="http://tomcat.apache.org/tomcat-8.5-doc/changelog.html" class="external-link">release notes</a> for Tomcat for more information.  
Possible problems which may be encountered: 

-   RFC6265 is now a default cookie specification, so all cookies must be compliant with it.
-   According to RFC 7230 tomcat stopped returning reason phrases in HTTP/1.1 response messages. So the client code may fail if it checks text of message instead of code.

## JIRA Core changes

No specific JIRA Core application changes.

## JIRA Software changes

No specific JIRA Software application changes.

## JIRA Service Desk changes

No specific JIRA Service Desk application changes.
