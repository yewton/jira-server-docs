---
platform: server
product: jira
subcategory: learning
category: devguide
title: "Tutorials and guides"
date: "2019-02-22"
---        

# Tutorials and guides

Our tutorials and guides are split into topics to help you learn to develop for different parts of Jira. Tutorials 
provide complete step-by-step instructions for working through an example, while guides explore information about a 
particular topic in depth. 

If you're getting started with developing on Atlassian products we recommend you work through some tutorials. 

## Issues

An issue in Jira can represent different things, depending on how your organization is using Jira. For example, an issue could 
represent a software bug, a project task, a helpdesk ticket, a leave request form, etc. The resources in this section 
will help you extend issues in Jira. For example, you could add content to the View Issue page or action issues 
programmatically.

**Tutorials**

- [Adding content to the Jira View Issue page](/server/jira/platform/adding-content-to-the-jira-view-issue-page/)
- [Displaying content in a dialog in Jira](/server/jira/platform/displaying-content-in-a-dialog-in-jira/)
- [Creating a Jira issue CRUD servlet and issue search](/server/jira/platform/creating-a-jira-issue-crud-servlet-and-issue-search/)

**Guides**

- [Performing issue operations](/server/jira/platform/performing-issue-operations/)

## Fields

An issue is a collection of fields. Jira provides a standard set of fields, but administrators can also add custom fields. 
The resources in this section will help you extend fields in Jira. For example, you could create your own custom field type.

**Tutorials**

- [Creating a custom field in Jira](/server/jira/platform/creating-a-custom-field-in-jira/)
- [Creating a custom field type](/server/jira/platform/creating-a-custom-field-type/)

**Guides**

- [Extending inline edit for Jira plugins](/server/jira/platform/extending-inline-edit-for-jira-plugins/)

## Workflow

A Jira workflow is the set of statuses and transitions that an issue goes through during its lifecycle. The resources in 
this section will help you extend workflows in Jira. For example, you could create custom elements (functions, validators,
etc) that can be added to workflows.

**Tutorials**

- [Creating custom workflow elements](/server/jira/platform/creating-custom-workflow-elements/)
- [Creating workflow extensions](/server/jira/platform/creating-workflow-extensions/)

**Guides**

- [Implementing the Jira workflow designer](/server/jira/platform/implementing-the-jira-workflow-designer/)

## Projects

A project is a collection of issues. The resources in this section will help you extend projects in Jira. For 
example, you could add links to the sidebar, create custom project templates, tweak the project configuration options, 
and more.

**Tutorials**

- [Creating a Jira report](/server/jira/platform/creating-a-jira-report/)
- [Creating a project template](/server/jira/platform/creating-a-project-template/)

**Guides**

- [Developing for the Jira project-centric view](/server/jira/platform/developing-for-the-jira-project-centric-view/)
- [Designing for the Jira project-centric view](/server/jira/platform/designing-for-the-jira-project-centric-view/)
- [Extending the Jira Import plugin](/server/jira/platform/extending-the-jira-import-plugin/)
- [Using the sidebar JavaScript API for the Jira project-centric view](/server/jira/platform/using-the-sidebar-javascript-api-for-the-jira-project-centric-view/)

## Boards
A board is used to display the progress of issues through their workflow. The resources in this section will help you 
work with boards. For example, you could add a board configuration page.

**Tutorials**

- [Adding a board configuration page](/server/jira/platform/adding-a-board-configuration-page/)
- [Adding a Detail View tab](/server/jira/platform/adding-a-detail-view-tab/)
- [Adding a dropdown to an agile board](/server/jira/platform/adding-a-dropdown-to-an-agile-board/)

## Search

Jira provides a powerful issue search facility with three different search methods: advanced searching, basic searching 
and quick search. Advanced searching is done using Jira Query language (JQL). The resources in this section will help 
you extend the issue search in Jira. For example, you could add your own JQL function to Jira.

**Tutorials**

- [Adding a JQL function to Jira](/server/jira/platform/adding-a-jql-function-to-jira/)
- [Creating a custom preset filter](/server/jira/platform/creating-a-custom-preset-filter/)
- [Role Members JQL function tutorial](/server/jira/platform/role-members-jql-function-tutorial/)

## Email

Jira can create issues based on incoming emails as well as send notifications to users via email. The resources in this 
section will help you extend email handling in Jira. For example, you could build your own mail handler (in addition to 
the built-in ones).

**Tutorials**

- [Creating a custom email handler for Jira](/server/jira/platform/creating-a-custom-mail-handler-for-jira/)
- [Adding custom fields to email](/server/jira/platform/adding-custom-fields-to-email/)

## Dashboards

A dashboard can be customized by adding gadgets or dashboard items. You can extend this by building your own 
gadgets or dashboard items. The resources in this section will help you work with gadgets in Jira. For example, you 
could write a Jira gadget that shows the days remaining in a version on a dashboard.

**Tutorials**

- [Writing gadgets for Jira](/server/jira/platform/writing-gadgets-for-jira/)
- [Writing a gadget that shows days left in a version](/server/jira/platform/writing-a-gadget-that-shows-days-left-in-a-version/)
- [Writing a JQL standalone gadget](/server/jira/platform/writing-a-jql-standalone-gadget/)
- [Writing a dashboard item app](/server/jira/platform/writing-a-dashboard-item-app/)

**Guides**

- [Building a dashboard item](/server/jira/platform/building-a-dashboard-item/)

## Remote issue links

Remote issue links create a connection between an object in your application and an issue in Jira. The resources in this
section will show you how to create these links.

**Tutorials**

- [Using fields in Remote Issue Links](/server/jira/platform/using-fields-in-remote-issue-links/)

**Guides**

- [Creating a custom issue link dialog for remote issue links](/server/jira/platform/creating-a-custom-issue-link-dialog-for-remote-issue-links/)
- [Creating Remote Issue Links](/server/jira/platform/creating-remote-issue-links/)

## Editor
The editor is used when entering text into Jira in places like the issue summary and commenting on issues. The resources
in this section will show you how the editor can be extended in various ways.

**Tutorials**

- [Customizing Rich Text Editor in JIRA](/server/jira/platform/customizing-rich-text-editor-in-jira/)
- [Customizing the TinyMCE within Rich Text Editor in JIRA](/server/jira/platform/customizing-the-tinymce-within-rich-text-editor-in-jira/)
- [Extending the rich text editor in JIRA](/server/jira/platform/extending-the-rich-text-editor-in-jira/)

## Other

This section lists tutorials that are not related to the major components of Jira (projects, issues, fields, etc). For 
example, how to internationalize your plugin, how to write tests for your plugin, etc.

**Tutorials**

- [Adding JavaScript to all pages for Google Analytics](/server/jira/platform/adding-javascript-to-all-pages-for-google-analytics/)
- [Adding menu items to Jira](/server/jira/platform/adding-menu-items-to-jira/)
- [Creating a custom Release Notes template containing release comments](/server/jira/platform/creating-a-custom-release-notes-template-containing-release-comments/)
- [Creating an AJAX dialog](/server/jira/platform/creating-an-ajax-dialog/)
- [Creating automation rule components](/server/jira/platform/creating-automation-rule-components/)
- [Customizing interface based on user's role](/server/jira/platform/customizing-interface-based-on-users-role/)
- [Customizing Jira Excel output](/server/jira/platform/customizing-jira-excel-output/)
- [Exploring the JIRA Service Desk domain model via the REST APIs](/server/jira/platform/exploring-the-jira-service-desk-domain-model-via-the-rest-apis/)
- [Implementing application links in Jira](/server/jira/platform/implementing-application-links-in-jira/)
- [Internationalizing your plugin](/server/jira/platform/internationalizing-your-plugin/)
- [Plugins2 add-ons](/server/jira/platform/plugins2-add-ons/)
- [Smarter integration testing with TestKit](/server/jira/platform/smarter-integration-testing-with-testkit/)
- [Writing a custom importer using the Jira importers add-on](/server/jira/platform/writing-a-custom-importer-using-the-jira-importers-add-on/)
- [Writing integration tests for your Jira add-on](/server/jira/platform/writing-integration-tests-for-your-jira-add-on/)
- [Writing integration tests using PageObjects](/server/jira/platform/writing-integration-tests-using-pageobjects/)
- [Writing Jira event listeners with the atlassian-event library](/server/jira/platform/writing-jira-event-listeners-with-the-atlassian-event-library/)

**Guides**

- [Extending Jira activity streams](/server/jira/platform/extending-jira-activity-streams/)
- [Extending Jira help links](/server/jira/platform/extending-jira-help-links/)
- [Using pretty URLs in a Jira plugin](/server/jira/platform/using-pretty-urls-in-a-jira-plugin/)