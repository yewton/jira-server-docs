---
category: devguide
subcategory: updates
date: '2017-12-19'
platform: server
product: jira
legacy_title: JIRA Software 7.7 EAP release notes
title: JIRA Software 7.7 EAP release notes
---
# JIRA Software 7.7 EAP release notes

*19 December 2017*

Atlassian is proud to present **JIRA Software 7.7 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Software 7.7** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.7, read the developer change management guide: [Preparing for JIRA 7.7](/server/jira/platform/preparing-for-jira-7-7). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120"/></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.6.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### REST API to manage priority schemes

JIRA 7.7 brings experimental REST APIs to let you manage priority schemes outside of the user interface. As a JIRA administrator, you’ll be able to perform the following actions on priority schemes:

- Create priority schemes (`POST /rest/api/2/priorityschemes`)
- Retrieve a list of priority schemes (`GET /rest/api/2/priorityschemes`)
- Retrieve a priority scheme (`GET /rest/api/2/priorityschemes/{schemeId}`)
- Delete priority schemes (`DELETE /rest/api/2/priorityschemes/{schemeId}`)
- Update priority schemes, e.g with new priorities (`PUT /rest/api/2/priorityschemes/{schemeId}`)
- Associate a project with a priority scheme (`PUT /rest/api/2/project/{projectKeyOrId}/priorityscheme`)
- Disassociate a project from a priority scheme (`DELETE /rest/api/2/project/{projectKeyOrId}/priorityscheme/{schemeId}`)
- Retrieve a priority scheme associated with a project (`GET /rest/api/2/project/{projectKeyOrId}/priorityscheme`)

### Small improvements to make your day

#### Fixed images in the issue's description

We fixed some problems with pasting images into issue's description to make it work without fuss. Previously, you might have encountered some problems with rendering images when switching from the visual mode to text. It works now, even if you switch the mode back and forth all the time.

#### We speak your language

We've added two more languages to JIRA - Italian and Finnish. Check out the [languages available](https://confluence.atlassian.com/display/AdminJIRAServer/Choosing+a+default+language) by default after you upgrade to JIRA Software 7.7.