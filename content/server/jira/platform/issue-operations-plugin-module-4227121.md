---
title: Issue Operations Plugin Module 4227121
aliases:
    - /server/jira/platform/issue-operations-plugin-module-4227121.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227121
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227121
confluence_id: 4227121
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Issue Operations Plugin Module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Issue Operation plugin modules are available in JIRA 3.4 through to 4.0.</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>This plugin module is not available in JIRA 4.1 and later. Please use <a href="/server/jira/platform/web-item">Web Items</a> instead.</p></td>
</tr>
</tbody>
</table>

The Issue Operations plugin module allows you to add new operations to the 'View Issue' screen.


![](/server/jira/platform/images/issue-operations.png)

You can add new operations with a plugin, linking to information about a single issue (most likely pulled from an external source).

### Simple Example

Here is an example descriptor that adds a link to Google a given issue's summary:

``` javascript
<!--
The module class should implement
com.atlassian.jira.plugin.issueoperation.PluggableIssueOperation
-->
<issue-operation key="google-summary" name="Google this issue"
class="com.atlassian.jira.plugin.issueoperation.DefaultPluggableIssueOperation">
<resource type="velocity" name="view">
&lt;img src="$req.contextPath/images/icons/bullet_creme.gif" height=8 width=8 border=0 align=absmiddle&gt;
&lt;b&gt;&lt;a href="http://www.google.com/search?q=${issue.summary}"&gt;Google&lt;/a&gt;&lt;/b&gt; issue summary
</resource>
<!-- the relative order of operations -->
<order>10</order>
</issue-operation>
```

Issue operations are very useful as a 'hook' to link to your other plugin components - such as [Webwork actions](/server/jira/platform/webwork), [project tab panels](/server/jira/platform/project-tab-panel), [reports](/server/jira/platform/report) etc.

### Notes

-   All issue operation classes implement `PluggableIssueOperation`.
-   The `showOperation(Issue issue)` method allows you to turn show or hide operations for certain users, permissions etc.
-   `DefaultPluggableIssueOperation` is a useful base that should work for most simple HTML operations, for instance where only a changed .vm template is required. See for example the Canned Response issue operation plugin
-   The view velocity resource can point to files just as in other plugins e.g.

        <resource type="velocity" name="view" location="templates/more-operations.vm"/>

-   All pluggable issue operations occur after system issue operations (i.e. order is among all pluggable operations, not all operations)

<!-- -->

-   Newer versions of JIRA use [web fragments](https://developer.atlassian.com/display/JIRADEV/View+Issue+Page+Locations) to do a similar thing.
