---
aliases:
- /server/jira/platform/tutorial-displaying-content-in-a-dialog-in-jira-8945945.html
- /server/jira/platform/tutorial-displaying-content-in-a-dialog-in-jira-8945945.md
category: devguide
confluence_id: 8945945
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8945945
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8945945
date: '2017-12-08'
guides: tutorials
legacy_title: Tutorial - Displaying content in a dialog in JIRA
platform: server
product: jira
subcategory: learning
title: Displaying content in a dialog in JIRA
---
# Displaying content in a dialog in JIRA

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>JIRA 5.0</strong>.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p><strong>Intermediate</strong>. Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'. This one is at 'intermediate' level. If you have never developed a plugin before, you may find this one a bit difficult.</p></td>
</tr>
<tr class="odd">
<td><p>Time to Complete:</p></td>
<td><p>1 1/4 hour</p></td>
</tr>
</tbody>
</table>

## Overview of the Tutorial

This tutorial shows you how to display a dialog with content from the server and have it interact with your page. In this tutorial, you will develop a dialog that sets the version in which an issue should be fixed.

This tutorial adds dialog functionality to already existing plugin that has:

-   A Webwork action for that contains the logic for scheduling an issue.
-   A Velocity template for rendering the form
-   A test for ensuring that the scheduling works.

To display this form in a dialog and have it interact with a the page we will through a few different phases:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Simplest Dialog</p></td>
<td><p>Adds a JavaScript resource to &quot;slurp&quot; the form content into a dialog using generic methods.</p></td>
</tr>
<tr class="even">
<td><p>Standard Dialog</p></td>
<td><p>Create a dialog for this specific use case that will work in all circumstances.</p></td>
</tr>
<tr class="odd">
<td><p>Advanced Dialog</p></td>
<td><p>We will update the underlying page so that we don't need to do a page refresh.</p></td>
</tr>
</tbody>
</table>

### Plugin Source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are done, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command to get the completed tutorial code:

``` bash
git clone https://bitbucket.org/atlassian_tutorial/jira-scheduler-dialog-complete.git
```

Alternatively, you can download the source using the **get source** option here: https://bitbucket.org/atlassian_tutorial/tutorial-jira-scheduler.

## Required Knowledge

You should already have installed the following software:

-   An integrated development environment(IDE) such as Eclipse or IDEA.
-   [Atlassian Plugin SDK 3.7](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) or higher.
-   Git distributed source control system

You should also understand the Java development basics including classes, interfaces, methods, how to compile source into a JAR file, and so on.

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to construct this plugin. These instructions were written using Eclipse Classic Version 3.7.1 on a MacBook Air running Mac OS X Version 10.7.2. If you are using another combination, you should use the equivalent operations for your specific environment.

{{% /note %}}

## Step 1. Getting the Base Plugin Code

1.  Change directory to your Eclipse workspace.
2.  Download the *base*tutorial source.

    ``` bash
    $ git clone https://bitbucket.org/atlassian_tutorial/jira-scheduler-dialog-base.git
    ```

3.  Change directory to the plugin's root directory (where the `pom.xml` files is located.
4.  Run some the tests to ensure that the starting point is working.

    ``` bash
    $ atlas-integration-test
    ```

    The command builds the code and runs the integration tests that come with the source. Throughout this tutorial, you'll run a number of `atlas-`commands that automate much of the work of plugin development for you. If everything run successfully, you should see a message similar to:

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 2 minutes 19 seconds
    [INFO] Finished at: Sat Nov 26 13:26:03 EST 2011
    [INFO] Final Memory: 70M/123M
    [INFO] ------------------------------------------------------------------------
    ```

## Step 2. Import the Code into Eclipse

1.  Change to the `tutorial-jira-scheduler` directory created by the previous step.
2.  Run the command:

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

3.  Start Eclipse.
4.  Select **File-&gt;Import**.  
    Eclipse starts the **Import** wizard.
5.  Filter for **Existing Projects into Workspace** (or expand the **General** folder tree).
6.  Press **Next** and enter the root directory of your workspace.  
    Your Atlassian plugin folder should appear under **Projects**.
7.  Select your plugin and click **Finish**.  
    Eclipse imports your project.

## Step 3. Verify Your JIRA Version

The source files were compiled against the JIRA version that existed when the code was created. The version designation is in the project `pom.xml` file (Project Object Model definition file). This file is located in the project project and declares the project dependencies. Take a moment and examine the JIRA dependency:

1.  Open the `pom.xml` file.
2.  Scroll to the bottom of the file.
3.  Find for the `<properties>` element.  
    This section lists the version of the JIRA version you selected in **Step 1** and also the version of the `atlas-` commands you are running.
4.  Review the `<jira.version>` element.  
    You can run this tutorial with JIRA version 4.3 or higher. If you chose to create a plugin for **Shiny new JIRA 5** your `<jira.version> `element will have a version listed like this:

    ``` xml
        <properties>
            <jira.version>5.0</jira.version>
            <amps.version>3.7</amps.version>
        </properties>
    ```

5.  Enter that version number in the `<jira.version> `element.  
      

    ``` xml
        <properties>
            <jira.version>5.2</jira.version>
            <amps.version>3.7</amps.version>
        </properties>
    ```

6.  Save the `pom.xml` file

## Step 4. Update Your Project and Refresh Your IDE

If you change your Atlassian project, Eclipse is not automatically aware of the changes. Moreover, sometimes your project dependencies require an update. We need to fix that.

1.  Switch to a terminal window.
2.  Change directory to the project root.  
    This is the directory with the `pom.xml` file.
3.  Update the on-disk project metadata with the new POM information.

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

4.  Back in Eclipse, refresh the plugin project to pickup the changes.

Remember to do this update and refresh step each time you edit your `pom.xml` or otherwise modify your plugin source with an Atlassian command.

## Step 5. Test the Base Plugin in JIRA

At this point, you haven't actually written any Java code. You can however run JIRA and see the base plugin in action. In this step, you will start JIRA, create a project you'll use later, and test the servlet.

1.  Open a terminal window and navigate to the plugin root folder (where the `pom.xml` file is).
2.  Run the following command:

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a JIRA instance, and installs your plugin in it. This may take several seconds or so, when the process completes you see many status lines on your screen concluding with something like the following lines:

    ``` bash
    [INFO] jira started successfully in 71s at http://localhost:2990/jira
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

3.  Open your browser and navigate to the local JIRA instance started by `atlas-run`.  
    If you followed the instructions, enter http://localhost:2990/jira in your browser.
4.  At the JIRA login, enter a username of `admin` and a password of `admin`.  
    The integration steps you ran earlier created a project called **TEST** in your JIRA instance. You should have an existing issue called **TEST-1** in the **Recent Issues** category.
5.  Display the **TEST-1** issue.  
    Between the **Edit** and **Assign** button you should see the **Schedule** button. This is button that launches the plugin.
6.  Experiment with the issue creation and notice how the button and the resulting dialog behave.

Now, we're ready to start writing some code!

## Step 6. Creating the Simplest of Dialogs

JIRA develop has a feature that allows you to simply add a class to a link in HTML to open the contents of the target page in a dialog. Start Eclipse and do the following:

1.  Edit the `atlassian-plugin.xml` file.
2.  Locate the `<web-item>` with a `key` value of `schedule-web-item`.
3.  Add the following `styleClass`element as a child element.

    ``` xml
    <styleClass>trigger-dialog</styleClass>
    ```

    When you are done, the `<web-item`appears as follows:

    ``` xml
    <web-item name="Schedule Web Item" i18n-name-key="schedule-web-item.name" key="schedule-web-item" section="operations-top-level" weight="3">
    <description key="schedule-web-item.description">The Schedule Web Item Plugin</description>
    <conditions type="and">
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.IsIssueEditableCondition"/>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.HasIssuePermissionCondition">
        <param name="permission" value="edit"/>
        </condition>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.IsFieldHiddenCondition" invert="true">
        <param name="field" value="fixVersions"/>
        </condition>
    </conditions>
    <label key="schedule-web-item.label"/>
    <link linkId="schedule-web-item-link">/secure/SchedulerWebworkModuleAction!default.jspa?id=${issue.id}</link>
    <styleClass>trigger-dialog</styleClass>
      </web-item>
    ```

4.  Save your changes.
5.  Return to a terminal window and navigate to the plugin directory.
6.  Enter `atlas-debug` at the prompt.  
    Starting in the debug mode will allow you to use the Fastdev technique.
7.  Open your JIRA test instance in a browser and log in.
8.  Open an existing issue (you should have at least one).
9.  Test the **Schedule** button's behavior.  
    Pressing the button will result in a **Schedule** dialog. When you press the **Schedule** button on this dialog it submits the dialog contents to the system. What happens to the UI when you press this **Schedule**button.

    {{% note %}}

"Not seeing the Schedule Button?"

Ensure the following:

-   There are issues in the system
-   There are versions in the project you issue is in
-   You have permission to edit the issue

    {{% /note %}}

Leave the JIRA server process running and the interface in an active browser. From here on out, we'll use the Fastdev approach to test our code changes.

## Step 7. Cleaning Up the Submit Action

In the previous step, you saw that submitting the form caused the interface to look jumbled. We need to make the server tell the dialog that it can close. There is a convenience method in Webwork Actions that allow you to do this.

1.  Open Eclipse.
2.  Locate the `SchedulerWebworkModuleAction.java` file and open it for editing.
3.  Change the `doExecute()`method in the class so that it looks returns the following:

    ``` java
    /**
        * The business logic of your form.
        * Only gets called if validation passes.
        *
        * @return the view to display - should usually be "success"
        */
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        // Business Logic
        final IssueService.IssueResult update = issueService.update(authenticationContext.getLoggedInUser(), updateValidationResult);

        if (!update.isValid())
        {
            return ERROR;
        }

        // We want to redirect back to the view issue page so
        return returnComplete("/browse/" + update.getIssue().getKey());
    }
    ```

    If you are following the tutorial, you should still have JIRA up and and running.

4.  Go to the browser page and a hard refresh of the page. To do this, you can:
    -   Shift+Reload in most browsers.
    -   Ctrl+Reload on Windows or in Internet Explorer.
    -   In Safari 5, you will need to hold down the Shift key while clicking the **Reload** icon in the **Location** bar.  
        Reloading the plugin should have it working correctly with the view issue page reloading after the dialog closes.

## Step 8. Learn What is Happening Behind the Scenes

This section discusses what happens behind on the scenes on the browser and the server.

#### In the Browser

On page load, the plugin overrides the link's click behavior. We specified this when we added the `<styleClass>trigger-dialog</styleClass>`. Instead of following the link, it loads the target page via Ajax and puts the contents in a dialog. We add some URL parameters to the links target to allow the server to know the contents are being rendered in a dialog and they can customize the content,  
The URL parameters are:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>inline=true</p></td>
<td><p>Specifies the content will open in a dialog.</p></td>
</tr>
<tr class="even">
<td><p>decorator=dialog</p></td>
<td><p>Ensures the content uses the <code>dialog</code> Sitemesh decorator instead of it's usual one.</p></td>
</tr>
</tbody>
</table>

The result of any form submision or the target of any links is put into this dialog until the server returns a special response header: `X-Atlassian-Dialog-Control:DONE`. Any response header that contains this will close the dialog and refresh the underlying page.

#### On the Server

The `returnComplete(url)` checks to see if the content is displayed in a dialog. If it is, the systems adds the header and return an empty response. If it is not, it will behave as normal.

You can also redirect to another page when dialog is finished. To do this, instead of closing the dialog and refreshing the current page, you can redirect to another page when the dialog closes. To do this, return `X-Atlassian-Dialog-Control:redirect:<url-to-redirect-to>` instead of `X-Atlassian-Dialog-Control:DONE`.

To do this in a Webwork Action, you would edit the SchedulerWebworkModuleAction class to use `returnCompleteWithInlineRedirect(<url-to-redirect-to>)` instead of calling `returnComplete(...)`.

#### Limitations of the Simplest Dialog

-   When using the simplest dialog, you won't be able to customize behavior on dialog close.

## Step 9. Creating a Standard Dialog

For our dialog to work every time, we will need to create a specialized version of a dialog.  
In this section, we add some JavaScript onto the page that will instantiate the form and bind it to the link. Additionally, we'll use the plugin module generator, part of the Atlassian Plugin SDK, to generate the stub for our web-resource.

1.  Open a command window and go to the plugin root folder (where the `pom.xml` is located).
2.  Run `atlas-create-jira-plugin-module`.
3.  Supply the following information as prompted:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Choose Plugin Module:</p></td>
    <td><p><code>Web Resource</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Enter Plugin Module Name:</p></td>
    <td><p><code>My Web Resource</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>Enter Resource Name:</p></td>
    <td><p><code>scheduler.js</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Enter Resource Type:</p></td>
    <td><p><code>download</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>Enter Location:</p></td>
    <td><p><code>javascript/scheduler.js</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Add Resource Parameter?:</p></td>
    <td><p><code>Y</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>param name:</p></td>
    <td><p><code>content-type</code></p></td>
    </tr>
    <tr class="even">
    <td><p>param value:</p></td>
    <td><p><code>text/javascript</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>Add Resource Parameter?:</p></td>
    <td><p><code>N</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Add Resource:</p></td>
    <td><p><code>N</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>Show Advanced Setup?:</p></td>
    <td><p><code>N</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Add Another Plugin Module?:</p></td>
    <td><p><code>N</code></p></td>
    </tr>
    </tbody>
    </table>

    This will have modified your `atlassian-plugin.xml` file to include a Web Resource.

4.  In Eclipse, create a folder called `javascript` in `src/main/resources`.
5.  Add a `scheduler.js` file in your new `javascript` folder.
6.  Edit `scheduler.js`and add the following form:

    ``` javascript
    AJS.$(function () {
        JIRA.Dialogs.scheduleIssue = new JIRA.FormDialog({
            id: "schedule-dialog",
            trigger: ".issueaction-schedule-issue",
            ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions,
            onSuccessfulSubmit : JIRA.Dialogs.storeCurrentIssueIdOnSucessfulSubmit,
            issueMsg : 'thanks_issue_updated'
        });
    });
    ```

7.  Save the file.
8.  Edit the `atlassian-plugin.xml` file.
9.  Change the `styleClass` element value from `trigger-dialog` to `issueaction-schedule-issue`as follows:

    ``` xml
    <styleClass>issueaction-schedule-issue</styleClass>
    ```

10. Add a `context `element to the new web-resource element (to allow the new dialog to be visible from all non-administrative pages) as follows:

    ``` xml
    <context>atl.general</context>
    ```

11. Save the file.
12. In eclipse, locate the `SchedulerWebworkModuleAction.java` file again and open it for editing.
13. Add a requireResource() call in the includeResources`()`method to register the new scheduler resource:

    ``` java
    private void includeResources() {
        webResourceManager.requireResource("jira.webresources:jira-fields");
        webResourceManager.requireResource("com.example.plugins.tutorial.tutorial-jira-scheduler:my-web-resource");
    }
    ```

14. Save the file.

## Step 10. Learn About the JavaScript and Test Again

Take a minute to examine the file in you examine the new a `scheduler.js` file you created in the last step. This code creates a form dialog by specifying the following:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><code>JIRA<br>
&nbsp;&nbsp;.Dialogs<br>
&nbsp;&nbsp;.scheduleIssue</code></p></td>
<td><p>This is the dialog instance. Any dialogs that live in the namespace <code>JIRA.DIALOGS</code> automatically get bound to dynamic issue content.</p></td>
</tr>
<tr class="even">
<td><p><code>new JIRA.FormDialog()</code></p></td>
<td><p>The standard form constructor this takes parameters to change it's behavior.</p></td>
</tr>
<tr class="odd">
<td><p><code>id</code></p></td>
<td><p>A unique DOM id for the dialog.</p></td>
</tr>
<tr class="even">
<td><p><code>trigger</code></p></td>
<td><p>This is what the dialog is bound to. This is usually a jQuery selector.</p></td>
</tr>
<tr class="odd">
<td><p><code>ajaxOptions</code></p></td>
<td><p>Options that passed to the <code>jQuery.ajax()</code> method and used to retrieve content for the dialog. <p><code>JIRA<br>
&nbsp;&nbsp;.Dialogs<br>
&nbsp;&nbsp;.getDefaultAjaxOptions</code></p> are the defaults.</p></td>
</tr>
<tr class="even">
<td><p><code>onSuccessfulSubmit</code></p></td>
<td><p>The function called after a form was submitted and come back successful.</p></td>
</tr>
<tr class="odd">
<td><p><code>JIRA<br>
&nbsp;&nbsp;.Dialogs<br>
&nbsp;&nbsp;.storeCurrentIssueIdOnSucessfulSubmit</code></p></td>
<td><p>Stores the current issue for later use in the message displayed on page refresh.</p></td>
</tr>
</tbody>
</table>

Test out your new code. Reload the plugin and this form should now work from not only the **View Issue** page, but also any of the cog menus (**Dashboard**, **Issue Navigator**, and the sub-task list). The image below shows the **Schedule** option in the **Issue Navigator**:

![](/server/jira/platform/images/issue-nav.png)

## Step 11. Creating an Advanced Dialog

Ideally we would want avoid all page refreshes and refresh only the content that was updated. To do this we will override the default behavior of the form to write the new versions directly to the field.

1.  In Eclipse, edit `scheduler.js`.
2.  Remove the existing form code.
3.  Add the following form code:

    ``` javascript
    AJS.$(function () {
        // Function for getting the issue key of the issu ebeing edited.
        var getIssueKey = function(){
            if (JIRA.IssueNavigator.isNavigator()){
                return JIRA.IssueNavigator.getSelectedIssueKey();
            } else {
                return AJS.$.trim(AJS.$("#key-val").text());
            }
        };

        // Function for getting the project key of the Issue being edited.
        var getProjectKey = function(){
            var issueKey = getIssueKey();
            if (issueKey){
                return issueKey.match("[A-Z]*")[0];
            }
        };

        JIRA.Dialogs.scheduleIssue = new JIRA.FormDialog({
            id: "schedule-dialog",
            trigger: ".issueaction-schedule-issue", 
            ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions,
            onSuccessfulSubmit : function(){  
    // This method defines behavior on a successful form submission.
    // We want to get the versions specified then place them in the view.
    // This selector gets the container for the FixFor Version for 
    // both a list of issues and the view issue page.
                var $fixForContainer = AJS.$("#issuerow" + JIRA.IssueNavigator.getSelectedIssueId() + " td.fixVersions, #fixfor-val" );
                $fixForContainer.html("");  // Blank out the existing versions
                // Now lets construct the html to place into the container
                var htmlToInsert = "";
                // this.getContentArea() return the contents of the dialog.  From this we will get teh selected values of the select list and iterate over them.
                this.getContentArea().find("#fixVersions option:selected").each(function(){
                    var $option = AJS.$(this);
                    // We want to comma separate them
                    if (htmlToInsert !== ""){
                        htmlToInsert += ", ";
                    }
                    var versionName = AJS.$.trim($option.text());
                    // Construct the link and append it to the html
                    htmlToInsert += "<a href='" + contextPath + "/browse/" + getProjectKey() + "/fixforversion/" + $option.val()+ "' title='" + versionName + "'>" + versionName + "</a>";
                });

                // If no options were selected, insert none.
                if (htmlToInsert === ""){
                    htmlToInsert = AJS.I18n.getText("common.words.none");
                }
                // set the html of the container.
                $fixForContainer.html(htmlToInsert);
            },
            onDialogFinished : function(){  
    // This function is used to define behavior after the form has finished
    // We want to display a notification telling people that the fix version was updated.
    // When displayed in the Issue Navigator also show the issue key of the issue updated.
                if (JIRA.IssueNavigator.isNavigator()){
                    JIRA.Messages.showSuccessMsg(AJS.I18n.getText("scheduler.success.issue", getIssueKey()));
                } else {
                    JIRA.Messages.showSuccessMsg(AJS.I18n.getText("scheduler.success"));
                }
            },
            autoClose : true // This tells the dialog to automatically close after a successful form submit.

        });
    });
    ```

4.  Save your work.

## Step 12. Test the Final Version of the Dialog

Do the following to test the plugin:

1.  Switch to a terminal window.
2.  If JIRA is running, stop it.
3.  Change directory to the project root.  
    This is the directory with the `pom.xml` file.
4.  Update the on-disk project metadata with the new POM information.

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

5.  Start JIRA by running `atlas-debug`.
6.  Login into JIRA and test the new, advanced **Schedule** dialog.

{{% tip %}}

Congratulations

You should now have a fully working dialog.  
Have a chocolate!

{{% /tip %}}
