---
title: JIRA Specific Atlassian Events 4227200
aliases:
    - /server/jira/platform/jira-specific-atlassian-events-4227200.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227200
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227200
confluence_id: 4227200
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA-Specific Atlassian Events

The Atlassian Event library is available to all plugins, allowing them to listen to events occurring within JIRA.

Here are some events available in JIRA:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Event Type</p></th>
<th><p>Description. Source</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/config/ApplicationPropertyChangeEvent.html" class="external-link">ApplicationPropertyChangeEvent</a></p></td>
<td><p>Description: Thrown when changes occur in the application properties.</p>
<p>Source: JIRA</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/issue/IssueEvent.html" class="external-link">IssueEvent</a></p></td>
<td><p>Description: Thrown when changes occur in issues.</p>
<p>Source: JIRA</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/user/UserEvent.html" class="external-link">UserEvent</a></p></td>
<td><p>Description: Thrown when changes occur in users.</p>
<p>Source: JIRA</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/ClearCacheEvent.html" class="external-link">ClearCacheEvent</a></p></td>
<td><p>Description: Thrown when JIRA should clear and reinitialise all of its caches.</p>
<p>Source: JIRA</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://developer.atlassian.com/static/javadoc/gadgets/3.1.12/reference/com/atlassian/gadgets/event/ClearHttpCacheEvent.html">ClearHttpCacheEvent</a></p></td>
<td><p>Description: Event that a host application can throw to clear Shindig's cache.</p>
<p>Source: Gadgets</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-crowd/latest/com/atlassian/crowd/event/migration/XMLRestoreFinishedEvent.html" class="external-link">XMLRestoreFinishedEvent</a></p></td>
<td><p>Description: Thrown when an XML backup is finished being restored.</p>
<p>Source: Crowd</p></td>
</tr>
<tr class="odd">
<td><p>Version events (all of these extend <a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/AbstractVersionEvent.html" class="external-link">AbstractVersionEvent</a>):</p>
<ul>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionArchiveEvent.html" class="external-link">VersionArchiveEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionCreateEvent.html" class="external-link">VersionCreateEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionDeleteEvent.html" class="external-link">VersionDeleteEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionMergeEvent.html" class="external-link">VersionMergeEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionMoveEvent.html" class="external-link">VersionMoveEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionReleaseEvent.html" class="external-link">VersionReleaseEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionUnarchiveEvent.html" class="external-link">VersionUnarchiveEvent</a></li>
<li><a href="http://docs.atlassian.com/jira/latest/com/atlassian/jira/event/project/VersionUnreleaseEvent.html" class="external-link">VersionUnreleaseEvent</a></li>
</ul></td>
<td><p>Description: Thrown when changes are made to project versions.</p>
<p>Source: JIRA</p></td>
</tr>
<tr class="even">
<td><p>Project events (all of these extend <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/AbstractProjectEvent.html" class="external-link">AbstractProjectEvent</a>):</p>
<ul>
<li><a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/ProjectCreatedEvent.html" class="external-link">ProjectCreatedEvent</a></li>
<li><a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/ProjectUpdatedEvent.html" class="external-link">ProjectUpdatedEvent</a></li>
<li><a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/ProjectDeletedEvent.html" class="external-link">ProjectDeletedEvent</a></li>
</ul></td>
<td><p>Description: Thrown when changes are made to a project.</p>
<p> Source: JIRA</p>
<p> </p></td>
</tr>
<tr class="odd">
<td><p>Issue link events:</p>
<ul>
<li><a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/issue/link/IssueLinkCreatedEvent.html" class="external-link">IssueLinkCreatedEvent</a></li>
<li><a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/issue/link/IssueLinkDeletedEvent.html" class="external-link">IssueLinkDeletedEvent</a></li>
</ul></td>
<td><p>Description: Thrown when issue links are created or deleted.</p>
<p> Source: JIRA</p>
<p> </p></td>
</tr>
</tbody>
</table>

##### RELATED TOPICS

-   [Tutorial - Writing JIRA event listeners with the atlassian-event library](/server/jira/platform/writing-jira-event-listeners-with-the-atlassian-event-library)
-   [JIRA Plugin Lifecycle](/server/jira/platform/jira-plugin-lifecycle-8946073.html)
