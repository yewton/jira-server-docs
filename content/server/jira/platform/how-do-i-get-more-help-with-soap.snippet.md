---
aliases:
- /server/jira/platform/4227137.html
- /server/jira/platform/4227137.md
category: devguide
confluence_id: 4227137
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227137
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227137
date: '2017-12-08'
legacy_title: How do I get more help with SOAP?
platform: server
product: jira
subcategory: other
title: How do I get more help with SOAP?
---
# How do I get more help with SOAP?

{{% warning %}}

JIRA's SOAP and XML-RPC remote APIs will be deprecated in JIRA 6.0.

[Read the announcement for more information.](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice)

{{% /warning %}}

### How do I get more help with SOAP?

The best place to get help for SOAP is at <a href="https://confluence.atlassian.com/display/JIRA043/Creating+a+SOAP+Client" class="createlink">Creating a SOAP Client</a>. This page links to the example <a href="http://repository.atlassian.com/jira-soapclient/distributions/" class="external-link">SOAP Client</a>, which is an excellent source to see how things work.

The SOAP client has example usages to most common functionality available in the SOAP interface. Including update issue and progressWorkflow

For example:

``` javascript
private static void testUpdateIssue(JiraSoapService jiraSoapService, String token, final String issueKey)
        throws RemoteException
{
    // Update the issue
    RemoteFieldValue[] actionParams = new RemoteFieldValue[]{
        new RemoteFieldValue("summary", new String[] {NEW_SUMMARY}),
        new RemoteFieldValue(CUSTOM_FIELD_KEY_1, new String[] {CUSTOM_FIELD_VALUE_1}),
        new RemoteFieldValue(CUSTOM_FIELD_KEY_2, new String[] {CUSTOM_FIELD_VALUE_2})};

    jiraSoapService.updateIssue(token, issueKey, actionParams);
}
```

 

This updates the summary, and custom fields of an issue.

{{% tip %}}

Need more help?

Try the <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">JIRA Developer Forum</a>. It contains a lot of helpful answers to most of the common SOAP problem, and there are helpful users and developers there to answer potential questions.

{{% /tip %}}{{% tip %}}

Willing to pay for help?

The <a href="http://confluence.atlassian.com/display/APW/Partner+Wiki+Home" class="external-link">Atlassian Partners</a> site has a listing of <a href="http://confluence.atlassian.com/display/APW/Partner+Specialty+Table" class="external-link">partner specialities</a>. You can also search for a specific keyword such as <a href="http://confluence.atlassian.com/dosearchsite.action?queryString=SOAP&amp;where=APW&amp;type=&amp;lastModified=&amp;contributor=&amp;contributorUsername=" class="external-link">SOAP</a>

{{% /tip %}}
