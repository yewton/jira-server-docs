---
aliases:
- /server/jira/platform/4227172.html
- /server/jira/platform/4227172.md
category: devguide
confluence_id: 4227172
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227172
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227172
date: '2017-12-08'
legacy_title: How do I know what else lurks in the Velocity context?
platform: server
product: jira
subcategory: other
title: How do I know what else lurks in the Velocity context?
---
# How do I know what else lurks in the Velocity context?

{{% note %}}

Moved to <a href="https://answers.atlassian.com/questions/31681397/answers/31681399" class="uri external-link">https://answers.atlassian.com/questions/31681397/answers/31681399</a>

{{% /note %}}

Most people scratch their head to know what parameters and `Object`'s are in the Velocity context, the links below may come into help:

-   [Contents of the Velocity Context](/server/jira/platform/contents-of-the-velocity-context-4227181.html)
-   [Velocity Context for Email Templates](/server/jira/platform/velocity-context-for-email-templates-4227144.html)

For developers, who want to know more, the `DefaultVelocityManager` creates a self-reference back to the `VelocityContext` with these lines:

``` javascript
    protected VelocityContext createVelocityContext(Map params)
    {
        if (params != null)
        {
            params.put("ctx", params);
        }
    ...
```

Therefore, doing this in the template will display the available parameters:

``` javascript
#foreach($p in $ctx.keySet().toArray())
  $p.toString() - $ctx.get($p).getClass().getName().toString()
#end
```

e.g.

``` javascript
  textutils - com.opensymphony.util.TextUtils
  dateformatter - com.atlassian.jira.web.util.OutlookDate
  stringUtils - org.apache.commons.lang.StringUtils
  formatter - java.text.SimpleDateFormat
  constantsManager - com.atlassian.jira.config.DefaultConstantsManager
  buildutils - com.atlassian.jira.util.BuildUtils
  i - java.lang.Integer
  context - com.atlassian.jira.mail.TemplateContext
  jiraUserUtils - com.atlassian.jira.util.JiraUserUtils
  baseurl - java.lang.String
  security - $ctx.get($p).getClass().getName().toString()
  numdashes - java.lang.Integer
  ctx - java.util.HashMap
  jirakeyutils - com.atlassian.jira.util.JiraKeyUtils
  i18n - com.atlassian.jira.web.bean.I18nBean
  recipient - com.opensymphony.user.User
  velocityCount - java.lang.Integer
  attachments - java.util.ArrayList
  remoteUser - com.opensymphony.user.User
  userutils - com.atlassian.core.user.UserUtils
  applicationProperties - com.atlassian.jira.config.properties.ApplicationPropertiesImpl
  padSize - java.lang.Integer
  customFieldManager - com.atlassian.jira.issue.managers.DefaultCustomFieldManager
  issue - com.atlassian.jira.mail.TemplateIssue
  jirautils - com.atlassian.jira.util.JiraUtils
  params - java.util.HashMap
  projectManager - com.atlassian.jira.project.CachingProjectManager
  velocityhelper - com.atlassian.jira.util.JiraVelocityHelper
  timeSpentFieldId - java.lang.String
```
