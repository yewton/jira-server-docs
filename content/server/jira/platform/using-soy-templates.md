---
aliases:
- /server/jira/platform/using-soy-templates-16973900.html
- /server/jira/platform/using-soy-templates-16973900.md
category: devguide
confluence_id: 16973900
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973900
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973900
date: '2018-06-26'
legacy_title: Using Soy Templates
platform: server
product: jira
subcategory: updates
title: "Using Soy templates"
---
# Using Soy templates

## Invocation

All invocations run through the` SoyTemplateRenderer` class' `render` method. It takes three parameters:

*   (`String`) – the fully-qualified plugin module key, for example, `jira.webresources:soy-templates`.
*   (`String`) – the Soy template name, including its namespace, for example, `JIRA.Templates.Headers.pageHeader`.
*   (`Map<String,Object>`) – the data for the template. The keys should match with the names of the parameters
in the Soy template.

## Registering templates

To register your Soy template, you will need to add it to a web-resource somewhere.

Jira has a web-resource called `soy-templates` that is defined in the `system-webresources-plugin.xml` file.
For core Jira assets, you should start there.

You can add a Soy template that is available on either the server-side only or on both the server and client.

``` xml
<web-resource key="soy-templates">

    <!-- Available to the server-side only -->
    <resource type="soy" name="links" location="/templates/jira/links.soy"/>
 
    <!-- Make available to the client-side in JS as well... -->
    <transformation extension="soy">
        <transformer key="soyTransformer"/>
    </transformation>
    <resource type="download" name="Links.soy.js" location="/templates/jira/links.soy"/>
</web-resource>
```

## Calling from JSP

``` xml
<%@ taglib uri="webwork" prefix="ui" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%
     String someOtherValue = "http://www.example.com/";
%>
<ui:soy moduleKey="'jira.webresources:soy-templates'" template="'JIRA.Templates.Links.helpLink'">
    <ui:param name="'isLocal'" value="true" />
    <ui:param name="'url'"><%= someOtherValue %></ui:param>
    <ui:param name="'title'"><ww:text name="'example'"/></ui:param>
</ui:soy>
```

## Calling from Velocity

``` java
#set($someOtherValue = "http://www.example.com/"
$soyRenderer.render('jira.webresources:soy-templates','JIRA.Templates.Links.helpLink',{
    'isLocal': true,
    'url': ${someOtherValue},
    'title': $i18n.getText('example')
})
```

## Calling from JavaScript

``` javascript
function getHtmlString() {
    return JIRA.Templates.Links.helpLink({
        isLocal: true,
        url: "http://www.example.com/",
        title: AJS.I18n.getText("example")
    });
}
 
var domElement = jQuery(getHtmlString())[0];
```

## Calling from a Java Class

{{% note %}}

This is not recommended.

{{% /note %}}

``` java
// see below to inject
private SoyTemplateRenderer soyTemplateRenderer;
 
public String getHtml() {
    Map<String, Object> data = new HashMap<String,Object>();
    data.put("isLocal", true);
    data.put("url", "http://www.example.com");
    data.put("title", getI18nHelper().getText("example"));
    return this.soyTemplateRenderer.render("jira.webresources:soy-templates", "JIRA.Templates.Links.helpLink", data);
}
 
private com.atlassian.jira.util.I18nHelper getI18nHelper(){
    // ... up to you
}
```

You inject the `SoyTemplateRenderer` differently in Jira core and apps. See the examples below.

### Injecting in Jira Core

The `SoyTemplateRenderer` cannot be injected via pico, so get it from an injectable `SoyTemplateRendererProvider`:

``` java
public MyConstructor(SoyTemplateRendererProvider soyTemplateRendererProvider){
    this.soyTemplateRenderer = soyTemplateRendererProvider.getRenderer();
}
```

### Injecting in an app

It can be directly injected via Atlassian Spring Scanner:

``` java
public MyConstructor(@ComponentImport SoyTemplateRenderer soyTemplateRenderer) {
    this.soyTemplateRenderer = soyTemplateRenderer;
}
```
