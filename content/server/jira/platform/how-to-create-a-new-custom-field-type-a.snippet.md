---
aliases:
- /server/jira/platform/how-to-create-a-new-custom-field-type-a-22511880.html
- /server/jira/platform/how-to-create-a-new-custom-field-type-a-22511880.md
category: devguide
confluence_id: 22511880
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22511880
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22511880
date: '2017-12-08'
legacy_title: How to create a new custom field type - A
platform: server
product: jira
subcategory: other
title: How to create a new custom field type - A
---
# How to create a new custom field type - A

Since JIRA 3.0 you have been able to create your own Custom Field Types through the plugin interface. In this tutorial, we'll take a look at a few simple examples and explain how you can easily achieve this.

Before you start, you may also want to familiarise yourself with the [About JIRA Plugin Development](/server/jira/platform/about-jira-plugin-development.snippet) and the JIRA user's documentation on <a href="http://confluence.atlassian.com/display/JIRA/Adding+a+Custom+Field" class="external-link">adding a Custom Field</a>.

![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) **A note about changed interfaces**  
We're always endeavouring to make JIRA better with each release. This often leads to new improvements and changes to the public interfaces in major JIRA versions. Please refer to the <a href="http://confluence.atlassian.com/display/JIRA/JIRA+Releases" class="external-link">JIRA release notes</a> for the latest information. In particular, please see [Preparing for JIRA 5.0](/server/jira/platform/preparing-for-jira-5-0).

# A Quick Custom Field Types Primer

There are a few things you need to understand before diving into custom fields. A custom field type can have three components.

-   Java Class encapsulating custom field logic
-   Resource templates for display of custom field
-   Module descriptor to enable the custom field module in JIRA

A custom field class extends the interface <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">CustomFieldType</a>. This interface provides methods to retrieve and store custom fields values. There are several *extension points* that are available to make creating new custom field types easier (e.g. <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/impl/CalculatedCFType.html" class="external-link">CalculatedCFType</a>, <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/impl/AbstractSingleFieldType.html" class="external-link">AbstractSingleFieldType</a>). It is also possible to extend existing custom field types to add functionality (e.g. A currency type extending <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/impl/NumberCFType.html" class="external-link">NumberCFType</a>).

The second component are the resource templates which render the custom field. There are four view types available, each representing a different context to render the custom field.

1.  `view` - basic read-only view of the value (e.g. view issue, move issue confirm screen)
2.  `column-view` - read-only view for displaying in the issue navigator. will default to *view* if omitted
3.  `edit` - renders the edit widget for the custom field (e.g. edit issue, edit defaults)
4.  `xml` - xml view of the value (e.g. rss, xml views)

The values of these templates are usually <a href="http://velocity.apache.org/engine/devel/user-guide.html" class="external-link">Velocity</a> template files that are either standard JIRA `.vm` files, or ones written for the custom field and reside in your `templates` directory. Make sure that their names are unique.

Linking the Java code and rendering views are the plugin-module descriptors in your `atlassian-plugin.xml`. They allow JIRA to recognise what custom fields are available to the system and how to render them.

**Example module descriptor**

``` javascript
<atlassian-plugin key="com.atlassian.jira.plugin.customfield.example" name="JIRA Customfields Examples Plugin">
<plugin-info>
<description>Customfields Examples Plugin.</description>
<version>1.0</version>
<application-version min="3.3" max="3.3"/>
<vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com"/>
</plugin-info>

<customfield-type key="textarea" name="Free Text Field (unlimited text)"
class="com.atlassian.jira.issue.customfields.impl.TextAreaCFType">
<description>A multiline text area custom field to allow input of longer text strings.</description>

<resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
<resource type="velocity" name="column-view" location="templates/plugins/fields/view/view-limited-text.vm"/>
<resource type="velocity" name="edit" location="templates/plugins/fields/edit/edit-textarea.vm"/>
<resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
</customfield-type>
</atlassian-plugin>
```

You can also take a look at the default custom fields that shipped with JIRA. See the JIRA user's documentation on <a href="http://confluence.atlassian.com/display/JIRA/Adding+a+Custom+Field" class="external-link">adding a Custom Field</a>.

Information about setting up a complete plugin development environment for a plugin can be found <a href="http://confluence.atlassian.com/display/DEVNET/How+to+Build+an+Atlassian+Plugin" class="external-link">here</a>.  
You can compile the examples below in the same way.

# Examples

## Admin-only editable field

For the first example, we'll construct a custom field that is only editable by JIRA administrators and appears as a plain text to others. This is a simple customization of the shipped `GenericTextCFType` field and can be customized by changing one template and extending GenericTextCFType in a new class. [Step 1. Create a Plugin Skeleton](https://developer.atlassian.com/display/DOCS/Creating+a+Plugin+Skeleton+with+the+Atlassian+SDK) is a good starting point.

First, we need to extend GenericTextCFType.  Create a `MyPlugin.java` like so (notice namespace.packagename.MyPlugin is this example's namespace and class name):

``` javascript
package namespace.packagename;

import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;

public class MyPlugin extends GenericTextCFType {

    public MyPlugin(CustomFieldValuePersister customFieldValuePersister,
    GenericConfigManager genericConfigManager) {

    super(customFieldValuePersister, genericConfigManager);
    }
}
```

Second, we need to add the module to the `atlassian-plugin.xml file:`

``` javascript
...
<customfield-type key="admintextfield" name="Admin Editable Text Field"
class="namespace.packagename.MyPlugin">
<description>A text field only editable by JIRA-administrators. Others will see only text.</description>
<resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
<resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
<resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
</customfield-type>
...
```

A few points:

-   *key* must uniquely identify the module in this plugin file.
-   *name* & *description* are displayed when creating a new custom field instance

This module definition exactly matches that of a standard text field except for one line.

``` javascript
<resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
```

We are customizing the edit Velocity template so that it displays as a text box for an administrator but appears as uneditable text for others. Source for `edit-jiraadminonlytext.vm` is below.

**edit-jiraadminonlytext.vm**

``` javascript
#controlHeader ($action $customField.id $customField.name $fieldLayoutItem.required $displayParameters.noHeader)

#if ($jiraUserUtils.getGroupNamesForUser($authcontext.loggedInUser.name).contains('jira-administrators'))
<input type="text"
name="$customField.id"
value="$!value" />
#else
#if($value && ! $value.equals(""))
#set ($displayValue = ${value})
#else
#set ($displayValue = 'N/A')
#end
<span title="This field is editable only by JIRA administrators">$!displayValue</span>
<input type="hidden"
name="$customField.id"
value="$!value" />
#end
#controlFooter ($action $fieldLayoutItem.fieldDescription $displayParameters.noHeader)
```

The above template checks if the user is part of group *jira-administrators*. If they are, display the text box, else display the value only as uneditable text.

There's a few points to note.

-   For what variables are available for a custom field you should check out the velocity context guide.
-   \#controlHeader and \#controlFooter provide each custom field with the appropriate label and surrounding HTML table tags. This is required for all edit templates.
-   Note: **edit-jiraadminonlytext.vm** needs to be in in a directory named templates/, in the same directory as atlassian-plugin.xml (which may need to be created if starting from [Step 1. Create a Plugin Skeleton](https://developer.atlassian.com/display/DOCS/Creating+a+Plugin+Skeleton+with+the+Atlassian+SDK)).

And that's it. Rebuild the plugin, deploy the JAR, login as an administrator and you should see it in the list of plugins. Once enabled, look for the custom field under 'Administration -&gt; Issue Fields -&gt; Custom Fields -&gt; Add Custom Field.' Once added, log in as a normal user as well to test it out.

  
![](/server/jira/platform/images/admin-field-admin.png)  
***<sup>Logged\ in\ as\ an\ admin</sup>***

  
![](/server/jira/platform/images/admin-field-dev.png)  
***<sup>Logged\ in\ as\ a\ non-admin</sup>***

## Last commented user calculated field

The next example deals with a *Calculated Custom Field*. Calculated don't actually store any values. You often want or need this when you want to search on fields not normally available in JIRA, but the information can be derived. In this case, we want to return the last user who commented on the issue, if they are not an administrator. We only want this field to be visible in the issue navigator and not the edit or view pages.

#### Coding the Custom Field Type

Before you implement the interface <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">CustomFieldType</a> you should check out the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/" class="external-link">latest javadoc</a>. A useful extension point for calculated custom fields is, unsurprisingly, <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/impl/CalculatedCFType.html" class="external-link">CalculatedCFType</a>, where only three methods need to be implemented (getStringFromSingularObject, getSingularObjectFromString, and getValueFromIssue). If you also choose to implement SortableCustomField you will need to implement compare() as well.

The key method used to retrieve the value of our custom field is getValueFromIssue.

``` javascript
public Object getValueFromIssue(CustomField field, Issue issue)
{
    User currentUser = authenticationContext.getUser();
    User lastUser = null;
    try
    {
        List comments = actionManager.getComments(issue.getGenericValue(), currentUser);
        if (comments != null && !comments.isEmpty())
        {
            Comment lastComment = (Comment) comments.get(comments.size()-1);
            User commenter = lastComment.getUser();
            if (!commenter.inGroup(JIRA_ADMIN))
            {
                lastUser = commenter;
            }
        }
   }
   catch (GenericEntityException e)
   {
     // Handle this
   }

   return lastUser;
}
```

Note that prior to 3.3, the method had a GenericValue as the issue parameter. If you're developing for those JIRA versions make sure you correct your method signatures.

The return type `Object` is also known as the **Transport Object**. In this instance it is of type `User`, but it could be any other type. The Transport type must remain consistent across all methods such as create, update and also the view and edit templates.

#### Wiring it together

Much like the previous example, we can reuse some of the the templates that ship with JIRA.

``` javascript
<customfield-type key="lastusercommented" name="Last user commenter"
class="com.atlassian.jira.plugin.customfield.example.LastUserCommentedCFType">
<description>This is a lookup field that displays the last commenter who is not a JIRA administrator</description>
<resource type="velocity" name="column-view" location="templates/plugins/fields/view/view-user.vm"/>
<resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-user.vm"/>
</customfield-type>
```

We can omit any resource types that we don't require. Thus both the edit and view templates are omitted here. The field should only appear when viewing through the issue navigator (column-view) and XML/RSS views (xml). The view user adds a link to the user details page and displays the full user name.

  
![](/server/jira/platform/images/last-commenter-fred.png)  
***<sup>Fred\ is\ the\ last\ commenter</sup>***

  
![](/server/jira/platform/images/last-commenter-issues.png)  
***<sup>View\ in\ issue\ navigator</sup>***

#### Enable Searching

The last commenter field in itself isn't all that useful. While we can see it in on the issue navigator, we can't search for a particular user who commented it last. Searching in JIRA is handled by `CustomFieldSearchers`. Again several pre-configured searchers are available. You must ensure that the Transport Object are compatible between the custom field and the custom field searcher. Thus we can only use the UserPicker searcher since this is the only one that indexes User objects.

``` javascript
<customfield-searcher key="userpickersearcher" name="User Picker Searcher"
    i18n-name-key="admin.customfield.searcher.userpickersearcher.name"
    class="com.atlassian.jira.issue.customfields.searchers.UserPickerSearcher">
    <description key="admin.customfield.searcher.userpickersearcher.desc">Allow to search for a user using a userpicker.</description>
    <resource type="velocity" name="label" location="templates/plugins/fields/view-searcher/label-searcher-user.vm"/>
    <resource type="velocity" name="search" location="templates/plugins/fields/edit-searcher/search-userpicker.vm"/>
    <resource type="velocity" name="view" location="templates/plugins/fields/view-searcher/view-searcher-basictext.vm"/>

    <valid-customfield-type package="com.atlassian.jira.plugin.customfield.example" key="lastusercommented"/>
</customfield-searcher>
```

This is quite similar to the CustomFieldType definition. The tag *valid-customfield-type* is used to associate the searcher to any number of custom field types. `Package` refers to the atlassian-plugin key attribute at the top of a plug-in and and the `key` refers to the module/customfield key.

Now when you create/edit your Last User Commented custom field, you'll be able to select the User Picker as a search template. You can now search on the last commenter field in the issue issue navigator.

**Important** When you change a search template for a custom field, you may need to perform a reindex before the search will work correctly. This issue is being tracked at <a href="http://jira.atlassian.com/browse/JRA-4641" class="external-link">JRA-4641</a>.

  
![](/server/jira/platform/images/last-commenter-search.png)  
***<sup>Searching\ enabled</sup>***

#### Sorting in Issue Navigator

To enable sorting you simply need to implement the interface `SortableCustomField`

``` javascript
public class LastUserCommentedCFType extends AbstractCustomFieldType implements SortableCustomField
```

The interface simply extends Comparable, so you need to implement the compare method.

``` javascript
public int compare(Object customFieldObjectValue1, Object customFieldObjectValue2, CustomFieldConfig customFieldConfig)
{
return new BestNameComparator().compare(customFieldObjectValue1, customFieldObjectValue2);
}
```

The BestNameComparator is a simple helper type to facilitate comparing two users. You could just as easily write your own custom compare method.

## Amazon search plugin

Lastly, a frivolous plug-in to give you some ideas on how to implement custom fields that perform remote look ups. Basically, we want a custom field that will take a text string and display a results from a search through the Amazon. There are several approaches to this, but by simplest solution is to treat the stored value as a simple text field and then add a *viewHelper* object that effectively transforms the string into the desired result.

#### Coding and Attaching the view helper

First we need to code our Amazon view helper. You can take a look in the source, but how it's been implemented isn't all that relevant. Once we have the view helper, we can pass this helper to the Velocity templates through the method *getVelocityParameters*

``` javascript
public Map getVelocityParameters(Issue issue)
{
Map map = new HashMap();
map.put("amazonSearchViewHelper", new AmazonSearchViewHelper());
return map;
}
```

The object `AmazonSearchViewHelper` is now accessible the velocity template. It has the method `searchForBooks` which returns a list of Books given some key words. We simply invoke this helper method in the template and display the results in a table.

``` javascript
#if ($value)
Results for search query "${value}" <br />

<table class="grid">
<tr>
<th>Title</th>
<th>Primary Author</th>
</tr>
#foreach ($book in $amazonSearchViewHelper.searchForBooks($value))
<tr>
<td><a target="_new" href="${book.link}">${book.title}</a></td>
<td>${book.firstAuthor}</td>
</tr>
#end
</table>
#end
```

You can utilise this same idea to display data from other remote systems, or even combine it with the readonly field to create your very own remote custom field. <img src="/server/jira/platform/images/amazon-search.png" class="image-center" />

## Confluence Page Link custom field

{{% note %}}

This plugin is available <a href="http://confluence.atlassian.com/display/JIRAEXT/JIRA+Linker+Plugin" class="external-link">here</a> - and is not included in the jira-development-kit.

{{% /note %}}

The 'Confluence Page Link' custom field plugin provides an example of implementing a custom field that performs a remote look up through XML/RPC..

This custom field provides a pop-up searcher - allowing the user to enter a search query that is executed over publicly accessible pages within a specified Confleunce instance. The user can select a result and the URL of thatpage is stored in the custom field - a simple text field. The Confluence instance to search against is specified in a properties file.

A new webwork action 'ConfluencePageBrowserAction' is required - allowing the popup window view to be associated with the action that performs and returns results from the Confluence page search.

The webwork action is registered in the atlassian-plugin.xml file as follows:

``` javascript
<webwork1 key="confluencepagebrowseraction" name="Confluence Page Browser Action" class="java.lang.Object">
<actions>
<action name="com.atlassian.jira.plugin.confluencelinker.ConfluencePageBrowserAction" alias="ConfluencePageBrowser">
<view name="success">/templates/confluence_page_browser.vm</view>
</action>
</actions>
</webwork1>
```

The ConfluencePageBrowserAction class is where the search logic is coded:

``` javascript
XmlRpcClient rpcClient = new XmlRpcClient(confluenceURL);
Vector xmlrpcResults = (Vector) rpcClient.execute("confluence1.search", makeParams(getSearchQuery(), 100));
if (xmlrpcResults != null)
{
earchResults = new ArrayList();
for (Iterator iterator = xmlrpcResults.iterator(); iterator.hasNext();)
{
Hashtable xmlrpcResult = (Hashtable) iterator.next();
searchResults.add(new SearchMatch(xmlrpcResult));
}
...
```

The Confluence page browser template displays the search query text box and the results:

``` javascript
#foreach ($result in $action.getSearchResults())
<tr onmouseover="rowHover(this)" onclick="selectLink('$result.getUrl()')">
<td>
<div class="borderedbox">
<b>Title</b>:   $result.getTitle()<br>
<b>URL</b>:     $result.getUrl()<br>
<b>Excerpt</b>: #if($result.getExcerpt())$result.getExcerpt() #else None #end
</div>
</td>
</tr>
```

The popup appears as follows:  
<img src="/server/jira/platform/images/confluence-search.jpg" class="image-center" />
