---
title: Enabling the Rpc Plugin 4227091
aliases:
    - /server/jira/platform/enabling-the-rpc-plugin-4227091.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227091
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227091
confluence_id: 4227091
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Enabling the RPC plugin

{{% note %}}

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

*JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](https://developer.atlassian.com/display/JIRADEV/JIRA+SOAP+to+REST+Migration+Guide)).*

{{% /note %}}

To invoke JIRA operations remotely, you should ensure that the RPC plugin is enabled on the JIRA installation you are targeting. If you simply want to create a client to <a href="http://jira.atlassian.com/" class="uri external-link">http://jira.atlassian.com/</a> then you can skip this step. First you need to check if the Accept Remote API Calls has been enabled in '**General Configuration**' under '**Global Settings**' in the left-hand menu:

<img src="/server/jira/platform/images/rpc-remote-calls.png" class="image-center" />

Then you need to enable the JIRA RPC Plugin in '**Plugins**' under '**System**' in the left-hand menu:

<img src="/server/jira/platform/images/plugins-rpcplugin.png" class="image-center" />

![(warning)](/server/jira/platform/images/icons/emoticons/warning.png) To get the source code of the RPC plugin, see <a href="https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin" class="uri external-link">https://bitbucket.org/atlassian_tutorial/jira-rpc-plugin</a>

Your server should now be ready to accept remote procedure calls.
