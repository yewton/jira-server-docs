---
aliases:
- /server/jira/platform/fields-in-remote-issue-links-6848560.html
- /server/jira/platform/fields-in-remote-issue-links-6848560.md
category: devguide
confluence_id: 6848560
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848560
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848560
date: '2018-06-26'
guides: tutorials
legacy_title: Fields in Remote Issue Links
platform: server
product: jira
subcategory: learning
title: "Using fields in Remote Issue Links"
---
# Using fields in Remote Issue Links

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 5.0 and later.</p></td>
</tr>
</tbody>
</table>

This page provides a guide to the fields used by the Remote Issue Link REST and Java APIs.

## Remote Issue Link field guide

See the [REST API Guide](/server/jira/platform/jira-rest-api-for-remote-issue-links) for examples with full JSON
sample data and screenshots.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field.</br>Required. </br>JSON Key</p></th>
<th><p>Description. </br>Impact if blank</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Field:</strong> GlobalID</p>
<p><strong>Required:</strong> we strongly recommend you pass in this value.</p>
<p><strong>JSON Key:</strong> globalId</p></td>
<td><p><strong>Description:</strong> A globally unique identifier that uniquely identifies the remote application
and the remote object within the remote system. The maximum length is 255 characters.</p>
<p><strong>Impact if blank:</strong> You will not be able to update or delete the link unless you know the internal
ID of the link.</p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Relationship</p>
<p><strong>JSON Key:</strong> relationship</p>
<p> </p></td>
<td><p><strong>Description:</strong> Relationship between the remote object and the Jira issue. This can be a
verb or a noun. It is used to group together links in the UI.</p>
<p><strong>Impact if blank:</strong> &quot;links to&quot; used in UI.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Object</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> URL</p>
<p><strong>Required:</strong> required otherwise you can't create the link.</p>
<p><strong>JSON Key:</strong> object.url</p></td>
<td><p><strong>Description:</strong> A hyperlink to the object in the remote system.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Title</p>
<p><strong>Required:</strong> required otherwise you can't create the link.</p>
<p><strong>JSON Key:</strong> object.title</p></td>
<td><p><strong>Description:</strong> The title of the remote object.</p>
<p> </p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Summary</p>
<p><strong>JSON Key:</strong> object.summary</p>
<p> </p></td>
<td><p><strong>Description:</strong> Textual summary of the remote object.</p>
<p><strong>Impact of blank:</strong> No text in UI.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Icon URL</p>
<p><strong>Required:</strong> we strongly recommend you pass in this value.</p>
<p><strong>JSON Key:</strong> object.icon.url16x16</p></td>
<td><p><strong>Description:</strong> A 16x16 icon representing the type of the object in the remote system.</p>
<p><strong>Impact of blank:</strong> Default link icon will be used in UI.</p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Icon Title</p>
<p><strong>Required:</strong> we strongly recommend you pass in this value.</p>
<p><strong>JSON Key:</strong> object.icon.title</p></td>
<td><p><strong>Description:</strong> Text for the tooltip of the main icon describing the type of the object in
the remote system. The <em>Icon Title</em> and <em>Application Name</em> are combined to form the tooltip.</p>
<p><strong>Impact of blank:</strong> Only <em>Application Name</em> is used in tooltip.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Resolved</p>
<p><strong>JSON Key</strong>: object.status.resolved</p></td>
<td><p><strong>Description:</strong> Whether the remote object is &quot;resolved&quot; (value is &quot;true&quot;) or
&quot;unresolved&quot; (value is &quot;false&quot;). This value will only make sense where the remote object is
something that is resolvable (for example, an issue, support ticket, and so on). It will not make sense for things
like Wiki pages.<br />
If Resolved is &quot;true&quot;, the link to the issue will have a be in a strikethrough font.</p>
<p><strong>Impact of blank:</strong> <em>Title</em> text will appear in normal font in UI.</p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Status Icon URL</p>
<p><strong>JSON Key:</strong> object.status.icon.url16x16</p></td>
<td><p><strong>Description:</strong> A 16x16 icon representing the status of the remote object.</p>
<p><strong>Impact of blank</strong>: No status icon in UI.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Status Icon Text</p>
<p><strong>JSON Key:</strong> object.status.icon.title</p></td>
<td><p><strong>Description:</strong> Textual description of the status for the tooltip of the the status icon.
Not used if <em>Status Icon URL</em> is blank.</p>
<p><strong>Impact of blank</strong>: Status icon doesn't have a tooltip.</p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Status Icon Link</p>
<p><strong>JSON Key:</strong> object.status.icon.link</p></td>
<td><p><strong>Description:</strong> A hyperlink for the tooltip of the the status icon. Not used if 
<em>Status Icon URL</em> is blank.</p>
<p><strong>Impact of blank:</strong> Status icon is not clickable.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field:</strong> Application</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><strong>Field:</strong> Application Name</p>
<p><strong>Required:</strong> we strongly recommend you pass in this value.</p>
<p><strong>JSON Key:</strong> application.name</p></td>
<td><p><strong>Description:</strong> The human-readable name of the remote application instance that stores the
remote object. Links are grouped on the <em>Application Type</em> and <em>Application Name _in the UI. The _Icon
Title</em> and <em>Application Name</em> are combined to form the tooltip of the main icon. </p>
<p><strong>Impact of blank:</strong> Only <em>Icon Title</em> is used in icon tooltip.<br />
If <em>Icon Title</em> is also blank, icon tooltop is &quot;Web Link&quot; <br />
Grouping and sorting of links may place these links last.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Field</strong>: Application Type</p>
<p><strong>Required:</strong> we strongly recommend you pass in this value.</p>
<p><strong>JSON Key:</strong> application.type</p></td>
<td><p><strong>Description:</strong> The name-spaced type of the application. It is not displayed to the user.
Rendering apps can register to render a certain type of application. Links are grouped based on the <em>Application Type</em> 
and _Application Name _in the UI.<br />
See sorting below.</p>
<p><strong>Impact of blank:</strong> You will not be able to provide a custom renderer for your links.</p></td>
</tr>
</tbody>
</table>

Uniqueness on *Global ID* for a particular issue key is enforced.

### Remote Jira Examples

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Remote Jira Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>GlobalID</p></td>
<td><p>appId=123&amp;issueId=123</p></td>
</tr>
<tr class="even">
<td><p>Relationship</p></td>
<td><p>duplicates</p></td>
</tr>
<tr class="odd">
<td><p>URL</p></td>
<td><p><a href="http://mycompany.com/jira/CWD-111" class="uri external-link">http://mycompany.com/jira/CWD-111</a></p></td>
</tr>
<tr class="even">
<td><p>Title</p></td>
<td><p>CWD-111</p></td>
</tr>
<tr class="odd">
<td><p>Summary</p></td>
<td><p>Support nested groups</p></td>
</tr>
<tr class="even">
<td><p>Icon URL</p></td>
<td><p><a href="http://mycompany.com/jira/bug.png" class="uri external-link">http://mycompany.com/jira/bug.png</a></p></td>
</tr>
<tr class="odd">
<td><p>Icon Title</p></td>
<td><p>Bug - A problem which impairs or prevents the functions of the product</p></td>
</tr>
<tr class="even">
<td><p>Resolved</p></td>
<td><p>false</p></td>
</tr>
<tr class="odd">
<td><p>Status Icon URL</p></td>
<td><p><a href="https://jira.atlassian.com/images/icons/newfeature.gif" class="external-link">https://www.mycompany.com/jira/images/icons/newfeature.gif</a></p></td>
</tr>
<tr class="even">
<td><p>Status Icon Text</p></td>
<td><p>Resolved - A resolution has been taken, and it is awaiting verification by reporter.  From here issues are either reopened, or are closed.</p></td>
</tr>
<tr class="odd">
<td><p>Application Name</p></td>
<td><p>My Company Jira</p></td>
</tr>
<tr class="even">
<td><p>Application Type</p></td>
<td><p>com.atlassian.jira</p></td>
</tr>
</tbody>
</table>

### Remote Confluence Examples

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Remote Confluence Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>GlobalID</p></td>
<td><p>appId=456&amp;pageId=123</p></td>
</tr>
<tr class="even">
<td><p>Relationship</p></td>
<td><p>mentioned in</p></td>
</tr>
<tr class="odd">
<td><p>URL</p></td>
<td><p><a href="http://www.website.com/hello.html" class="uri external-link">http://www.website.com/hello.html</a></p></td>
</tr>
<tr class="even">
<td><p>Title</p></td>
<td><p>My First Page</p></td>
</tr>
<tr class="odd">
<td><p>Icon URL</p></td>
<td><p><a href="http://mycompany.com/wiki/page.png" class="uri external-link">http://mycompany.com/wiki/page.png</a></p></td>
</tr>
<tr class="even">
<td><p>Icon Title</p></td>
<td><p>Page</p></td>
</tr>
<tr class="odd">
<td><p>Status Icon Link</p></td>
<td><p>My Company Wiki</p></td>
</tr>
<tr class="even">
<td><p>Application Type</p></td>
<td><p>com.atlassian.confluence</p></td>
</tr>
</tbody>
</table>

### Web Link Examples

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Web Link Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>URL</p></td>
<td><a href="http://www.website.com/hello.html" class="uri external-link">http://www.website.com/hello.html</a></td>
</tr>
<tr class="even">
<td><p>Title</p></td>
<td><p>Hello World</p></td>
</tr>
</tbody>
</table>
