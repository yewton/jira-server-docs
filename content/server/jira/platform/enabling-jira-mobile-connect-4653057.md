---
title: Enabling JIRA Mobile Connect 4653057
aliases:
    - /server/jira/platform/enabling-jira-mobile-connect-4653057.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4653057
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4653057
confluence_id: 4653057
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Enabling JIRA Mobile Connect

[JIRA Mobile Connect](https://developer.atlassian.com/display/JMC/JIRA+Mobile+Connect+Developer+Documentation) is comprised of both a plugin for JIRA and an SDK for your mobile app. This means that you will need access to a JIRA site with the <a href="https://plugins.atlassian.com/plugin/details/322837" class="external-link">JIRA Mobile Connect Plugin</a> installed, as well as access to your mobile application code.

There are three steps required to enable JIRA Mobile Connect:

-   [Download](#download) the JIRA Mobile Connect SDK
-   [Enable](#enable) the JIRA Mobile Connect plugin on your instance of JIRA
-   [Embed](#embed) the JIRA Mobile Connect SDK within your mobile app

Below are the installation guides for both components.

Here is a quick video that shows you how to configure JIRA Mobile Connect. It assumes that you have already completed Steps 1 and 2 below.

{{&lt; youtube VlDyjA37rjU &gt;}}

## Step 1. Downloading the JIRA Mobile Connect SDK

You can download the JIRA Mobile Connect SDK from: <a href="https://bitbucket.org/atlassian/jiraconnect-ios/get/tip.zip" class="uri external-link">https://bitbucket.org/atlassian/jiraconnect-ios/get/tip.zip</a>

Alternatively, you can clone the JIRA Mobile Connect SDK from <a href="https://bitbucket.org/atlassian/jiraconnect-ios" class="external-link">Bitbucket</a>:

``` javascript
hg clone https://bitbucket.org/atlassian/jiraconnect-ios
```

(It's a Mercurial repository. New to Bitbucket? See <a href="http://confluence.atlassian.com/display/BITBUCKET/Getting+Started+with+Bitbucket" class="external-link">Getting Started with Bitbucket</a>.)

## Step 2. Enabling the JIRA Mobile Connect plugin for your JIRA project

1.  **Server Customers** - Install the <a href="https://plugins.atlassian.com/plugin/details/322837" class="external-link">JIRA Mobile Connect plugin</a> on your instance of JIRA by following <a href="http://confluence.atlassian.com/display/JIRA044/Managing+JIRA%27s+Plugins#ManagingJIRAsPlugins-InstallingaJIRAPlugin" class="external-link">these instructions</a>. **Note** - Atlassian Cloud customers already have the JIRA Mobile Connect plugin installed on their instance of JIRA.
2.  **Server Customers** - Go to the `Settings` section within your project administration.
    **Cloud Customers** - Go to the `Settings` section within your project administration. This may be located under the `General` tab.
3.  Click `Enable` next to **JIRA Mobile Connect**. The following screen will display with your instance URL and project details, which you will be embedding in your app to connect back to JIRA:

![](/server/jira/platform/images/admin-settings-jmc.png)

Now JIRA is set up and ready to receive feedback from JIRA Mobile Connect. Next step is to embed the JIRA Mobile Connect SDK in your app.

## Step 3. Embedding the JIRA Mobile Connect SDK in your mobile app

Follow the instructions for your respective operating system:

-   [iOS SDK](/server/jira/platform/installing-the-jira-mobile-connect-sdk-on-ios-8946505.html)
-   Android SDK - coming soon!

## Issues?

Please raise any issues and feature requests in our issue tracker for the JIRA Mobile Connect SDK: <a href="http://connect.onjira.com/browse/CONNECT" class="uri external-link">http://connect.onjira.com/browse/CONNECT</a>

## Questions?

If you have any questions about JIRA Mobile Connect, please raise them on <a href="https://answers.atlassian.com/tags/jira-mobile-connect/" class="external-link">Atlassian Answers</a>.
