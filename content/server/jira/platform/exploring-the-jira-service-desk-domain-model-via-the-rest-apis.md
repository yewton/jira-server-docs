---
aliases:
- /server/jira/platform/guide-exploring-the-jira-service-desk-domain-model-via-the-rest-apis-37238392.html
- /server/jira/platform/guide-exploring-the-jira-service-desk-domain-model-via-the-rest-apis-37238392.md
category: devguide
confluence_id: 37238392
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=37238392
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=37238392
date: '2017-12-08'
guides: tutorials
legacy_title: Guide - Exploring the JIRA Service Desk domain model via the REST APIs
platform: server
product: jira
subcategory: learning
title: Exploring the JIRA Service Desk domain model via the REST APIs
---
# Exploring the JIRA Service Desk domain model via the REST APIs

This guide will help you understand the JIRA Service Desk domain model, by using the <a href="https://docs.atlassian.com/jira-servicedesk/REST/latest/" class="external-link">JIRA Service Desk REST API</a> and <a href="https://docs.atlassian.com/jira/REST/latest/" class="external-link">JIRA platform REST API</a> to examine it. JIRA Service Desk is built upon the JIRA platform, so key domain objects of the JIRA platform will also be discussed.

You should start by reading the "Overview" section. Each section after that covers different objects in the JIRA Service Desk domain model. We recommend that you read all of those sections from top to bottom, but you can read them in a different order or skip sections if you want to.

If you want to browse the REST APIs for JIRA Service Desk or JIRA platform instead, you can find them here: [JIRA APIs](/server/jira/platform/jira-apis-32344053.html).

## Before you begin

-   You can use any REST client to work through the examples in this tutorial. The examples were written using the <a href="http://curl.haxx.se/docs/manpage.html" class="external-link">curl tool</a>. We have also piped the results through Python to make the JSON responses easier to read. 
-   The examples in this guide were last tested with **JIRA Service Desk 3.1**.

## Overview

### Design philosophy -- specific versus generic domain objects

The JIRA Service Desk domain objects are built upon peer JIRA platform counterparts. For example, service desks are based on projects. However, the JIRA Service Desk objects have a much more specific purpose, compared to the JIRA platform objects. For example, you might have a Request Type named "Laptop Purchase" in JIRA Service Desk, which has a much more specific use case than the "Task" Issue Type (in JIRA platform) that it is based on. 

The reason for this is that a key design goal for JIRA Service Desk is to have a simple and targeted experience for people asking for help from a service desk team. The JIRA platform has to cater for many different user experiences, and as a result, it has generic domain objects that are more flexible. However, these generic concepts and terms may not make sense for your service desk customers. Therefore, JIRA Service Desk maps the generic objects to specific JIRA Service Desk objects. For example, you could map the "Summary" issue field (JIRA platform) to a request type field, which you might name "What do you need?". The same field has a dual purpose -- it still functions like the "Summary" field but it is presented as a "What do you need?" field.

If you understand this design philosophy that underlies how JIRA platform and JIRA Service Desk relate to each other, the JIRA Service Desk Domain model will make more sense. 

### Summary of the domain objects

<img src="/server/jira/platform/images/37240766.png" class="gliffy-macro-image" />

This section outlines the key domain objects in JIRA Service Desk, as well as the relevant objects in the JIRA platform. The table below describes the objects at a high level. We'll explore each object in more depth, using the REST APIs, in the later sections on this page.

**Service Desk** *(JIRA Service Desk)*

-   Service Desks are the top level "container" objects in JSD, and contain the configuration for a particular service desk. An example might be your IT help desk. 

-   The peer object for the Service Desk is the Project. There is a 1:1 mapping between a service desk and a project. 

**Project** *(JIRA platform)*

-   A Project is simply a collection of issues. In JIRA Service Desk, configuration that is specific to JIRA platform (e.g. workflow, permissions, etc) is defined in terms of the project, not the service desk.

-   The peer object for the Project is the Service Desk. 

**Request** *(JIRA Service Desk)*

-   A Request contains the data from when a customer asks for help from the service desk team. An example might be a customer's request for a new email account. 

-   A Request consists of a number of Request Fields. 

-   The peer object for the Request is the Issue. Requests are implemented in terms of Issues. A Request must be an Issue, although an Issue may not be a Request.

**Issue** *(JIRA platform)*

-   An Issue is the smallest container of data in the JIRA platform, and can represent different things (e.g. a bug, a task, a request, etc) depending on how the organisation is using JIRA.

-   An Issue consists of a number of Issue Fields, and has an "issue key" (derived from the Project key) that is unique across the JIRA instance.

-   The peer object for the Issue is the Request.  

**Request Field** *(JIRA Service Desk)*

-   Request Fields are the information you want to capture in Requests. Request Fields are implemented in terms of Issue Fields.

-   You can think of Request Fields as a a logical "view" over Issue Fields, which allow the generic JIRA fields to be renamed into the language that your customers will understand. For example, you might have a Request Field named "What do you need?" which is based on the "Summary" Issue Field.

-   The peer object for the Request Field is the Issue Field.

**Issue Field** *(JIRA platform)*

-   Issue Fields are the "named" values contained within Issues. For example, the issue's summary, created date, status, etc.  

-   The peer object for the Issue Field is the Request Field.

**Comment** *(JIRA platform & JIRA Service Desk)*

-   Comments are attached to Requests / Issues and represent the conversation involved in servicing Requests.

**Request Type** *(JIRA Service Desk)*

-   A Request Type defines the information that needs to be captured for particular type of requests. You can think of them as a way of categorising requests in a Service Desk. For example, you might have a "Systems Access" request type or a "Laptop Purchase" request type.  

-   The peer object for the Request Type is the Issue Type, however Request Types and Issues Types are not 1:1 peers. Different Request Types can be defined for the same Issue Type. An individual Request / Issue will always have one Request Type.

**Issue Type** *(JIRA platform)*

-   An Issue Type is used in the platform to map JIRA configuration, such as fields and workflows, to Issues.

-   The peer object for the Issue Type is the Request Type. An individual Request / Issue will always have one Issue Type.

**Request Status** *(JIRA Service Desk)*

-   The Request Status represents the current state of a Request. JIRA Service Desk customers can see the state of of a Request from the Request Status.  

-   A Request Status is based on an Issue Status. For example, you might have a "Completed" Request Status which is based on the "Resolved" Issue Status.

-   The peer object for the Request Status is the Issue Status.

**Issue Status** *(JIRA platform)*

-   The Issue Status represents the state of an Issue inside its defined JIRA workflow. Not all Issue Statuses are shown to JIRA Service Desk customers -- it's possible to have workflow steps that are visible only to the service desk team.

-   The peer object for the Issue Status is the Request Status.  

**Request Service Level Agreement** *(JIRA Service Desk)*

-   A Request SLA is used to track metrics on how much time is spent servicing Requests. Timers are set when Requests enter certain conditions, and goals are defined for moving those Requests in and out of the conditions. For example, you may have a "Time to first response" SLA defined so that your team can aim to get back to a customer within 4 hours of a request being raised.

-   There is no peer object for the Request Service Level Agreement

## Service Desks and Projects

The basic container of JIRA Service Desk domain objects is the **Service Desk** and its peer object, the JIRA platform **Project**. There is a one to one relationship between a Service Desk and a Project; and a Service Desk is implemented from a JIRA platform project. The permissions for managing the users that can access a service desk are controlled via the peer project.

A Service Desk is the starting point for all things in JIRA Service Desk. It contains most of the other domain objects, such as Request Types, Request Type Fields, etc. Let's see what a Service Desk looks like, by getting a list of the Service Desks in the system via the REST API:

###### Example A Request

``` bash
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk?start=0&limit=5" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "next": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk?limit=5&start=5",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk?start=0&limit=5"
    },
    "isLastPage": false,
    "limit": 5,
    "size": 5,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15"
            },
            "id": 15,
            "projectId": 11041,
            "projectName": "All Teams Service Desk"
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/19"
            },
            "id": 19,
            "projectId": 11241,
            "projectName": "Foundation Leave"
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/21"
            },
            "id": 21,
            "projectId": 11243,
            "projectName": "Human Resources Service Desk"
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/16"
            },
            "id": 16,
            "projectId": 11042,
            "projectName": "IT Help"
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/23"
            },
            "id": 23,
            "projectId": 11245,
            "projectName": "Lawyer Up - Company Lawyer Requests"
        }
    ]
}
```

Here's a few things to note about the Service Desk:

-   Each Service Desk has an `Id`, a JIRA `projectId,` and `projectName`. 
-   The `projectId` comes from the peer JIRA platform domain Project object. You can access more information about the Project via the <a href="https://docs.atlassian.com/jira/REST/latest/" class="external-link">JIRA Platform REST API</a>. In many cases, you can use either the JIRA Service Desk REST API or JIRA Platform REST API to find the same information -- when to use which API depends on your use case and what you want to know.
-   The *\_links* section at the top contains useful HTTP URIs that point other places in the system, such as the JIRA platform base URL.
-   The JIRA Service Desk REST API uses paging for lists of resources. In this case, we asked for `5` in a page starting from `0`; that is, the first five Service Desks.  The *next* link would be used to get the next set of items in the list.

You can see from the response that there is a direct relationship between the Service Desk and its peer Project. Let's explore this further by getting the Project information for one of the Service Desks, via the REST API: the "All Teams Service Desk" (`projectId` = 11041):

###### Example B Request

``` bash
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/project/11041" | python -mjson.tool
```

###### Response

``` javascript
{
    "assigneeType": "UNASSIGNED",
    "avatarUrls": {
        "16x16": "http://localhost:2990/jira/secure/projectavatar?size=xsmall&avatarId=10873",
        "24x24": "http://localhost:2990/jira/secure/projectavatar?size=small&avatarId=10873",
        "32x32": "http://localhost:2990/jira/secure/projectavatar?size=medium&avatarId=10873",
        "48x48": "http://localhost:2990/jira/secure/projectavatar?avatarId=10873"
    },
    "components": [
        {
            "description": "Issues related to the intranet. Created by JIRA Service Desk.",
            "id": "10414",
            "isAssigneeTypeValid": false,
            "name": "Intranet",
            "self": "http://localhost:2990/jira/rest/api/2/component/10414"
        },
        {
            "description": "Issues related to JIRA. Created by JIRA Service Desk.",
            "id": "10413",
            "isAssigneeTypeValid": false,
            "name": "JIRA",
            "self": "http://localhost:2990/jira/rest/api/2/component/10413"
        },
        {
            "description": "Issues related to the public website. Created by JIRA Service Desk.",
            "id": "10415",
            "isAssigneeTypeValid": false,
            "name": "Public website",
            "self": "http://localhost:2990/jira/rest/api/2/component/10415"
        }
    ],
    "description": "",
    "expand": "description,lead,url,projectKeys",
    "id": "11041",
    "issueTypes": [
        {
            "description": "For general IT problems and questions. Created by JIRA Service Desk.",
            "iconUrl": "http://localhost:2990/jira/servicedesk/issue-type-icons?icon=it-help",
            "id": "13",
            "name": "IT Help",
            "self": "http://localhost:2990/jira/rest/api/2/issuetype/13",
            "subtask": false
        },
        {
            "description": "Track items that need to be bought. Created by JIRA Service Desk.",
            "iconUrl": "http://localhost:2990/jira/servicedesk/issue-type-icons?icon=purchase",
            "id": "14",
            "name": "Purchase",
            "self": "http://localhost:2990/jira/rest/api/2/issuetype/14",
            "subtask": false
        },
        {
            "description": "Track system outages or incidents. Created by JIRA Service Desk.",
            "iconUrl": "http://localhost:2990/jira/servicedesk/issue-type-icons?icon=fault",
            "id": "16",
            "name": "Fault",
            "self": "http://localhost:2990/jira/rest/api/2/issuetype/16",
            "subtask": false
        },
        {
            "description": "For new system accounts or passwords. Created by JIRA Service Desk.",
            "iconUrl": "http://localhost:2990/jira/servicedesk/issue-type-icons?icon=access",
            "id": "17",
            "name": "Access",
            "self": "http://localhost:2990/jira/rest/api/2/issuetype/17",
            "subtask": false
        }
    ],
    "key": "SD",
    "lead": {
        "active": true,
        "avatarUrls": {
            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
        },
        "displayName": "Project Admin of some projects",
        "key": "patricia",
        "name": "projectad",
        "self": "http://localhost:2990/jira/rest/api/2/user?username=projectad"
    },
    "name": "All Teams Service Desk",
    "projectTypeKey": "service_desk",
    "roles": {
        "Administrators": "http://localhost:2990/jira/rest/api/2/project/11041/role/10002",
        "Developers": "http://localhost:2990/jira/rest/api/2/project/11041/role/10001",
        "Doco Team": "http://localhost:2990/jira/rest/api/2/project/11041/role/10010",
        "Service Desk Collaborators": "http://localhost:2990/jira/rest/api/2/project/11041/role/10210",
        "Service Desk Customers": "http://localhost:2990/jira/rest/api/2/project/11041/role/10310",
        "Service Desk Team": "http://localhost:2990/jira/rest/api/2/project/11041/role/10111",
        "Users": "http://localhost:2990/jira/rest/api/2/project/11041/role/10000"
    },
    "self": "http://localhost:2990/jira/rest/api/2/project/11041",
    "versions": []
}
```

As you can see, there is plenty of additional information about the Project, which you can obtain via the JIRA platform REST API. This includes domain objects like Issue Types, Components, etc. We won't discuss this in detail here, but if you want to know more, check out the REST API documentation on the <a href="https://docs.atlassian.com/jira/REST/latest/#api/2/project" class="external-link">Project resource</a>.

## Requests and Issues

When someone asks for help from a service desk, that is called a **Request**. The Request also has a peer object: the **Issue**. Requests are implemented from Issues, much like Service Desks are implemented from Projects. However, not all Issues are Requests. This is because JIRA can be used for other purposes, such as JIRA Software and JIRA Core. The basic rule is that anything created via a Service Desk Portal is considered a Request.  

Requests can be examined in terms of the person who reported them. We're going to have a look at a few examples from the point of view of a customer (named "Ronald the Customer"). We'll see how they would use the JIRA Service Desk REST API to get information about the Requests they have created. 

The first example shows how the customer would retrieve all of the requests that they have created:

######  Example C Request

``` bash
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/request"
    },
    "expand": "customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk",
    "isLastPage": true,
    "limit": 50,
    "size": 3,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17229"
            },
            "createdDate": {
                "epochMillis": 1446776670000,
                "friendly": "06/Nov/15 1:24 PM",
                "iso8601": "2015-11-06T13:24:30+1100"
            },
            "currentStatus": {
                "status": "Resolved",
                "statusDate": {
                    "epochMillis": 1446776741021,
                    "friendly": "06/Nov/15 1:25 PM",
                    "iso8601": "2015-11-06T13:25:41+1100"
                }
            },
            "expand": "customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk",
            "issueId": 17229,
            "issueKey": "SD-12",
            "issueTypeId": "17",
            "projectId": 11041,
            "reporter": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "requestFieldValues": [
                {
                    "fieldId": "components",
                    "label": "Which system?",
                    "value": [
                        {
                            "description": "Issues related to JIRA. Created by JIRA Service Desk.",
                            "id": "10413",
                            "name": "JIRA",
                            "self": "http://localhost:2990/jira/rest/api/2/component/10413"
                        }
                    ]
                },
                {
                    "fieldId": "summary",
                    "label": "What type of access do you need?",
                    "value": "root access"
                },
                {
                    "fieldId": "description",
                    "label": "Why do you need this?",
                    "value": "I want to be able to do stuff on JIRA.  Giving me root access seems like a good way to do that."
                }
            ],
            "requestTypeId": 63,
            "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17229",
            "serviceDeskId": 15
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17228"
            },
            "createdDate": {
                "epochMillis": 1446769068000,
                "friendly": "06/Nov/15 11:17 AM",
                "iso8601": "2015-11-06T11:17:48+1100"
            },
            "currentStatus": {
                "status": "Resolved",
                "statusDate": {
                    "epochMillis": 1446776578551,
                    "friendly": "06/Nov/15 1:22 PM",
                    "iso8601": "2015-11-06T13:22:58+1100"
                }
            },
            "expand": "customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk",
            "issueId": 17228,
            "issueKey": "SD-11",
            "issueTypeId": "17",
            "projectId": 11041,
            "reporter": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "requestFieldValues": [
                {
                    "fieldId": "components",
                    "label": "Which system?",
                    "value": [
                        {
                            "description": "Issues related to the public website. Created by JIRA Service Desk.",
                            "id": "10415",
                            "name": "Public website",
                            "self": "http://localhost:2990/jira/rest/api/2/component/10415"
                        }
                    ]
                },
                {
                    "fieldId": "summary",
                    "label": "What type of access do you need?",
                    "value": "I need basic login access"
                },
                {
                    "fieldId": "description",
                    "label": "Why do you need this?",
                    "value": "I have joined the Marketing Zoom team and I need access to the Content Management system to update our public messaging."
                }
            ],
            "requestTypeId": 63,
            "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17228",
            "serviceDeskId": 15
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227"
            },
            "createdDate": {
                "epochMillis": 1446768040000,
                "friendly": "06/Nov/15 11:00 AM",
                "iso8601": "2015-11-06T11:00:40+1100"
            },
            "currentStatus": {
                "status": "Waiting for Support",
                "statusDate": {
                    "epochMillis": 1446768682499,
                    "friendly": "06/Nov/15 11:11 AM",
                    "iso8601": "2015-11-06T11:11:22+1100"
                }
            },
            "expand": "customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk",
            "issueId": 17227,
            "issueKey": "SD-10",
            "issueTypeId": "13",
            "projectId": 11041,
            "reporter": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "requestFieldValues": [
                {
                    "fieldId": "summary",
                    "label": "What do you need?",
                    "value": "I need help with the L8 printer"
                },
                {
                    "fieldId": "description",
                    "label": "Why do you need this?",
                    "value": "Its not printing at all.  I have changed the cartridge and pressed reset but not deal.\n\nI am at a loss as to what to do next"
                },
                {
                    "fieldId": "customfield_11440",
                    "label": "Your Office?"
                }
            ],
            "requestTypeId": 62,
            "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227",
            "serviceDeskId": 15
        }
    ]
}
```

Let's have a closer look at the following data items:

``` javascript
            "issueId": 17229,
            "issueKey": "SD-12",
```

The `issueId` and `issueKey` entries are the Id and key of the JIRA platform Issue used to represent this Request. You can use the JIRA platform REST API to find out even more about this Issue, as shown below:

###### Example D Request

``` bash
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/issue/17227" | python -mjson.tool
```

###### Response

``` javascript
{
    "expand": "renderedFields,names,schema,transitions,operations,editmeta,changelog,versionedRepresentations",
    "fields": {
        "aggregateprogress": {
            "progress": 0,
            "total": 0
        },
        "aggregatetimeestimate": null,
        "aggregatetimeoriginalestimate": null,
        "aggregatetimespent": null,
        "assignee": null,
        "attachment": [],
        "comment": {
            "comments": [
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122",
                    "created": "2015-11-06T11:02:04.601+1100",
                    "id": "37860",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37860",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:02:04.601+1100"
                },
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                        },
                        "displayName": "Ronald The Customer",
                        "emailAddress": "customer0@atlassian.com",
                        "key": "ronald",
                        "name": "customer",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "Ahh good point.  I too will be there so I will maybe see you there.",
                    "created": "2015-11-06T11:02:41.960+1100",
                    "id": "37861",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37861",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                        },
                        "displayName": "Ronald The Customer",
                        "emailAddress": "customer0@atlassian.com",
                        "key": "ronald",
                        "name": "customer",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:02:41.960+1100"
                },
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122",
                    "created": "2015-11-06T11:09:34.272+1100",
                    "id": "37862",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37862",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:09:34.272+1100"
                },
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                        },
                        "displayName": "Ronald The Customer",
                        "emailAddress": "customer0@atlassian.com",
                        "key": "ronald",
                        "name": "customer",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.",
                    "created": "2015-11-06T11:11:22.404+1100",
                    "id": "37863",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37863",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                        },
                        "displayName": "Ronald The Customer",
                        "emailAddress": "customer0@atlassian.com",
                        "key": "ronald",
                        "name": "customer",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:11:22.404+1100"
                },
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "\nNote to team - we need to get the directory service back up and running.  We cant find out what floor any one is on without.\n",
                    "created": "2015-11-06T11:12:27.488+1100",
                    "id": "37864",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37864",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:12:27.488+1100"
                },
                {
                    "author": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "body": "Also whats up with all the dust coming down the stair well with the new renovations.  If it gets into the server room there will be hell to pay.",
                    "created": "2015-11-06T11:13:22.477+1100",
                    "id": "37865",
                    "self": "http://localhost:2990/jira/rest/api/2/issue/17227/comment/37865",
                    "updateAuthor": {
                        "active": true,
                        "avatarUrls": {
                            "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                            "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                            "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                            "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                        },
                        "displayName": "\u00aagent\u2122",
                        "emailAddress": "agent@example.com",
                        "key": "waldo",
                        "name": "agent",
                        "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                        "timeZone": "Australia/Sydney"
                    },
                    "updated": "2015-11-06T11:13:22.477+1100"
                }
            ],
            "maxResults": 6,
            "startAt": 0,
            "total": 6
        },
        "components": [],
        "created": "2015-11-06T11:00:40.459+1100",
        "creator": {
            "active": true,
            "avatarUrls": {
                "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
            },
            "displayName": "Ronald The Customer",
            "emailAddress": "customer0@atlassian.com",
            "key": "ronald",
            "name": "customer",
            "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
            "timeZone": "Australia/Sydney"
        },
        "customfield_10001": null,
        "customfield_10010": null,
        "customfield_10020": null,
        "customfield_10022": null,
        "customfield_10040": null,
        "customfield_10050": null,
        "customfield_10051": null,
        "customfield_10071": null,
        "customfield_10080": null,
        "customfield_10090": null,
        "customfield_10100": null,
        "customfield_10110": null,
        "customfield_10120": null,
        "customfield_10144": null,
        "customfield_10540": null,
        "customfield_10541": "She sells sea shells.",
        "customfield_10641": null,
        "customfield_10642": null,
        "customfield_10740": null,
        "customfield_11040": {
            "_links": {
                "jiraRest": "http://localhost:2990/jira/rest/api/2/issue/17227",
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227"
            },
            "currentStatus": {
                "status": "Waiting for Support",
                "statusDate": {
                    "epochMillis": 1446768682499,
                    "friendly": "06/Nov/15 11:11 AM",
                    "iso8601": "2015-11-06T11:11:22+1100"
                }
            },
            "requestType": {
                "_links": {
                    "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62"
                },
                "description": "Get assistance for general IT problems and questions [example]",
                "id": 62,
                "name": "Get IT help",
                "serviceDeskId": 15
            }
        },
        "customfield_11043": {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/30"
            },
            "completedCycles": [],
            "id": 30,
            "name": "Time to resolution",
            "ongoingCycle": {
                "breachTime": {
                    "epochMillis": 1446789600000,
                    "friendly": "06/Nov/15 5:00 PM",
                    "iso8601": "2015-11-06T17:00:00+1100"
                },
                "breached": true,
                "elapsedTime": {
                    "friendly": "136h 18m",
                    "millis": 490681509
                },
                "goalDuration": {
                    "friendly": "4h",
                    "millis": 14400000
                },
                "paused": false,
                "remainingTime": {
                    "friendly": "-132h 18m",
                    "millis": -476281509
                },
                "startTime": {
                    "epochMillis": 1446768040477,
                    "friendly": "06/Nov/15 11:00 AM",
                    "iso8601": "2015-11-06T11:00:40+1100"
                },
                "withinCalendarHours": true
            }
        },
        "customfield_11044": {
            "errorMessage": "metric not found"
        },
        "customfield_11045": {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/43"
            },
            "completedCycles": [
                {
                    "breached": false,
                    "elapsedTime": {
                        "friendly": "0m",
                        "millis": 0
                    },
                    "goalDuration": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "remainingTime": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "startTime": {
                        "epochMillis": 1446768040477,
                        "friendly": "06/Nov/15 11:00 AM",
                        "iso8601": "2015-11-06T11:00:40+1100"
                    },
                    "stopTime": {
                        "epochMillis": 1446768124651,
                        "friendly": "06/Nov/15 11:02 AM",
                        "iso8601": "2015-11-06T11:02:04+1100"
                    }
                },
                {
                    "breached": false,
                    "elapsedTime": {
                        "friendly": "0m",
                        "millis": 0
                    },
                    "goalDuration": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "remainingTime": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "startTime": {
                        "epochMillis": 1446768162075,
                        "friendly": "06/Nov/15 11:02 AM",
                        "iso8601": "2015-11-06T11:02:42+1100"
                    },
                    "stopTime": {
                        "epochMillis": 1446768574276,
                        "friendly": "06/Nov/15 11:09 AM",
                        "iso8601": "2015-11-06T11:09:34+1100"
                    }
                }
            ],
            "id": 43,
            "name": "Time waiting for support",
            "ongoingCycle": {
                "breachTime": {
                    "epochMillis": 1446776100000,
                    "friendly": "06/Nov/15 1:15 PM",
                    "iso8601": "2015-11-06T13:15:00+1100"
                },
                "breached": true,
                "elapsedTime": {
                    "friendly": "136h 18m",
                    "millis": 490681521
                },
                "goalDuration": {
                    "friendly": "15m",
                    "millis": 900000
                },
                "paused": false,
                "remainingTime": {
                    "friendly": "-136h 3m",
                    "millis": -489781521
                },
                "startTime": {
                    "epochMillis": 1446768682500,
                    "friendly": "06/Nov/15 11:11 AM",
                    "iso8601": "2015-11-06T11:11:22+1100"
                },
                "withinCalendarHours": true
            }
        },
        "customfield_11046": null,
        "customfield_11047": null,
        "customfield_11140": [],
        "customfield_11340": null,
        "customfield_11341": null,
        "customfield_11440": null,
        "description": "Its not printing at all.  I have changed the cartridge and pressed reset but not deal.\n\nI am at a loss as to what to do next",
        "duedate": null,
        "environment": null,
        "fixVersions": [],
        "issuelinks": [],
        "issuetype": {
            "description": "For general IT problems and questions. Created by JIRA Service Desk.",
            "iconUrl": "http://localhost:2990/jira/servicedesk/issue-type-icons?icon=it-help",
            "id": "13",
            "name": "IT Help",
            "self": "http://localhost:2990/jira/rest/api/2/issuetype/13",
            "subtask": false
        },
        "labels": [],
        "lastViewed": "2015-11-06T13:24:34.643+1100",
        "priority": {
            "iconUrl": "http://localhost:2990/jira/images/icons/priorities/major.svg",
            "id": "3",
            "name": "Major",
            "self": "http://localhost:2990/jira/rest/api/2/priority/3"
        },
        "progress": {
            "progress": 0,
            "total": 0
        },
        "project": {
            "avatarUrls": {
                "16x16": "http://localhost:2990/jira/secure/projectavatar?size=xsmall&avatarId=10873",
                "24x24": "http://localhost:2990/jira/secure/projectavatar?size=small&avatarId=10873",
                "32x32": "http://localhost:2990/jira/secure/projectavatar?size=medium&avatarId=10873",
                "48x48": "http://localhost:2990/jira/secure/projectavatar?avatarId=10873"
            },
            "id": "11041",
            "key": "SD",
            "name": "All Teams Service Desk",
            "self": "http://localhost:2990/jira/rest/api/2/project/11041"
        },
        "reporter": {
            "active": true,
            "avatarUrls": {
                "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
            },
            "displayName": "Ronald The Customer",
            "emailAddress": "customer0@atlassian.com",
            "key": "ronald",
            "name": "customer",
            "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
            "timeZone": "Australia/Sydney"
        },
        "resolution": null,
        "resolutiondate": null,
        "status": {
            "description": "This was auto-generated by JIRA Service Desk during workflow import",
            "iconUrl": "http://localhost:2990/jira/images/icons/statuses/generic.png",
            "id": "10021",
            "name": "Waiting for Support",
            "self": "http://localhost:2990/jira/rest/api/2/status/10021",
            "statusCategory": {
                "colorName": "yellow",
                "id": 4,
                "key": "indeterminate",
                "name": "In Progress",
                "self": "http://localhost:2990/jira/rest/api/2/statuscategory/4"
            }
        },
        "subtasks": [],
        "summary": "I need help with the L8 printer",
        "timeestimate": null,
        "timeoriginalestimate": null,
        "timespent": null,
        "timetracking": {},
        "updated": "2015-11-06T11:13:22.477+1100",
        "versions": [],
        "votes": {
            "hasVoted": false,
            "self": "http://localhost:2990/jira/rest/api/2/issue/SD-10/votes",
            "votes": 0
        },
        "watches": {
            "isWatching": true,
            "self": "http://localhost:2990/jira/rest/api/2/issue/SD-10/watchers",
            "watchCount": 1
        },
        "worklog": {
            "maxResults": 20,
            "startAt": 0,
            "total": 0,
            "worklogs": []
        },
        "workratio": -1
    },
    "id": "17227",
    "key": "SD-10",
    "self": "http://localhost:2990/jira/rest/api/2/issue/17227"
}
```

Looking at this data, you can see the fields recorded on this issue. Contrast how the JIRA Service Desk field values differ in representation to their JIRA platform peers, as shown below:

JIRA Service Desk:

``` javascript
"requestFieldValues": [
    {
      "fieldId": "summary",
      "label": "What do you need?",
      "value": "I need help with the L8 printer"
    },
    {
      "fieldId": "description",
      "label": "Why do you need this?",
      "value": "Its not printing at all. I have 
                changed the cartridge and pressed 
                reset.\r\n\r\nI am at a loss as 
                to what to do next"
    }
```

JIRA platform:

``` javascript
"description": "Its not printing at all. I 
    have changed the cartridge and pressed 
    reset.\r\n\r\nI am at a loss as to what 
    to do next",
"summary": "I need help with the L8 printer",
```

You will notice that we accessed the request as the user "customer" using the JIRA Service Desk REST API, but we switched to the user "agent" when using the JIRA platform REST API. This is because of the way that the JIRA Service Desk licensing works: Customers are allowed to use the JIRA Service Desk REST API to access their requests for free, just like they are allowed to use the JIRA Service Desk Portal for free. However, in exchange for that zero dollar cost, they are not allowed to use the JIRA REST API (nor the JIRA web interface).

However, licensed JIRA Service Desk agents are allowed to use both REST APIs, which is why we switched to "agent" to get a JIRA platform view the of the Issue.

## Request Types and Request Type Fields

A Request Type defines what information you want to capture from your customers when they ask for help.  The Request Type Fields are the fields you want to capture and are specified in the language that your customers better understand.

If you want to create a Request, then enumerating the Request Types and their Request Type Fields will allow you to do so in a more generic fashion.

###### Example E Request

``` bash
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype"
    },
    "isLastPage": true,
    "limit": 50,
    "size": 2,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62"
            },
            "description": "Get assistance for general IT problems and questions [example]",
            "id": 62,
            "name": "Get IT help",
            "serviceDeskId": 15
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/63"
            },
            "description": "Request a new account for an internal system [example]",
            "id": 63,
            "name": "Request a new account",
            "serviceDeskId": 15
        }
    ]
}
```

This shows that there are two Request Types defined for this Service Desk.  Lets examine the required fields for the first one called "Get IT help"

###### Example F Request

``` bash
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62/field" | python -mjson.tool
```

###### Response

``` javascript
{
    "canAddRequestParticipants": true,
    "canRaiseOnBehalfOf": false,
    "requestTypeFields": [
        {
            "description": "e.g. 'new mailing list'",
            "fieldId": "summary",
            "jiraSchema": {
                "system": "summary",
                "type": "string"
            },
            "name": "What do you need?",
            "required": true,
            "validValues": []
        },
        {
            "description": "",
            "fieldId": "description",
            "jiraSchema": {
                "system": "description",
                "type": "string"
            },
            "name": "Why do you need this?",
            "required": true,
            "validValues": []
        },
        {
            "description": "The best place to find you if we need to chat in person",
            "fieldId": "customfield_11440",
            "jiraSchema": {
                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:select",
                "customId": 11440,
                "type": "option"
            },
            "name": "Your Office?",
            "required": false,
            "validValues": [
                {
                    "children": [],
                    "label": "Bradfield",
                    "value": "10320"
                },
                {
                    "children": [],
                    "label": "North Hampton",
                    "value": "10321"
                },
                {
                    "children": [],
                    "label": "City",
                    "value": "10322"
                }
            ]
        }
    ]
}
```

We have several request type fields here.  One of the more interesting ones here is "customfield\_11440". Here's some things to note regarding that field:

-   The `jiraSchema` entries tell you about the field and how it operates. In this case, we can see that this is a "select" custom field containing a fixed list of office locations.
-   The `validValues` entries tell you what is considered a valid value for this field and the name and descriptions of that field. The field itself is not a required field, which means that if you created a Request using this Request Type then you wouldn't need to provide a value. However, if you did need to, then it would have to be one of the valid values.

## Comments

You can list the comments that have been made on a request.  Comments come in two flavours: public comments and internal comments.  If a customer (free) is using the REST API, then they will only see public comments. However, if a licensed JIRA Service Desk agent is using the REST API, then they will be able to see all comments, both public and internal. Compare the following two REST API calls, which retrieve the comments for a request: 

A customer retrieves all comments for a request:

###### Example G Request

``` bash
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment"
    },
    "isLastPage": true,
    "limit": 50,
    "size": 4,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37860"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122",
            "created": {
                "epochMillis": 1446768124601,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:04+1100"
            },
            "id": 37860,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37861"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "body": "Ahh good point.  I too will be there so I will maybe see you there.",
            "created": {
                "epochMillis": 1446768161960,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:41+1100"
            },
            "id": 37861,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37862"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122",
            "created": {
                "epochMillis": 1446768574272,
                "friendly": "06/Nov/15 11:09 AM",
                "iso8601": "2015-11-06T11:09:34+1100"
            },
            "id": 37862,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37863"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "body": "I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.",
            "created": {
                "epochMillis": 1446768682404,
                "friendly": "06/Nov/15 11:11 AM",
                "iso8601": "2015-11-06T11:11:22+1100"
            },
            "id": 37863,
            "public": true
        }
    ]
}
```

An agent retrieves all comments for a request:

###### Example H Request

``` bash
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment"
    },
    "isLastPage": true,
    "limit": 50,
    "size": 6,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37860"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122",
            "created": {
                "epochMillis": 1446768124601,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:04+1100"
            },
            "id": 37860,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37861"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "body": "Ahh good point.  I too will be there so I will maybe see you there.",
            "created": {
                "epochMillis": 1446768161960,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:41+1100"
            },
            "id": 37861,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37862"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122",
            "created": {
                "epochMillis": 1446768574272,
                "friendly": "06/Nov/15 11:09 AM",
                "iso8601": "2015-11-06T11:09:34+1100"
            },
            "id": 37862,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37863"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10092",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10092",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10092",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10092"
                },
                "displayName": "Ronald The Customer",
                "emailAddress": "customer0@atlassian.com",
                "key": "ronald",
                "name": "customer",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=customer",
                "timeZone": "Australia/Sydney"
            },
            "body": "I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.",
            "created": {
                "epochMillis": 1446768682404,
                "friendly": "06/Nov/15 11:11 AM",
                "iso8601": "2015-11-06T11:11:22+1100"
            },
            "id": 37863,
            "public": true
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37864"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "\nNote to team - we need to get the directory service back up and running.  We cant find out what floor any one is on without.\n",
            "created": {
                "epochMillis": 1446768747488,
                "friendly": "06/Nov/15 11:12 AM",
                "iso8601": "2015-11-06T11:12:27+1100"
            },
            "id": 37864,
            "public": false
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37865"
            },
            "author": {
                "active": true,
                "avatarUrls": {
                    "16x16": "http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10082",
                    "24x24": "http://localhost:2990/jira/secure/useravatar?size=small&avatarId=10082",
                    "32x32": "http://localhost:2990/jira/secure/useravatar?size=medium&avatarId=10082",
                    "48x48": "http://localhost:2990/jira/secure/useravatar?avatarId=10082"
                },
                "displayName": "\u00aagent\u2122",
                "emailAddress": "agent@example.com",
                "key": "waldo",
                "name": "agent",
                "self": "http://localhost:2990/jira/rest/api/2/user?username=agent",
                "timeZone": "Australia/Sydney"
            },
            "body": "Also whats up with all the dust coming down the stair well with the new renovations.  If it gets into the server room there will be hell to pay.",
            "created": {
                "epochMillis": 1446768802477,
                "friendly": "06/Nov/15 11:13 AM",
                "iso8601": "2015-11-06T11:13:22+1100"
            },
            "id": 37865,
            "public": false
        }
    ]
}
```

## Request Statuses

A request can change status several times while you are trying to help the customer. Each time it does, the customer will be able to see what status the request is in.  Here's an example of an request that was re-opened after it was thought to have been handled:

###### Example I Request

``` bash
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/status" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/status"
    },
    "isLastPage": true,
    "limit": 50,
    "size": 4,
    "start": 0,
    "values": [
        {
            "status": "Waiting for Support",
            "statusDate": {
                "epochMillis": 1446768682499,
                "friendly": "06/Nov/15 11:11 AM",
                "iso8601": "2015-11-06T11:11:22+1100"
            }
        },
        {
            "status": "Waiting for Customer",
            "statusDate": {
                "epochMillis": 1446768574275,
                "friendly": "06/Nov/15 11:09 AM",
                "iso8601": "2015-11-06T11:09:34+1100"
            }
        },
        {
            "status": "Waiting for Support",
            "statusDate": {
                "epochMillis": 1446768162074,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:42+1100"
            }
        },
        {
            "status": "Waiting for Customer",
            "statusDate": {
                "epochMillis": 1446768124625,
                "friendly": "06/Nov/15 11:02 AM",
                "iso8601": "2015-11-06T11:02:04+1100"
            }
        }
    ]
}
```

Note that these are the public statuses that customer can see. It's possible to have internal statuses that are there to help the service team manage their work (for example "Under Review") but don't need to be shared with the customers.

To see the state transitions that are possible for this issue you can use the JIRA platform REST API, as shown below:

###### Example J Request

``` bash
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/issue/17227/transitions" | python -mjson.tool
```

###### Response

``` javascript
{
    "expand": "transitions",
    "transitions": [
        {
            "id": "801",
            "name": "Resolve this issue",
            "to": {
                "description": "A resolution has been taken, and it is awaiting verification by reporter. From here issues are either reopened, or are closed.",
                "iconUrl": "http://localhost:2990/jira/images/icons/statuses/resolved.png",
                "id": "5",
                "name": "Resolved",
                "self": "http://localhost:2990/jira/rest/api/2/status/5",
                "statusCategory": {
                    "colorName": "green",
                    "id": 3,
                    "key": "done",
                    "name": "Done",
                    "self": "http://localhost:2990/jira/rest/api/2/statuscategory/3"
                }
            }
        },
        {
            "id": "851",
            "name": "Respond to customer",
            "to": {
                "description": "This was auto-generated by JIRA Service Desk during workflow import",
                "iconUrl": "http://localhost:2990/jira/images/icons/statuses/generic.png",
                "id": "10022",
                "name": "Waiting for Customer",
                "self": "http://localhost:2990/jira/rest/api/2/status/10022",
                "statusCategory": {
                    "colorName": "yellow",
                    "id": 4,
                    "key": "indeterminate",
                    "name": "In Progress",
                    "self": "http://localhost:2990/jira/rest/api/2/statuscategory/4"
                }
            }
        }
    ]
}
```

## Request Service Level Agreements

Request Service Level Agreements (SLAs) are used to track how much time is spent servicing customer requests. Timers are set when Requests enter certain conditions. Goal times can be defined as an ideal time to do work before it is considered a breach of service. For example, you may aim to get back to a customer within four hours, and hence may have a "Time to first response" SLA defined.

SLA information is not visible to customers. It is used to help the service team know what is the most important task they should look at next.

SLA metrics can start and stop during the life time of an request.  For example, you may have a "Time waiting for support" SLA defined, where the timers are only running while it is in the service desk teams hands. So, an SLA can have many repeated "cycles" and a possible "ongoing cycle". Each cycle may have been completed within target time or it may have "breached".

Enough theory, let's see what it looks like! The following REST API call returns the SLA information for a particular Request:

###### Example K Request

``` bash
url -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla" | python -mjson.tool
```

###### Response

``` javascript
{
    "_links": {
        "base": "http://localhost:2990/jira",
        "context": "/jira",
        "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla"
    },
    "isLastPage": true,
    "limit": 50,
    "size": 2,
    "start": 0,
    "values": [
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/30"
            },
            "completedCycles": [],
            "id": 30,
            "name": "Time to resolution",
            "ongoingCycle": {
                "breachTime": {
                    "epochMillis": 1446789600000,
                    "friendly": "06/Nov/15 5:00 PM",
                    "iso8601": "2015-11-06T17:00:00+1100"
                },
                "breached": true,
                "elapsedTime": {
                    "friendly": "136h 18m",
                    "millis": 490682163
                },
                "goalDuration": {
                    "friendly": "4h",
                    "millis": 14400000
                },
                "paused": false,
                "remainingTime": {
                    "friendly": "-132h 18m",
                    "millis": -476282163
                },
                "startTime": {
                    "epochMillis": 1446768040477,
                    "friendly": "06/Nov/15 11:00 AM",
                    "iso8601": "2015-11-06T11:00:40+1100"
                },
                "withinCalendarHours": true
            }
        },
        {
            "_links": {
                "self": "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/43"
            },
            "completedCycles": [
                {
                    "breached": false,
                    "elapsedTime": {
                        "friendly": "0m",
                        "millis": 0
                    },
                    "goalDuration": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "remainingTime": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "startTime": {
                        "epochMillis": 1446768040477,
                        "friendly": "06/Nov/15 11:00 AM",
                        "iso8601": "2015-11-06T11:00:40+1100"
                    },
                    "stopTime": {
                        "epochMillis": 1446768124651,
                        "friendly": "06/Nov/15 11:02 AM",
                        "iso8601": "2015-11-06T11:02:04+1100"
                    }
                },
                {
                    "breached": false,
                    "elapsedTime": {
                        "friendly": "0m",
                        "millis": 0
                    },
                    "goalDuration": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "remainingTime": {
                        "friendly": "15m",
                        "millis": 900000
                    },
                    "startTime": {
                        "epochMillis": 1446768162075,
                        "friendly": "06/Nov/15 11:02 AM",
                        "iso8601": "2015-11-06T11:02:42+1100"
                    },
                    "stopTime": {
                        "epochMillis": 1446768574276,
                        "friendly": "06/Nov/15 11:09 AM",
                        "iso8601": "2015-11-06T11:09:34+1100"
                    }
                }
            ],
            "id": 43,
            "name": "Time waiting for support",
            "ongoingCycle": {
                "breachTime": {
                    "epochMillis": 1446776100000,
                    "friendly": "06/Nov/15 1:15 PM",
                    "iso8601": "2015-11-06T13:15:00+1100"
                },
                "breached": true,
                "elapsedTime": {
                    "friendly": "136h 18m",
                    "millis": 490682163
                },
                "goalDuration": {
                    "friendly": "15m",
                    "millis": 900000
                },
                "paused": false,
                "remainingTime": {
                    "friendly": "-136h 3m",
                    "millis": -489782163
                },
                "startTime": {
                    "epochMillis": 1446768682500,
                    "friendly": "06/Nov/15 11:11 AM",
                    "iso8601": "2015-11-06T11:11:22+1100"
                },
                "withinCalendarHours": true
            }
        }
    ]
}
```

**Congratulations!** You've completed this overview of the JIRA Service Desk domain model. Have a chocolate! You should now have a good grasp of how the objects work. Next, check out the JIRA Service Desk REST API documentation and try using it yourself.
